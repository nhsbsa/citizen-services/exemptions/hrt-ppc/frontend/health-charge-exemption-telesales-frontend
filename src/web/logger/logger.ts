import { winstonLogger } from "./winston-logger";
import { REQUEST_ID_HEADER } from "../server/headers";
import { config } from "../../config";
import { LeveledLogMethod } from "winston";

const additionalProperties = { version: config.environment.APP_VERSION };

const logger = {
  info: function (message: any, req?: any, meta?: any) {
    logWithMessageAndRequestDetails(winstonLogger.info, message, req, meta);
  },
  error: function (message: string, req?: any) {
    logWithMessageAndRequestDetails(winstonLogger.error, message, req);
  },
  warn: function (message: string, req?: any) {
    logWithMessageAndRequestDetails(winstonLogger.warn, message, req);
  },
  debug: function (message: string, req?: any) {
    logWithMessageAndRequestDetails(winstonLogger.debug, message, req);
  },
};

function logWithMessageAndRequestDetails(
  level: LeveledLogMethod,
  message: any,
  req?: any,
  meta?: any,
) {
  const requestId =
    req && req.headers ? req.headers[REQUEST_ID_HEADER] : undefined;
  const locatorId = req && req.session ? req.session.locator : undefined;
  let requestMeta;

  if (requestId) {
    requestMeta = {
      headers: { [REQUEST_ID_HEADER]: requestId },
      session: { locator: locatorId },
    };
  }
  if (req && req.session && req.session.issuedCertificateDetails) {
    meta = {
      ...meta,
      certificate: req.session.issuedCertificateDetails,
    };
  }

  level(message, {
    ...additionalProperties,
    ...meta,
    req: requestMeta,
  });
}

export { logger, logWithMessageAndRequestDetails };
