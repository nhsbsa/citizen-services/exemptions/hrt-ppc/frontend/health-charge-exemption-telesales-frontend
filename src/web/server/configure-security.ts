import helmet from "helmet";
import { v4 } from "uuid";

const configureSecurity = (app) => {
  app.use(helmet());
  app.use((req, res, next) => {
    // nonce should be base64 encoded
    res.locals.scriptNonce = Buffer.from(v4()).toString("base64");
    next();
  });

  /**
   * browsers supporting csp 3 will use nonce
   * browser not supporting nonce will fallback to unsafe-inline
   * 'unsafe-inline' is listed in scriptSrc for two use cases:
   *
   * 1. To allow Google Tag Manager to function correctly for older browsers (CSP 1)
   * 2. To initialise GOV.UK front end javascript - this could be addressed by initialising
   *    inside a javascript file, see:
   *    https://github.com/alphagov/govuk-frontend/blob/master/docs/installation/installing-with-npm.md
   *
   */
  app.use((req, res, next) => {
    helmet.contentSecurityPolicy({
      directives: {
        baseUri: ["'self'"],
        objectSrc: ["'none'"],
        defaultSrc: ["'self'"],
        styleSrc: [
          "'self'",
          "'unsafe-inline'",
          () => `'nonce-${res.locals.scriptNonce}'`,
        ],
        scriptSrc: [
          "'self'",
          "'strict-dynamic'",
          "'unsafe-inline'",
          "https://www.googletagmanager.com",
          "https://www.google-analytics.com",
          () => `'nonce-${res.locals.scriptNonce}'`,
        ],
        imgSrc: [
          "'self'",
          "https://www.googletagmanager.com",
          "https://www.google-analytics.com",
          "https: data:",
          () => `'nonce-${res.locals.scriptNonce}'`,
        ],
        fontSrc: ["'self'", "https://assets.nhs.uk"],
        connectSrc: [
          "'self'",
          "https://www.googletagmanager.com",
          "https://*.google-analytics.com",
          "https://www.google-analytics.com",
          () => `'nonce-${res.locals.scriptNonce}'`,
        ],
        formAction: ["'self'", "https://www.payments.service.gov.uk"],
      },
    })(req, res, next);
  });
};

export { configureSecurity };
