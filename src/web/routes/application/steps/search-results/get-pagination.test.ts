import expect from "expect";
import { buildPageSection, getPagination } from "./get-pagination";

const localisation = {
  paginationPrevious: "previous",
  paginationNext: "next",
  paginationResults: "results",
};

describe("getPagination", () => {
  test("should build a page number section which is not selected", () => {
    const result = buildPageSection(1, 2);

    expect(result).toEqual({
      text: `2`,
      href: `/test-context/search-results?page=2`,
      selected: false,
    });
  });

  test("should build a page number section which is selected", () => {
    const result = buildPageSection(1, 1);

    expect(result).toEqual({
      text: `1`,
      href: `/test-context/search-results?page=1`,
      selected: true,
    });
  });
});

describe("getPagination", () => {
  test("should return pagination for page 1 when total records is 1", () => {
    const result = getPagination(1, 1, 1, localisation);

    expect(result).toEqual({
      items: [
        {
          text: `1`,
          href: `/test-context/search-results?page=1`,
          selected: true,
        },
      ],
      results: {
        count: 1,
        from: 1,
        to: 1,
        text: localisation.paginationResults,
      },
      previous: undefined,
      next: undefined,
    });
  });

  test("should return pagination for page 1 when total records is 3", () => {
    const result = getPagination(1, 3, 2, localisation);

    expect(result).toEqual({
      items: [
        {
          text: `1`,
          href: `/test-context/search-results?page=1`,
          selected: true,
        },
        {
          text: `2`,
          href: `/test-context/search-results?page=2`,
          selected: false,
        },
      ],
      results: {
        count: 3,
        from: 1,
        to: 2,
        text: localisation.paginationResults,
      },
      previous: undefined,
      next: {
        text: localisation.paginationNext,
        href: `/test-context/search-results?page=2`,
      },
    });
  });

  test("should return pagination for page 2 when total records is 3", () => {
    const result = getPagination(2, 3, 2, localisation);

    expect(result).toEqual({
      items: [
        {
          text: `1`,
          href: `/test-context/search-results?page=1`,
          selected: false,
        },
        {
          text: `2`,
          href: `/test-context/search-results?page=2`,
          selected: true,
        },
      ],
      results: {
        count: 3,
        from: 3,
        to: 3,
        text: localisation.paginationResults,
      },
      previous: {
        text: localisation.paginationPrevious,
        href: `/test-context/search-results?page=1`,
      },
      next: undefined,
    });
  });

  test("should return pagination for page 2 when total records is 5", () => {
    const result = getPagination(2, 5, 3, localisation);

    expect(result).toEqual({
      items: [
        {
          text: `1`,
          href: `/test-context/search-results?page=1`,
          selected: false,
        },
        {
          text: `2`,
          href: `/test-context/search-results?page=2`,
          selected: true,
        },
        {
          text: `3`,
          href: `/test-context/search-results?page=3`,
          selected: false,
        },
      ],
      results: {
        count: 5,
        from: 3,
        to: 4,
        text: localisation.paginationResults,
      },
      previous: {
        text: localisation.paginationPrevious,
        href: `/test-context/search-results?page=1`,
      },
      next: {
        text: localisation.paginationNext,
        href: `/test-context/search-results?page=3`,
      },
    });
  });
});
