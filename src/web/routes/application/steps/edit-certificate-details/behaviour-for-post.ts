import { certificateClient } from "../../../../client/certificate-client";
import { preferenceClient } from "../../../../client/preference-client";
import { wrapError } from "../../errors";
import { StatusCodes } from "http-status-codes";
import { validationResult } from "express-validator";
import { contextPath } from "../../paths/context-path";
import { EXEMPTION_VIEW_URL } from "../../paths/paths";
import { NextFunction, Request, Response } from "express";
import { productClient } from "../../../../client/product-client";
import { copyCertificateSessionDataToLocals } from "./edit-certificate-details";
import { Event } from "@nhsbsa/health-charge-exemption-common-frontend";
import { isEligibleForEditing } from "../common/certificate-utils";

export const behaviourForPost =
  (config) => async (req: Request, res: Response, next: NextFunction) => {
    if (!validationResult(req).isEmpty()) {
      copyCertificateSessionDataToLocals(req, res);
      res.locals.certificateDetailsData = {
        ...res.locals.certificateDetailsData,
        ...req.body,
      };
      return next();
    }

    try {
      const preferencePromise = new preferenceClient(config).makeRequest(
        {
          method: "PATCH",
          url: `/v1/user-preference/${req.session.certificate["preference"].id}`,
          data: {
            event: Event.ANY,
            preference: req.body.certificateFulfillment,
          },
          responseType: "json",
        },
        req.session,
      );

      const promises = [preferencePromise];

      if (isEligibleForEditing(req.session.certificate)) {
        const endDateResponse = await new productClient(config).makeRequest({
          method: "GET",
          url: `v1/products/HRT_12m/end-date?startDate=${req.body.certificateStartDate}`,
        });
        const { endDate } = endDateResponse.data;

        const certificatePromise = new certificateClient(config).makeRequest(
          {
            method: "PATCH",
            url: `/v1/certificates/${req.session.certificate.id}`,
            data: {
              startDate: req.body.certificateStartDate,
              endDate: endDate,
            },
            responseType: "json",
          },
          req.session,
        );

        promises.push(certificatePromise);
      }
      await Promise.all(promises);

      res.redirect(contextPath(`${EXEMPTION_VIEW_URL}/${req.params.id}`));
    } catch (error) {
      next(
        wrapError({
          cause: error,
          message: `Error posting ${req.path}`,
          statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        }),
      );
    }
  };
