FROM node:18-alpine

# variables
ARG app_version
ARG port
ARG config_file
ARG app_env
ARG app_service

WORKDIR /app
COPY . /app
COPY $config_file /app/.env

# Set environment variables

ENV APP_VERSION=$app_version
ENV DD_PROFILING_ENABLED=true
ENV DD_ENV=$app_env
ENV DD_SERVICE=$app_service
ENV DD_VERSION=$app_version

RUN sed -i 's/USE_LOCALSTACK=true/USE_LOCALSTACK=false/' /app/.env
##RUN npm rebuild node-sass --force

RUN npm uninstall node-sass
RUN npm i node-sass
RUN npm rebuild node-sass
RUN npm install && npm run build

EXPOSE $port

ENTRYPOINT ["npm", "start"]


