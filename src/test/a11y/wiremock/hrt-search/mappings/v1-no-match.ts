export default {
  status: 200,
  jsonBody: {
    exemptions: {
      "hrt-ppcs": [],
      others: [],
    },
  },
  headers: {
    "Content-Type": "application/json",
  },
};
