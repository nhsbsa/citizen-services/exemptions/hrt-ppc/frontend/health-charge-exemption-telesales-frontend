import expect from "expect";
const mockSanitizeStartDate = jest.fn();
jest.mock(
  "@nhsbsa/health-charge-exemption-common-frontend/dist/src/certificate-start-date/sanitize",
  () => ({
    sanitizeStartDate: mockSanitizeStartDate,
  }),
);
import { Request, Response } from "express";
import { sanitize } from "./sanitize";

const next = jest.fn();

const request = ({
  year,
  month,
  day,
}: {
  year?: string;
  month?: string;
  day?: string;
} = {}) =>
  ({
    body: {
      "certificateStartDate-day": day || "20",
      "certificateStartDate-month": month || "11",
      "certificateStartDate-year": year || "2023",
    },
  }) as Request;
describe("sanitize()", () => {
  test("calls sanitizeStartDate and next", () => {
    const req = request();

    sanitize()(req, {} as Response, next);

    expect(mockSanitizeStartDate).toHaveBeenCalledTimes(1);
    expect(mockSanitizeStartDate).toHaveBeenCalledWith(req);
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
  });
});
