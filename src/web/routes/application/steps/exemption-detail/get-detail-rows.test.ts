import expect from "expect";
import moment from "moment";
import {
  getRowsForCertificateDetailsTable,
  getRowsForPersonalDetailsTable,
  getRowsForNotesTable,
  getNotesHeadings,
  getRowsForAuditHistoryTable,
  getAuditHistoryHeadings,
  constructAuditData,
} from "./get-detail-rows";
import {
  Event,
  Preference,
  CertificateStatus,
  AddressType,
  CertificateType,
  Channel,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/enums";
import * as auditSingleRowResponse from "../../../../../__mocks__/api-responses/composition-api/get-audit-single-row.json";
import * as auditMultipleRowResponse from "../../../../../__mocks__/api-responses/composition-api/get-audit-multiple-rows.json";
import {
  AuditResponseEntity,
  ChangesEntity,
} from "../../../../client/composition-client";
import {
  AUDIT_CITIZEN_ENTITY,
  AUDIT_INITITAL_VALUE_CHANGE_TYPE,
} from "../common/constants";

jest.mock("../common/preference-fulfilment-utils", () => ({
  translateFulfilmentToText: jest.fn().mockReturnValue("Post"),
}));

const translate = {
  tables: {
    certificateDetails: {
      title: "Certificate details",
      rows: {
        reference: "Certificate reference",
        type: "Certificate type",
        status: "Certificate status",
        channel: "Certificate channel",
        fulfillment: "Certificate fulfillment",
        startDate: "Certificate start",
        endDate: "Certificate expiry",
        applicationDate: "Application date",
      },
    },
    personalDetails: {
      title: "Personal details",
      link: "Edit (E)",
      rows: {
        name: "Name",
        address: "Address",
        dateOfBirth: "Date of birth",
        email: "Email address",
        nhsNumber: "NHS number",
      },
    },
    notes: {
      title: "Notes",
      link: "Add note (N)",
      rows: {
        date: "Date",
        time: "Time",
        agent: "Agent",
        description: "Description",
      },
    },
    auditHistory: {
      title: "Activity log",
      rows: {
        date: "Date",
        type: "Type",
        from: "From",
        to: "To",
        description: "Description",
      },
      type: {
        citizen: "Personal details",
        certificate: "Certificate details",
        payment: "Card payment",
      },
      description: {
        citizen: {
          firstName: "first name",
          lastName: "last name",
          addressLine1: "address line 1",
          addressLine2: "address line 2",
          townOrCity: "town",
          postcode: "postcode",
          dateOfBirth: "date of birth",
          emailAddress: "email address",
          nhsNumber: "NHS number",
        },
        certificate: {
          status: "certificate status",
          startDate: "certificate start date",
        },
        payment: {
          status: "payment status",
        },
        updated: "Updated",
        removed: "Removed",
      },
    },
  },
};

describe("getRowsForCertificateDetailsTable()", () => {
  test("should return certificate details table rows with correct data and content", () => {
    const date = new Date("2022-12-12");
    const certificateData = {
      id: "HRT",
      citizenId: "CITIZEN_ID",
      reference: "HRT",
      type: CertificateType.HRT_PPC,
      applicationDate: moment(date).subtract(5, "d").toDate(),
      duration: "P12M",
      startDate: moment(date).subtract(5, "d").toDate(),
      endDate: moment(date).add(1, "d").toDate(),
      cost: 1870,
      status: CertificateStatus.PENDING,
      pharmacyId: "ABCD",
      _meta: {
        channel: Channel.ONLINE,
        createdTimestamp: "2023-03-14T09:26:36.513075",
        createdBy: "POSTMAN",
        updatedTimestamp: "2023-03-14T09:26:36.513075",
        updatedB: "POSTMAN",
      },
      preference: {
        event: Event.ANY,
        preference: Preference.POSTAL,
      },
    };

    const result = getRowsForCertificateDetailsTable(
      certificateData,
      translate,
    );

    expect(result).toEqual([
      {
        key: { text: "Certificate reference" },
        value: { text: "HRT" },
      },
      {
        key: { text: "Certificate type" },
        value: {
          html: '<strong class="nhsuk-tag nhsuk-tag--green">HRT PPC</strong>',
        },
      },
      {
        key: { text: "Certificate status" },
        value: { text: CertificateStatus.PENDING },
      },
      {
        key: { text: "Certificate channel" },
        value: { text: "ONLINE" },
      },
      {
        key: { text: "Certificate fulfillment" },
        value: { text: "Post" },
      },
      {
        key: { text: "Certificate start" },
        value: {
          text: "07/12/2022",
        },
      },
      {
        key: { text: "Certificate expiry" },
        value: {
          text: "13/12/2022",
        },
      },
      {
        key: { text: "Application date" },
        value: {
          text: "07/12/2022",
        },
      },
    ]);
  });
});

describe("getRowsForPersonalDetailsTable()", () => {
  const date = new Date("2022-01-01");
  test("should return personal details rows with correct data and content", () => {
    const citizenData = {
      id: "myCitizenId",
      firstName: "myFname",
      lastName: "myLname",
      dateOfBirth: moment(date).toDate(),
      addresses: [
        {
          id: "myAddId",
          addressLine1: "add1",
          addressLine2: "add2",
          townOrCity: "addTown",
          country: "addCountry",
          postcode: "NE15 8NY",
          type: AddressType.POSTAL,
        },
      ],
      emails: [],
    };

    const result = getRowsForPersonalDetailsTable(citizenData, translate);

    expect(result).toEqual([
      {
        key: { text: "Name" },
        value: { text: "myFname myLname" },
      },
      {
        key: { text: "Address" },
        value: {
          html: `add1 <br>
              add2 <br>
              addTown <br>
              NE15 8NY`,
        },
      },
      {
        key: { text: "Date of birth" },
        value: { text: "01/01/2022" },
      },
      {
        key: { text: "Email address" },
        value: { text: "" },
      },
      {
        key: { text: "NHS number" },
        value: { text: "" },
      },
    ]);
  });

  test("should return personal details rows with address line 1 missing", () => {
    const citizenData = {
      id: "myCitizenId",
      firstName: "myFname",
      lastName: "myLname",
      dateOfBirth: moment(date).toDate(),
      addresses: [
        {
          id: "myAddId",
          addressLine2: "add2",
          townOrCity: "addTown",
          country: "addCountry",
          postcode: "NE15 8NY",
          type: AddressType.POSTAL,
        },
      ],
    };

    const result = getRowsForPersonalDetailsTable(citizenData, translate);

    expect(result).toEqual([
      {
        key: { text: "Name" },
        value: { text: "myFname myLname" },
      },
      {
        key: { text: "Address" },
        value: {
          html: ` <br>
              add2 <br>
              addTown <br>
              NE15 8NY`,
        },
      },
      {
        key: { text: "Date of birth" },
        value: { text: "01/01/2022" },
      },
      {
        key: { text: "Email address" },
        value: { text: "" },
      },
      {
        key: { text: "NHS number" },
        value: { text: "" },
      },
    ]);
  });

  test("should return personal details rows with address line 2 missing", () => {
    const citizenData = {
      id: "myCitizenId",
      firstName: "myFname",
      lastName: "myLname",
      dateOfBirth: moment(date).toDate(),
      addresses: [
        {
          id: "myAddId",
          addressLine1: "add1",
          townOrCity: "addTown",
          country: "addCountry",
          postcode: "NE15 8NY",
          type: AddressType.POSTAL,
        },
      ],
    };

    const result = getRowsForPersonalDetailsTable(citizenData, translate);

    expect(result).toEqual([
      {
        key: { text: "Name" },
        value: { text: "myFname myLname" },
      },
      {
        key: { text: "Address" },
        value: {
          html: `add1 <br>
               <br>
              addTown <br>
              NE15 8NY`,
        },
      },
      {
        key: { text: "Date of birth" },
        value: { text: "01/01/2022" },
      },
      {
        key: { text: "Email address" },
        value: { text: "" },
      },
      {
        key: { text: "NHS number" },
        value: { text: "" },
      },
    ]);
  });

  test("should return personal details rows with address townOrCity missing", () => {
    const citizenData = {
      id: "myCitizenId",
      firstName: "myFname",
      lastName: "myLname",
      dateOfBirth: moment(date).toDate(),
      addresses: [
        {
          id: "myAddId",
          addressLine1: "add1",
          addressLine2: "add2",
          country: "addCountry",
          postcode: "NE15 8NY",
          type: AddressType.POSTAL,
        },
      ],
    };

    const result = getRowsForPersonalDetailsTable(citizenData, translate);

    expect(result).toEqual([
      {
        key: { text: "Name" },
        value: { text: "myFname myLname" },
      },
      {
        key: { text: "Address" },
        value: {
          html: `add1 <br>
              add2 <br>
               <br>
              NE15 8NY`,
        },
      },
      {
        key: { text: "Date of birth" },
        value: { text: "01/01/2022" },
      },
      {
        key: { text: "Email address" },
        value: { text: "" },
      },
      {
        key: { text: "NHS number" },
        value: { text: "" },
      },
    ]);
  });

  test("should return personal details rows with address postcode missing", () => {
    const citizenData = {
      id: "myCitizenId",
      firstName: "myFname",
      lastName: "myLname",
      dateOfBirth: moment(date).toDate(),
      addresses: [
        {
          id: "myAddId",
          addressLine1: "add1",
          addressLine2: "add2",
          townOrCity: "addTown",
          country: "addCountry",
          type: AddressType.POSTAL,
        },
      ],
    };

    const result = getRowsForPersonalDetailsTable(citizenData, translate);

    expect(result).toEqual([
      {
        key: { text: "Name" },
        value: { text: "myFname myLname" },
      },
      {
        key: { text: "Address" },
        value: {
          html: `add1 <br>
              add2 <br>
              addTown <br>
              `,
        },
      },
      {
        key: { text: "Date of birth" },
        value: { text: "01/01/2022" },
      },
      {
        key: { text: "Email address" },
        value: { text: "" },
      },
      {
        key: { text: "NHS number" },
        value: { text: "" },
      },
    ]);
  });

  test("should return personal detail rows with nhsnumber and without address", () => {
    const citizenData = {
      id: "myCitizenId",
      nhsNumber: "123456789",
      firstName: "myFname",
      lastName: "myLname",
      dateOfBirth: moment(date).toDate(),
    };

    const result = getRowsForPersonalDetailsTable(citizenData, translate);

    expect(result).toEqual([
      {
        key: { text: "Name" },
        value: { text: "myFname myLname" },
      },
      {
        key: { text: "Address" },
        value: { html: "" },
      },
      {
        key: { text: "Date of birth" },
        value: { text: "01/01/2022" },
      },
      {
        key: { text: "Email address" },
        value: { text: "" },
      },
      {
        key: { text: "NHS number" },
        value: { text: "123456789" },
      },
    ]);
  });

  test("should return personal detail rows with email address", () => {
    const citizenData = {
      id: "myCitizenId",
      firstName: "myFname",
      lastName: "myLname",
      dateOfBirth: moment(date).toDate(),
      emails: [{ emailAddress: "test@test.com" }],
    };

    const result = getRowsForPersonalDetailsTable(citizenData, translate);

    expect(result).toEqual([
      {
        key: { text: "Name" },
        value: { text: "myFname myLname" },
      },
      {
        key: { text: "Address" },
        value: { html: "" },
      },
      {
        key: { text: "Date of birth" },
        value: { text: "01/01/2022" },
      },
      {
        key: { text: "Email address" },
        value: { text: "test@test.com" },
      },
      {
        key: { text: "NHS number" },
        value: { text: "" },
      },
    ]);
  });
});

describe("buildNoteTableRows()", () => {
  test("should return empty array when no note records", () => {
    const notesData = {
      _embedded: {
        notes: [],
      },
    };

    const result = getRowsForNotesTable(notesData, translate);

    expect(result).toEqual([]);
  });
  test("should return single note row and correct data and content", () => {
    const notesData = {
      _embedded: {
        notes: [
          {
            id: "HRT",
            certificateId: "CERT",
            citizenId: "CIT",
            description: "this is note 1",
            _meta: {
              channel: Channel.ONLINE,
              createdTimestamp: "2023-03-13T15:11:09.111371",
              createdBy: "HRT",
              updatedTimestamp: "2023-03-13T15:11:09.111371",
              updatedBy: "HRT",
            },
          },
        ],
      },
    };

    const result = getRowsForNotesTable(notesData, translate);

    expect(result).toEqual([
      [
        {
          header: "Date",
          text: "13/03/2023",
        },
        {
          header: "Time",
          text: "15:11",
        },
        {
          header: "Agent",
          text: "HRT",
        },
        {
          header: "Description",
          text: "this is note 1",
        },
      ],
    ]);
  });

  test("should return multiple note rows and correct data and content", () => {
    const notesData = {
      _embedded: {
        notes: [
          {
            id: "NOTE1",
            certificateId: "CERT",
            citizenId: "CIT",
            description: "this is note 1",
            _meta: {
              channel: Channel.ONLINE,
              createdTimestamp: "2023-03-13T15:11:09.111371",
              createdBy: "HRT",
              updatedTimestamp: "2023-03-13T15:11:09.111371",
              updatedBy: "HRT",
            },
          },
          {
            id: "NOTE2",
            certificateId: "CERT",
            citizenId: "CIT",
            description: "this is note 2",
            _meta: {
              channel: Channel.ONLINE,
              createdTimestamp: "2023-02-13T15:13:09.111371",
              createdBy: "HRT2",
              updatedTimestamp: "2023-03-13T15:13:09.111371",
              updatedBy: "HRT",
            },
          },
        ],
      },
    };

    const result = getRowsForNotesTable(notesData, translate);

    expect(result).toEqual([
      [
        {
          header: "Date",
          text: "13/02/2023",
        },
        {
          header: "Time",
          text: "15:13",
        },
        {
          header: "Agent",
          text: "HRT2",
        },
        {
          header: "Description",
          text: "this is note 2",
        },
      ],
      [
        {
          header: "Date",
          text: "13/03/2023",
        },
        {
          header: "Time",
          text: "15:11",
        },
        {
          header: "Agent",
          text: "HRT",
        },
        {
          header: "Description",
          text: "this is note 1",
        },
      ],
    ]);
  });
});

describe("getNotesHeadings()", () => {
  test("should return correct note table heading content", () => {
    const result = getNotesHeadings(translate);

    expect(result).toEqual([
      { text: "Date" },
      { text: "Time" },
      { text: "Agent" },
      { text: "Description" },
    ]);
  });
});

describe("getRowsForAuditHistoryTable()", () => {
  test("should return empty array when no audit history records", async () => {
    const auditData = {
      content: [],
    };

    const result = await getRowsForAuditHistoryTable(auditData, translate);

    expect(result).toEqual([]);
  });

  test("should return single audit row and correct data and content", async () => {
    const result = await getRowsForAuditHistoryTable(
      auditSingleRowResponse,
      translate,
    );
    expect(result).toEqual([
      [
        {
          header: "Date",
          html: "02/10/2023",
        },
        {
          header: "Type",
          html: "Personal details",
        },
        {
          header: "From",
          html: "John",
        },
        {
          header: "To",
          html: "New John",
        },
        {
          header: "Description",
          html: "Updated first name",
        },
      ],
    ]);
  });

  test("should return multiple audit rows and correct data and content", async () => {
    const result = await getRowsForAuditHistoryTable(
      auditMultipleRowResponse,
      translate,
    );

    expect(result).toEqual([
      [
        {
          header: "Date",
          html: "02/10/2023",
        },
        {
          header: "Type",
          html: "Personal details",
        },
        {
          header: "From",
          html: "Goldcrest Way",
        },
        {
          header: "To",
          html: "New Goldcrest Way",
        },
        {
          header: "Description",
          html: "Updated address line 1",
        },
      ],
      [
        {
          header: "Date",
          html: "02/10/2023",
        },
        {
          header: "Type",
          html: "Personal details",
        },
        {
          header: "From",
          html: "crossway",
        },
        {
          header: "To",
          html: "New crossway",
        },
        {
          header: "Description",
          html: "Updated address line 2",
        },
      ],
    ]);
  });
});

describe("getAuditHistoryHeadings()", () => {
  test("should return correct audit table heading content", () => {
    const result = getAuditHistoryHeadings(translate);

    expect(result).toEqual([
      { text: "Date" },
      { text: "Type" },
      { text: "From" },
      { text: "To" },
      { text: "Description" },
    ]);
  });
});

describe("constructAuditData()", () => {
  let firstChange;
  let secondChange;

  beforeEach(() => {
    firstChange = {
      changes: [
        {
          changeType: AUDIT_INITITAL_VALUE_CHANGE_TYPE,
          globalId: {
            entity: AUDIT_CITIZEN_ENTITY,
            cdoId: "123",
          },
          commitMetadata: {
            author: "Author 1",
            commitDate: "2023-01-01T12:00:00Z",
            commitDateInstant: "2023-01-01T12:00:00Z",
            id: 1,
          },
          property: "property1",
          propertyChangeType: "propertyChangeType1",
          left: "left1",
          right: "right1",
        },
      ],
      commitMetadata: {
        author: "Author 1",
        commitDate: "2023-01-01T12:00:00Z",
        commitDateInstant: "2023-01-01T12:00:00Z",
        id: 1,
      },
    };
    secondChange = {
      changes: [
        {
          changeType: AUDIT_INITITAL_VALUE_CHANGE_TYPE,
          globalId: {
            entity: AUDIT_CITIZEN_ENTITY,
            cdoId: "111",
          },
          commitMetadata: {
            author: "Author 1",
            commitDate: "2023-01-01T12:00:00Z",
            commitDateInstant: "2023-01-01T12:00:00Z",
            id: 1,
          },
          property: "property1",
          propertyChangeType: "propertyChangeType1",
          left: "left1",
          right: "right1",
        },
      ],
      commitMetadata: {
        author: "Author 1",
        commitDate: "2023-01-01T12:00:00Z",
        commitDateInstant: "2023-01-01T12:00:00Z",
        id: 1,
      },
    };
  });

  test("should extract out first object when there is a citizen and initial change", async () => {
    const auditData: AuditResponseEntity = {
      content: [firstChange],
    };
    const constructedAuditData: ChangesEntity[] =
      await constructAuditData(auditData);

    expect(constructedAuditData).toEqual([]);
  });

  test("should construct audit data correctly when there is a citizen and initial change", async () => {
    const auditData: AuditResponseEntity = {
      content: [secondChange, firstChange],
    };
    const constructedAuditData: ChangesEntity[] =
      await constructAuditData(auditData);

    expect(constructedAuditData).toEqual(secondChange.changes);
  });

  test("should construct audit data correctly when there is a citizen and non initial change", async () => {
    firstChange.changes[0].changeType = "other_change_type";
    const auditData: AuditResponseEntity = {
      content: [firstChange],
    };
    const constructedAuditData: ChangesEntity[] =
      await constructAuditData(auditData);

    expect(constructedAuditData).toEqual(firstChange.changes);
  });

  test("should construct audit data correctly when there is a non citizen and non initial change", async () => {
    firstChange.changes[0].changeType = "other_change_type";
    firstChange.changes[0].globalId.entity = "other_entity";
    const auditData: AuditResponseEntity = {
      content: [firstChange],
    };

    const constructedAuditData: ChangesEntity[] =
      await constructAuditData(auditData);

    expect(constructedAuditData).toEqual(firstChange.changes);
  });

  test("should construct audit data correctly when there is a non citizen and initial change", async () => {
    const auditData: AuditResponseEntity = {
      content: [
        {
          changes: [
            {
              changeType: AUDIT_INITITAL_VALUE_CHANGE_TYPE,
              globalId: {
                entity: "other_entity",
                cdoId: "456",
              },
              commitMetadata: {
                author: "Author 2",
                commitDate: "2023-01-02T12:00:00Z",
                commitDateInstant: "2023-01-02T12:00:00Z",
                id: 2,
              },
              property: "property2",
              propertyChangeType: "propertyChangeType2",
              left: "left2",
              right: "right2",
            },
            {
              changeType: "other_change_type",
              globalId: {
                entity: "other_entity",
                cdoId: "123",
              },
              commitMetadata: {
                author: "Author 2",
                commitDate: "2023-01-02T12:00:00Z",
                commitDateInstant: "2023-01-02T12:00:00Z",
                id: 2,
              },
              property: "property2",
              propertyChangeType: "propertyChangeType2",
              left: "left2",
              right: "right2",
            },
          ],
          commitMetadata: {
            author: "Author 2",
            commitDate: "2023-01-02T12:00:00Z",
            commitDateInstant: "2023-01-02T12:00:00Z",
            id: 2,
          },
        },
      ],
    };

    const constructedAuditData: ChangesEntity[] =
      await constructAuditData(auditData);

    expect(constructedAuditData).toEqual([
      {
        changeType: "other_change_type",
        globalId: {
          entity: "other_entity",
          cdoId: "123",
        },
        commitMetadata: {
          author: "Author 2",
          commitDate: "2023-01-02T12:00:00Z",
          commitDateInstant: "2023-01-02T12:00:00Z",
          id: 2,
        },
        property: "property2",
        propertyChangeType: "propertyChangeType2",
        left: "left2",
        right: "right2",
      },
    ]);
  });

  test("should handle empty content array", async () => {
    const auditData: AuditResponseEntity = {
      content: [],
    };

    const constructedAuditData: ChangesEntity[] =
      await constructAuditData(auditData);

    expect(constructedAuditData).toEqual([]);
  });

  test('should handle missing "changes" property in content items', async () => {
    const auditData: AuditResponseEntity = {
      content: [
        {
          commitMetadata: {
            author: "Author 3",
            commitDate: "2023-01-03T12:00:00Z",
            commitDateInstant: "2023-01-03T12:00:00Z",
            id: 3,
          },
        },
      ],
    };

    const constructedAuditData: ChangesEntity[] =
      await constructAuditData(auditData);

    expect(constructedAuditData).toEqual([]);
  });
});
