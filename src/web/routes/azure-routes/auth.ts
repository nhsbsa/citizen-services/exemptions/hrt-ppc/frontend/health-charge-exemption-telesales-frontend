import * as msal from "@azure/msal-node";
import { environment } from "../../../config/environment";

const msalInstance = new msal.ConfidentialClientApplication(
  environment.MSALCONFIG,
);
const cryptoProvider = new msal.CryptoProvider();

declare module "express-session" {
  interface SessionData {
    csrfToken: string;
    accessToken: string;
    authCodeRequest: any;
    idToken: string;
    account: any;
    isAuthenticated: boolean;
    pkceCodes: any;
  }
}
async function redirectToAuthCodeUrl(
  req,
  res,
  next,
  authCodeUrlRequestParams,
  authCodeRequestParams,
) {
  const { verifier, challenge } = await cryptoProvider.generatePkceCodes();

  req.session.pkceCodes = {
    challengeMethod: "S256",
    verifier: verifier,
    challenge: challenge,
  };

  req.session.authCodeUrlRequest = {
    redirectUri: environment.REDIRECT_URI,
    responseMode: "form_post",
    codeChallenge: req.session.pkceCodes.challenge,
    codeChallengeMethod: req.session.pkceCodes.challengeMethod,
    ...authCodeUrlRequestParams,
  };

  req.session.authCodeRequest = {
    redirectUri: environment.REDIRECT_URI,
    code: "",
    ...authCodeRequestParams,
  };

  try {
    const authCodeUrlResponse = await msalInstance.getAuthCodeUrl(
      req.session.authCodeUrlRequest,
    );

    return res.redirect(authCodeUrlResponse);
  } catch (error) {
    next(error);
  }
}

export { redirectToAuthCodeUrl };
