const mockFindRecordInSearchResults = jest.fn();
const mockBehaviourForPost = jest.fn();
const mockIsActiveAndHasNotExpired = jest.fn();
const isEmpty = jest.fn();

import moment from "moment";
import {
  EDIT_PERSONAL_DETAILS_URL,
  EXEMPTION_VIEW_URL,
} from "../../paths/paths";
import {
  behaviourForGet,
  editPersonalDetails,
  isNavigable,
} from "./edit-personal-details";
import { pageContent } from "./edit-personal-details";
import { sanitize } from "./sanitize";
import { validate } from "./validate";
import { behaviourForPost } from "./behaviour-for-post";
import { CertificateStatus } from "@nhsbsa/health-charge-exemption-common-frontend";

jest.mock("../common/search-utils", () => ({
  findRecordInSearchResults: mockFindRecordInSearchResults,
}));

jest.mock("../common/certificate-utils", () => ({
  isActiveAndHasNotExpired: mockIsActiveAndHasNotExpired,
}));

jest.mock("./behaviour-for-post", () => ({
  behaviourForPost: mockBehaviourForPost,
}));

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

beforeEach(() => {
  jest.clearAllMocks();
  mockBehaviourForPost.mockResolvedValue("updated");
});

describe("pageContent()", () => {
  test(`should return expected results`, () => {
    const translate = (string) => string;
    const req = { params: { id: "HRT123" } };
    const expected = {
      title: translate("editPersonalDetails.title"),
      heading: translate("editPersonalDetails.heading"),
      warningCalloutHeading: translate(
        "editPersonalDetails.warningCalloutHeading",
      ),
      warningCalloutHTML: translate("editPersonalDetails.warningCalloutHTML"),
      summaryCardTitle: translate("editPersonalDetails.summaryCardTitle"),
      firstNameLabel: translate("editPersonalDetails.firstNameLabel"),
      lastNameLabel: translate("editPersonalDetails.lastNameLabel"),
      addressLineOneLabel: translate("editPersonalDetails.addressLineOneLabel"),
      addressLineTwoLabel: translate("editPersonalDetails.addressLineTwoLabel"),
      townLabel: translate("editPersonalDetails.townLabel"),
      postcodeLabel: translate("editPersonalDetails.postcodeLabel"),
      dateOfBirthLabel: translate("editPersonalDetails.dateOfBirthLabel"),
      emailAddressLabel: translate("editPersonalDetails.emailAddressLabel"),
      nhsNumberLabel: translate("editPersonalDetails.nhsNumberLabel"),
      submitButtonText: translate("buttons:submit"),
      previous: `${EXEMPTION_VIEW_URL}/${req.params.id}`,
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
    expect(mockFindRecordInSearchResults).not.toBeCalled();
    expect(mockBehaviourForPost).not.toBeCalled();
  });
});

describe("isNavigable()", () => {
  let req;

  beforeEach(() => {
    req = {
      params: { id: "1234" },
      session: {
        certificate: {
          reference: "1234",
          status: CertificateStatus.ACTIVE,
          startDate: moment().toISOString(),
          endDate: moment().toISOString(),
          applicationDate: moment().toISOString(),
        },
      },
    };
  });

  it.each([[], undefined, ""])(
    "returns false when findRecordInSearchResults() returns %p",
    (value) => {
      mockFindRecordInSearchResults.mockReturnValue(value);

      const result = isNavigable(req);

      expect(result).toBe(false);
      expect(mockFindRecordInSearchResults).toBeCalledTimes(1);
      expect(mockFindRecordInSearchResults).toBeCalledWith(req);
      expect(mockBehaviourForPost).not.toBeCalled();
    },
  );

  test("returns false when findRecordInSearchResults() returns true, reference does not match & status is active", () => {
    mockFindRecordInSearchResults.mockReturnValue(true);
    req.params.id = "nomatch";

    const result = isNavigable(req);

    expect(result).toBe(false);
    expect(mockFindRecordInSearchResults).toBeCalledTimes(1);
    expect(mockFindRecordInSearchResults).toBeCalledWith(req);
    expect(mockBehaviourForPost).not.toBeCalled();
  });

  test("returns false when findRecordInSearchResults() returns true, reference matches & status is not active", () => {
    mockFindRecordInSearchResults.mockReturnValue(true);
    req.session.certificate.status = CertificateStatus.PENDING;

    const result = isNavigable(req);

    expect(result).toBe(false);
    expect(mockFindRecordInSearchResults).toBeCalledTimes(1);
    expect(mockFindRecordInSearchResults).toBeCalledWith(req);
    expect(mockBehaviourForPost).not.toBeCalled();
  });

  test("returns true when findRecordInSearchResults() returns true, reference matches & status is active", () => {
    mockFindRecordInSearchResults.mockReturnValue(true);
    mockIsActiveAndHasNotExpired.mockReturnValue(true);

    const result = isNavigable(req);

    expect(result).toBe(true);
    expect(mockFindRecordInSearchResults).toBeCalledTimes(1);
    expect(mockFindRecordInSearchResults).toBeCalledWith(req);
    expect(mockBehaviourForPost).not.toBeCalled();
  });
});

describe("behaviourForGet()", () => {
  let res;
  const next = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    res = { locals: {} };
  });

  test("should set res.locals when all fields in citizen", async () => {
    const req = {
      session: {
        citizen: {
          firstName: "John",
          lastName: "Smith",
          dateOfBirth: "1997-03-24",
          nhsNumber: "4857773457",
          addresses: [
            {
              addressLine1: "Stella House",
              addressLine2: "Goldcrest Way",
              townOrCity: "Newcastle upon Tyne",
              postcode: "NE15 8NY",
            },
          ],
          emails: [
            {
              emailAddress: "test@test.com",
            },
          ],
        },
      },
    };
    await behaviourForGet()(req, res, next);

    expect(res.locals).toEqual({
      citizen: {
        addressLine1: "Stella House",
        addressLine2: "Goldcrest Way",
        dateOfBirth: "1997-03-24",
        "dateOfBirth-day": "24",
        "dateOfBirth-month": "03",
        "dateOfBirth-year": 1997,
        emailAddress: "test@test.com",
        firstName: "John",
        lastName: "Smith",
        nhsNumber: "4857773457",
        postcode: "NE15 8NY",
        townOrCity: "Newcastle upon Tyne",
      },
    });

    expect(mockFindRecordInSearchResults).not.toBeCalled();
    expect(mockBehaviourForPost).not.toBeCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
  });

  test("should set res.locals when addressLine2 missing in citizen", async () => {
    const req = {
      session: {
        citizen: {
          firstName: "John",
          lastName: "Smith",
          dateOfBirth: "1997-03-24",
          nhsNumber: "4857773457",
          addresses: [
            {
              addressLine1: "Stella House",
              townOrCity: "Newcastle upon Tyne",
              postcode: "NE15 8NY",
            },
          ],
          emails: [
            {
              emailAddress: "test@test.com",
            },
          ],
        },
      },
    };
    await behaviourForGet()(req, res, next);

    expect(res.locals).toEqual({
      citizen: {
        addressLine1: "Stella House",
        dateOfBirth: "1997-03-24",
        "dateOfBirth-day": "24",
        "dateOfBirth-month": "03",
        "dateOfBirth-year": 1997,
        emailAddress: "test@test.com",
        firstName: "John",
        lastName: "Smith",
        nhsNumber: "4857773457",
        postcode: "NE15 8NY",
        townOrCity: "Newcastle upon Tyne",
      },
    });

    expect(mockFindRecordInSearchResults).not.toBeCalled();
    expect(mockBehaviourForPost).not.toBeCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
  });
  it.each([[], ""])(
    "should set res.locals when emails is %p in citizen",
    async (email: any) => {
      const req = {
        session: {
          citizen: {
            firstName: "John",
            lastName: "Smith",
            dateOfBirth: "1997-03-24",
            nhsNumber: "4857773457",
            addresses: [
              {
                addressLine1: "Stella House",
                townOrCity: "Newcastle upon Tyne",
                postcode: "NE15 8NY",
              },
            ],
            emails: [email],
          },
        },
      };
      await behaviourForGet()(req, res, next);

      expect(res.locals).toEqual({
        citizen: {
          addressLine1: "Stella House",
          dateOfBirth: "1997-03-24",
          "dateOfBirth-day": "24",
          "dateOfBirth-month": "03",
          "dateOfBirth-year": 1997,
          firstName: "John",
          lastName: "Smith",
          nhsNumber: "4857773457",
          postcode: "NE15 8NY",
          townOrCity: "Newcastle upon Tyne",
        },
      });

      expect(mockFindRecordInSearchResults).not.toBeCalled();
      expect(mockBehaviourForPost).not.toBeCalled();
      expect(next).toBeCalledTimes(1);
      expect(next).toBeCalledWith();
    },
  );
});

describe("editPersonalDetails", () => {
  test(`should match expected outcomes`, () => {
    const expectedResults = {
      path: `/:id(HRT[A-Za-z0-9]+)${EDIT_PERSONAL_DETAILS_URL}`,
      template: "edit-personal-details",
      pageContent,
      isNavigable,
      behaviourForGet,
      behaviourForPost,
      sanitize,
      validate,
    };

    expect(editPersonalDetails).toEqual(expectedResults);
  });
});
