import {
  telesalesSearchNoResults,
  telesalesSearchFoundResults,
} from "./telesales-search";
import { telesalesNewApplicant } from "./telesales-new-applicant";
import {
  telesalesPageNotFound,
  telesalesProblemWithService,
} from "./telesales-errors";
import { telesalesCookiesNotEnabled } from "./telesales-cookies";
import { configureTestSuite } from "./configure-test-suite";
import {
  telesalesReissueErrorJourney,
  telesalesReissueJourney,
} from "./telesales-reissue";
import { telesalesAmendCertificateJourney } from "./telesales-amend-certificate-details";

const ALL_TESTS = [
  telesalesSearchNoResults,
  telesalesSearchFoundResults,
  telesalesReissueJourney,
  telesalesReissueErrorJourney,
  telesalesNewApplicant,
  telesalesCookiesNotEnabled,
  telesalesPageNotFound,
  telesalesProblemWithService,
  telesalesAmendCertificateJourney,
];

const TEST_SUITE = ALL_TESTS.map(configureTestSuite);

export { TEST_SUITE };
