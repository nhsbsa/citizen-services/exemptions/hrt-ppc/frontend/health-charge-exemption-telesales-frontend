import expect from "expect";
import { getDecisionStatus } from "./get-decision-status";
import { DecisionStatus } from "./decision-statuses";
import { EligibilityStatus } from "./eligibility-statuses";

const SUCCESSFUL_RESULT = {
  applicant: {
    identityStatus: "matched",
    eligibilityStatus: "confirmed",
    addressLine1Match: "matched",
    postcodeMatch: "matched",
    mobilePhoneMatch: "not_set",
    emailAddressMatch: "not_set",
    deathVerificationFlag: "n/a",
  },
};

const IDENTITY_NOT_MATCHED_RESULT = {
  applicant: {
    identityStatus: "not_matched",
    eligibilityStatus: "not_set",
    addressLine1Match: "not_set",
    postcodeMatch: "not_set",
    mobilePhoneMatch: "not_set",
    emailAddressMatch: "not_set",
    deathVerificationFlag: "n/a",
  },
};

const ELIGIBILITY_NOT_CONFIRMED_RESULT = {
  applicant: {
    identityStatus: "matched",
    eligibilityStatus: "not_confirmed",
    addressLine1Match: "not_set",
    postcodeMatch: "not_set",
    mobilePhoneMatch: "not_set",
    emailAddressMatch: "not_set",
    deathVerificationFlag: "n/a",
  },
};

const ELIGIBILITY_CONFIRMED_RESULT = {
  applicant: {
    identityStatus: "matched",
    eligibilityStatus: "confirmed",
    addressLine1Match: "not_set",
    postcodeMatch: "not_set",
    mobilePhoneMatch: "not_set",
    emailAddressMatch: "not_set",
    deathVerificationFlag: "n/a",
  },
};

const DUPLICATE_RESULT = undefined; // A DUPLICATE response from the hrt exemption service will not return a verification result

test(`getDecisionStatus() should return ${DecisionStatus.SUCCESS} if verification result has matching decision and is ELIGIBLE`, () => {
  expect(
    getDecisionStatus({
      verificationResult: SUCCESSFUL_RESULT,
      eligibilityStatus: "ELIGIBLE",
    }),
  ).toBe(DecisionStatus.SUCCESS);
});

test(`getDecisionStatus() should return ${DecisionStatus.FAIL} if result is INELIGIBLE`, () => {
  expect(
    getDecisionStatus({
      verificationResult: SUCCESSFUL_RESULT,
      eligibilityStatus: "INELIGIBLE",
    }),
  ).toBe(DecisionStatus.FAIL);
});

test(`getDecisionStatus() should return ${DecisionStatus.FAIL} if identity not matched`, () => {
  expect(
    getDecisionStatus({ verificationResult: IDENTITY_NOT_MATCHED_RESULT }),
  ).toBe(DecisionStatus.FAIL);
});

test(`getDecisionStatus() should return ${DecisionStatus.FAIL} if eligibility not confirmed`, () => {
  expect(
    getDecisionStatus({ verificationResult: ELIGIBILITY_NOT_CONFIRMED_RESULT }),
  ).toBe(DecisionStatus.FAIL);
});

test(`getDecisionStatus() should return ${DecisionStatus.FAIL} if eligibility status is duplicate`, () => {
  expect(
    getDecisionStatus({
      verificationResult: DUPLICATE_RESULT,
      eligibilityStatus: EligibilityStatus.DUPLICATE,
    }),
  ).toBe(DecisionStatus.FAIL);
});

test(`getDecisionStatus() should return ${DecisionStatus.FAIL} if address mismatches`, () => {
  const mismatchingAddressResults = [
    {
      ...ELIGIBILITY_CONFIRMED_RESULT,
      applicant: {
        ...ELIGIBILITY_CONFIRMED_RESULT.applicant,
        addressLine1Match: "not_matched",
        postcodeMatch: "matched",
      },
    },
    {
      ...ELIGIBILITY_CONFIRMED_RESULT,
      applicant: {
        ...ELIGIBILITY_CONFIRMED_RESULT.applicant,
        addressLine1Match: "matched",
        postcodeMatch: "not_matched",
      },
    },
    {
      ...ELIGIBILITY_CONFIRMED_RESULT,
      applicant: {
        ...ELIGIBILITY_CONFIRMED_RESULT.applicant,
        addressLine1Match: "not_matched",
        postcodeMatch: "not_matched",
      },
    },
  ];

  mismatchingAddressResults.forEach((verificationResult) => {
    expect(
      getDecisionStatus({
        verificationResult,
        eligibilityStatus: EligibilityStatus.ELIGIBLE,
      }),
    ).toBe(DecisionStatus.FAIL);
  });
});

test(`getDecisionStatus() should return ${DecisionStatus.SUCCESS} if address or postcode not set`, () => {
  const mismatchingAddressResults = [
    {
      ...ELIGIBILITY_CONFIRMED_RESULT,
      applicant: {
        ...ELIGIBILITY_CONFIRMED_RESULT.applicant,
        addressLine1Match: "not_set",
        postcodeMatch: "matched",
      },
    },
    {
      ...ELIGIBILITY_CONFIRMED_RESULT,
      applicant: {
        ...ELIGIBILITY_CONFIRMED_RESULT.applicant,
        addressLine1Match: "matched",
        postcodeMatch: "not_set",
      },
    },
  ];

  mismatchingAddressResults.forEach((verificationResult) => {
    expect(
      getDecisionStatus({
        verificationResult,
        eligibilityStatus: EligibilityStatus.ELIGIBLE,
      }),
    ).toBe(DecisionStatus.SUCCESS);
  });
});
