const mockSearchExemptions = jest.fn();

import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { search } from "./post";

jest.mock("./search-citizen-certificates-provider", () => ({
  searchExemptions: mockSearchExemptions,
}));

beforeEach(() => {
  jest.clearAllMocks();
});

describe("search()", () => {
  test("should return result", async () => {
    mockSearchExemptions.mockResolvedValue({ something: "data" });

    const req = {};
    const res = {
      redirect: jest.fn(),
    };
    const next = jest.fn();

    await search(req, res, next);

    expect(next).not.toBeCalled();
    expect(req).toEqual({});
    expect(mockSearchExemptions).toBeCalledTimes(1);
    expect(mockSearchExemptions).toBeCalledWith(req);
  });

  test("should call next with wrapped error when searchExemptions throws error", async () => {
    mockSearchExemptions.mockReturnValue(
      Promise.reject(new Error("Axios error")),
    );
    const req = {
      path: "/test",
    };
    const res = {};
    const next = jest.fn();
    const expectedError = wrapError({
      cause: new Error("Axios error"),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });

    await search(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(mockSearchExemptions).toBeCalledTimes(1);
    expect(mockSearchExemptions).toBeCalledWith(req);
  });
});
