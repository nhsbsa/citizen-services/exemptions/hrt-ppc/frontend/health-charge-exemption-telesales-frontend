import {
  getRowsForPersonalDetailsTable,
  getRowsForCertificateDetailsTable,
} from "../exemption-detail/get-detail-rows";
import { pageContent } from "./page-content";

export const behaviourForGet = () => (req, res, next) => {
  const localisation = pageContent({ translate: req.t, req });
  const certificateDetailsRowList = getRowsForCertificateDetailsTable(
    req.session.certificate,
    localisation,
  );
  const personalDetailsRowList = getRowsForPersonalDetailsTable(
    req.session.citizen,
    localisation,
  );
  res.locals.certificateDetailsData = certificateDetailsRowList;
  res.locals.personalDetailsData = personalDetailsRowList;
  next();
};
