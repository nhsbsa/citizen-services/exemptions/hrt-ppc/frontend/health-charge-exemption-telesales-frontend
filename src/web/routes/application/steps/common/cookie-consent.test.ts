import expect from "expect";
import {
  getCookie,
  getConsent,
  isConsentedToStatistics,
} from "./cookie-consent";

test("getCookie", () => {
  const cookieConsent =
    '{"necessary":true,"preferences":false,"statistics":false,"marketing":false,"consented":true,"version":3}';
  const reqWithCookie = {
    cookies: {
      "nhsuk-cookie-consent": cookieConsent,
    },
  };
  const reqWithNoCookie = {
    cookies: {},
  };

  const expected = JSON.parse(cookieConsent);

  expect(getCookie(reqWithCookie)).toEqual(expected);
  expect(getCookie(reqWithNoCookie)).toEqual(undefined);
});

test("getConsent", () => {
  const cookieConsent =
    '{"necessary":true,"preferences":false,"statistics":false,"marketing":false,"consented":true,"version":3}';
  const reqWithCookie = {
    cookies: { "nhsuk-cookie-consent": cookieConsent },
  };
  const reqWithNoCookie = {
    cookies: {},
  };

  const expected = JSON.parse(
    '{"necessary":true,"preferences":false,"statistics":false,"marketing":false,"consented":true}',
  );

  expect(getConsent(reqWithCookie)).toEqual(expected);
  expect(getConsent(reqWithNoCookie)).toEqual({});
});

test("isConsentedToStatistics", () => {
  const cookieConsentStatisticsFalse =
    '{"necessary":true,"preferences":false,"statistics":false,"marketing":false,"consented":true,"version":3}';
  const cookieConsentStatisticsTrue =
    '{"necessary":true,"preferences":false,"statistics":true,"marketing":false,"consented":true,"version":3}';
  const reqWithCookieStatisticsFalse = {
    cookies: { "nhsuk-cookie-consent": cookieConsentStatisticsFalse },
  };
  const reqWithCookieStatisticsTrue = {
    cookies: { "nhsuk-cookie-consent": cookieConsentStatisticsTrue },
  };
  const reqWithCookieInvalid = {
    cookies: { "nhsuk-cookie-consent": "invalidcookie" },
  };
  const reqWithNoCookie = {
    cookies: {},
  };

  expect(isConsentedToStatistics(reqWithCookieStatisticsFalse)).toEqual(false);
  expect(isConsentedToStatistics(reqWithCookieStatisticsTrue)).toEqual(true);
  expect(isConsentedToStatistics(reqWithNoCookie)).toEqual(false);
  expect(() => isConsentedToStatistics(reqWithCookieInvalid)).toThrowError(
    /Unexpected token/,
  );
});
