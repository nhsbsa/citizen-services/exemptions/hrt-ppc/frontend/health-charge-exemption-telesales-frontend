import { setCommonTemplateValues } from "./register-routes";

jest.mock("./azure-routes/auth-routes", () => ({
  registerAuthRoutes: jest.fn(),
}));
jest.mock("../../common/azure-utils", () => ({
  getUserName: jest.fn(),
}));

beforeEach(() => {
  jest.clearAllMocks();
});

const req = {
  language: "en",
  t: jest.fn((key) => key),
  session: {
    user: {
      name: "Alice",
    },
  },
};
const res = {
  locals: {},
  next: jest.fn(),
};
describe("setCommonTemplateValues", () => {
  test("should set the expected properties on res.locals when getUserName returns undefined", () => {
    jest
      .requireMock("../../common/azure-utils")
      .getUserName.mockReturnValue(undefined);

    setCommonTemplateValues(req, res, res.next);

    expect(res.locals["htmlLang"]).toBe(req.language);
    expect(res.locals["language"]).toBe("en");
    expect(res.locals["cookieLinkName"]).toBe("cookies.linkName");
    expect(res.locals["privacyNoticeLinkName"]).toBe("privacyNotice.linkName");
    expect(res.locals["footerAccessibilityLink"]).toBe(
      "footer.accessibilityLink",
    );
    expect(res.locals["footerContactUsLink"]).toBe("footer.contactUsLink");
    expect(res.locals["footerCookiesLink"]).toBe("footer.cookiesLink");
    expect(res.locals["footerPrivacyLink"]).toBe("footer.privacyLink");
    expect(res.locals["footerTermsLink"]).toBe("footer.termsLink");
    expect(res.locals["phaseBannerHtml"]).toBe("phaseBanner.html");
    expect(res.locals["back"]).toBe("back");
    expect(res.locals["serviceName"]).toBe("header.serviceName");
    expect(res.locals["serviceDescription"]).toBe("header.serviceDescription");
    expect(res.locals["signoutText"]).toBe("header.signoutText");
    expect(res.locals["username"]).toBe("LOCAL_USER");
    expect(res.locals["signoutUrl"]).toBe("");
    expect(res.locals["backButtonText"]).toBe("buttons:back");
    expect(res.locals["cancelButtonText"]).toBe("buttons:cancel");
  });

  test("should set the expected properties on res.locals when getUserName does not returns undefined", () => {
    jest
      .requireMock("../../common/azure-utils")
      .getUserName.mockReturnValue("MOCK_USER_NAME");

    setCommonTemplateValues(req, res, res.next);

    expect(res.locals["htmlLang"]).toBe(req.language);
    expect(res.locals["language"]).toBe("en");
    expect(res.locals["cookieLinkName"]).toBe("cookies.linkName");
    expect(res.locals["privacyNoticeLinkName"]).toBe("privacyNotice.linkName");
    expect(res.locals["footerAccessibilityLink"]).toBe(
      "footer.accessibilityLink",
    );
    expect(res.locals["footerContactUsLink"]).toBe("footer.contactUsLink");
    expect(res.locals["footerCookiesLink"]).toBe("footer.cookiesLink");
    expect(res.locals["footerPrivacyLink"]).toBe("footer.privacyLink");
    expect(res.locals["footerTermsLink"]).toBe("footer.termsLink");
    expect(res.locals["phaseBannerHtml"]).toBe("phaseBanner.html");
    expect(res.locals["back"]).toBe("back");
    expect(res.locals["serviceName"]).toBe("header.serviceName");
    expect(res.locals["serviceDescription"]).toBe("header.serviceDescription");
    expect(res.locals["signoutText"]).toBe("header.signoutText");
    expect(res.locals["username"]).toBe("MOCK_USER_NAME");
    expect(res.locals["signoutUrl"]).toBe("/test-context/auth/signout");
    expect(res.locals["backButtonText"]).toBe("buttons:back");
    expect(res.locals["cancelButtonText"]).toBe("buttons:cancel");
  });
});
