import {
  Event,
  Preference,
  CertificateType,
  Channel,
} from "@nhsbsa/health-charge-exemption-common-frontend";

export default {
  status: 200,
  jsonBody: {
    id: "c0a81da7-893f-17e3-8189-3fb039460000",
    citizenId: "c0a8000a-86da-1e45-8186-ee98c395000f",
    certificateType: CertificateType.HRT_PPC,
    event: Event.ANY,
    preference: Preference.EMAIL,
    _meta: {
      channel: Channel.ONLINE,
      createdTimestamp: "2023-07-10T17:34:20.933473",
      createdBy: "ONLINE",
      updatedTimestamp: "2023-07-10T17:34:20.933473",
      updatedBy: "ONLINE",
    },
    _links: {
      self: {
        href: "http://localhost:8150/v1/user-preference/c0a81da7-893f-17e3-8189-3fb039460000",
      },
      userPreference: {
        href: "http://localhost:8150/v1/user-preference/c0a81da7-893f-17e3-8189-3fb039460000",
      },
    },
  },
  headers: {
    "Content-Type": "application/json",
  },
};
