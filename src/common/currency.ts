const toPounds = (pence) => (parseInt(pence, 10) / 100).toFixed(2);

export { toPounds };
