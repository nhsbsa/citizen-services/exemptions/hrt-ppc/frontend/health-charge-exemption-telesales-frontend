import moment from "moment";

import {
  CertificateStatus,
  CertificateType,
  Channel,
} from "@nhsbsa/health-charge-exemption-common-frontend";

export default {
  status: 200,
  jsonBody: {
    id: "c0a8000a-86da-1e6a-8186-ee9a40dd000f",
    citizenId: "c0a8000a-86da-1e45-8186-ee98c395000f",
    reference: "HRTC50ABAA9",
    type: CertificateType.HRT_PPC,
    applicationDate: moment().subtract(15, "days").format("YYYY-MM-DD"),
    duration: "P12M",
    startDate: "2022-12-12",
    endDate: "2099-12-12",
    cost: 1870,
    status: CertificateStatus.ACTIVE,
    pharmacyId: "ABCD",
    _meta: {
      channel: Channel.ONLINE,
      createdTimestamp: "2023-03-17T08:05:31.990953",
      createdBy: "POSTMAN",
      updatedTimestamp: "2023-03-17T08:05:31.990953",
      updatedBy: "POSTMAN",
    },
    _links: {
      self: {
        href: "http://localhost:8090/v1/certificates/c0a8000a-86da-1e6a-8186-ee9a40dd000f",
      },
      certificate: {
        ref: "http://localhost:8090/v1/certificates/c0a8000a-86da-1e6a-8186-ee9a40dd000f",
      },
    },
  },
  headers: {
    "Content-Type": "application/json",
  },
};
