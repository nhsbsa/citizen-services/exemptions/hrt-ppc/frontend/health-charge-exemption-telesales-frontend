import { formatDateForDisplayFromDate } from "@nhsbsa/health-charge-exemption-common-frontend";
import { PaymentStatus } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/enums";
import { Section } from "./enums";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import {
  AUDIT_CERTIFICATE_ENTITY,
  AUDIT_CITIZEN_ENTITY,
  AUDIT_PAYMENT_ENTITY,
  AUDIT_USER_PREFERENCE_ENTITY,
} from "../common/constants";
import { formatCertificateFulfillment } from "../common/formatters/certificate-fulfillment";

export const getSectionFromEntity = (entity) => {
  if (entity.includes(AUDIT_CITIZEN_ENTITY)) {
    return Section.CITIZEN_SECTION;
  } else if (
    entity.includes(AUDIT_CERTIFICATE_ENTITY) ||
    entity.includes(AUDIT_USER_PREFERENCE_ENTITY)
  ) {
    return Section.CERTIFICATE_SECTION;
  } else if (entity.includes(AUDIT_PAYMENT_ENTITY)) {
    return Section.PAYMENT_SECTION;
  } else {
    throw new Error(`Unknown entity: ${entity}`);
  }
};

export const formatAuditType = (entity, translate) => {
  const section: string = getSectionFromEntity(entity);
  return translate[section];
};

export const formatAuditDescription = (
  entity,
  fieldName,
  toValue,
  translate,
) => {
  const section: string = getSectionFromEntity(entity);
  if (notIsUndefinedOrNullOrEmpty(toValue)) {
    return `${translate.updated} ${translate[section][fieldName]}`;
  }
  return `${translate.removed} ${translate[section][fieldName]}`;
};

/**
 * Formats a value for display, especially when it represents a date or user-preference.
 * also add a class to value when Payment status is FAILED.
 * @param {string|Date} value - The value to be formatted.
 * @returns {string} - The formatted value, potentially as a formatted date.
 */
export const formatAuditFromAndTo = (value, entity, translate) => {
  // Check if the value can be parsed as a Date (likely a date string)
  if (!isNaN(Date.parse(value))) {
    return formatDateForDisplayFromDate(new Date(value));
  } // Check if the entity is preference and format
  if (entity.includes(AUDIT_USER_PREFERENCE_ENTITY)) {
    return formatCertificateFulfillment(value, translate);
  } // Check if the value to be red coloured if failed payment
  if (entity.includes(AUDIT_PAYMENT_ENTITY) && value === PaymentStatus.FAILED) {
    return `<span class="hce-telesales-error-color"> ${value} </span>`;
  }
  return value;
};
