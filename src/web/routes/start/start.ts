import * as express from "express";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";
import { sessionDestroyButKeepLoggedIn } from "../application/steps/common/session-destroy";

const CONTEXT_PATH = process.env.CONTEXT_PATH || "";
const SEARCH_URL = "/search-hrt-records";

const renderStartRoute = (app) => {
  const router = express.Router();
  app.use(router);
  // This will create a redirect on the route to go to the start url
  router.get(CONTEXT_PATH, configureAuthentication, (req, res) => {
    sessionDestroyButKeepLoggedIn(req);
    res.clearCookie("lang");
    res.cookie("isBrowserCookieEnabled", true, { httpOnly: true });
    res.redirect(CONTEXT_PATH + SEARCH_URL);
  });
};

const registerStartRoute = (app) => renderStartRoute(app);

export { registerStartRoute };
