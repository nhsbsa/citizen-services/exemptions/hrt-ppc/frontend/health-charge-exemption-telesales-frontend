import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";

const pageContent = ({ translate }) => ({
  title: translate("noResults.title"),
  heading: translate("noResults.heading"),
  searchBoxHeading: translate("noResults.searchBoxHeading"),
  paragraphOne: translate("noResults.paragraphOne"),
  newApplicationButtonText: translate("buttons:newApplication"),
  previous: false,
});

const isNavigable = (req) =>
  !notIsUndefinedOrNullOrEmpty(req.session.searchResult);

const searchNoResults = {
  path: "/no-results",
  template: "no-results",
  pageContent,
  isNavigable,
};

export { searchNoResults, pageContent, isNavigable };
