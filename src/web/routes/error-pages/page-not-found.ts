const CONTEXT_PATH = process.env.CONTEXT_PATH || "";
import { sessionDestroyButKeepLoggedIn } from "../application/steps/common/session-destroy";
import { injectAdditionalHeaders } from "../application/flow-control/middleware/inject-headers";
import { getErrorPageContent } from "./error-page-content";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";

const getPageNotFoundErrorPage = (req, res) => {
  sessionDestroyButKeepLoggedIn(req);
  injectAdditionalHeaders(req, res);

  res.render("error-pages/page-not-found", getErrorPageContent(req));
};

const registerPageNotFoundRoute = (app) => {
  app.get(
    `${CONTEXT_PATH}/page-not-found`,
    configureAuthentication,
    getPageNotFoundErrorPage,
  );
  app.all("*", configureAuthentication, (req, res) => {
    res.status(404).redirect(`${CONTEXT_PATH}/page-not-found`);
  });
};

export { registerPageNotFoundRoute, getPageNotFoundErrorPage };
