import { stateMachine } from "../../../flow-control/state-machine";
import { COMPLETED } from "../../../flow-control/states";
import { reissue } from "./post";

export const behaviourForPost = (config, journey) => async (req, res, next) => {
  await reissue(req, res, next);
  stateMachine.setState(COMPLETED, req, journey);
  next();
};
