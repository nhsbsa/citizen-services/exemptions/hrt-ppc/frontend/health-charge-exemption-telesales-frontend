import * as sinon from "sinon";
import * as registerJourneyRoutes from "./register-journey-routes";
import * as injectAdditionalHeaders from "./flow-control/middleware/inject-headers";
import * as configureSessionDetails from "./flow-control/middleware/session-details";
import * as configureGet from "./flow-control/middleware/configure-get";
import * as handleRequestForPath from "./flow-control/middleware/handle-path-request";
import * as checkBrowserCookie from "./flow-control/middleware/cookies-check";
import * as renderView from "./flow-control/middleware/render-view";
import * as sanitize from "./flow-control/middleware/sanitize";
import * as escape from "./flow-control/middleware/unescape-specific";
import * as configurePost from "./flow-control/middleware/configure-post";
import * as handlePost from "./flow-control/middleware/handle-post";
import * as handlePostRedirects from "./flow-control/middleware//handle-post-redirects";
import { registerCheckAnswersRoutes, registerCardPaymentRoutes } from "./steps";

const mockPost = jest.fn(() => Promise.resolve([]));
const mockGet = jest.fn(() => ({
  post: mockPost,
}));
const mockRouter = {
  route: jest.fn(() => ({
    get: mockGet,
  })),
};
jest.mock("express", () => ({
  Router: () => mockRouter,
}));
const app = {
  use: jest.fn(),
  listen: jest.fn(),
};

jest.mock("./steps", () => ({
  ...jest.requireActual("./steps"),
  registerCheckAnswersRoutes: jest.fn(),
  registerCardPaymentRoutes: jest.fn(),
  registerDecisionRoute: jest.fn(),
}));

const mockInjectAdditionalHeaders = jest.spyOn(
  injectAdditionalHeaders,
  "injectAdditionalHeaders",
);
const mockConfigureSessionDetails = jest.spyOn(
  configureSessionDetails,
  "configureSessionDetails",
);
const mockConfigureGet = jest.spyOn(configureGet, "configureGet");
const mockHandleRequestForPath = jest.spyOn(
  handleRequestForPath,
  "handleRequestForPath",
);
const mockCheckBrowserCookie = jest.spyOn(
  checkBrowserCookie,
  "checkBrowserCookie",
);
const mockRenderView = jest.spyOn(renderView, "renderView");
const mockHandleOptionalMiddleware = jest.spyOn(
  registerJourneyRoutes,
  "handleOptionalMiddleware",
);
const mockSanitize = jest.spyOn(sanitize, "sanitize");
const mockEscape = jest.spyOn(escape, "escape");
const mockConfigurePost = jest.spyOn(configurePost, "configurePost");
const mockHandlePost = jest.spyOn(handlePost, "handlePost");
const mockHandlePostRedirects = jest.spyOn(
  handlePostRedirects,
  "handlePostRedirects",
);

describe("handleOptionalMiddleware() function", () => {
  test("should call the operation if defined with correct arguments", () => {
    const operation = sinon.spy();
    const fallback = sinon.spy();
    const args = ["first argument", "second argument"];

    registerJourneyRoutes.handleOptionalMiddleware(args)(operation, fallback);
    expect(operation.getCall(0).args).toEqual(args);
    expect(fallback.called).toBe(false);
  });

  test("should call the fallback if operation not defined with correct arguments", () => {
    const operation = undefined;
    const fallback = sinon.spy();
    const args = ["first argument", "second argument"];
    registerJourneyRoutes.handleOptionalMiddleware(args)(operation, fallback);

    expect(fallback.getCall(0).args).toEqual([]);
  });

  test("should return the default fallback if operation and fallback are not defined", () => {
    const req = {};
    const res = {};
    const next = sinon.spy();
    registerJourneyRoutes.handleOptionalMiddleware(undefined)(undefined)(
      req,
      res,
      next,
    );

    expect(next.called).toBe(true);
  });
});

describe("registerJourneyRoutes() function", () => {
  test("should register all routes correctly", () => {
    const steps = [
      {
        path: "/first",
      },
      {
        path: "/second",
        toggle: "STEP_TWO_ENABLED",
      },
    ];
    const journey = {
      steps,
    };

    const config = { example: "value" };

    registerJourneyRoutes.registerJourneyRoutes(config, {}, app)(journey);

    expect(mockRouter.route).toBeCalledTimes(2);
    expect(mockGet).toBeCalledTimes(2);
    expect(mockPost).toBeCalledTimes(2);
    expect(mockRouter.route.mock.calls).toEqual([
      [steps[0].path], // First call
      [steps[1].path], // Second call
    ]);

    // the injectAdditionalHeaders is only called at runtime
    expect(mockInjectAdditionalHeaders).not.toBeCalled();
    // the checkBrowserCookie is only called at runtime
    expect(mockCheckBrowserCookie).not.toBeCalled();
    // Configure session details called twice for each step because of get and post
    expect(mockConfigureSessionDetails).toBeCalledTimes(4);
    expect(mockConfigureSessionDetails.mock.calls[0]).toEqual([journey]);
    expect(mockConfigureSessionDetails.mock.calls[1]).toEqual([journey]);
    expect(mockConfigureSessionDetails.mock.calls[2]).toEqual([journey]);
    expect(mockConfigureSessionDetails.mock.calls[3]).toEqual([journey]);
    // Handle request for path called twice for each step because of get and post
    expect(mockHandleRequestForPath).toBeCalledTimes(4);
    expect(mockHandleRequestForPath.mock.calls[0]).toEqual([journey, steps[0]]);
    expect(mockHandleRequestForPath.mock.calls[1]).toEqual([journey, steps[0]]);
    expect(mockHandleRequestForPath.mock.calls[2]).toEqual([journey, steps[1]]);
    expect(mockHandleRequestForPath.mock.calls[3]).toEqual([journey, steps[1]]);
    // Configure get middleware for both steps
    expect(mockConfigureGet).toBeCalledTimes(2);
    expect(mockConfigureGet.mock.calls[0]).toEqual([
      steps,
      steps[0],
      journey,
      config,
    ]);
    expect(mockConfigureGet.mock.calls[1]).toEqual([
      steps,
      steps[1],
      journey,
      config,
    ]);
    // Optional Middleware for both steps
    expect(mockHandleOptionalMiddleware).toBeCalledTimes(2);
    expect(mockHandleOptionalMiddleware.mock.calls[0]).toContainEqual([
      config,
      journey,
      steps[0],
    ]);
    expect(mockHandleOptionalMiddleware.mock.calls[1]).toContainEqual([
      config,
      journey,
      steps[1],
    ]);
    // Render view called twice for each step because of get and post
    expect(mockRenderView).toBeCalledTimes(4);
    expect(mockRenderView.mock.calls[0]).toEqual([steps[0]]);
    expect(mockRenderView.mock.calls[1]).toEqual([steps[0]]);
    expect(mockRenderView.mock.calls[2]).toEqual([steps[1]]);
    expect(mockRenderView.mock.calls[3]).toEqual([steps[1]]);
    // the mockSanitize is only called at runtime
    expect(mockSanitize).not.toBeCalled();
    expect(mockEscape).toBeCalledTimes(2);
    // Configure post configuration for both steps
    expect(mockConfigurePost).toBeCalledTimes(2);
    expect(mockConfigurePost.mock.calls[0]).toEqual([steps, steps[0], journey]);
    expect(mockConfigurePost.mock.calls[1]).toEqual([steps, steps[1], journey]);
    // Handle Post configuration for both steps
    expect(mockHandlePost).toBeCalledTimes(2);
    expect(mockHandlePost.mock.calls[0]).toEqual([journey, steps[0]]);
    expect(mockHandlePost.mock.calls[1]).toEqual([journey, steps[1]]);
    // Handle Post redirects for both steps
    expect(mockHandlePostRedirects).toBeCalledTimes(2);
    expect(mockHandlePostRedirects.mock.calls[0]).toEqual([journey]);
    expect(mockHandlePostRedirects.mock.calls[1]).toEqual([journey]);

    expect(app.use).toBeCalledTimes(2);
    expect(registerCheckAnswersRoutes).toBeCalledTimes(1);
    expect(registerCardPaymentRoutes).toBeCalledTimes(1);
  });
});
