const getUserName = (session) => {
  if (session.account && session.account.username) {
    return session.account.username.toLowerCase();
  }
  return undefined;
};

const getUserCypher = (session) => {
  if (session.account && session.account.username) {
    const cypher = session.account.username.split("@")[0];
    return cypher.toUpperCase();
  }
  throw new Error("Unable to derive cypher from username");
};

function isUserAuthenticated(req: any) {
  if (req.session["isAuthenticated"]) {
    return true;
  }
  return false;
}

export { getUserName, isUserAuthenticated, getUserCypher };
