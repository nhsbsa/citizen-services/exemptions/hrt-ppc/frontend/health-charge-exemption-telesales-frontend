import { registerCheckAnswersRoutes } from "./check-answers";

const config = {
  environment: {
    CARD_PAYMENTS_USE_MOCK: true,
  },
};

describe("registerCheckAnswersRoutes", () => {
  test("should register check answers route with correct middleware", () => {
    const journey = {
      pathPrefix: "/your-path-prefix",
    };

    const app = {
      route: jest.fn().mockReturnThis(),
      get: jest.fn().mockReturnThis(),
      post: jest.fn().mockReturnThis(),
    };

    const csrfProtection = jest.fn();

    registerCheckAnswersRoutes(journey, app, csrfProtection, config);

    expect(app.route).toHaveBeenCalledWith(
      "/your-path-prefix/check-their-answers",
    );
    expect(app.get).toHaveBeenCalledTimes(1);
    expect(app.get).toHaveBeenCalledWith(
      expect.any(Function),
      expect.any(Function),
      csrfProtection,
      expect.any(Function),
    );
    expect(app.post).toHaveBeenCalledTimes(1);
    expect(app.post).toHaveBeenCalledWith(csrfProtection, expect.any(Function));
  });
});
