import * as httpStatus from "http-status-codes";
import moment from "moment";
import { wrapError } from "../../errors";
import {
  Event,
  Preference,
  CertificateStatus,
  CertificateType,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/enums";

const data = {
  data: {
    certificate: {
      id: "123",
      reference: "HRT123",
      status: CertificateStatus.PENDING,
      startDate: moment().subtract(2, "d").toDate(),
      endDate: moment().add(2, "d").toDate(),
      applicationDate: moment().subtract(3, "d").toDate(),
    },
    citizen: {
      emails: "test@email.com",
    },
    notes: [{ id: "789" }],
    preference: {
      id: "111",
      event: Event.ANY,
      preference: Preference.EMAIL,
    },
    audit: [{ id: "789" }],
  },
};
const certificate = {
  id: "1234",
  reference: "ref",
  type: CertificateType.HRT_PPC,
  status: CertificateStatus.PENDING,
  startDate: moment().add(1, "d").toDate(),
  endDate: moment().add(2, "d").toDate(),
  citizen: {
    firstName: "fName",
    lastName: "lName",
    dateOfBirth: moment().subtract(2, "d").toDate(),
  },
  preference: {
    id: "111",
    event: Event.ANY,
    preference: Preference.EMAIL,
  },
};
const notesHeading = [
  { text: "02/02/1990" },
  { text: "19:38" },
  { text: "AGENT" },
  { text: "note desc" },
];
const auditHistoryHeading = [
  { text: "02/02/1990" },
  { text: "Personal details" },
  { text: "fname" },
  { text: "new fname" },
  { text: "updated firstName" },
];
const pageContent = "page content";
const rowsCertificateDetailsTable = "rows certificate";
const rowsPersonalDetailsTable = "rows citizen";
const rowsNotesTable = "rows notes";
const rowsAuditTable = "rows audit";
const mockPageContent = jest.fn().mockReturnValue(pageContent);
const mockFindRecordInSearchData = jest.fn().mockReturnValue(certificate);
const mockMakeRequests = jest.fn().mockReturnValue(data);
const mockGetRowsForCertificateDetailsTable = jest
  .fn()
  .mockReturnValue(rowsCertificateDetailsTable);
const mockGetRowsForPersonalDetailsTable = jest
  .fn()
  .mockReturnValue(rowsPersonalDetailsTable);
const mockGetRowsForNotesTable = jest.fn().mockReturnValue(rowsNotesTable);
const mockGetRowsForAuditHistoryTable = jest
  .fn()
  .mockResolvedValue(rowsAuditTable);

import { behaviourForGet } from "./behaviour-for-get";

jest.mock("../common/search-utils", () => ({
  findRecordInSearchResults: mockFindRecordInSearchData,
}));

jest.mock("./make-calls", () => ({
  makeRequests: mockMakeRequests,
}));

jest.mock("./page-content", () => ({
  pageContent: mockPageContent,
}));

jest.mock("./get-detail-rows", () => ({
  getRowsForCertificateDetailsTable: mockGetRowsForCertificateDetailsTable,
  getRowsForPersonalDetailsTable: mockGetRowsForPersonalDetailsTable,
  getRowsForNotesTable: mockGetRowsForNotesTable,
  getNotesHeadings: jest.fn().mockReturnValue(notesHeading),
  getAuditHistoryHeadings: jest.fn().mockReturnValue(auditHistoryHeading),
  getRowsForAuditHistoryTable: mockGetRowsForAuditHistoryTable,
}));

beforeEach(() => {
  data.data.certificate.status = CertificateStatus.PENDING;
  data.data.preference.event = Event.ANY;
});

describe("behaviourForGet()", () => {
  const req = {
    t: "",
    path: "/page",
    session: {
      citizen: {},
      certificate: {},
    },
  };
  let res;
  const next = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    res = { locals: {} };
  });

  test("should call pageContent and findRecordInSearchResults with the correct arguments", async () => {
    await behaviourForGet()(req, res, next);

    expect(res).toStrictEqual({
      locals: {
        certificateDetailsData: "rows certificate",
        notesData: {
          headings: [
            { text: "02/02/1990" },
            { text: "19:38" },
            { text: "AGENT" },
            { text: "note desc" },
          ],
          rows: "rows notes",
        },
        auditHistoryData: {
          headings: [
            { text: "02/02/1990" },
            { text: "Personal details" },
            { text: "fname" },
            { text: "new fname" },
            { text: "updated firstName" },
          ],
          rows: "rows audit",
        },
        personalDetailsData: "rows citizen",
        showReissue: false,
        showEditPersonalDetails: false,
        reference: "HRT123",
      },
    });
    expect(req.session.citizen).toEqual(data.data.citizen);
    expect(req.session.certificate).toEqual(data.data.certificate);
    expect(req.session.certificate["preference"]).toEqual(
      data.data.certificate["preference"],
    );
    expect(next).toBeCalledTimes(1);
    expect(mockPageContent).toBeCalledTimes(1);
    expect(mockFindRecordInSearchData).toBeCalledTimes(1);
    expect(mockMakeRequests).toBeCalledTimes(1);
    expect(mockGetRowsForCertificateDetailsTable).toBeCalledTimes(1);
    expect(mockGetRowsForPersonalDetailsTable).toBeCalledTimes(1);
    expect(mockGetRowsForNotesTable).toBeCalledTimes(1);
    expect(mockGetRowsForAuditHistoryTable).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
    expect(mockPageContent).toBeCalledWith({ translate: req.t, req });
    expect(mockFindRecordInSearchData).toBeCalledWith(req);
    expect(mockMakeRequests).toBeCalledWith(req, certificate);
    expect(mockGetRowsForCertificateDetailsTable).toBeCalledWith(
      data.data.certificate,
      pageContent,
    );
    expect(mockGetRowsForPersonalDetailsTable).toBeCalledWith(
      data.data.citizen,
      pageContent,
    );
    expect(mockGetRowsForNotesTable).toBeCalledWith(
      data.data.notes,
      pageContent,
    );
    expect(mockGetRowsForAuditHistoryTable).toBeCalledWith(
      data.data.audit,
      pageContent,
    );
    expect(req.session["notes"]).toEqual(data.data.notes);
  });

  test("should call pageContent and findRecordInSearchResults and certificate util functions with the correct arguments when certificate status active", async () => {
    data.data.certificate.status = CertificateStatus.ACTIVE;
    await behaviourForGet()(req, res, next);

    expect(res.locals.showEditPersonalDetails).toBeTruthy();
    expect(res.locals.showReissue).toBeTruthy();
    expect(res.locals.reference).toBe("HRT123");
  });

  test("should set reissue in locals as true when certificate status is active and event set to ANY", async () => {
    data.data.certificate.status = CertificateStatus.ACTIVE;
    await behaviourForGet()(req, res, next);

    expect(res.locals.showReissue).toBeTruthy();
  });

  test("should set reissue in locals as false when certificate status is active and event set not set to ANY", async () => {
    data.data.preference.event = Event.REMINDER;
    data.data.certificate.status = CertificateStatus.ACTIVE;
    await behaviourForGet()(req, res, next);

    expect(res.locals.showReissue).toBeFalsy();
  });

  test("should call next with wrapError() when making request fails", async () => {
    mockMakeRequests.mockReturnValue(Promise.reject(new Error("Axios error")));

    await behaviourForGet()(req, res, next);

    expect(res).toEqual({ locals: {} });

    const expectedError = wrapError({
      cause: new Error("Axios error"),
      message: `Error on load ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });

    expect(next).toBeCalledTimes(1);
    expect(mockPageContent).toBeCalledTimes(1);
    expect(mockFindRecordInSearchData).toBeCalledTimes(1);
    expect(mockMakeRequests).toBeCalledTimes(1);
    expect(mockGetRowsForCertificateDetailsTable).not.toBeCalled();
    expect(mockGetRowsForPersonalDetailsTable).not.toBeCalled();
    expect(mockGetRowsForNotesTable).not.toBeCalled();
    expect(mockGetRowsForAuditHistoryTable).not.toBeCalled();
    expect(next).toBeCalledWith(expectedError);
    expect(mockPageContent).toBeCalledWith({ translate: req.t, req });
    expect(mockFindRecordInSearchData).toBeCalledWith(req);
    expect(mockMakeRequests).toBeCalledWith(req, certificate);
  });
});
