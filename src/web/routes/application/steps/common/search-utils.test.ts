import expect from "expect";
import { findRecordInSearchResults } from "./search-utils";

describe("findRecordInSearchResults()", () => {
  test("should return undefined when req.params.id undefined", () => {
    const searchResult = [{ id: "otherData" }];
    const req = {
      params: { id: undefined },
      session: { searchResult },
    };
    const result = findRecordInSearchResults(req);
    expect(result).toBe(undefined);
  });

  test("should return undefined when req.session undefined", () => {
    const req = {
      params: { id: "HPC" },
      session: undefined,
    };
    const result = findRecordInSearchResults(req);
    expect(result).toBe(undefined);
  });

  test("should return undefined when req.session.searchResult undefined", () => {
    const req = {
      params: { id: "HPC" },
      session: { searchResult: undefined },
    };
    const result = findRecordInSearchResults(req);
    expect(result).toBe(undefined);
  });

  test("should return undefined when reference not found", () => {
    const searchResult = [{ id: "otherData" }];
    const req = {
      params: { id: "HPC" },
      session: { searchResult },
    };
    const result = findRecordInSearchResults(req);
    expect(result).toBe(undefined);
  });

  test("should find reference in searchResult when param.id present and return searchResult", () => {
    const searchResult = [
      {
        id: "otherData",
        reference: "HPC",
      },
    ];
    const req = {
      params: { id: "HPC" },
      session: { searchResult },
    };
    const result = findRecordInSearchResults(req);
    expect(result.id).toBe("otherData");
    expect(result.reference).toBe("HPC");
  });
});
