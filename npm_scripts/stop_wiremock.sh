#!/bin/bash
hrt_search_service_port=${HRT_SEARCH_SERVICE_PORT:-8400}
hrt_citizen_service_port=${HRT_CITIZEN_SERVICE_PORT:-8100}
hrt_certificate_service_port=${HRT_CERTIFICATE_SERVICE_PORT:-8090}
hrt_payment_service_port=${HRT_PAYMENT_SERVICE_PORT:-8110}
hrt_note_service_port=${HRT_NOTE_SERVICE_PORT:-8140}
hrt_reissue_service_port=${HRT_REISSUE_SERVICE_PORT:-8401}
hrt_issue_certificate_service_port=${HRT_ISSUE_CERTIFICATE_SERVICE_PORT:-8500}
hrt_product_service_port=${HRT_PRODUCT_SERVICE_PORT:-8120}
hrt_preference_service_port=${HRT_PREFERENCE_SERVICE_PORT:-8150}
hrt_composition_service_port=${HRT_COMPOSITION_SERVICE_PORT:-8180}
cps_service_port=${CARD_PAYMENT_SERVICE_PORT:-8302}

curl -d "{}" http://localhost:${hrt_search_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_citizen_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_product_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_certificate_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_payment_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_note_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_reissue_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_issue_certificate_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_preference_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${hrt_composition_service_port}/__admin/shutdown
curl -d "{}" http://localhost:${cps_service_port}/__admin/shutdown
