import expect from "expect";
import {
  CHECK_ANSWERS_URL,
  CARD_PAYMENT_URL,
  DECISION_URL,
} from "./paths/paths";

const mockDoubleCsrf = jest.fn();
jest.mock("csrf-csrf", () => ({
  doubleCsrf: mockDoubleCsrf,
}));
import {
  getPathsInSequence,
  registerJourney,
  doubleCsrfOptions,
} from "./register-journeys";

const steps = [
  {
    path: "/first",
  },
  {
    path: "/second",
    toggle: "STEP_TWO_ENABLED",
  },
  {
    path: "/third",
  },
];

test("getPathsInSequence() returns the correct sequence of paths when no prefix is defined", () => {
  const expected = [
    "/first",
    "/second",
    "/third",
    CHECK_ANSWERS_URL,
    CARD_PAYMENT_URL,
    DECISION_URL,
  ];
  const result = getPathsInSequence(undefined, steps);

  expect(result).toEqual(expected);
});

test("getPathsInSequence() returns the correct sequence of paths when a prefix is defined", () => {
  const prefix = "/my-journey";
  const expected = [
    "/my-journey/first",
    "/my-journey/second",
    "/my-journey/third",
    `/my-journey${CHECK_ANSWERS_URL}`,
    `/my-journey${CARD_PAYMENT_URL}`,
    `/my-journey${DECISION_URL}`,
  ];
  const result = getPathsInSequence(prefix, steps);

  expect(result).toEqual(expected);
});

test("registerJourney() registers journeys with correct properties when no path prefix is defined", () => {
  const features = {
    STEP_TWO_ENABLED: false,
  };

  const journey = {
    steps,
  };

  const result = registerJourney(features)(journey);

  const expected = {
    steps: [{ path: "/first" }, { path: "/third" }],
    pathsInSequence: [
      "/first",
      "/third",
      CHECK_ANSWERS_URL,
      CARD_PAYMENT_URL,
      DECISION_URL,
    ],
  };

  expect(result).toEqual(expected);
});

test("registerJourney() registers journeys with correct properties when path prefix is defined", () => {
  const features = {
    STEP_TWO_ENABLED: false,
  };

  const pathPrefix = "/my-journey";

  const journey = {
    steps,
    pathPrefix,
  };

  const result = registerJourney(features)(journey);

  const expected = {
    steps: [{ path: "/my-journey/first" }, { path: "/my-journey/third" }],
    pathPrefix: "/my-journey",
    pathsInSequence: [
      "/my-journey/first",
      "/my-journey/third",
      `/my-journey${CHECK_ANSWERS_URL}`,
      `/my-journey${CARD_PAYMENT_URL}`,
      `/my-journey${DECISION_URL}`,
    ],
  };

  expect(result).toEqual(expected);
});

describe("doubleCsrfOptions()", () => {
  test("sets options when secure cookies set", () => {
    const environment = {
      USE_UNSECURE_COOKIE: false,
    };
    const config = { environment };

    doubleCsrfOptions(config);

    expect(mockDoubleCsrf).toBeCalledWith({
      cookieName: "__Host-nhsbsa.x-csrf-token",
      cookieOptions: { secure: true },
      getSecret: expect.any(Function),
      getTokenFromRequest: expect.any(Function),
    });
  });

  test("sets options when secure cookies not set", () => {
    const environment = {
      USE_UNSECURE_COOKIE: true,
    };
    const config = { environment };

    doubleCsrfOptions(config);

    expect(mockDoubleCsrf).toBeCalledWith({
      cookieName: "nhsbsa.x-csrf-token",
      cookieOptions: { secure: false },
      getSecret: expect.any(Function),
      getTokenFromRequest: expect.any(Function),
    });
  });
});
