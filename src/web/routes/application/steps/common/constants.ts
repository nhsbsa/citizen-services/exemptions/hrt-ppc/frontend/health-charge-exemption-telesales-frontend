export const YES: string = "yes";
export const NO: string = "no";
export const NONE: string = "NONE";
export const TEXT: string = "text";
export const CONFIRMATION_CODE_SESSION_PROPERTY: string = "confirmationCode";
export const CONFIRMATION_CODE_ENTERED_SESSION_PROPERTY: string =
  "confirmationCodeEntered";
export const CONTACT_TELEPHONE: string | undefined =
  process.env.CONTACT_TELEPHONE;
export const CONTACT_EMAIL: string | undefined = process.env.CONTACT_EMAIL;
export const ON: string = "ON";
export const OFF: string = "OFF";
export const SIGN_IN_URL: string = "/auth/signin";
export const SIGN_OUT_URL: string = "/auth/signout";
export const LOCAL_USER_NAME: string = "LOCAL_USER";
export const LOCAL_SIGN_OUT_URL: string = "";
export const LOCAL_USER_CYPHER: string = "LOCAL_USER_CYPHER";
export const AUDIT_CITIZEN_ENTITY: string = "uk.nhs.nhsbsa.exemptions.citizen";
export const AUDIT_CERTIFICATE_ENTITY: string =
  "uk.nhs.nhsbsa.exemptions.certificate";
export const AUDIT_PAYMENT_ENTITY: string = "uk.nhs.nhsbsa.exemptions.payment";
export const AUDIT_USER_PREFERENCE_ENTITY: string =
  "uk.nhs.nhsbsa.exemptions.user.preference";
export const AUDIT_INITITAL_VALUE_CHANGE_TYPE: string = "InitialValueChange";
