#!/bin/bash

bin_dir=${BIN_DIR:-./bin}
hrt_search_service_port=${HRT_SEARCH_SERVICE_PORT:-8400}
hrt_citizen_service_port=${HRT_CITIZEN_SERVICE_PORT:-8100}
hrt_certificate_service_port=${HRT_CERTIFICATE_SERVICE_PORT:-8090}
hrt_payment_service_port=${HRT_PAYMENT_SERVICE_PORT:-8110}
hrt_note_service_port=${HRT_NOTE_SERVICE_PORT:-8140}
hrt_reissue_service_port=${HRT_REISSUE_SERVICE_PORT:-8401}
hrt_product_service_port=${HRT_PRODUCT_SERVICE_PORT:-8120}
hrt_issue_certificate_service_port=${HRT_ISSUE_CERTIFICATE_SERVICE_PORT:-8500}
hrt_preference_service_port=${HRT_PREFERENCE_SERVICE_PORT:-8150}
hrt_composition_service_port=${HRT_COMPOSITION_SERVICE_PORT:-8180}
cps_service_port=${CARD_PAYMENT_SERVICE_PORT:-8302}

SCRIPT_DIR=$(dirname "$0")
source ${SCRIPT_DIR}/wait_for_url.sh

mkdir -p ${bin_dir}
if [ ! -f ${bin_dir}/wiremock.jar ]; then
  wget https://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-standalone/2.27.0/wiremock-standalone-2.27.0.jar -O ${bin_dir}/wiremock.jar
fi

echo "Starting Wiremock on port ${hrt_search_service_port} for hrt search service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_search_service_port} &

echo "Starting Wiremock on port ${hrt_citizen_service_port} for hrt citizen service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_citizen_service_port} &

echo "Starting Wiremock on port ${hrt_certificate_service_port} for hrt certificate service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_certificate_service_port} &

echo "Starting Wiremock on port ${hrt_product_service_port} for hrt product service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_product_service_port} --root-dir npm_scripts/mappings_product-api &

echo "Starting Wiremock on port ${hrt_payment_service_port} for hrt payment service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_payment_service_port} &

echo "Starting Wiremock on port ${hrt_note_service_port} for hrt note service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_note_service_port} &

echo "Starting Wiremock on port ${hrt_reissue_service_port} for hrt reissue service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_reissue_service_port} &

echo "Starting Wiremock on port ${hrt_issue_certificate_service_port} for hrt issue certificate service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_issue_certificate_service_port} &

echo "Starting Wiremock on port ${hrt_preference_service_port} for hrt preference service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_preference_service_port} &

echo "Starting Wiremock on port ${hrt_composition_service_port} for hrt composition service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --port ${hrt_composition_service_port} &

echo "Starting Wiremock on port ${cps_service_port} for card payment service"
java -Xmx64m -jar ${bin_dir}/wiremock.jar --local-response-templating --port ${cps_service_port} &

echo "Waiting for wiremock to start"

wait_for_url http://localhost:${hrt_search_service_port}/__admin

wait_for_url http://localhost:${hrt_citizen_service_port}/__admin

wait_for_url http://localhost:${hrt_certificate_service_port}/__admin

wait_for_url http://localhost:${hrt_product_service_port}/__admin

wait_for_url http://localhost:${hrt_payment_service_port}/__admin

wait_for_url http://localhost:${hrt_note_service_port}/__admin

wait_for_url http://localhost:${hrt_reissue_service_port}/__admin

wait_for_url http://localhost:${hrt_issue_certificate_service_port}/__admin

wait_for_url http://localhost:${hrt_preference_service_port}/__admin

wait_for_url http://localhost:${hrt_composition_service_port}/__admin

wait_for_url http://localhost:${cps_service_port}/__admin

echo "Wiremock started"
