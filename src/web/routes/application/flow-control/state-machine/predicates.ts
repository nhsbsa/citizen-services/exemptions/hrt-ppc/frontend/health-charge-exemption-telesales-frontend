import { equals, isNil } from "ramda";

/**
 * Next path is navigable if the next step does not exist, if it doesn't define an isNavigable function, or that function returns true.
 * For dynamics urls i.e. /HRTEF6E750B/exemption-information this will also allow the path since findIndex returns -1
 * E.g. if the current step is the end of the 'in-progress' journey, the next path will be /check-their-answers. There is no step matching /check-their-answers, so we assume it is navigable.
 */
const isNextPathNavigable = (nextStep, req) =>
  isNil(nextStep) || isNil(nextStep.isNavigable) || nextStep.isNavigable(req);

const isPathAllowed = (sequence, allowed, path) => {
  return (
    sequence.findIndex(equals(path)) <= sequence.findIndex(equals(allowed))
  );
};

export { isNextPathNavigable, isPathAllowed };
