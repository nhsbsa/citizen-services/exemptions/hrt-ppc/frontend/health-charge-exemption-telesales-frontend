import expect from "expect";
import { behaviourForPost } from "./behaviour-for-post";
import { noteClient } from "../../../../client/note-client";
import { wrapError } from "../../errors";
import * as httpStatus from "http-status-codes";
import { EXEMPTION_VIEW_URL } from "../../paths/paths";

import { getNotesRows } from "./get-notes-rows";
jest.mock("./get-notes-rows", () => ({
  getNotesRows: jest.fn(),
}));

const isEmpty = jest.fn();
const config = {
  environment: {
    OUTBOUND_API_TIMEOUT: 20000,
    NOTE_API_URI: "baseURI",
    NOTE_API_KEY: "test-key",
  },
};
const req = {
  path: "/test",
  session: {
    certificate: {
      id: "ac100ad8-865b-1267-8186-8356002902db",
      citizenId: "ac100b40-865b-1ad3-8186-8355ffec0a37",
    },
  },
  body: {
    description: "this is note",
  },
  params: {
    id: "HRT123",
  },
};
const res = {
  redirect: jest.fn(),
};
const next = jest.fn();

jest.mock("../../../../client/note-client");
jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

beforeEach(() => {
  jest.clearAllMocks();
  isEmpty.mockReset();
});

describe("behaviourForPost()", () => {
  test("should call note api and redirect to exemption-information page", async () => {
    isEmpty.mockReturnValue(true);
    const makeRequestMock = jest.fn();
    noteClient.prototype.makeRequest = makeRequestMock;
    makeRequestMock.mockResolvedValue({ data: "mockData" });

    await behaviourForPost(config)(req, res, next);

    expect(noteClient.prototype.makeRequest).toBeCalledWith(
      {
        method: "POST",
        url: "/v1/notes?citizenId=ac100b40-865b-1ad3-8186-8355ffec0a37&certificateId=ac100ad8-865b-1267-8186-8356002902db",
        data: {
          description: req.body.description,
        },
        responseType: "json",
      },
      req.session,
    );
    expect(res.redirect).toHaveBeenCalled();
    expect(res.redirect).toBeCalledWith(
      `/test-context${EXEMPTION_VIEW_URL}/HRT123`,
    );
    expect(next).not.toBeCalled();
    expect(getNotesRows).toBeCalledTimes(1);
    expect(getNotesRows).toHaveBeenCalledWith(req, res);
  });

  test("should not call note api when there are validation errors ", async () => {
    isEmpty.mockReturnValue(false);
    const makeRequestMock = jest.fn();
    noteClient.prototype.makeRequest = makeRequestMock;
    makeRequestMock.mockResolvedValue({ data: "mockData" });

    await behaviourForPost(config)(req, res, next);

    expect(noteClient).not.toBeCalled();
    expect(res.redirect).not.toHaveBeenCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
    expect(getNotesRows).toBeCalledTimes(1);
    expect(getNotesRows).toHaveBeenCalledWith(req, res);
  });

  test("calls next with wrapped error when error is thrown", async () => {
    const expectedError = wrapError({
      cause: new Error("Axios error"),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
    });
    isEmpty.mockReturnValue(true);
    const makeRequestMock = jest.fn();
    noteClient.prototype.makeRequest = makeRequestMock;
    makeRequestMock.mockReturnValue(Promise.reject(new Error("Axios error")));

    await behaviourForPost(config)(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(getNotesRows).toBeCalledTimes(1);
    expect(getNotesRows).toHaveBeenCalledWith(req, res);
  });
});
