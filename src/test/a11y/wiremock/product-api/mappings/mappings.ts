const SUCCESS = "success";
import v1RetrieveEndDate from "./v1-retrieve-end-date";

const RESPONSE_MAP = {
  [SUCCESS]: v1RetrieveEndDate,
};

const createGetRetrieveEndDateMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "GET",
      urlPattern:
        "/v1/products/HRT_12m/end-date\\?startDate=\\d{4}-\\d{2}-\\d{2}",
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
      },
    },
    response: RESPONSE_MAP[scenario],
  });

export { createGetRetrieveEndDateMapping, SUCCESS };
