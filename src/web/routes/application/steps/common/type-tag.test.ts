import { getTypeTag } from "./type-tag";

describe("getTypeTag()", () => {
  test("should wrap input in expected html", () => {
    const tag = getTypeTag("lorem ipsum dolor sit amet");
    expect(tag).toEqual(
      '<strong class="nhsuk-tag nhsuk-tag--green">lorem ipsum dolor sit amet</strong>',
    );
  });
});
