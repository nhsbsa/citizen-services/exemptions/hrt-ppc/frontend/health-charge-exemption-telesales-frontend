const SUCCESS = "SUCCESS";
import successWithPin from "../success-with-pin";

const RESPONSE_MAP = {
  [SUCCESS]: successWithPin,
};

const createSuccessWithPinMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "POST",
      url: `/card-payments-services/telephony-transactions/hrttelephony/payments`,
    },
    response: RESPONSE_MAP[scenario],
  });

export { createSuccessWithPinMapping, SUCCESS };
