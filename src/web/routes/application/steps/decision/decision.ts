import { configureSessionDetails } from "../../flow-control/middleware/session-details";
import { handleRequestForPath } from "../../flow-control/middleware/handle-path-request";
import { DECISION_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";
import { isUndefined } from "../../../../../common/predicates";
import { getDecisionStatus } from "./get-decision-status";
import { DecisionStatus } from "./decision-statuses";
import { CONTACT_TELEPHONE, CONTACT_EMAIL } from "../common/constants";
import { wrapError } from "../../errors";
import { logger } from "../../../../../web/logger/logger";

const STATUS_TEMPLATE_MAP = {
  [DecisionStatus.FAIL]: "failure",
  [DecisionStatus.SUCCESS]: "success",
};

const getDecisionPage = (req, res, next) => {
  const { verificationResult, eligibilityStatus, claimStatus, reference } =
    req.session;
  const decisionStatus = getDecisionStatus({
    verificationResult,
    eligibilityStatus,
    claimStatus,
  });
  const template = STATUS_TEMPLATE_MAP[decisionStatus];

  if (isUndefined(decisionStatus)) {
    throw new Error(
      `No decision status generated from EligibilityStatus: ${eligibilityStatus} and VerificationResult: ${JSON.stringify(
        verificationResult,
      )}`,
    );
  }

  if (decisionStatus === DecisionStatus.FAIL) {
    logger.info("Decision ineligible claim for " + reference, req);
    return res.render("decision", {
      title: req.t("decision.failureRejected.title"),
      heading: req.t("decision.failureRejected.heading"),
      referenceNumber: reference,
      panelContent: {
        html: req.t("decision.failureRejected.panel.html"),
        concludingHtml: req.t("decision.failureRejected.panel.concludingHtml", {
          referenceNumber: reference,
        }),
      },
      body: {
        header: {
          lineOne: req.t("decision.failureRejected.body.header.lineOne"),
          lineTwo: req.t("decision.failureRejected.body.header.lineTwo"),
        },
        paragraph: {
          lineOne: req.t("decision.failureRejected.body.paragraph.lineOne"),
          lineTwo: req.t("decision.failureRejected.body.paragraph.lineTwo"),
          lineThree: req.t("decision.failureRejected.body.paragraph.lineThree"),
          lineFour: req.t("decision.failureRejected.body.paragraph.lineFour"),
          lineFive: req.t("decision.failureRejected.body.paragraph.lineFive", {
            helpTelephoneNumber: CONTACT_TELEPHONE,
            helpEmailAddress: CONTACT_EMAIL,
          }),
          lineSix: req.t("decision.failureRejected.body.paragraph.lineSix"),
        },
      },
      template,
    });
  }

  if (decisionStatus === DecisionStatus.SUCCESS) {
    logger.info("Decision success claim for " + reference, req);

    return res.render("decision", {
      title: req.t("decision.success.title"),
      heading: req.t("decision.success.heading"),
      referenceNumber: reference,
      panelContent: {
        html: req.t("decision.success.panel.html"),
        concludingHtml: req.t("decision.success.panel.concludingHtml", {
          referenceNumber: reference,
        }),
      },
      body: {
        header: {
          lineOne: req.t("decision.success.body.header.lineOne"),
          lineTwo: req.t("decision.success.body.header.lineTwo"),
          lineThree: req.t("decision.success.body.header.lineThree"),
        },
        paragraph: {
          lineOne: req.t("decision.success.body.paragraph.lineOne"),
          lineTwo: req.t("decision.success.body.paragraph.lineTwo"),
          lineThree: req.t("decision.success.body.paragraph.lineThree"),
          lineFour: req.t("decision.success.body.paragraph.lineFour"),
          lineFive: req.t("decision.success.body.paragraph.lineFive", {
            helpTelephoneNumber: CONTACT_TELEPHONE,
            helpEmailAddress: CONTACT_EMAIL,
          }),
          lineSix: req.t("decision.success.body.paragraph.lineSix"),
        },
      },
      template,
    });
  }

  return next(
    wrapError({
      cause: new Error(
        `Since the decision status is ${decisionStatus}, routed to generic error`,
      ),
      message: `Since the decision status is ${decisionStatus}, routed to generic error`,
      statusCode: res.statusCode,
    }),
  );
};

const registerDecisionRoute = (journey, app) =>
  app.get(
    prefixPath(journey.pathPrefix, DECISION_URL),
    configureSessionDetails(journey),
    handleRequestForPath(journey, undefined),
    getDecisionPage,
  );

export { registerDecisionRoute, getDecisionPage };
