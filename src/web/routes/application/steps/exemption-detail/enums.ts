export enum Section {
  CITIZEN_SECTION = "citizen",
  CERTIFICATE_SECTION = "certificate",
  PAYMENT_SECTION = "payment",
}
