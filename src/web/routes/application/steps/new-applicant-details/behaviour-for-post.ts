import { wrapError } from "../../errors";
import { StatusCodes } from "http-status-codes";
import { productClient } from "../../../../client/product-client";
import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";

const behaviourForPost =
  (config) => async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (!validationResult(req).isEmpty()) {
        return next();
      }

      req.session["newApplicant"] = {
        ...req.session["newApplicant"],
        ...req.body,
      };

      const endDateResponse = await new productClient(config).makeRequest({
        method: "GET",
        url: `v1/products/HRT_12m/end-date?startDate=${req.session["newApplicant"].certificateStartDate}`,
      });
      req.session["newApplicant"].certificateEndDate =
        endDateResponse.data.endDate;

      return next();
    } catch (error) {
      next(
        wrapError({
          cause: error,
          message: `Error posting ${req.path}`,
          statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        }),
      );
    }
  };

export { behaviourForPost };
