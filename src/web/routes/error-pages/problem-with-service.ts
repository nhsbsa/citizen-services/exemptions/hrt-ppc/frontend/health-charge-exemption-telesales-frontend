import { renderErrorPageContent } from "./error-page-content";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";

const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const registerProblemWithServiceError = (app) => {
  app.get(
    [`${CONTEXT_PATH}/problem-with-service`, `${CONTEXT_PATH}/timed-out`],
    configureAuthentication,
    renderErrorPageContent("problem-with-service"),
  );
};

export { registerProblemWithServiceError };
