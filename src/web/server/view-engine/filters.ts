import { compose, map, values, isNil } from "ramda";
import { ErrorObject } from "../../routes/application/steps/common/types";

const camelToKebabCase = (string) =>
  string.replace(/([a-z0-9])([A-Z0-9])/g, "$1-$2").toLowerCase();

const customHashToError = (param) => {
  const customHashError = {
    dateOfBirth: "date-of-birth-day",
    inEnglandWalesOrNI: "in-england-wales-or-ni-1",
    certificateFulfillment: "certificate-fulfillment-1",
    certificateStartDate: "certificate-start-date-day",
  };
  for (const key in customHashError) {
    if (key === param) {
      return customHashError[key];
    }
  }
  return param;
};

const toError = (error): ErrorObject => ({
  text: error.msg,
  href: `#${camelToKebabCase(customHashToError(error.path))}`,
  attributes: {
    id: `error-link-${camelToKebabCase(error.path)}`,
  },
});

const toErrorList = compose(values, map(toError));

const getErrorByParam = (errors, field) =>
  isNil(errors) ? null : errors.find((error) => error.path === field);

const getErrorForField = (errors, field) => {
  const error = getErrorByParam(errors, field);

  return error
    ? {
        text: error.msg,
      }
    : null;
};

export { toErrorList, getErrorForField, camelToKebabCase };
