import expect from "expect";
import { toBoolean } from "./to-boolean";

test("toBoolean()", () => {
  expect(toBoolean(undefined)).toBe(false);
  expect(toBoolean(null)).toBe(false);
  expect(toBoolean(33)).toBe(false);
  expect(toBoolean("random string")).toBe(false);
  expect(toBoolean("false")).toBe(false);
  expect(toBoolean("FALSE")).toBe(false);
  expect(toBoolean("true")).toBe(true);
  expect(toBoolean("TRUE")).toBe(true);
  expect(toBoolean(false)).toBe(false);
  expect(toBoolean(true)).toBe(true);
});
