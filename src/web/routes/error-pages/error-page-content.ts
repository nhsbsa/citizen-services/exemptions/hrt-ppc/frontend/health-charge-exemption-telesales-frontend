export const renderErrorPageContent = (url) => (req, res) => {
  res.render("error-pages/" + url, getErrorPageContent(req));
};

export const getErrorPageContent = (req) => {
  return {
    title: req.t("errors:problemWithTheService.title"),
    heading: req.t("errors:problemWithTheService.heading"),
    paragraphOne: req.t("errors:problemWithTheService.paragraphOne"),
    paragraphTwo: req.t("errors:problemWithTheService.paragraphTwo"),
  };
};
