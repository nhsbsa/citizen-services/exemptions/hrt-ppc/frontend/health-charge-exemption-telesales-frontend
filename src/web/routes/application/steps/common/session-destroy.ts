import { logger } from "../../../../logger/logger";

const sessionDestroy = (req, res) => {
  logger.info("Session has been destroyed", req);
  req.session.destroy();
  res.clearCookie("lang");
};

const deleteSessionExcept = (req, parametersToKeep) => {
  Object.keys(req.session).forEach(function (key) {
    if (!parametersToKeep.includes(key)) {
      delete req.session[key];
    }
  });
};

const sessionDestroyButKeepLoggedIn = (req) => {
  deleteSessionExcept(req, [
    "cookie",
    "csrfToken",
    "pkceCodes",
    "authCodeUrlRequest",
    "authCodeRequest",
    "accessToken",
    "idToken",
    "account",
    "isAuthenticated",
    "userCypher",
  ]);
};

export { sessionDestroy, deleteSessionExcept, sessionDestroyButKeepLoggedIn };
