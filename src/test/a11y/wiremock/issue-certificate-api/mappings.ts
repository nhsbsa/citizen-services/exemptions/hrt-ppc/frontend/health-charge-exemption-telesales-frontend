import {
  CertificateStatus,
  CertificateType,
  Channel,
} from "@nhsbsa/health-charge-exemption-common-frontend";

const createIssueCertificateResultsMapping = () => {
  return JSON.stringify({
    request: {
      method: "POST",
      url: `/v1/issue-certificate`,
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
        channel: {
          equalTo: Channel.STAFF,
        },
        "user-id": {
          equalTo: "LOCAL_USER_CYPHER",
        },
        "correlation-id": {
          matches: "([A-Za-z0-9_-])+",
        },
      },
    },
    response: {
      status: 200,
      headers: {
        "Content-Type": "application/json",
      },
      jsonBody: {
        citizen: {
          id: "c0a81da7-85b8-1f1a-8185-b936fc890000",
          firstName: "John",
          lastName: "Smith",
          dateOfBirth: "1990-11-18",
          nhsNumber: "4857773457",
          _meta: {
            channel: Channel.STAFF,
            createdTimestamp: "2023-01-16T11:44:26.6928273",
            createdBy: "ONLINE",
            updatedTimestamp: "2023-01-16T11:44:26.6928273",
            updatedBy: "ONLINE",
          },
          _links: {
            self: {
              href: "http://localhost:8500/v1/citizens/c0a81da7-85b8-1f1a-8185-b936fc890000",
            },
            citizen: {
              href: "http://localhost:8500/v1/citizens/c0a81da7-85b8-1f1a-8185-b936fc890000",
            },
            addresses: {
              href: "http://localhost:8500/v1/citizens/c0a81da7-85b8-1f1a-8185-b936fc890000/addresses",
            },
            emails: {
              href: "http://localhost:8500/v1/citizens/c0a81da7-85b8-1f1a-8185-b936fc890000/emails",
            },
          },
        },
        certificate: {
          id: "c0a81da7-85b8-1fa3-8185-b936fef50000",
          citizenId: "c0a81da7-85b8-1f1a-8185-b936fc890000",
          reference: "HRT27CD561F",
          type: CertificateType.HRT_PPC,
          applicationDate: "2023-01-16",
          duration: "P12M",
          startDate: "2023-01-17",
          endDate: "2024-01-17",
          cost: 1870,
          status: CertificateStatus.PENDING,
          _meta: {
            channel: Channel.STAFF,
            createdTimestamp: "2023-01-16T11:44:27.3157391",
            createdBy: "ONLINE",
            updatedTimestamp: "2023-01-16T11:44:27.3157391",
            updatedBy: "ONLINE",
          },
          _links: {
            self: {
              href: "http://localhost:8500/v1/certificates/c0a81da7-85b8-1fa3-8185-b936fef50000",
            },
            certificate: {
              href: "http://localhost:8500/v1/certificates/c0a81da7-85b8-1fa3-8185-b936fef50000",
            },
          },
        },
      },
    },
  });
};

export { createIssueCertificateResultsMapping };
