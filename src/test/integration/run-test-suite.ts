/* no-process-exit */
"use strict";

import { runAllTests } from "../a11y/test-suite";
import { TEST_SUITE } from "../a11y/tests";

/*
  Runs the accessibility test suite, relying on those tests to use process.env.APP_BASE_URL.
 */
const runTestSuite = async (testSuite) => {
  try {
    await runAllTests(testSuite);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runTestSuite(TEST_SUITE);
