import { environment } from "./environment";
import { server } from "./server";
import { redis } from "./redis";
import { features } from "./features";

export const config = {
  environment,
  server,
  redis,
  features,
};
