const POSTCODE: string = "BS14TB";
const PHONE_NUMBER: string = "07123456789";
const EMAIL_ADDRESS: string = "test@email.com";
const TEXT: string = "text";

export { POSTCODE, PHONE_NUMBER, EMAIL_ADDRESS, TEXT };
