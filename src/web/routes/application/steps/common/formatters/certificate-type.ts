const formatCertificateType = (type) => {
  if (type === undefined) {
    throw new Error("A certificate type must be provided");
  }
  return type.replace("_", " ");
};

export { formatCertificateType };
