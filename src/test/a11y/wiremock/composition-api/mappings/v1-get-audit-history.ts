export default {
  status: 200,
  jsonBody: {
    content: [
      {
        changes: [
          {
            changeType: "ValueChange",
            globalId: {
              entity: "uk.nhs.nhsbsa.exemptions.citizen.entity.Citizen",
              cdoId: "c0a8000a-86da-1e6a-8186-ee9a40dd000f",
            },
            commitMetadata: {
              author: "POSTMAN",
              properties: [
                {
                  key: "citizenId",
                  value: "c0a8000a-86da-1e6a-8186-ee9a40dd000f",
                },
              ],
              commitDate: "2023-10-02T10:43:58.884",
              commitDateInstant: "2023-10-02T09:43:58.884411Z",
              id: 9998,
            },
            property: "firstName",
            propertyChangeType: "PROPERTY_VALUE_CHANGED",
            left: "John",
            right: "New John",
          },
        ],
        commitMetadata: {
          author: "POSTMAN",
          properties: [
            {
              key: "citizenId",
              value: "c0a8000a-86da-1e6a-8186-ee9a40dd000f",
            },
          ],
          commitDate: "2023-10-02T10:43:58.884",
          commitDateInstant: "2023-10-02T09:43:58.884411Z",
          id: 9998,
        },
      },
    ],
  },
  headers: {
    "Content-Type": "application/json",
  },
};
