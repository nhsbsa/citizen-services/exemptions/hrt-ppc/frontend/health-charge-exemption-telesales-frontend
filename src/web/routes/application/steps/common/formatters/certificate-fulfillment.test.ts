import { formatCertificateFulfillment } from "./certificate-fulfillment";
import { NONE } from "../../common/constants";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";

const translate = {
  preferencesText: {
    email: "Email",
    post: "Post",
    none: "None",
  },
};

describe("formatCertificateFulfillment", () => {
  test(`should return "Email" for certificate fulfillment ${Preference.EMAIL}`, () => {
    expect(formatCertificateFulfillment(Preference.EMAIL, translate)).toBe(
      translate.preferencesText.email,
    );
  });

  test(`should return "Post" for certificate fulfillment ${Preference.POSTAL}`, () => {
    expect(formatCertificateFulfillment(Preference.POSTAL, translate)).toBe(
      translate.preferencesText.post,
    );
  });

  test(`should return "None" for certificate fulfillment ${NONE}`, () => {
    expect(formatCertificateFulfillment(NONE, translate)).toBe(
      translate.preferencesText.none,
    );
  });

  test("should throw an error for invalid certificate fulfillment", () => {
    expect(() => {
      formatCertificateFulfillment("INVALID", translate);
    }).toThrowError(
      "A certificate fulfillment INVALID is not in correct type format",
    );
  });
});
