import expect from "expect";
import {
  newApplicantDetailsPage,
  pageContent,
  behaviourForGet,
  customLocalsAndSessionFormProperty,
} from "./new-applicant-details";
import { sanitize } from "./sanitize";
import { validate } from "./validate";
import { behaviourForPost } from "./behaviour-for-post";

jest.mock("../../../../../config", () => ({
  config: {
    features: {
      FULFILLMENT_POST_ENABLED: true,
      TELEPHONE_ENABLED: false,
    },
    environment: {
      HRT_PPC_VALUE: 1930,
    },
  },
}));

test(`newApplicantDetailsPage should match expected outcomes`, () => {
  const expectedResults = {
    path: "/new-applicant-details",
    template: "new-applicant-details",
    validate,
    embeddedMainJourney: true,
    pageContent,
    sanitize,
    behaviourForGet,
    customLocalsAndSessionFormProperty,
    behaviourForPost,
  };

  expect(newApplicantDetailsPage).toStrictEqual(expectedResults);
});

test(`pageContent() should return expected results`, () => {
  const translate = (string, object?) => `${string}${object}`;
  const expected = {
    title: translate("newApplicantDetails.title"),
    heading: translate("newApplicantDetails.heading"),
    addressLegend: translate("newApplicantDetails.addressLegend"),
    addressLineOneLabel: translate("newApplicantDetails.addressLineOneLabel"),
    addressLineTwoLabel: translate("newApplicantDetails.addressLineTwoLabel"),
    townOrCityLabel: translate("newApplicantDetails.townOrCityLabel"),
    postcodeLabel: translate("newApplicantDetails.postcodeLabel"),
    addressDetailsTitle: translate("newApplicantDetails.addressDetailsTitle"),
    addressDetailsHtml: translate("newApplicantDetails.addressDetailsHtml"),
    nameLegend: translate("newApplicantDetails.nameLegend"),
    firstNameLabel: translate("newApplicantDetails.firstNameLabel"),
    lastNameLabel: translate("newApplicantDetails.lastNameLabel"),
    contactLegend: translate("newApplicantDetails.contactLegend"),
    certificateStartDateLegend: translate(
      "newApplicantDetails.certificateStartDateLegend",
    ),
    certificateStartDateLabel: translate(
      "newApplicantDetails.certificateStartDateLabel",
    ),
    certificateStartDateHint: translate(
      "newApplicantDetails.certificateStartDateHint",
    ),
    telephoneNumberLabel: translate("newApplicantDetails.telephoneNumberLabel"),
    noteDescriptionLabel: translate("newApplicantDetails.noteDescriptionLabel"),
    dateOfBirthLegend: translate("newApplicantDetails.dateOfBirthLegend"),
    dateOfBirthLabel: translate("newApplicantDetails.dateOfBirthLabel"),
    emailLabel: translate("newApplicantDetails.emailLabel"),
    nhsNumberLabel: translate("newApplicantDetails.nhsNumberLabel"),
    nhsNumberLegend: translate("newApplicantDetails.nhsNumberLegend"),
    paymentLegend: translate("newApplicantDetails.paymentLegend"),
    paymentHeading: translate("newApplicantDetails.paymentHeading"),
    paymentHtml: translate("newApplicantDetails.paymentHtml", {
      ppcValue: "£19.30",
    }),
    certificateFulfillmentLegend: translate(
      "newApplicantDetails.certificateFulfillmentLegend",
    ),
    certificateFulfillmentLabel: translate(
      "newApplicantDetails.certificateFulfillmentLabel",
    ),
    certificateFulfillmentEmailText: translate(
      "newApplicantDetails.certificateFulfillmentEmailText",
    ),
    certificateFulfillmentPostText: translate(
      "newApplicantDetails.certificateFulfillmentPostText",
    ),
    submitButtonText: translate("buttons:submit"),
    fulfillmentPostEnabed: true,
    telephoneEnabled: false,
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("behaviourForGet()", () => {
  let res;
  let req;
  const next = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    req = {
      session: {},
      t: jest.fn(),
    };
    res = { locals: {} };
  });

  test("should set res.locals.newApplicant correctly if res.locals.newApplicant is not defined and req.session.newApplicant is defined", () => {
    const newApplicant = {
      description: "newApplicantDetails.noteDescriptionContent",
    };
    req.session.newApplicant = newApplicant;

    behaviourForGet()(req, res, next);

    expect(res.locals.newApplicant).toEqual(newApplicant);
    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });

  test("should set res.locals.newApplicant correctly if res.locals.newApplicant is not defined and req.session.newApplicant is not defined", () => {
    const claim = {
      lastName: "John",
      dateOfBirth: "1997-3-24",
      "dateOfBirth-day": "24",
      "dateOfBirth-month": "03",
      "dateOfBirth-year": 1997,
      postcode: "NE15 8NY",
    };
    req.session.claim = claim;

    behaviourForGet()(req, res, next);

    expect(res.locals.newApplicant.description).toBe(
      req.t("newApplicantDetails.noteDescriptionContent"),
    );
    expect(res.locals.newApplicant.lastName).toBe(claim.lastName);
    expect(res.locals.newApplicant.dateOfBirth).toBe(claim.dateOfBirth);
    expect(res.locals.newApplicant["dateOfBirth-day"]).toBe(
      claim["dateOfBirth-day"],
    );
    expect(res.locals.newApplicant["dateOfBirth-month"]).toBe(
      claim["dateOfBirth-month"],
    );
    expect(res.locals.newApplicant["dateOfBirth-year"]).toBe(
      claim["dateOfBirth-year"],
    );
    expect(res.locals.newApplicant.postcode).toBe(claim.postcode);

    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });

  test("should set res.locals.newApplicant correctly if res.locals.newApplicant is already defined", () => {
    const existingNewApplicant = { description: "Existing description" };
    res.locals.newApplicant = existingNewApplicant;

    behaviourForGet()(req, res, next);

    expect(res.locals.newApplicant).toBe(existingNewApplicant);
    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });
});
