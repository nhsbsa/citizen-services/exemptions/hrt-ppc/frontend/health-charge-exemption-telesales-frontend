import { URLS } from "../paths";

const telesalesReissueJourney = [
  {
    url: URLS.SEARCH_HRT_RECORDS,
    formData: () => {
      return {
        "dateOfBirth-day": "30",
        "dateOfBirth-month": "05",
        "dateOfBirth-year": "1920",
        dateOfBirth: "1920-05-30",
        lastName: "doe",
        postcode: "AA1 1AA",
      };
    },
  },
  {
    url: URLS.SEARCH_RESULTS,
    formData: undefined,
  },
  {
    url: URLS.EXEMPTION_DETAIL,
    formData: undefined,
  },
  {
    url: URLS.REISSUE,
    formData: undefined,
  },
  {
    url: URLS.SEARCH_HRT_RECORDS,
    formData: undefined,
  },
];

const telesalesReissueErrorJourney = [
  {
    url: URLS.SEARCH_HRT_RECORDS,
    formData: () => {
      return {
        "dateOfBirth-day": "30",
        "dateOfBirth-month": "05",
        "dateOfBirth-year": "1920",
        dateOfBirth: "1920-05-30",
        lastName: "doe",
        postcode: "AA1 1AA",
      };
    },
  },
  {
    url: URLS.SEARCH_RESULTS,
    formData: undefined,
  },
  {
    url: URLS.EXEMPTION_DETAIL,
    formData: undefined,
  },
  {
    url: URLS.REISSUE,
    formData: undefined,
  },
  {
    url: URLS.PROBLEM_WITH_SERVICE,
    formData: undefined,
  },
];

export { telesalesReissueJourney, telesalesReissueErrorJourney };
