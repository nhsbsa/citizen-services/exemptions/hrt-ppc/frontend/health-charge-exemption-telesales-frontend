import expect from "expect";
import * as AxiosLogger from "axios-logger";
import { addRequestAndLoggingInterceptors } from "./interceptors";

const appConfig = {
  environment: {
    LOG_REQUESTS_AND_RESPONSES: false,
  },
};

const instance = {
  interceptors: {
    request: {
      use: jest.fn(),
    },
    response: {
      use: jest.fn(),
    },
  },
};

describe("addRequestAndLoggingInterceptors()", () => {
  test("should set the interceptors to instance when LOG_REQUESTS_AND_RESPONSES set to true", () => {
    appConfig.environment.LOG_REQUESTS_AND_RESPONSES = true;

    addRequestAndLoggingInterceptors(appConfig, instance);

    expect(instance.interceptors.request.use).toBeCalledTimes(1);
    expect(instance.interceptors.request.use).toHaveBeenCalledWith(
      expect.any(Function),
    );
    expect(instance.interceptors.response.use).toBeCalledTimes(1);
    expect(instance.interceptors.response.use).toBeCalledWith(
      AxiosLogger.responseLogger,
      AxiosLogger.errorLogger,
    );
  });

  test("should not set the interceptors to instance when LOG_REQUESTS_AND_RESPONSES set to false", () => {
    appConfig.environment.LOG_REQUESTS_AND_RESPONSES = false;

    addRequestAndLoggingInterceptors(appConfig, instance);

    expect(instance.interceptors.request.use).not.toBeCalled();
    expect(instance.interceptors.response.use).not.toBeCalled();
  });
});
