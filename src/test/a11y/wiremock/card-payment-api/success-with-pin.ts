export default {
  status: 200,
  jsonBody: {
    pin: "1234",
  },
  transformers: ["response-template"],
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
  },
};
