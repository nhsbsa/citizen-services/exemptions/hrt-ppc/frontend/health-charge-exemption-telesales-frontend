import { EXEMPTION_VIEW_URL } from "../../paths/paths";
import { pageContent } from "./page-content";

describe("pageContent()", () => {
  const COMMON_ID_PREFIX = "exemptionDetail.";
  const COMMON_TABLE_PREFIX = COMMON_ID_PREFIX + "tables.";

  test("should return correct page content", () => {
    const translate = (string) => string;
    const req = { params: { id: "1234" }, session: {} };
    const expected = {
      title: translate(COMMON_ID_PREFIX + "title"),
      accessKeyButtonText: translate("buttons:accessKey"),
      accessKeyLink: "/test-context" + EXEMPTION_VIEW_URL,
      preferencesText: {
        email: translate("preferencesText.email"),
        post: translate("preferencesText.post"),
        none: translate("preferencesText.none"),
      },
      tables: {
        certificateDetails: {
          title: translate(COMMON_TABLE_PREFIX + "certificateDetails.title"),
          link: translate(COMMON_TABLE_PREFIX + "certificateDetails.link"),
          rows: {
            reference: translate(
              COMMON_TABLE_PREFIX + "certificateDetails.rows.reference",
            ),
            type: translate(
              COMMON_TABLE_PREFIX + "certificateDetails.rows.type",
            ),
            status: translate(
              COMMON_TABLE_PREFIX + "certificateDetails.rows.status",
            ),
            channel: translate(
              COMMON_TABLE_PREFIX + "certificateDetails.rows.channel",
            ),
            fulfillment: translate(
              COMMON_TABLE_PREFIX + "certificateDetails.rows.fulfillment",
            ),
            startDate: translate(
              COMMON_TABLE_PREFIX + "certificateDetails.rows.startDate",
            ),
            endDate: translate(
              COMMON_TABLE_PREFIX + "certificateDetails.rows.endDate",
            ),
            applicationDate: translate(
              COMMON_TABLE_PREFIX + "certificateDetails.rows.applicationDate",
            ),
          },
        },
        personalDetails: {
          title: translate(COMMON_TABLE_PREFIX + "personalDetails.title"),
          link: translate(COMMON_TABLE_PREFIX + "personalDetails.link"),
          rows: {
            name: translate(COMMON_TABLE_PREFIX + "personalDetails.rows.name"),
            address: translate(
              COMMON_TABLE_PREFIX + "personalDetails.rows.address",
            ),
            telephoneNumber: translate(
              COMMON_TABLE_PREFIX + "personalDetails.rows.telephoneNumber",
            ),
            dateOfBirth: translate(
              COMMON_TABLE_PREFIX + "personalDetails.rows.dateOfBirth",
            ),
            email: translate(
              COMMON_TABLE_PREFIX + "personalDetails.rows.email",
            ),
            nhsNumber: translate(
              COMMON_TABLE_PREFIX + "personalDetails.rows.nhsNumber",
            ),
          },
        },
        notes: {
          title: translate(COMMON_TABLE_PREFIX + "notes.title"),
          link: translate(COMMON_TABLE_PREFIX + "notes.link"),
          rows: {
            date: translate(COMMON_TABLE_PREFIX + "notes.rows.date"),
            time: translate(COMMON_TABLE_PREFIX + "notes.rows.time"),
            agent: translate(COMMON_TABLE_PREFIX + "notes.rows.agent"),
            description: translate(
              COMMON_TABLE_PREFIX + "notes.rows.description",
            ),
          },
        },
        auditHistory: {
          title: translate(COMMON_TABLE_PREFIX + "auditHistory.title"),
          rows: {
            date: translate(COMMON_TABLE_PREFIX + "auditHistory.rows.date"),
            type: translate(COMMON_TABLE_PREFIX + "auditHistory.rows.type"),
            from: translate(COMMON_TABLE_PREFIX + "auditHistory.rows.from"),
            to: translate(COMMON_TABLE_PREFIX + "auditHistory.rows.to"),
            description: translate(
              COMMON_TABLE_PREFIX + "auditHistory.rows.description",
            ),
          },
          type: {
            citizen: translate(
              COMMON_TABLE_PREFIX + "auditHistory.type.citizen",
            ),
            certificate: translate(
              COMMON_TABLE_PREFIX + "auditHistory.type.certificate",
            ),
            payment: translate(
              COMMON_TABLE_PREFIX + "auditHistory.type.payment",
            ),
          },
          description: {
            citizen: {
              firstName: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.firstName",
              ),
              lastName: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.lastName",
              ),
              addressLine1: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.addressLine1",
              ),
              addressLine2: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.addressLine2",
              ),
              townOrCity: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.townOrCity",
              ),
              postcode: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.postcode",
              ),
              dateOfBirth: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.dateOfBirth",
              ),
              nhsNumber: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.nhsNumber",
              ),
              emailAddress: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.citizen.emailAddress",
              ),
            },
            certificate: {
              status: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.certificate.status",
              ),
              startDate: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.certificate.startDate",
              ),
              preference: translate(
                COMMON_TABLE_PREFIX +
                  "auditHistory.description.certificate.preference",
              ),
            },
            payment: {
              status: translate(
                COMMON_TABLE_PREFIX + "auditHistory.description.payment.status",
              ),
            },
            updated: translate(
              COMMON_TABLE_PREFIX + "auditHistory.description.updated",
            ),
            removed: translate(
              COMMON_TABLE_PREFIX + "auditHistory.description.removed",
            ),
          },
          preferencesText: {
            email: translate("preferencesText.email"),
            post: translate("preferencesText.post"),
            none: translate("preferencesText.none"),
          },
        },
      },
      reissueButtonText: translate("buttons:reissue"),
      editCitizenLink: "/test-context/1234/edit-personal-details",
      addNoteLink: "/test-context/1234/add-note",
      editCertificateLink: "/test-context/1234/edit-certificate-details",
      previous: false,
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });
});
