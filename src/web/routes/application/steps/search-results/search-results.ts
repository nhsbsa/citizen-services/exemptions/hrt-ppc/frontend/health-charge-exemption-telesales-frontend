import { splitEvery } from "ramda";
import {
  isNilOrEmpty,
  notIsUndefinedOrNullOrEmpty,
} from "../../../../../common/predicates";
import { config } from "../../../../../config";
import { logger } from "../../../../logger/logger";
import { stateMachine } from "../../flow-control/state-machine";
import { VIEW } from "../../flow-control/states";
import { getCertificateAndCitizenDetailsRows } from "./get-detail-rows";
import { getPagination } from "./get-pagination";

const pageSize = config.environment.SEARCH_RESULTS_PAGE_SIZE;

const pageContent = ({ translate }) => ({
  title: translate("searchResults.title"),
  heading: translate("searchResults.heading"),
  searchBoxHeading: translate("searchResults.searchBoxHeading"),
  paragraphOne: translate("searchResults.paragraphOne"),
  tableHeadingOne: translate("searchResults.tableHeadingOne"),
  tableHeadingTwo: translate("searchResults.tableHeadingTwo"),
  tableHeadingThree: translate("searchResults.tableHeadingThree"),
  tableHeadingFour: translate("searchResults.tableHeadingFour"),
  tableHeadingFive: translate("searchResults.tableHeadingFive"),
  tableHeadingSix: translate("searchResults.tableHeadingSix"),
  tableHeadingSeven: translate("searchResults.tableHeadingSeven"),
  tableHeadingEight: translate("searchResults.tableHeadingEight"),
  tableHeadingNine: translate("searchResults.tableHeadingNine"),
  paginationPrevious: translate("previous"),
  paginationNext: translate("next"),
  paginationResults: translate("results"),
  newApplicationButtonText: translate("buttons:newApplication"),
  previous: false,
});

const behaviourForGet = (config, journey) => (req, res, next) => {
  stateMachine.setState(VIEW, req, journey);
  const localisation = pageContent({ translate: req.t });

  const results: CertificateTransformed[] = req.session.searchResult;
  const page: number = req.query.page || 1;
  const splitResults = splitEvery(pageSize, results);

  const count = results.length;
  const totalPages = Math.ceil(count / pageSize);

  logger.info(
    `Viewing results, total: [${count}], page: [${page}] of [${totalPages}]`,
    req,
  );

  if (
    page > totalPages ||
    isNaN(page) ||
    page <= 0 ||
    isNilOrEmpty(page.toString().trim())
  ) {
    logger.info("Redirecting to first page as page parameter invalid", req);
    res.redirect(
      `${config.server.CONTEXT_PATH}${searchResultsFound.path}?page=1`,
    );
    res.end();
    return;
  }

  const certificateDetailsRowList = getCertificateAndCitizenDetailsRows(
    splitResults[page - 1],
    localisation,
  );

  res.locals.results = certificateDetailsRowList;
  res.locals.totalCount = count;
  res.locals.totalPages = totalPages;
  res.locals.pagination = getPagination(page, count, totalPages, localisation);

  next();
};

const isNavigable = (req) =>
  notIsUndefinedOrNullOrEmpty(req.session.searchResult);

const searchResultsFound = {
  path: "/search-results",
  template: "search-results",
  pageContent,
  behaviourForGet,
  isNavigable,
};

export { searchResultsFound, pageContent, behaviourForGet, isNavigable };
