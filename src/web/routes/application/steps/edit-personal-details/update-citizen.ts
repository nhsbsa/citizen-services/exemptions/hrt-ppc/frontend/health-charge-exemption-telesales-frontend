import { config } from "../../../../../config";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import { citizenClient } from "../../../../client/citizen-client";

const updateCitizen = async (req, citizenId) => {
  const client = new citizenClient(config);
  const body = createRequestBody(req);
  const response = await client.makeRequest(
    {
      method: "PATCH",
      url: "/v1/citizens/" + citizenId,
      data: body,
      responseType: "json",
    },
    req.session,
  );
  return response;
};

const createRequestBody = (req) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const dateOfBirth = req.body.dateOfBirth;
  const nhsNumber = req.body.nhsNumber;
  const addressLine1 = req.body.addressLine1;
  const addressLine2 = req.body.addressLine2;
  const townOrCity = req.body.townOrCity;
  const postcode = req.body.sanitizedPostcode;
  const emailAddress = req.body.emailAddress;

  const request = {
    addresses: new Array(), // eslint-disable-line
  };

  const email = {};
  const address = {};
  request["firstName"] = firstName;
  request["lastName"] = lastName;
  request["dateOfBirth"] = dateOfBirth;
  // nhsNumber is an optional field which needs removing if not provided
  if (notIsUndefinedOrNullOrEmpty(nhsNumber)) {
    request["nhsNumber"] = nhsNumber;
  } else {
    request["nhsNumber"] = null;
  }
  address["addressLine1"] = addressLine1;
  // addressLine2 is an optional field which needs removing if not provided
  if (notIsUndefinedOrNullOrEmpty(addressLine2)) {
    address["addressLine2"] = addressLine2;
  } else {
    address["addressLine2"] = null;
  }
  if (notIsUndefinedOrNullOrEmpty(emailAddress)) {
    email["emailAddress"] = emailAddress;
    request["emails"] = [email];
  }
  address["townOrCity"] = townOrCity;
  address["postcode"] = postcode;
  request.addresses.push(address);
  return request;
};

export { createRequestBody, updateCitizen };
