import {
  AddressType,
  Channel,
  EmailType,
} from "@nhsbsa/health-charge-exemption-common-frontend";

export default {
  status: 200,
  jsonBody: {
    id: "c0a8000a-86da-1e45-8186-ee98c395000f",
    firstName: "John",
    lastName: "Smith",
    dateOfBirth: "1997-03-24",
    addresses: [
      {
        id: "c0a8000a-86da-1e45-8186-ee98c39c0010",
        type: AddressType.POSTAL,
        addressLine1: "Stella House",
        addressLine2: "Goldcrest Way",
        townOrCity: "Newcastle upon Tyne",
        country: "England",
        postcode: "NE15 8NY",
        _meta: {
          channel: Channel.ONLINE,
          createdTimestamp: "2023-03-17T08:03:54.396227",
          createdBy: "POSTMAN",
          updatedTimestamp: "2023-03-17T08:03:54.396227",
          updatedBy: "POSTMAN",
        },
        _links: {
          citizen: {
            href: "http://localhost:8100/v1/citizens/c0a8000a-86da-1e45-8186-ee98c395000f",
          },
        },
      },
    ],
    emails: [
      {
        id: "c0a8000a-86da-1e45-8186-ee98c39d0011",
        type: EmailType.PERSONAL,
        emailAddress: "test@test.com",
        _meta: {
          channel: Channel.ONLINE,
          createdTimestamp: "2023-03-17T08:03:54.397392",
          createdBy: "POSTMAN",
          updatedTimestamp: "2023-03-17T08:03:54.397392",
          updatedBy: "POSTMAN",
        },
        _links: {
          citizen: {
            href: "http://localhost:8100/v1/citizens/c0a8000a-86da-1e45-8186-ee98c395000f",
          },
        },
      },
    ],
    _meta: {
      channel: Channel.ONLINE,
      createdTimestamp: "2023-03-17T08:03:54.386775",
      createdBy: "POSTMAN",
      updatedTimestamp: "2023-03-17T08:03:54.386775",
      updatedBy: "POSTMAN",
    },
    _links: {
      self: {
        href: "http://localhost:8100/v1/citizens/c0a8000a-86da-1e45-8186-ee98c395000f",
      },
      citizen: {
        href: "http://localhost:8100/v1/citizens/c0a8000a-86da-1e45-8186-ee98c395000f",
      },
    },
  },
  headers: {
    "Content-Type": "application/json",
  },
};
