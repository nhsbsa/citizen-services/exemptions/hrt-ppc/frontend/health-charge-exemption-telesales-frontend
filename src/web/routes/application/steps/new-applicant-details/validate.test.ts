import expect from "expect";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/test/apply-express-validation";
import { FieldValidationError } from "express-validator";

const mockValidateAddress = jest.fn();
const mockValidateDateOfBirth = jest.fn();
const mockValidateDateOfBirthIsEmpty = jest.fn();
const mockValidateDateOfBirthAgeCap = jest.fn();
const mockValidateDateOfBirthDay = jest.fn();
const mockValidateDateOfBirthMonth = jest.fn();
const mockValidateDateOfBirthYear = jest.fn();
const mockValidateStartDate = jest.fn();
const mockValidateStartDateDay = jest.fn();
const mockValidateStartDateMonth = jest.fn();
const mockValidateStartDateYear = jest.fn();
const mockAddDateToBody = jest.fn();

jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  ...jest.requireActual("@nhsbsa/health-charge-exemption-common-frontend"),
  addDateToBody: mockAddDateToBody,
  validateAddress: mockValidateAddress,
  validateDateOfBirth: mockValidateDateOfBirth,
  validateDateOfBirthIsEmpty: mockValidateDateOfBirthIsEmpty,
  validateDateOfBirthAgeCap: mockValidateDateOfBirthAgeCap,
  validateDateOfBirthDay: mockValidateDateOfBirthDay,
  validateDateOfBirthMonth: mockValidateDateOfBirthMonth,
  validateDateOfBirthYear: mockValidateDateOfBirthYear,
  validateCertificateStartDate: mockValidateStartDate,
  validateCertificateStartDateDay: mockValidateStartDateDay,
  validateCertificateStartDateMonth: mockValidateStartDateMonth,
  validateCertificateStartDateYear: mockValidateStartDateYear,
}));

import {
  addStartDateToBody,
  validate,
  validateCertificateStartDate,
  validatePhoneNumber,
} from "./validate";
import { entirePermittedStartDateRange } from "@nhsbsa/health-charge-exemption-common-frontend";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";

let request;
let config;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2023-04-01"));
  request = {
    t: (string) => string,
    body: {
      firstName: "John",
      lastName: "Smith",
      description: "This is a note",
      emailAddress: "test@test.com",
      telephoneNumber: "7812345678",
      nhsNumber: "4857773457",
      "dateOfBirth-day": "24",
      "dateOfBirth-month": "03",
      "dateOfBirth-year": "1997",
      "certificateStartDate-day": "20",
      "certificateStartDate-month": "04",
      "certificateStartDate-year": "2023",
      certificateStartDate: "2023-04-20",
      certificateFulfillment: Preference.POSTAL,
    },
  };
  config = {
    features: {
      FULFILLMENT_POST_ENABLED: true,
    },
  };

  mockValidateAddress.mockReturnValue(true);
  mockValidateDateOfBirth.mockReturnValue(true);
  mockValidateDateOfBirthIsEmpty.mockReturnValue(true);
  mockValidateDateOfBirthAgeCap.mockReturnValue(true);
  mockValidateDateOfBirthDay.mockReturnValue(true);
  mockValidateDateOfBirthMonth.mockReturnValue(true);
  mockValidateDateOfBirthYear.mockReturnValue(true);
  mockValidateStartDate.mockReturnValue(true);
  mockValidateStartDateDay.mockReturnValue(true);
  mockValidateStartDateMonth.mockReturnValue(true);
  mockValidateStartDateYear.mockReturnValue(true);
});

describe("validate", () => {
  test("calls validateAddress", async () => {
    await applyExpressValidation(request, validate(config));

    expect(mockValidateAddress).toHaveBeenCalledTimes(1);
    expect(mockValidateAddress).toHaveBeenCalledWith();
  });
});

describe("validate first name", () => {
  it.each([null, undefined, ""])(
    "returns error when firstName field is %p",
    async (value: null | undefined | string) => {
      request.body.firstName = value;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      const errorOne = result.array()[0] as FieldValidationError;

      expect(result.array().length).toBe(1);
      expect(errorOne.path).toBe("firstName");
      expect(errorOne.msg).toBe("validation:missingFirstName");
    },
  );

  test("returns error when firstName field is invalid", async () => {
    request.body.firstName = "L1$a";

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("firstName");
    expect(errorOne.msg).toBe("validation:patternFirstName");
  });

  test("returns error when firstName field exceeds min length", async () => {
    request.body.firstName =
      "DavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavid";

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("firstName");
    expect(errorOne.msg).toBe("validation:firstNameTooLong");
  });
});

describe("validate last name", () => {
  it.each([null, undefined, ""])(
    "returns error when lastName field is %p",
    async (value: null | undefined | string) => {
      request.body.lastName = value;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      const errorOne = result.array()[0] as FieldValidationError;

      expect(result.array().length).toBe(1);
      expect(errorOne.path).toBe("lastName");
      expect(errorOne.msg).toBe("validation:missingLastName");
    },
  );

  test("returns error when lastName field is invalid", async () => {
    request.body.lastName = "$m1th";

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("lastName");
    expect(errorOne.msg).toBe("validation:patternLastName");
  });

  test("returns error when lastName field exceeds min length", async () => {
    request.body.lastName =
      "CamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonC";

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("lastName");
    expect(errorOne.msg).toBe("validation:lastNameTooLong");
  });
});

describe("validate nhs number", () => {
  it.each([
    ["9992158107", 0],
    ["9999999999", 0],
    ["9979989998", 0],
    ["9979989696", 0],
    ["1123443986", 1],
    ["@!@££*£*£test", 1],
    ["9999999999999999999999", 1],
    ["9999", 1],
  ])(
    "checks if %p is a valid nhs number expecting %p validation error",
    async (nhsNumber: string, errorCount: number) => {
      request.body.nhsNumber = nhsNumber;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(errorCount);
      if (errorCount > 0) {
        const errorOne = result.array()[0] as FieldValidationError;
        expect(errorOne.path).toBe("nhsNumber");
        expect(errorOne.msg).toBe("validation:invalidNhsNumber");
      }
    },
  );

  it.each(["", null, undefined])(
    "passes validation when nhs number is %p",
    async (nhsNumber: null | undefined | string) => {
      request.body.nhsNumber = nhsNumber;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(0);
    },
  );
});

describe("validate telephone number", () => {
  const req = {
    t: (string) => string,
  };
  it.each([
    ["0712345678", 1, "is too short for mobile"],
    ["07123456784522", 1, "is too long"],
    ["07700 900431", 1, "is not valid uk number(offCom)"],
    ["+937123456789", 1, "is non uk area code"],
    ["01171234567", 0, "is geographic number"],
    ["07123-456789", 0, "contains hyphens"],
    ["(07123)(456789)", 0, "contains braces"],
    ["00447123456789", 0, "contains leading 0044"],
    ["07123456789", 0, "contains leading 07 with 11 digits"],
    ["07123 456789", 0, "contains spaces with leading 07 and 11 digits"],
  ])(
    "checks if %p is a valid phone number expecting %p validation error which %p",
    async (telephoneNumber: string, errorCount: number) => {
      request.body.telephoneNumber = telephoneNumber;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(errorCount);
      if (errorCount > 0) {
        const errorOne = result.array()[0] as FieldValidationError;
        expect(errorOne.path).toBe("telephoneNumber");
        expect(errorOne.msg).toBe("validation:invalidPhoneNumber");
        expect(
          validatePhoneNumber.bind(null, telephoneNumber, { req }),
        ).toThrowError("validation:invalidPhoneNumber");
      }
    },
  );

  it.each(["", null, undefined])(
    "passes validation when telephone number is %p",
    async (telephoneNumber: null | undefined | string) => {
      request.body.telephoneNumber = telephoneNumber;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(0);
    },
  );
});

describe("validate date of birth", () => {
  test("returns error when dateOfBirth field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirth");
    mockValidateDateOfBirth.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirth");
  });

  test("returns error when dateOfBirth-day field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthDay");
    mockValidateDateOfBirthDay.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-day");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthDay");
  });

  test("returns error when dateOfBirth-month field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthMonth");
    mockValidateDateOfBirthMonth.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-month");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthMonth");
  });

  test("returns error when dateOfBirth-year field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthYear");
    mockValidateDateOfBirthYear.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-year");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthYear");
  });

  test("calls addDateToBody", async () => {
    await applyExpressValidation(request, validate(config));

    expect(mockAddDateToBody).toHaveBeenCalledTimes(1);
  });
});

describe("validate certificate fulfilment", () => {
  test("passes validation when valid option 'email' is selected", async () => {
    request.body.certificateFulfillment = Preference.EMAIL;
    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }
    expect(result.isEmpty()).toBe(true);
  });

  test("passes validation when valid option 'post' is selected and USE_FULFILMENT_POST is true", async () => {
    request.body.certificateFulfillment = Preference.POSTAL;
    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }
    expect(result.isEmpty()).toBe(true);
  });

  test("return error when option 'post' is selected and USE_FULFILMENT_POST is false", async () => {
    request.body.certificateFulfillment = Preference.POSTAL;
    config.features.FULFILLMENT_POST_ENABLED = false;

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }
    const error = result.array()[0] as FieldValidationError;
    expect(result.isEmpty()).toBe(false);
    expect(result.array().length).toBe(1);
    expect(error.path).toBe("certificateFulfillment");
    expect(error.msg).toBe("validation:missingCertificateFulfillment");
  });

  test("return error when invalid option invalidContent is selected", async () => {
    request.body.certificateFulfillment = "invalidContent";

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }
    const error = result.array()[0] as FieldValidationError;
    expect(result.isEmpty()).toBe(false);
    expect(result.array().length).toBe(1);
    expect(error.path).toBe("certificateFulfillment");
    expect(error.msg).toBe("validation:missingCertificateFulfillment");
  });

  it.each([null, undefined, ""])(
    "returns error when %p",
    async (value: null | undefined | string) => {
      request.body.certificateFulfillment = value;
      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      const error = result.array()[0] as FieldValidationError;
      expect(result.array().length).toBe(1);
      expect(error.path).toBe("certificateFulfillment");
      expect(error.msg).toBe("validation:missingCertificateFulfillment");
    },
  );
});

describe("validate email address", () => {
  test("passes validation when certificateFulfillment is selected as 'EMAIL' & emailAddress is provided", async () => {
    request.body.certificateFulfillment = Preference.EMAIL;

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.isEmpty()).toBe(true);
  });

  test("passes validation when certificateFulfillment is selected as 'POSTAL' & emailAddress is provided", async () => {
    request.body.certificateFulfillment = Preference.POSTAL;

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.isEmpty()).toBe(true);
  });
  it.each(["", null, undefined])(
    "returns error when certificateFulfillment is selected as 'EMAIL' & emailAddress is %p",
    async (emailAddress: null | undefined | string) => {
      request.body.certificateFulfillment = Preference.EMAIL;
      request.body.emailAddress = emailAddress;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(1);

      const error = result.array()[0] as FieldValidationError;
      expect(error.path).toBe("emailAddress");
      expect(error.msg).toBe("validation:missingEmailAddress");
    },
  );

  it.each(["", null, undefined])(
    "passes validation when certificateFulfillment is selected as 'POSTAL' & emailAddress is %p",
    async (emailAddress: null | undefined | string) => {
      request.body.certificateFulfillment = Preference.POSTAL;
      request.body.emailAddress = emailAddress;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(0);
    },
  );

  it.each([
    "email@domain.com",
    "firstname.lastname@domain.com",
    "email@subdomain.domain.com",
    "firstname+lastname@domain.com",
    "1234567890@domain.com",
    "email@domain-one.com",
    "_______@domain.com",
    "email@domain.name",
    "email@domain.co.jp",
    "firstname-lastname@domain.com",
    "test@domain.com",
    "Pälé@example.com",
  ])(
    "checks if %p is a valid email address expecting 0 validation errors",
    async (emailAddress: string) => {
      request.body.emailAddress = emailAddress;

      const result = await applyExpressValidation(request, validate(config));

      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(0);
    },
  );

  it.each([
    "test@@domain.com",
    "@@@@domain.com",
    "123.com",
    "plainaddress",
    "#@%^%#$@#$@#.com",
    "@domain.com",
    "Joe Smith <email@domain.com>",
    "email.domain.com",
    "email@domain@domain.com",
    "email@domain.com (Joe Smith)",
    "email@domain",
    "email@domain..com",
    ".email@domain.com",
    "email.@domain.com",
    "email..email@domain.com",
  ])(
    "checks if %p is a valid email address expecting 1 validation error",
    async (emailAddress: string) => {
      request.body.emailAddress = emailAddress;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(1);

      const errorOne = result.array()[0] as FieldValidationError;
      expect(errorOne.path).toBe("emailAddress");
      expect(errorOne.msg).toBe("validation:invalidEmailAddress");
    },
  );
});

describe("add start date to body", () => {
  test("should add date to body", () => {
    const req = {
      t: (string) => string,
      body: {
        "certificateStartDate-day": "10",
        "certificateStartDate-month": "04",
        "certificateStartDate-year": "2023",
      },
    };
    const res = {};
    const next = jest.fn();

    addStartDateToBody(req, res, next);

    expect(req.body["certificateStartDate"]).toEqual("2023-04-10");
    expect(next).toBeCalled();
    expect(next).toBeCalledWith();
  });
});

describe("validate start date", () => {
  test("calls validateCertificateStartDateCommon() with entirePermittedStartDateRange", async () => {
    const req = {
      body: {},
    };

    expect(validateCertificateStartDate("2023-04-01", { req })).toBeTruthy();
    const permittedStartDateRange = entirePermittedStartDateRange(new Date());

    expect(mockValidateStartDate).toBeCalledTimes(1);
    expect(mockValidateStartDate).toBeCalledWith(
      "2023-04-01",
      permittedStartDateRange,
      { req },
    );
  });

  test("validate() calls validateCertificateStartDate, validateCertificateStartDateDay, validateCertificateStartDateMonth, validateCertificateStartDateYear", async () => {
    await applyExpressValidation(request, validate(config));

    expect(mockValidateStartDate).toHaveBeenCalledTimes(1);
    expect(mockValidateStartDateDay).toHaveBeenCalledTimes(1);
    expect(mockValidateStartDateDay).toHaveBeenCalledWith(
      "20",
      expect.anything(),
    );
    expect(mockValidateStartDateMonth).toHaveBeenCalledTimes(1);
    expect(mockValidateStartDateMonth).toHaveBeenCalledWith(
      "04",
      expect.anything(),
    );
    expect(mockValidateStartDateYear).toHaveBeenCalledTimes(1);
    expect(mockValidateStartDateYear).toHaveBeenCalledWith(
      "2023",
      expect.anything(),
    );
  });
});

describe("add note description", () => {
  it.each(["", null, undefined])(
    "returns error when note description is %p",
    async (description: null | undefined | string) => {
      request.body.description = description;

      const result = await applyExpressValidation(request, validate(config));
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(1);

      const error = result.array()[0] as FieldValidationError;
      expect(error.path).toBe("description");
      expect(error.msg).toBe("validation:missingDescription");
    },
  );

  test("returns error when note description is too long", async () => {
    request.body.description =
      "SLxUaa4VsvcIwoqenNRuhY0PeMTcsTiWTOJkD8cPzcdg9J10sSNo1AaJ5XRfVaINRavxVcgbDFclg3eC7eFsBAxyYmbE7WrhRtfOP4oZFBX5B5EhZvxDB3Stl289nDNd54cPqK5I7dh6pzyR6v7QXYRNfAYSfxqp98CfYfxz0ZXVU3m0EQDzfxVjbwlX6XV99sEygV3cDStUeadnD0O8JjT2WEbBqZFIo3o8zPpGmzQ5uT0n8p4ampPx1ab2XlYMhm6UZIxn6aSbQ1mRt5OXWyGOnMXmQ2ADwnWU5yScxn3Ps3VEi9eUTVuzoyXEDcanjb4eUDzeMNpJuXKb1oc34SDF5FiXlJZH9bdLRIXfurDImytPwybO4QeayQuktQL1mHBHqp7KA4tjpxl8s3oQNfIlrP0gArgDCU18jngAkGPF8bicHKKlXY1DkwAZoD62BzF4DAWXP6DPeyoO8oURVqDIgJHCMmZRvAqpUPH7RjdtlTNeliZM7DS3IL6NfVxr6bm2BdVSTVkKJEkHVLfgQx3lcsmQpO9RDEFpZ442iChiKOvRjqqyQWQxLIGz9cKkw8H5xnyHIKErRpXOs5VT0D9I2MMOqgQBMF5cfSxvw3YChEpC307nclZlN5MRuAPQ11aQsP59GzAbSatDmbQSLv71il0py61LujBwG6dWBDItxDmdvBUJu8900rcDdn3QHlYIwu0ewhVXVE41ZsRGfk0mAJXrkcWzFrGeTnEXpPArKg54Ke3lHYsoz1qp8GPdXqlSfk0ZYmeSsR0rFbhyFQAez6AtlhmrOxd2W8kSlqT7ue6odO0qzZHxHr5W7OeUirJhdn6jPcd5T7Ad7BFp2bH0d8s01krF3lmS5Sbcp0UA9jRwxo5CQEpCU1VBBqRGJsLjX3HwtxHTTrl0JAOQGsiZk76BAoHHTtYt7g8PkRVX1J6parikOuGBy8qWBOU4DSxVMxWldqRqpenswqHqle2SSm3HJxjHCoyUzM4qp";

    const result = await applyExpressValidation(request, validate(config));
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);

    const error = result.array()[0] as FieldValidationError;
    expect(error.path).toBe("description");
    expect(error.msg).toBe("validation:descriptionTooLong");
  });
});
