import { DECISION_URL, APPLICATION_COMPLETE_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";
import { logger } from "../../../../logger/logger";
import * as states from "../states";
import { isPathAllowed } from "./predicates";
import {
  getNextNavigablePath,
  getNextInReviewPath,
} from "./selectors/selectors";
import {
  setNextAllowedPathInSession,
  setStateInSession,
  getNextAllowedPathFromSession,
  getStateFromSession,
  setCurrentPathInSession,
  getCurrentPathFromSession,
} from "../session-accessors";

const {
  IN_PROGRESS,
  IN_REVIEW,
  COMPLETED,
  COMPLETED_SEARCH,
  VIEW,
  IN_PROGRESS_COOKIES,
  COMPLETED_COOKIES,
} = states;

const stateMachine = {
  [IN_PROGRESS]: {
    getNextPath: (req, journey) => getNextNavigablePath(req.path, req, journey),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path,
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextNavigablePath(req.path, req, journey),
      ),
  },
  [IN_REVIEW]: {
    getNextPath: (req, journey) => getNextInReviewPath(req, journey.pathPrefix),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path,
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextInReviewPath(req, journey.pathPrefix),
      ),
    invalidateReview: (req, journey) =>
      setStateInSession(req, journey, IN_PROGRESS),
  },
  [COMPLETED]: {
    getNextPath: (req, journey) => prefixPath(journey.pathPrefix, DECISION_URL),
    isPathAllowed: (req, journey) =>
      req.path === prefixPath(journey.pathPrefix, DECISION_URL),
    getNextAllowedPath: (req, journey) =>
      prefixPath(journey.pathPrefix, DECISION_URL),
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        prefixPath(journey.pathPrefix, DECISION_URL),
      ),
  },
  [COMPLETED_SEARCH]: {
    getNextPath: (req, journey) => getNextNavigablePath(req.path, req, journey),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path,
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextNavigablePath(req.path, req, journey),
      ),
  },
  [VIEW]: {
    getNextPath: (req, journey) => getNextNavigablePath(req.path, req, journey),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path,
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextNavigablePath(req.path, req, journey),
      ),
  },
  [IN_PROGRESS_COOKIES]: {
    getNextPath: (req, journey) => getNextNavigablePath(req.path, req, journey),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path,
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextNavigablePath(req.path, req, journey),
      ),
  },
  [COMPLETED_COOKIES]: {
    getNextPath: (req, journey) =>
      prefixPath(journey.pathPrefix, APPLICATION_COMPLETE_URL),
    isPathAllowed: (req, journey) =>
      req.path === prefixPath(journey.pathPrefix, APPLICATION_COMPLETE_URL),
    getNextAllowedPath: (req, journey) =>
      prefixPath(journey.pathPrefix, APPLICATION_COMPLETE_URL),
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        prefixPath(journey.pathPrefix, APPLICATION_COMPLETE_URL),
      ),
  },

  getState: getStateFromSession,

  getCurrentPath: getCurrentPathFromSession,

  setState: (state, req, journey) => {
    if (getStateFromSession(req, journey) !== state) {
      logger.info(`State set to ${state}`, req);
      setStateInSession(req, journey, state);
    }
  },

  dispatch: (actionType, req, journey, ...args) => {
    const state = getStateFromSession(req, journey);
    const action = stateMachine[state][actionType];
    if (typeof action !== "undefined") {
      setCurrentPathInSession(req, journey, req.path);
      return action(req, journey, ...args);
    }

    return null;
  },
};

export { stateMachine };
