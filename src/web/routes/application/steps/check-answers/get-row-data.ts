import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import { formatDateForDisplayFromDate } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/formatters/dates";
import { formatCertificateFulfillment } from "../common/formatters/certificate-fulfillment";
import { join, filter, compose } from "ramda";
import { notIsNilOrEmpty } from "../../../../../common/predicates";
import { ADDRESS_KEYS } from "../../constants";
import { config } from "../../../../../config";
import { toPounds } from "../../../../../common/currency";
import {
  NotesDetailsRowList,
  ApplicationDetailsRowList,
  CertificateDetailsRowList,
} from "../common/types";

const newLineChar = "<br>";
const toMultiLineString = compose(join(newLineChar), filter(notIsNilOrEmpty));

const getKeyFromNewAppicant = (newApplicant) => (key) => newApplicant[key];

const applicantDetailsRowList = (
  req,
  translate,
): (ApplicationDetailsRowList | null)[] =>
  [
    {
      key: {
        text: translate.name,
      },
      value: {
        text: `${req.session.newApplicant.firstName} ${req.session.newApplicant.lastName}`.trim(),
      },
    },
    {
      key: {
        text: translate.dateOfBirth,
      },
      value: {
        text: formatDateForDisplayFromDate(
          new Date(req.session.newApplicant.dateOfBirth),
        ),
      },
    },
    notIsUndefinedOrNullOrEmpty(req.session.newApplicant.nhsNumber)
      ? {
          key: {
            text: translate.nhsNumber,
          },
          value: {
            text: req.session.newApplicant.nhsNumber,
          },
        }
      : null,
    {
      key: {
        text: translate.address,
      },
      value: {
        html: toMultiLineString(
          ADDRESS_KEYS.map(getKeyFromNewAppicant(req.session.newApplicant)),
        ),
      },
    },
    {
      key: {
        text: translate.certificateFulfillment,
      },
      value: {
        text: formatCertificateFulfillment(
          req.session.newApplicant.certificateFulfillment,
          translate,
        ),
      },
    },
    notIsUndefinedOrNullOrEmpty(req.session.newApplicant.emailAddress)
      ? {
          key: {
            text: translate.email,
          },
          value: {
            text: req.session.newApplicant.emailAddress,
          },
        }
      : null,
    notIsUndefinedOrNullOrEmpty(req.session.newApplicant.telephoneNumber)
      ? {
          key: {
            text: translate.telephone,
          },
          value: {
            text: req.session.newApplicant.telephoneNumber,
          },
        }
      : null,
  ].filter((x) => x !== null);

const certificateDetailsRowList = (
  req,
  translate,
): CertificateDetailsRowList[] => [
  {
    key: {
      text: translate.length,
    },
    value: {
      text: "12 months",
    },
  },
  {
    key: {
      text: translate.startsOn,
    },
    value: {
      text: formatDateForDisplayFromDate(
        new Date(req.session.newApplicant.certificateStartDate),
      ),
    },
  },
  {
    key: {
      text: translate.endsOn,
    },
    value: {
      text: formatDateForDisplayFromDate(
        new Date(req.session.newApplicant.certificateEndDate),
      ),
    },
  },
  {
    key: {
      text: translate.totalCost,
    },
    value: {
      text: "£" + toPounds(config.environment.HRT_PPC_VALUE),
    },
  },
  {
    key: {
      text: translate.paymentMethod,
    },
    value: {
      text: "Card payment",
    },
  },
];

const notesDetailsRowList = (req, translate): NotesDetailsRowList[] => [
  {
    key: {
      text: translate.notes,
    },
    value: {
      text: req.session.newApplicant.description,
    },
  },
];

export {
  applicantDetailsRowList,
  certificateDetailsRowList,
  notesDetailsRowList,
};
