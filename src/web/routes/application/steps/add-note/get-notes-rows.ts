import {
  getNotesHeadings,
  getRowsForNotesTable,
} from "../exemption-detail/get-detail-rows";
import { pageContent } from "./add-note";

const getNotesRows = async (req, res) => {
  const localisation = pageContent({ translate: req.t, req });
  const notesRowList = getRowsForNotesTable(req.session.notes, localisation);
  const notesTableHeadings = getNotesHeadings(localisation);

  res.locals.notesData = {
    headings: notesTableHeadings,
    rows: notesRowList,
  };
};

export { getNotesRows };
