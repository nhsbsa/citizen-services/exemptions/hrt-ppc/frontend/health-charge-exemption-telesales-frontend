const REQUEST_ID_HEADER = "x-request-id";

const requestID = (uuid) => (req, res, next) => {
  if (!req.headers[REQUEST_ID_HEADER]) {
    req.headers[REQUEST_ID_HEADER] = uuid;
  }

  next();
};

export { REQUEST_ID_HEADER, requestID };
