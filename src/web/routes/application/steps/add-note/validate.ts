import { check } from "express-validator";
import { translateValidationMessage } from "../common/translate-validation-message";
import { NOTE_DESCRIPTION_MAX_LENGTH } from "../../constants";

const validate = () => [
  check("description")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingDescription"))
    .isLength({ max: NOTE_DESCRIPTION_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:descriptionTooLong"))
    .bail(),
];

export { validate };
