import { ProductCode } from "@nhsbsa/health-charge-exemption-common-frontend";
import { productClient } from "../client/product-client";
import { logger } from "../logger/logger";

const productPriceProvider = async (config, callFrom?) => {
  try {
    const response = await new productClient(config).makeRequest({
      method: "GET",
      url: "/v1/products/search/findByCode?code=" + ProductCode.HRT_12M,
    });
    config.environment.HRT_PPC_VALUE = Number(response.data["price"]);
  } catch (error) {
    config.environment.HRT_PPC_VALUE = undefined;
    if (callFrom === "server-start") {
      throw new Error("Price is not defined");
    }
  }
  config.environment.HRT_PPC_VALUE_UPDATE_TIME = new Date();
  logger.info(`12 month value is ${config.environment.HRT_PPC_VALUE}`);
  logger.info(
    `12 month value updated on ${config.environment.HRT_PPC_VALUE_UPDATE_TIME}`,
  );
};

export { productPriceProvider };
