import * as httpStatus from "http-status-codes";
import { validationResult } from "express-validator";
import { wrapError } from "../../errors";
import { stateMachine } from "../state-machine";

import {
  INVALIDATE_REVIEW,
  INCREMENT_NEXT_ALLOWED_PATH,
} from "../state-machine/actions";
import { logger } from "../../../../logger/logger";

const stepInvalidatesReview = (step, claim) =>
  typeof step.shouldInvalidateReview === "function" &&
  step.shouldInvalidateReview(claim);

const customLocalsAndSessionFormProperty = (step) =>
  typeof step.customLocalsAndSessionFormProperty === "function" &&
  step.customLocalsAndSessionFormProperty();

const handlePost = (journey, step) => (req, res, next) => {
  try {
    const formProperty = customLocalsAndSessionFormProperty(step);
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      logger.info(`Validation errors count ${errors.array.length}`, req);
      res.locals.errors = errors.array();
      if (formProperty) {
        res.locals[formProperty] = {
          ...res.locals[formProperty],
          ...req.body,
        };
      } else {
        res.locals.claim = {
          ...res.locals.claim,
          ...req.body,
        };
      }

      return next();
    }

    if (formProperty) {
      const formProperty = customLocalsAndSessionFormProperty(step);
      req.session[formProperty] = {
        ...req.session[formProperty],
        ...req.body,
      };
    } else {
      req.session.claim = {
        ...req.session.claim,
        ...req.body,
      };
    }

    if (stepInvalidatesReview(step, req.session.claim)) {
      stateMachine.dispatch(INVALIDATE_REVIEW, req, journey);
    }

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);

    return next();
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

export { handlePost };
