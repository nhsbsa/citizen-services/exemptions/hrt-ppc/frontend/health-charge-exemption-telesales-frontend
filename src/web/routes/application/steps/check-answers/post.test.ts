const issueCertificateRequestMock = jest.fn();
const makePaymentRequestMock = jest.fn();
const cardPaymentRequestMock = jest.fn();
import expect from "expect";
import { postCheckAnswers, setHeadersIfMockEnabled } from "./post";
import { issueCertificateClient } from "../../../../client/issue-certificate-client";
import { paymentClient } from "../../../../client/payment-client";
import { cardPaymentsClient } from "../../../../client/cps-client";
import * as exemptionsIssueCertificate from "../../../../../__mocks__/api-responses/issue-certificate-api/exemption-hrt-issue-certificate-lambda.json";
import * as exemptionsPayment from "../../../../../__mocks__/api-responses/payment-api/exemption-payment.json";
import * as createRequestBody from "./create-request-body";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import {
  Preference,
  CertificateType,
  PaymentMethod,
  Duration,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import {
  PaymentRequest,
  IssueCertificateRequest,
  CardPaymentRequest,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/types";
import moment from "moment";

jest.mock("../../../../client/issue-certificate-client");
jest.mock("../../../../client/payment-client");
jest.mock("../../../../client/cps-client");

const journey = {
  pathPrefix: "/test-prefix",
};

const config = {
  environment: {
    OUTBOUND_API_TIMEOUT: 20000,
    ISSUE_CERTIFICATE_API_URI: "baseURI",
    ISSUE_CERTIFICATE_API_KEY: "test-key",
    PAYMENT_CERTIFICATE_API_URI: "baseURI",
    PAYMENT_CERTIFICATE_API_KEY: "test-key",
    CARD_PAYMENTS_API_URI: "baseURI",
    CARD_PAYMENTS_SERVICE_NAME: "test-service-name",
    CARD_PAYMENTS_USE_MOCK: true,
  },
};

const issueCertificateRequestBody: IssueCertificateRequest = {
  citizen: {
    emails: [
      {
        emailAddress: "test@example.com",
      },
    ],
    addresses: [
      {
        addressLine1: "Stella House",
        addressLine2: "Goldcrest Way",
        townOrCity: "Newcastle upon Tyne",
        postcode: "NE15 8NY",
      },
    ],
    telephones: [
      {
        phoneNumber: "71234567891",
      },
    ],
    firstName: "Fname",
    lastName: "Lname",
    dateOfBirth: moment("2022-01-01").toDate(),
    nhsNumber: "9999999999",
  },
  certificate: {
    startDate: moment("2023-04-01").toDate(),
    endDate: moment("2024-04-01").toDate(),
    type: CertificateType.HRT_PPC,
    duration: Duration.P12M,
    cost: "1930",
    applicationDate: moment("2023-01-19").toDate(),
  },
  userPreference: Preference.EMAIL,
};

const paymentRequestBody: PaymentRequest = {
  amount: "1930",
  date: moment("2023-01-19").toDate(),
  method: PaymentMethod.CARD,
};

const cardPaymentRequestBody: CardPaymentRequest = {
  reference: "3569BDF217A46923DB1A",
  transactionId: "906944761072A2088F71",
  amount: "1930",
  serviceDescription: "Twelve Months single payment",
};

const mockIssueCertificateRequestBody = jest
  .spyOn(createRequestBody, "createIssueCertificateRequestBody")
  .mockReturnValue(issueCertificateRequestBody);

const mockPaymentRequestBody = jest
  .spyOn(createRequestBody, "createPaymentRequestBody")
  .mockReturnValue(paymentRequestBody);

const mockCardPaymentRequestBody = jest
  .spyOn(createRequestBody, "createCardPaymentRequestBody")
  .mockReturnValue(cardPaymentRequestBody);

const expectedIssueCertificateMockRequestCall = {
  method: "POST",
  url: "/v1/issue-certificate",
  data: issueCertificateRequestBody,
  responseType: "json",
};

const expectedPaymentMockRequestCall = {
  method: "POST",
  url: "/v1/payments?certificateId=ac1be9c0-8578-15b0-8185-7da2d6a00010",
  data: paymentRequestBody,
  responseType: "json",
};

const expectedCardPaymentMockRequestCall = {
  method: "POST",
  url: `/payments`,
  headers: {
    firstName: "fname",
  },
  data: cardPaymentRequestBody,
  responseType: "json",
};

const req = {
  path: "/test",
  session: {
    newApplicant: { firstName: "fname" },
  },
};
const next = jest.fn();
const res = {
  redirect: jest.fn(),
};

const expectedError = wrapError({
  cause: new Error("Axios error"),
  message: `Error posting ${req.path}`,
  statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
});

beforeEach(() => {
  jest.clearAllMocks();
  issueCertificateClient.prototype.makeRequest = issueCertificateRequestMock;
  req.session["issuedCertificateDetails"] = undefined;
  paymentClient.prototype.makeRequest = makePaymentRequestMock;
  req.session["paymentDetails"] = undefined;
  cardPaymentsClient.prototype.makeRequestCreatePayment =
    cardPaymentRequestMock;
  req.session["cardPaymentDetails"] = undefined;
  config.environment["CARD_PAYMENTS_USE_MOCK"] = true;
});

describe("postCheckAnswers()", () => {
  test("calls issue certificate api, payments api, cardPayments api and redirects to /payment-ref", async () => {
    const transactionId = "906944761072A2088F71";

    issueCertificateRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsIssueCertificate }),
    );
    makePaymentRequestMock.mockReturnValue(
      Promise.resolve({ data: exemptionsPayment }),
    );
    cardPaymentRequestMock.mockReturnValue(
      Promise.resolve({ data: { pin: 1234 } }),
    );

    await postCheckAnswers(config, journey)(req, res, next);

    expect(req.session["issuedCertificateDetails"]).toEqual({
      certificateId: "ac1be9c0-8578-15b0-8185-7da2d6a00010",
      certificateReference: "3569BDF217A46923DB1A",
      citizenId: "0a004b01-857c-1e59-8185-7da2d678004e",
    });
    expect(req.session["paymentDetails"]).toEqual({
      paymentId: "c0a80196-85cf-1c6d-8185-cf8cf96a0006",
      transactionId: transactionId,
    });
    expect(req.session["cardPaymentDetails"]).toEqual({
      pin: 1234,
    });
    expect(issueCertificateClient).toBeCalledTimes(1);
    expect(issueCertificateClient).toBeCalledWith(config);
    expect(issueCertificateClient.prototype.makeRequest).toBeCalledTimes(1);
    expect(issueCertificateClient.prototype.makeRequest).toBeCalledWith(
      expectedIssueCertificateMockRequestCall,
      req,
    );
    expect(mockIssueCertificateRequestBody).toBeCalledTimes(1);
    expect(mockIssueCertificateRequestBody).toHaveBeenCalledWith(req.session);

    expect(paymentClient).toBeCalledTimes(1);
    expect(paymentClient).toBeCalledWith(config);
    expect(paymentClient.prototype.makeRequest).toBeCalledTimes(1);
    expect(paymentClient.prototype.makeRequest).toBeCalledWith(
      expectedPaymentMockRequestCall,
      req.session,
    );
    expect(mockPaymentRequestBody).toBeCalledTimes(1);
    expect(mockPaymentRequestBody).toHaveBeenCalledWith();

    expect(cardPaymentsClient).toBeCalledTimes(1);
    expect(cardPaymentsClient).toBeCalledWith(config);
    expect(
      cardPaymentsClient.prototype.makeRequestCreatePayment,
    ).toBeCalledTimes(1);
    expect(
      cardPaymentsClient.prototype.makeRequestCreatePayment,
    ).toBeCalledWith(expectedCardPaymentMockRequestCall);
    expect(mockCardPaymentRequestBody).toBeCalledTimes(1);
    expect(mockCardPaymentRequestBody).toHaveBeenCalledWith(req.session);

    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toHaveBeenCalledWith("/test-prefix/payment-ref");
  });

  test("calls next with wrapped error when postCheckAnswers throws error", async () => {
    issueCertificateRequestMock.mockReturnValue(
      Promise.reject(new Error("Axios error")),
    );

    await postCheckAnswers(config, journey)(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(req.session["issuedCertificateDetails"]).toBeUndefined();
    expect(req.session["paymentDetails"]).toBeUndefined();
    expect(req.session["cardPaymentDetails"]).toBeUndefined();
    expect(res.redirect).not.toBeCalled();
  });
});

describe("setHeadersIfMockEnabled()", () => {
  const req = {
    session: {
      newApplicant: {
        firstName: "fname",
      },
    },
  };

  test("should return firstName in object when CARD_PAYMENTS_USE_MOCK is set to true", () => {
    const returnHeader = setHeadersIfMockEnabled(config, req);
    expect(returnHeader).toEqual({ firstName: "fname" });
  });

  test("should return empty object when CARD_PAYMENTS_USE_MOCK is set to false", () => {
    config.environment["CARD_PAYMENTS_USE_MOCK"] = false;

    setHeadersIfMockEnabled(config, req);
    const returnHeader = setHeadersIfMockEnabled(config, req);
    expect(returnHeader).toEqual({});
  });
});
