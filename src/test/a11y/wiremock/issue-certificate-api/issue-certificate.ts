import { postJsonData } from "../../request";
import { createIssueCertificateResultsMapping } from "./mappings";
import { WIREMOCK_ISSUE_CERTIFICATE_MAPPING_URL } from "../paths";

async function setupSuccessfulIssueCertificateMapping() {
  await postJsonData(
    WIREMOCK_ISSUE_CERTIFICATE_MAPPING_URL,
    createIssueCertificateResultsMapping(),
  );
}

export { setupSuccessfulIssueCertificateMapping };
