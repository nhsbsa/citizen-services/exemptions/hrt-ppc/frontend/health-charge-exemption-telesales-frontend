import { REISSUE_URL } from "../../paths/paths";
import { Event } from "@nhsbsa/health-charge-exemption-common-frontend";
import {
  behaviourForGet,
  isNavigable,
  pageContent,
  reissue,
  behaviourForPost,
} from "./reissue";

describe("isNavigable()", () => {
  it.each([[], undefined, ""])(
    "returns false when certificate in session set to %p",
    (certificate: unknown) => {
      const req = {
        params: { id: "HRT123" },
        session: {
          certificate: certificate,
          citizen: { id: "123" },
        },
      };

      const result = isNavigable(req);

      expect(result).toBeFalsy();
    },
  );

  it.each([[], undefined, ""])(
    "returns false when citizen in session set to %p",
    (citizen: unknown) => {
      const req = {
        params: { id: "HRT123" },
        session: {
          certificate: { id: "123" },
          citizen: citizen,
        },
      };

      const result = isNavigable(req);

      expect(result).toBeFalsy();
    },
  );

  it.each([[], undefined, ""])(
    "returns false when id in params set to %p",
    (id: unknown) => {
      const req = {
        params: { id: id },
        session: {
          certificate: { id: "123" },
          citizen: { id: "123" },
        },
      };

      const result = isNavigable(req);

      expect(result).toBeFalsy();
    },
  );

  it.each([[], undefined, "", Event.REMINDER])(
    "returns false when event in session set to %p",
    (event: unknown) => {
      const req = {
        params: { id: 123 },
        session: {
          certificate: { id: "123", preference: { event: event } },
          citizen: { id: "123" },
        },
      };

      const result = isNavigable(req);

      expect(result).toBeFalsy();
    },
  );

  it.each([[], undefined, ""])(
    "should return false when id, certificate and citizen are each %p",
    (value: unknown) => {
      const req = {
        params: { id: value },
        session: {
          certificate: value,
          citizen: value,
        },
      };

      const result = isNavigable(req);

      expect(result).toBeFalsy();
    },
  );

  test("should return true when id, certificate and citizen are correctly populated", () => {
    const req = {
      params: { id: "HRT123" },
      session: {
        certificate: { id: "123", preference: { event: Event.ANY } },
        citizen: { id: "123" },
      },
    };

    const result = isNavigable(req);

    expect(result).toBeTruthy();
  });
});

describe("reissue", () => {
  test("reissue should match expected outcomes", () => {
    const reissueResult = {
      path: `/:id(HRT[A-Za-z0-9]+)${REISSUE_URL}`,
      template: "reissue",
      pageContent,
      isNavigable,
      behaviourForGet,
      behaviourForPost,
    };

    expect(reissue).toEqual(reissueResult);
  });
});
