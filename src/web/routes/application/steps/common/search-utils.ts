import { find, propEq } from "ramda";

const findRecordInSearchResults = (req) => {
  if (
    req.params.id === undefined ||
    req.session === undefined ||
    req.session.searchResult === undefined
  ) {
    return undefined;
  }
  return find(propEq(req.params.id, "reference"), req.session.searchResult);
};

export { findRecordInSearchResults };
