import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

export default {
  status: 200,
  jsonBody: {
    _embedded: {
      notes: [
        {
          id: "c0a8000a-86ee-1fc9-8186-ee9d59d70000",
          citizenId: "c0a8000a-86da-1e45-8186-ee98c395000f",
          certificateId: "c0a8000a-86da-1e6a-8186-ee9a40dd000f",
          description: "this is note 1",
          _meta: {
            channel: Channel.ONLINE,
            createdTimestamp: "2023-03-17T08:08:54.997668",
            createdBy: "POSTMAN",
            updatedTimestamp: "2023-03-17T08:08:54.997668",
            updatedBy: "POSTMAN",
          },
          _links: {
            self: {
              href: "http://localhost:8140/v1/notes/c0a8000a-86ee-1fc9-8186-ee9d59d70000",
            },
            note: {
              href: "http://localhost:8140/v1/notes/c0a8000a-86ee-1fc9-8186-ee9d59d70000",
            },
          },
        },
        {
          id: "c0a8000a-86ee-1fc9-8186-ee9d67ef0001",
          citizenId: "c0a8000a-86da-1e45-8186-ee98c395000f",
          certificateId: "c0a8000a-86da-1e6a-8186-ee9a40dd000f",
          description: "this is note 2",
          _meta: {
            channel: Channel.ONLINE,
            createdTimestamp: "2023-03-17T08:08:58.607574",
            createdBy: "POSTMAN",
            updatedTimestamp: "2023-03-17T08:08:58.607574",
            updatedBy: "POSTMAN",
          },
          _links: {
            self: {
              href: "http://localhost:8140/v1/notes/c0a8000a-86ee-1fc9-8186-ee9d67ef0001",
            },
            note: {
              href: "http://localhost:8140/v1/notes/c0a8000a-86ee-1fc9-8186-ee9d67ef0001",
            },
          },
        },
      ],
    },
    _links: {
      self: {
        href: "http://localhost:8140/v1/notes/search/findByCertificateId?certificateId=c0a8000a-86da-1e6a-8186-ee9a40dd000f&page=0&size=20",
      },
    },
    page: {
      size: 20,
      totalElements: 2,
      totalPages: 1,
      number: 0,
    },
  },
  headers: {
    "Content-Type": "application/json",
  },
};
