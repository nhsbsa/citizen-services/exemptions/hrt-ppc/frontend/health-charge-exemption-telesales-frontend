import { deleteEmail } from "./delete-email";
import { citizenClient } from "../../../../client/citizen-client";

const mockMakeRequestCitizenClient = jest.fn();

const response = "dummy response";

beforeEach(() => {
  jest.clearAllMocks();
  citizenClient.prototype.makeRequest = mockMakeRequestCitizenClient;
  mockMakeRequestCitizenClient.mockResolvedValue(response);
});

describe("deleteEmail()", () => {
  it("should call the citizen delete and return the response", async () => {
    const emailId = "123";
    const mockReq = {
      session: { example: "test" },
    };

    const result = await deleteEmail(mockReq, emailId);
    expect(result).toEqual(response);

    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledWith(
      {
        method: "DELETE",
        responseType: "json",
        url: "/v1/emails/123",
      },
      mockReq.session,
    );
  });
});
