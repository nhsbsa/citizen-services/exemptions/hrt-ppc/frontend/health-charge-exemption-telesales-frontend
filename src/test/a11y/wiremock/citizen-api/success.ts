import { postJsonData } from "../../request";
import { WIREMOCK_CITIZEN_MAPPING_URL } from "../paths";
import {
  createGetCitizenByIdSuccessMapping,
  SUCCESS,
} from "./mappings/mappings";

async function setupSuccessfulGetCitizenMapping() {
  await postJsonData(
    WIREMOCK_CITIZEN_MAPPING_URL,
    createGetCitizenByIdSuccessMapping(SUCCESS),
  );
}

export { setupSuccessfulGetCitizenMapping };
