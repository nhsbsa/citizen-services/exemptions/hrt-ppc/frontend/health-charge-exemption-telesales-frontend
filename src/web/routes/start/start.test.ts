const mockSessionDestroyButKeepLoggedIn = jest.fn();

import expect from "expect";
import express from "express";
import { registerStartRoute } from "./start";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";

const router = { ...jest.requireActual("express"), get: jest.fn() };
jest.spyOn(express, "Router").mockImplementationOnce(() => router);
jest.mock("../application/steps/common/session-destroy", () => ({
  sessionDestroyButKeepLoggedIn: mockSessionDestroyButKeepLoggedIn,
}));

const res = {
  redirect: jest.fn(),
  cookie: jest.fn(),
  clearCookie: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};
const next = jest.fn();
const app = {
  use: jest.fn(),
};

test("Start page registerStartRoute() should destroy session and return page content", () => {
  const req = {
    t: (string) => string,
    session: {
      destroy: jest.fn(),
      isAuthenticated: true,
      account: {
        username: "test@mail.com",
      },
    },
  };

  router.get.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res, next);
  });

  registerStartRoute(app);

  expect(app.use).toBeCalledTimes(1);
  expect(router.get).toBeCalledTimes(1);
  expect(router.get).toHaveBeenNthCalledWith(
    1,
    "/test-context",
    configureAuthentication,
    expect.anything(),
  );
  expect(mockSessionDestroyButKeepLoggedIn).toBeCalled();
  expect(mockSessionDestroyButKeepLoggedIn).toBeCalledWith(req);
  expect(res.redirect).toBeCalledWith("/test-context/search-hrt-records");
  expect(res.clearCookie).toBeCalledWith("lang");
  expect(res.cookie).toBeCalledWith("isBrowserCookieEnabled", true, {
    httpOnly: true,
  });
});
