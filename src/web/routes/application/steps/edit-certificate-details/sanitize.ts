import { NextFunction, Request, Response } from "express";
import { sanitizeStartDate } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/certificate-start-date";

const sanitize = () => (req: Request, res: Response, next: NextFunction) => {
  sanitizeStartDate(req);
  next();
};

export { sanitize };
