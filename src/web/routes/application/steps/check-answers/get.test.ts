import { getCheckAnswers, getLastNavigablePath, pageContent } from "./get";
import { stateMachine } from "../../flow-control/state-machine";

jest.mock("../../flow-control/state-machine", () => ({
  stateMachine: {
    setState: jest.fn(),
    dispatch: jest.fn(),
  },
}));
jest.mock("../../flow-control/states", () => ({
  IN_REVIEW: "mocked-in-review-state",
}));
jest.mock("../../flow-control/state-machine/actions", () => ({
  INCREMENT_NEXT_ALLOWED_PATH: "mocked-increment-action",
}));
jest.mock("../../flow-control/get-previous-path", () => ({
  getPreviousPath: jest.fn(() => "/mocked-previous-path"),
}));
jest.mock("./get-row-data", () => ({
  applicantDetailsRowList: jest.fn(() => []),
  certificateDetailsRowList: jest.fn(() => []),
  notesDetailsRowList: jest.fn(() => []),
}));

describe("getCheckAnswers", () => {
  test("should set the state and dispatch the action correctly", () => {
    const req = {
      t: jest.fn((key) => `Translated: ${key}`),
      session: {},
      csrfToken: jest.fn(),
    };
    const res = {
      render: jest.fn(),
    };
    const journey = {
      steps: [
        { path: "/first", next: () => "/second" },
        { path: "/second", next: () => "/third" },
        { path: "/third", isNavigable: () => false, next: () => "/fourth" },
      ],
    };

    const handler = getCheckAnswers(journey);
    handler(req, res);

    expect(stateMachine.setState).toHaveBeenCalledWith(
      "mocked-in-review-state",
      req,
      journey,
    );
    expect(stateMachine.dispatch).toHaveBeenCalledWith(
      "mocked-increment-action",
      req,
      journey,
    );
  });

  test('should render the "check-answers" template with the correct data', () => {
    const req = {
      t: jest.fn((key) => `Translated: ${key}`),
      session: {},
      csrfToken: jest.fn(),
    };
    const res = {
      render: jest.fn(),
    };

    const journey = {
      steps: [
        { path: "/first", next: () => "/second" },
        { path: "/second", next: () => "/third" },
        { path: "/third", isNavigable: () => false, next: () => "/fourth" },
      ],
    };

    const handler = getCheckAnswers(journey);
    handler(req, res);

    expect(res.render).toHaveBeenCalledWith("check-answers", {
      title: "Translated: checkAnswers.title",
      heading: "Translated: checkAnswers.heading",
      preferencesText: {
        email: "Translated: preferencesText.email",
        post: "Translated: preferencesText.post",
        none: "Translated: preferencesText.none",
      },
      amendDetailsButtonText: "Translated: buttons:amendDetails",
      cardPaymentCallButtonText: "Translated: buttons:cardPaymentCall",
      cancelButtonText: "Translated: buttons:cancel",
      warningCalloutHeading: "Translated: checkAnswers.warningCalloutHeading",
      warningCalloutParagraphOne:
        "Translated: checkAnswers.warningCalloutParagraphOne",
      warningCalloutParagraphTwo:
        "Translated: checkAnswers.warningCalloutParagraphTwo",
      summaryListHeadings: {
        applicantDetails: "Translated: checkAnswers.applicantDetails",
        certificateDetails: "Translated: checkAnswers.certificateDetails",
        notesDetails: "Translated: checkAnswers.notesDetails",
      },
      name: "Translated: checkAnswers.summaryKey.name",
      dateOfBirth: "Translated: checkAnswers.summaryKey.dateOfBirth",
      nhsNumber: "Translated: checkAnswers.summaryKey.nhsNumber",
      address: "Translated: checkAnswers.summaryKey.address",
      certificateFulfillment:
        "Translated: checkAnswers.summaryKey.certificateFulfillment",
      email: "Translated: checkAnswers.summaryKey.email",
      telephone: "Translated: checkAnswers.summaryKey.telephone",
      length: "Translated: checkAnswers.summaryKey.length",
      startsOn: "Translated: checkAnswers.summaryKey.startsOn",
      endsOn: "Translated: checkAnswers.summaryKey.endsOn",
      totalCost: "Translated: checkAnswers.summaryKey.totalCost",
      paymentMethod: "Translated: checkAnswers.summaryKey.paymentMethod",
      notes: "Translated: checkAnswers.summaryKey.notes",
      csrfToken: req.csrfToken(),
      applicantDetailsLists: [],
      certificateDetailsLists: [],
      notesDetailsLists: [],
      previous: "/mocked-previous-path",
    });
  });
});

describe("getLastNavigablePath", () => {
  test("should return the path of the last navigable step", () => {
    const steps = [
      { path: "/step1", isNavigable: () => true },
      { path: "/step2", isNavigable: () => false },
      { path: "/step3", isNavigable: () => true },
    ];
    const req = {
      session: {},
    };

    const result = getLastNavigablePath(steps, req);

    expect(result).toBe("/step3");
  });
});

describe("pageContent", () => {
  test("should return an object with the correct properties", () => {
    const translate = jest.fn((key) => `Translated: ${key}`);

    const result = pageContent({ translate });

    expect(result).toEqual({
      title: "Translated: checkAnswers.title",
      heading: "Translated: checkAnswers.heading",
      preferencesText: {
        email: "Translated: preferencesText.email",
        post: "Translated: preferencesText.post",
        none: "Translated: preferencesText.none",
      },
      amendDetailsButtonText: "Translated: buttons:amendDetails",
      cardPaymentCallButtonText: "Translated: buttons:cardPaymentCall",
      cancelButtonText: "Translated: buttons:cancel",
      warningCalloutHeading: "Translated: checkAnswers.warningCalloutHeading",
      warningCalloutParagraphOne:
        "Translated: checkAnswers.warningCalloutParagraphOne",
      warningCalloutParagraphTwo:
        "Translated: checkAnswers.warningCalloutParagraphTwo",
      summaryListHeadings: {
        applicantDetails: "Translated: checkAnswers.applicantDetails",
        certificateDetails: "Translated: checkAnswers.certificateDetails",
        notesDetails: "Translated: checkAnswers.notesDetails",
      },
      name: "Translated: checkAnswers.summaryKey.name",
      dateOfBirth: "Translated: checkAnswers.summaryKey.dateOfBirth",
      nhsNumber: "Translated: checkAnswers.summaryKey.nhsNumber",
      address: "Translated: checkAnswers.summaryKey.address",
      certificateFulfillment:
        "Translated: checkAnswers.summaryKey.certificateFulfillment",
      email: "Translated: checkAnswers.summaryKey.email",
      telephone: "Translated: checkAnswers.summaryKey.telephone",
      length: "Translated: checkAnswers.summaryKey.length",
      startsOn: "Translated: checkAnswers.summaryKey.startsOn",
      endsOn: "Translated: checkAnswers.summaryKey.endsOn",
      totalCost: "Translated: checkAnswers.summaryKey.totalCost",
      paymentMethod: "Translated: checkAnswers.summaryKey.paymentMethod",
      notes: "Translated: checkAnswers.summaryKey.notes",
    });
  });
});
