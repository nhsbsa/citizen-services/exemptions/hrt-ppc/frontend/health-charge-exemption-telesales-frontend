import expect from "expect";
import * as sinon from "sinon";
import { injectAdditionalHeaders } from "./inject-headers";
// process.env.USE_UNSECURE_COOKIE = false
const cookieEnvVariable = process.env.USE_UNSECURE_COOKIE;

test("injectAdditionalHeaders() sets encrypted to true on req and browser cookie check enabled set", () => {
  const req = {
    connection: {
      encrypted: undefined,
    },
  };
  const res = {
    cookie: sinon.spy(),
  };
  const next = sinon.stub();
  const expected = {
    connection: {
      encrypted: cookieEnvVariable === "false" ? true : undefined,
    },
  };

  injectAdditionalHeaders(req, res, next);
  expect(res.cookie.calledWith("isBrowserCookieEnabled")).toBe(true);
  expect(req).toEqual(expected);
  expect(next.called).toBe(true);
});

test("injectAdditionalHeaders() sets encrypted to true on req and browser cookie check enabled set when next is undefined", () => {
  const req = {
    connection: {
      encrypted: undefined,
    },
  };
  const res = {
    cookie: sinon.spy(),
  };
  const next = sinon.stub();
  const expected = {
    connection: {
      encrypted: cookieEnvVariable === "false" ? true : undefined,
    },
  };

  injectAdditionalHeaders(req, res);
  expect(res.cookie.calledWith("isBrowserCookieEnabled")).toBe(true);
  expect(req).toEqual(expected);
  expect(next.called).toBe(false);
});
