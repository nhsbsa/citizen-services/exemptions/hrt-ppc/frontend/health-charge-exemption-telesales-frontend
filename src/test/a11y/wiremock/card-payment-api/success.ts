import { postJsonData } from "../../request";
import { WIREMOCK_CPS_MAPPING_URL } from "../paths";
import { createSuccessWithPinMapping, SUCCESS } from "./mappings/mappings";

async function setupSuccessfulPinMapping() {
  await postJsonData(
    WIREMOCK_CPS_MAPPING_URL,
    createSuccessWithPinMapping(SUCCESS),
  );
}

export { setupSuccessfulPinMapping };
