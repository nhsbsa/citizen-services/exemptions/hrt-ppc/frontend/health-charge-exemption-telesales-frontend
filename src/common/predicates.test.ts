import expect from "expect";
import {
  isNilOrEmpty,
  isUndefined,
  isBoolean,
  isString,
  notIsUndefinedOrNullOrEmpty,
} from "./predicates";

test("isNilOrEmpty", () => {
  expect(isNilOrEmpty("")).toBe(true);
  expect(isNilOrEmpty(null)).toBe(true);
  expect(isNilOrEmpty(undefined)).toBe(true);
  expect(isNilOrEmpty("Something")).toBe(false);
});

test("isUndefined()", () => {
  expect(isUndefined(undefined)).toBe(true);
  expect(isUndefined(null)).toBe(false);
  expect(isUndefined(true)).toBe(false);
  expect(isUndefined(false)).toBe(false);
  expect(isUndefined(44)).toBe(false);
  expect(isUndefined("a string")).toBe(false);
});

test("isBoolean()", () => {
  expect(isBoolean(undefined)).toBe(false);
  expect(isBoolean(null)).toBe(false);
  expect(isBoolean(true)).toBe(true);
  expect(isBoolean(false)).toBe(true);
  expect(isBoolean(44)).toBe(false);
  expect(isBoolean("a string")).toBe(false);
});

test("isString()", () => {
  expect(isString(undefined)).toBe(false);
  expect(isString(null)).toBe(false);
  expect(isString(true)).toBe(false);
  expect(isString(false)).toBe(false);
  expect(isString(44)).toBe(false);
  expect(isString("a string")).toBe(true);
});

test("notIsUndefinedOrNullOrEmpty()", () => {
  expect(notIsUndefinedOrNullOrEmpty(undefined)).toBe(false);
  expect(notIsUndefinedOrNullOrEmpty(null)).toBe(false);
  expect(notIsUndefinedOrNullOrEmpty([])).toBe(false);
  expect(notIsUndefinedOrNullOrEmpty("")).toBe(false);
  expect(notIsUndefinedOrNullOrEmpty("Address")).toBe(true);
  expect(notIsUndefinedOrNullOrEmpty(["Item 1", "Item 2"])).toBe(true);
});
