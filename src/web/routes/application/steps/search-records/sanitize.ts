import { toUpper, compose } from "ramda";
import { sanitizeDateOfBirth } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/date-of-birth";

const replaceMultipleSpacesWithOne = (value) => {
  return value.replace(/  +/g, " ");
};

const addSpaceBeforeLastThreeCharacters = (value) => {
  return value.replace(/^(.*)(\d)/, "$1 $2");
};

const replaceMultipleSpacesAndAddSingleSpaceThenUppercase = compose(
  replaceMultipleSpacesWithOne,
  addSpaceBeforeLastThreeCharacters,
  toUpper,
);

const sanitize = () => (req, res, next) => {
  req.body.sanitizedPostcode =
    replaceMultipleSpacesAndAddSingleSpaceThenUppercase(req.body.postcode);
  sanitizeDateOfBirth(req);
  next();
};

export { sanitize };
