import * as httpStatus from "http-status-codes";
import { validationResult } from "express-validator";
import {
  isNilOrEmpty,
  notIsUndefinedOrNullOrEmpty,
} from "../../../../../common/predicates";
import { wrapError } from "../../errors";
import { contextPath } from "../../paths/context-path";
import { EXEMPTION_VIEW_URL } from "../../paths/paths";
import { updateCitizen } from "./update-citizen";
import { deleteEmail } from "./delete-email";
import { logger } from "../../../../logger/logger";

const behaviourForPost = () => async (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    res.locals.citizen = {
      ...res.locals.citizen,
      ...req.body,
    };

    return next();
  }

  req.session.citizen = {
    ...req.session.citizen,
    ...req.body,
  };

  const citizen = req.session.citizen;
  const citizenId = citizen.id;
  const emailAddress = req.body.emailAddress;

  try {
    logger.info(`Updating citizen details for ${citizenId}`, req);
    await updateCitizen(req, citizenId);
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error on post when updating citizen ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
    return;
  }

  /**
   * We only want to delete the email if:
   * 1. the user has removed the email address from the input field
   * 2. the email existed in the session prior
   */
  if (
    isNilOrEmpty(emailAddress) &&
    notIsUndefinedOrNullOrEmpty(citizen.emails)
  ) {
    const emailId = citizen.emails[0].id;
    try {
      logger.info(
        `Deleting email for citizen ${citizenId} and email ${emailId}`,
        req,
      );
      await deleteEmail(req, emailId);
    } catch (error) {
      next(
        wrapError({
          cause: error,
          message: `Error on post when deleting email ${req.path}`,
          statusCode: httpStatus.INTERNAL_SERVER_ERROR,
        }),
      );
      return;
    }
  }

  res.redirect(contextPath(`${EXEMPTION_VIEW_URL}/${req.params.id}`));
};

export { behaviourForPost };
