const SUCCESS = "success";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";
import v1FindByCitizenId from "./v1-find-citizen-by-id";

const RESPONSE_MAP = {
  [SUCCESS]: v1FindByCitizenId,
};

const createGetCitizenByIdSuccessMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "GET",
      url: `/v1/citizens/c0a8000a-86da-1e45-8186-ee98c395000f`,
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
        channel: {
          equalTo: Channel.STAFF,
        },
        "user-id": {
          equalTo: "LOCAL_USER_CYPHER",
        },
      },
    },
    response: RESPONSE_MAP[scenario],
  });

export { createGetCitizenByIdSuccessMapping, SUCCESS };
