import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { searchExemptions } from "./search-citizen-certificates-provider";

const search = async (req, res, next) => {
  try {
    const result = await searchExemptions(req);

    return result;
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

export { search };
