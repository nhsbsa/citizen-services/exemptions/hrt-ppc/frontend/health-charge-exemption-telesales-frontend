import { toPounds } from "../../../../../common/currency";
import { contextPath } from "../../paths/context-path";
import { validate } from "./validate";
import { sanitize } from "./sanitize";
import { validationResult } from "express-validator";
import { search } from "./post";
import { stateMachine } from "../../flow-control/state-machine";
import { COMPLETED_SEARCH } from "../../flow-control/states";
import { config } from "../../../../../config";

const pageContent = ({ translate }) => ({
  title: translate("start.title"),
  heading: translate("start.heading"),
  warningCalloutHeading: translate(
    "start.warningCallout.warningCalloutHeading",
  ),
  warningCalloutHTML: translate("start.warningCallout.warningCalloutHTML", {
    ppcValue: toPounds(config.environment.HRT_PPC_VALUE),
  }),
  accessKeyButtonText: translate("buttons:accessKey"),
  accessKeyLink: contextPath("/search-hrt-records"),
  searchBoxHeading: translate("start.searchBoxHeading"),
  lastNameLabel: translate("start.lastNameLabel"),
  dateOfBirthLabel: translate("start.dateOfBirthLabel"),
  hint: translate("start.hint"),
  dayLabel: translate("start.dayLabel"),
  monthLabel: translate("start.monthLabel"),
  yearLabel: translate("start.yearLabel"),
  postcodeLabel: translate("start.postcodeLabel"),
  submitButtonText: translate("buttons:submit"),
  cancelButtonText: translate("buttons:cancel"),
  previous: false,
});

const behaviourForPost = (config, journey) => async (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  const result = await search(req, res, next);

  req.session.searchResult = result;

  stateMachine.setState(COMPLETED_SEARCH, req, journey);

  next();
};

const searchRecords = {
  path: "/search-hrt-records",
  template: "search-hrt-records",
  pageContent,
  sanitize,
  validate,
  behaviourForPost,
};

export { searchRecords, pageContent, sanitize, validate, behaviourForPost };
