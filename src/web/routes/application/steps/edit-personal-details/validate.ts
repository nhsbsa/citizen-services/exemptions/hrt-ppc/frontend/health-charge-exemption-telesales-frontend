import { check } from "express-validator";
import { translateValidationMessage } from "../common/translate-validation-message";
import {
  addDateToBody,
  validateAddress,
  validateDateOfBirth,
  validateDateOfBirthAgeCap,
  validateDateOfBirthIsEmpty,
  validateDateOfBirthDay,
  validateDateOfBirthMonth,
  validateDateOfBirthYear,
  validateEmailAddress,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import {
  FIRSTNAME_MAX_LENGTH,
  LASTNAME_MAX_LENGTH,
  NAME_PATTERN,
} from "../../constants";
import {
  DATE_OF_BIRTH_DAY_KEY,
  DATE_OF_BIRTH_MONTH_KEY,
  DATE_OF_BIRTH_YEAR_KEY,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";
import { isNilOrEmpty } from "../../../../../common/predicates";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";
import { deriveFulfilmentOnEvent } from "../common/preference-fulfilment-utils";
import validateNhsNumber from "nhs-numbers/lib/validateNhsNumber";

const validate = () => [
  check("firstName")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingFirstName"))
    .isLength({ max: FIRSTNAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:firstNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternFirstName")),

  check("lastName")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingLastName"))
    .isLength({ max: LASTNAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:lastNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternLastName")),

  validateAddress(),

  addDateToBody,
  check("dateOfBirth")
    .custom(validateDateOfBirth)
    .bail()
    .custom(validateDateOfBirthIsEmpty)
    .bail()
    .custom(validateDateOfBirthAgeCap),
  check(DATE_OF_BIRTH_DAY_KEY).custom(validateDateOfBirthDay),
  check(DATE_OF_BIRTH_MONTH_KEY).custom(validateDateOfBirthMonth),
  check(DATE_OF_BIRTH_YEAR_KEY).custom(validateDateOfBirthYear),

  validateEmailAddress(),
  check("emailAddress").custom((value, { req }) => {
    if (
      deriveFulfilmentOnEvent(
        req.session.certificate.preference.event,
        req.session.certificate.preference.preference,
      ) == Preference.EMAIL &&
      isNilOrEmpty(value)
    ) {
      throw new Error(req.t("validation:invalidEmailAddressDeletion"));
    }
    return true;
  }),

  check("nhsNumber")
    .optional({ checkFalsy: true })
    .custom((value, { req }) => {
      if (!validateNhsNumber(value)) {
        throw new Error(req.t("validation:invalidNhsNumber"));
      }
      if (value.length > 10) {
        throw new Error(req.t("validation:invalidNhsNumber"));
      }
      return true;
    }),
];

export { validate };
