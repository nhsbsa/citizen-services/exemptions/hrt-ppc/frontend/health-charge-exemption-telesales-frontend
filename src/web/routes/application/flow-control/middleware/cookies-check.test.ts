import expect from "expect";
import * as sinon from "sinon";
import { checkBrowserCookie } from "./cookies-check";

test("checkBrowserCookie() should redirect if browser cookie is not set", () => {
  const redirect = sinon.spy();
  const req = {
    cookies: {},
  };
  const res = {
    redirect,
    end: sinon.spy(),
  };
  const next = sinon.spy();
  checkBrowserCookie(req, res, next);
  expect(redirect.called).toBe(true);
});

test("checkBrowserCookie() should call next() if browser cookie is set", () => {
  const req = {
    cookies: {
      isBrowserCookieEnabled: true,
    },
  };
  const res = {};
  const next = sinon.spy();
  checkBrowserCookie(req, res, next);
  expect(next.called).toBe(true);
});
