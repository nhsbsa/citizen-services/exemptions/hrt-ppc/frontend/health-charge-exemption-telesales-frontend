module.exports = {
  ...require("./hrt-search"),
  ...require("./citizen-api"),
  ...require("./certificate-api"),
  ...require("./payment-api"),
  ...require("./note-api"),
  ...require("./reissue-api"),
  ...require("./issue-certificate-api"),
  ...require("./card-payment-api"),
  ...require("./preference-api"),
  ...require("./composition-api"),
  ...require("./wiremock"),
};
