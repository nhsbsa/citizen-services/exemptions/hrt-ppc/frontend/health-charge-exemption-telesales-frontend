import { check } from "express-validator";
import { translateValidationMessage } from "../common/translate-validation-message";
import {
  addDateToBody,
  validateDateOfBirth,
  validateDateOfBirthDay,
  validateDateOfBirthMonth,
  validateDateOfBirthYear,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/date-of-birth";
import { isNilOrEmpty } from "../../../../../common/predicates";
import {
  DATE_OF_BIRTH_DAY_KEY,
  DATE_OF_BIRTH_MONTH_KEY,
  DATE_OF_BIRTH_YEAR_KEY,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";

const lastNameMaxLength = 50;
const namePattern =
  /^(?!.*[\u00D7\u00F7])[A-Za-z\u1E00-\u1EFF\u0100-\u017F\u00c0-\u00ff\s\-']*$/;

const UK_POSTCODE_PATTERN =
  /^$|(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$/;

const checkPostcodeIsValid = (_, { req }) =>
  UK_POSTCODE_PATTERN.test(req.body.sanitizedPostcode);

const checkForInput = (_, { req }) => {
  if (
    isNilOrEmpty(req.body.lastName) &&
    isNilOrEmpty(req.body.dateOfBirth) &&
    isNilOrEmpty(req.body.postcode)
  ) {
    return false;
  }
  return true;
};

const validate = () => [
  check("lastName")
    .isLength({ max: lastNameMaxLength })
    .bail()
    .withMessage(translateValidationMessage("validation:lastNameTooLong"))
    .matches(namePattern)
    .bail()
    .withMessage(translateValidationMessage("validation:patternLastName"))
    .bail(),

  addDateToBody,
  check("dateOfBirth").custom(validateDateOfBirth),
  check(DATE_OF_BIRTH_DAY_KEY).custom(validateDateOfBirthDay),
  check(DATE_OF_BIRTH_MONTH_KEY).custom(validateDateOfBirthMonth),
  check(DATE_OF_BIRTH_YEAR_KEY).custom(validateDateOfBirthYear),

  check("postcode")
    .custom(checkPostcodeIsValid)
    .bail()
    .withMessage(translateValidationMessage("validation:invalidPostcode")),

  check("lastName")
    .custom(checkForInput)
    .withMessage(translateValidationMessage("validation:missingLastName")),

  check("dateOfBirth")
    .custom(checkForInput)
    .withMessage(translateValidationMessage("validation:missingDateOfBirth")),

  check("postcode")
    .custom(checkForInput)
    .withMessage(translateValidationMessage("validation:missingPostcode")),
];
export {
  namePattern,
  lastNameMaxLength,
  UK_POSTCODE_PATTERN,
  checkPostcodeIsValid,
  checkForInput,
  validate,
};
