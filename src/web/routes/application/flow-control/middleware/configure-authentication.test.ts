import { configureAuthentication } from "./configure-authentication";
import { config } from "../../../../../config";

const next = jest.fn();
const res = {
  redirect: jest.fn(),
};
const req = {
  session: {},
};

jest.mock("../../../../../common/azure-utils", () => ({
  isUserAuthenticated: jest.fn(),
  getUserCypher: jest.fn(),
}));

beforeEach(() => {
  res.redirect.mockClear();
  req.session = {};
  next.mockClear();
  jest.clearAllMocks();
});

describe("configureAuthentication when USE_AUTHENTICATION flag is set to true", () => {
  test("should call next() if authenticated and next() is provided", () => {
    config.environment.USE_AUTHENTICATION = true;
    config.server.CONTEXT_PATH = "/test-context";

    const isUserAuthenticated = jest
      .requireMock("../../../../../common/azure-utils")
      .isUserAuthenticated.mockReturnValue(true);

    jest
      .requireMock("../../../../../common/azure-utils")
      .getUserCypher.mockReturnValue("mockCypher");

    configureAuthentication(req, res, next);

    expect(isUserAuthenticated).toHaveBeenCalledWith(req);
    expect(req.session["userCypher"]).toBe("mockCypher");
    expect(req.session["isAuthenticated"]).toBeUndefined();
    expect(next).toHaveBeenCalled();
  });

  test("should not call next() if authenticated and next() is not provided", () => {
    config.environment.USE_AUTHENTICATION = true;
    config.server.CONTEXT_PATH = "/test-context";

    const isUserAuthenticated = jest
      .requireMock("../../../../../common/azure-utils")
      .isUserAuthenticated.mockReturnValue(true);

    jest
      .requireMock("../../../../../common/azure-utils")
      .getUserCypher.mockReturnValue("mockCypher");

    configureAuthentication(req, res);

    expect(isUserAuthenticated).toHaveBeenCalledWith(req);
    expect(req.session["userCypher"]).toBe("mockCypher");
    expect(req.session["isAuthenticated"]).toBeUndefined();
    expect(next).not.toHaveBeenCalled();
  });

  test("should call redirect() if user is not authenticated", () => {
    config.environment.USE_AUTHENTICATION = true;
    config.server.CONTEXT_PATH = "/test-context";

    const isUserAuthenticated = jest
      .requireMock("../../../../../common/azure-utils")
      .isUserAuthenticated.mockReturnValue(false);

    configureAuthentication(req, res, next);

    expect(isUserAuthenticated).toHaveBeenCalledWith(req);
    expect(res.redirect).toHaveBeenCalledWith("/test-context/auth/signin");
    expect(req.session["userCypher"]).toBeUndefined();
    expect(req.session["isAuthenticated"]).toEqual(false);
    expect(next).not.toHaveBeenCalled();
  });
});

describe("configureAuthentication when USE_AUTHENTICATION flag is set to false", () => {
  test("should call next() and sets cypher as local user when next() is provided", () => {
    config.environment.USE_AUTHENTICATION = false;

    configureAuthentication(req, res, next);

    expect(req.session["userCypher"]).toBe("LOCAL_USER_CYPHER");
    expect(req.session["isAuthenticated"]).toBeUndefined();
    expect(next).toHaveBeenCalled();
    expect(res.redirect).not.toHaveBeenCalled();
  });

  test("should not call next() and sets cypher as local user when next() is not provided", () => {
    config.environment.USE_AUTHENTICATION = false;

    configureAuthentication(req, res);

    expect(req.session["userCypher"]).toBe("LOCAL_USER_CYPHER");
    expect(req.session["isAuthenticated"]).toBeUndefined();
    expect(next).not.toBeCalled();
    expect(res.redirect).not.toHaveBeenCalled();
  });
});
