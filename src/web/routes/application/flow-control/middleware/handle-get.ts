import { isUndefined } from "../../../../../common/predicates";
import { logger } from "../../../../logger/logger";
import { stateMachine } from "../state-machine";
import { COMPLETED } from "../states";

const handleStateDestroySession = (journey) => (req, res, next) => {
  if (!isUndefined(req.session.journeys)) {
    stateMachine.setState(COMPLETED, req, journey);
    handleSessionDestruction(req);
  }
  next();
};

const handleSessionDestruction = (req) => {
  req.session.destroy(sessionDestroyCallback);
};

const sessionDestroyCallback = () => (req) => {
  logger.debug("Session has been destroyed", req);
};

export {
  handleStateDestroySession,
  handleSessionDestruction,
  sessionDestroyCallback,
};
