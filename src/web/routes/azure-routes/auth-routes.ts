import express from "express";
import * as httpStatus from "http-status-codes";
import * as msal from "@azure/msal-node";
import { redirectToAuthCodeUrl } from "./auth";
import { wrapError } from "../../routes/application/errors";
import { config } from "../../../config";
import { environment } from "../../../config/environment";
import { logger } from "../../logger/logger";

const CONTEXT_PATH = process.env.CONTEXT_PATH || "";
const msalInstance = new msal.ConfidentialClientApplication(
  environment.MSALCONFIG,
);

const registerAuthRoutes = (app) => {
  const router = express.Router();
  app.use(router);

  registerSignIn(router);
  registerAuthRedirect(router);
  registerSignOut(router);
};

function registerSignOut(router) {
  router.get(`${CONTEXT_PATH}/auth/signout`, function (req, res) {
    const logoutUri = `${environment.MSALCONFIG.auth.authority}/oauth2/v2.0/logout?post_logout_redirect_uri=${environment.POST_LOGOUT_REDIRECT_URI}`;
    req.session.isAuthenticated = false;
    req.session.destroy(() => {
      res.redirect(logoutUri);
    });
  });
}

function registerAuthRedirect(router) {
  router.post(`${CONTEXT_PATH}/auth/redirect`, async function (req, res, next) {
    if (req.body.state) {
      const cryptoProvider = new msal.CryptoProvider();
      const state = JSON.parse(cryptoProvider.base64Decode(req.body.state));
      logger.debug(`Auth redirect session is: ${JSON.stringify(req.session)}`);
      logger.debug(`Auth redirect state is: ${JSON.stringify(state)}`);

      if (state.csrfToken === req.session.csrfToken) {
        req.session.authCodeRequest.code = req.body.code;
        req.session.authCodeRequest.codeVerifier =
          req.session.pkceCodes.verifier;

        try {
          const tokenResponse = await msalInstance.acquireTokenByCode(
            req.session.authCodeRequest,
          );
          logger.debug(`Auth redirect token: ${tokenResponse}`);

          req.session.accessToken = tokenResponse.accessToken;
          req.session.idToken = tokenResponse.idToken;
          req.session.account = tokenResponse.account;
          req.session.isAuthenticated = true;
          logger.info(`Logged in as: ${req.session.account.localAccountId}`);
          res.redirect(state.redirectTo);
        } catch (error) {
          next(error);
        }
      } else {
        next(
          wrapError({
            cause: new Error("csrf token does not match"),
            message: "csrf token does not match",
            statusCode: httpStatus.StatusCodes.FORBIDDEN,
          }),
        );
      }
    } else {
      next(
        wrapError({
          cause: new Error("state is missing"),
          message: "state is missing",
          statusCode: httpStatus.StatusCodes.FORBIDDEN,
        }),
      );
    }
  });
}

function registerSignIn(router) {
  router.get(`${CONTEXT_PATH}/auth/signin`, async function (req, res, next) {
    const cryptoProvider = new msal.CryptoProvider();
    req.session.csrfToken = cryptoProvider.createNewGuid();
    /**
     * The MSAL Node library allows you to pass your custom state as state parameter in the Request object.
     * The state parameter can also be used to encode information of the app's state before redirect.
     * You can pass the user's state in the app, such as the page or view they were on, as input to this parameter.
     */
    const state = cryptoProvider.base64Encode(
      JSON.stringify({
        csrfToken: req.session.csrfToken,
        redirectTo: config.server.CONTEXT_PATH,
      }),
    );
    logger.debug(`Azure sign-in state csrf: ${req.session.csrfToken}`);

    const authCodeUrlRequestParams = {
      state: state,
      /**
       * By default, MSAL Node will add OIDC scopes to the auth code url request. For more information, visit:
       * https://docs.microsoft.com/azure/active-directory/develop/v2-permissions-and-consent#openid-connect-scopes
       */
      scopes: ["openid", "profile"],
    };

    const authCodeRequestParams = {
      /**
       * By default, MSAL Node will add OIDC scopes to the auth code request. For more information, visit:
       * https://docs.microsoft.com/azure/active-directory/develop/v2-permissions-and-consent#openid-connect-scopes
       */
      scopes: ["openid", "profile"],
    };

    return await redirectToAuthCodeUrl(
      req,
      res,
      next,
      authCodeUrlRequestParams,
      authCodeRequestParams,
    );
  });
}

export {
  registerAuthRoutes,
  registerSignIn,
  registerAuthRedirect,
  registerSignOut,
};
