export default {
  ...require("./configure-get"),
  ...require("./configure-post"),
  ...require("./cookies-check"),
  ...require("./handle-post"),
  ...require("./handle-post-redirects"),
  ...require("./inject-headers"),
  ...require("./render-view"),
  ...require("./sanitize"),
  ...require("./session-details"),
  ...require("./handle-path-request"),
  ...require("./unescape-specific"),
  ...require("./custom-clear-data"),
};
