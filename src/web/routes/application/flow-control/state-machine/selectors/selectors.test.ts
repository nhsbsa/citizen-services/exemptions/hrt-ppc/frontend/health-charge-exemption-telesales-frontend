import expect from "expect";
import { getStepForPath, getNextInReviewPath } from "./selectors";
import { CHECK_ANSWERS_URL, CARD_PAYMENT_URL } from "../../../paths/paths";

const step1 = {
  path: "/first",
};

const step2 = {
  path: "/second",
};

const step3 = {
  path: "/third",
};

test("getNextNavigablePath() gets the next step in sequence of steps", () => {
  const steps = [step1, step2, step3];
  const result = getStepForPath("/first", steps);

  expect(result).toBe(step1);
});

test(`getNextInReviewPath() should return ${CHECK_ANSWERS_URL} url when req.path is not ${CHECK_ANSWERS_URL}`, () => {
  const req = {
    path: "/first",
  };
  const prefix = "/context-path";
  const result = getNextInReviewPath(req, prefix);

  expect(result).toBe(`${prefix}${CHECK_ANSWERS_URL}`);
});

test(`getNextInReviewPath() should return ${CARD_PAYMENT_URL} url when req.path is ${CHECK_ANSWERS_URL}`, () => {
  const prefix = "/context-path";
  const req = {
    path: `${prefix}${CHECK_ANSWERS_URL}`,
  };
  const result = getNextInReviewPath(req, prefix);

  expect(result).toBe(`${prefix}${CARD_PAYMENT_URL}`);
});
