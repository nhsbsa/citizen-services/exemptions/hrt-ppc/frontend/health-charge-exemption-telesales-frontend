import expect from "expect";
import * as sinon from "sinon";
import {
  sessionDestroy,
  sessionDestroyButKeepLoggedIn,
  deleteSessionExcept,
} from "./session-destroy";
import { IN_PROGRESS } from "../../flow-control/states";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";

const TELESALES = "telesales";

test(`sessionDestroy() should destroy the session and clear the cookie`, () => {
  const destroy = sinon.spy();
  const clearCookie = sinon.spy();

  const req = {
    path: "/second",
    session: {
      destroy,
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {
    clearCookie,
  };

  sessionDestroy(req, res);

  expect(destroy.called).toBe(true);
  expect(clearCookie.calledWith("lang")).toBe(true);
});

describe("deleteSessionExcept", () => {
  const req = {
    session: {
      param1: "value1",
      param2: "value2",
      param3: "value3",
    },
  };

  test("should delete the correct session parameters", () => {
    deleteSessionExcept(req, ["param1", "param2"]);

    expect(req.session.param1).toBeDefined();
    expect(req.session.param2).toBeDefined();
    expect(req.session.param3).toBeUndefined();
  });
});

describe("sessionDestroyButKeepLoggedIn", () => {
  const req = {
    session: {
      param1: "value1",
      cookie: "value2",
      csrfToken: "value3",
      pkceCodes: "value4",
      authCodeUrlRequest: "value5",
      authCodeRequest: "value6",
      accessToken: "value7",
      idToken: "value8",
      account: "value9",
      isAuthenticated: "value10",
      userCypher: "value10",
    },
  };

  test("should delete everything except session data required for auth", () => {
    sessionDestroyButKeepLoggedIn(req);

    expect(req.session).toEqual({
      cookie: "value2",
      csrfToken: "value3",
      pkceCodes: "value4",
      authCodeUrlRequest: "value5",
      authCodeRequest: "value6",
      accessToken: "value7",
      idToken: "value8",
      account: "value9",
      isAuthenticated: "value10",
      userCypher: "value10",
    });
  });
});
