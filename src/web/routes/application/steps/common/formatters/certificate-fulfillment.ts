import { cond, equals, always, T } from "ramda";
import { NONE } from "../../common/constants";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";

const formatCertificateFulfillment = (certificateFulfillment, translate) => {
  return cond([
    [equals(Preference.EMAIL), always(translate.preferencesText.email)],
    [equals(Preference.POSTAL), always(translate.preferencesText.post)],
    [equals(NONE), always(translate.preferencesText.none)],
    [
      T,
      () => {
        throw new Error(
          `A certificate fulfillment ${certificateFulfillment} is not in correct type format`,
        );
      },
    ],
  ])(certificateFulfillment);
};

export { formatCertificateFulfillment };
