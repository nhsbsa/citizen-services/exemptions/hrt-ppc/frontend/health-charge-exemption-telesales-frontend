import { postJsonData } from "../../request";
import { WIREMOCK_COMPOSITION_MAPPING_URL } from "../paths";
import {
  createGetAuditHistorySuccessMapping,
  SUCCESS,
} from "./mappings/mappings";

async function setupSuccessfulGetAuditHistoryMapping() {
  await postJsonData(
    WIREMOCK_COMPOSITION_MAPPING_URL,
    createGetAuditHistorySuccessMapping(SUCCESS),
  );
}

export { setupSuccessfulGetAuditHistoryMapping };
