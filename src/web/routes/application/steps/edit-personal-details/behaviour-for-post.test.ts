const mockUpdateCitizen = jest.fn();
const mockDeleteEmail = jest.fn();
const isEmpty = jest.fn();

import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { behaviourForPost } from "./behaviour-for-post";

jest.mock("./update-citizen", () => ({
  updateCitizen: mockUpdateCitizen,
}));

jest.mock("./delete-email", () => ({
  deleteEmail: mockDeleteEmail,
}));

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty,
  }),
}));

beforeEach(() => {
  jest.clearAllMocks();
  mockUpdateCitizen.mockResolvedValue("updated");
  mockDeleteEmail.mockResolvedValue("deleted");
});

describe("behaviourForPost", () => {
  test("calls next if validation errors exist", async () => {
    // Set return values for stubs
    isEmpty.mockReturnValue(false);

    const citizen = {
      addressLine1: "add1",
      addressLine2: "add2",
    };

    // Create copies of citizen object to ensure they are compared by value
    const req = {
      params: { id: "123" },
      session: { citizen: { ...citizen } },
      body: { addressLine1: "changed" },
    };
    const res = { locals: { citizen: { ...citizen } } };
    const next = jest.fn();

    await behaviourForPost()(req, res, next);

    expect(res.locals).toEqual({
      citizen: { addressLine1: "changed", addressLine2: "add2" },
    });
    expect(req).toEqual({
      body: { addressLine1: "changed" },
      params: { id: "123" },
      session: { citizen: { addressLine1: "add1", addressLine2: "add2" } },
    });
    expect(mockUpdateCitizen).not.toBeCalled();
    expect(mockDeleteEmail).not.toBeCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
  });

  test("calls next if validation passes then redirects to same page when no email in session", async () => {
    const mockRedirect = jest.fn();
    // Set return values for stubs
    isEmpty.mockReturnValue(true);

    const citizen = {
      id: "001",
      addressLine1: "add1",
      addressLine2: "add2",
    };

    // Create copies of citizen object to ensure they are compared by value
    const req = {
      params: { id: "123" },
      session: { citizen: { ...citizen } },
      body: { addressLine1: "changed" },
    };
    const res = {
      locals: {},
      redirect: mockRedirect,
    };
    const next = jest.fn();

    await behaviourForPost()(req, res, next);

    expect(res.locals).toEqual({});
    expect(req).toEqual({
      body: { addressLine1: "changed" },
      params: { id: "123" },
      session: {
        citizen: { id: "001", addressLine1: "changed", addressLine2: "add2" },
      },
    });
    expect(mockUpdateCitizen).toBeCalledTimes(1);
    expect(mockUpdateCitizen).toBeCalledWith(req, citizen.id);
    expect(mockDeleteEmail).not.toBeCalled();
    expect(next).not.toBeCalled();
    expect(mockRedirect).toBeCalledTimes(1);
    expect(mockRedirect).toBeCalledWith(
      "/test-context/exemption-information/123",
    );
  });

  test("calls next if validation passes then redirects to same page when email in session & user does not remove email", async () => {
    const mockRedirect = jest.fn();
    // Set return values for stubs
    isEmpty.mockReturnValue(true);

    const updatedEmail = "testchanged@mail.com";
    const emails = {
      emails: [
        {
          id: "123",
          emailAddress: "test@mail.com",
        },
      ],
    };
    const citizen = {
      id: "001",
      addressLine1: "add1",
      addressLine2: "add2",
      ...emails,
    };

    // Create copies of citizen object to ensure they are compared by value
    const req = {
      params: { id: "123" },
      session: { citizen: { ...citizen } },
      body: { addressLine1: "changed", emailAddress: updatedEmail },
    };
    const res = {
      locals: {},
      redirect: mockRedirect,
    };
    const next = jest.fn();

    await behaviourForPost()(req, res, next);

    expect(res.locals).toEqual({});
    expect(req).toEqual({
      body: { addressLine1: "changed", emailAddress: updatedEmail },
      params: { id: "123" },
      session: {
        citizen: {
          id: "001",
          addressLine1: "changed",
          addressLine2: "add2",
          emailAddress: updatedEmail,
          ...emails,
        },
      },
    });
    expect(mockUpdateCitizen).toBeCalledTimes(1);
    expect(mockUpdateCitizen).toBeCalledWith(req, citizen.id);
    expect(mockDeleteEmail).not.toBeCalled();
    expect(next).not.toBeCalled();
    expect(mockRedirect).toBeCalledTimes(1);
    expect(mockRedirect).toBeCalledWith(
      "/test-context/exemption-information/123",
    );
  });

  test("calls next if validation passes then redirects to same page when email in session & user removes email", async () => {
    const mockRedirect = jest.fn();
    // Set return values for stubs
    isEmpty.mockReturnValue(true);

    const emails = {
      emails: [
        {
          id: "123",
          emailAddress: "test@mail.com",
        },
      ],
    };
    const citizen = {
      id: "001",
      addressLine1: "add1",
      addressLine2: "add2",
      ...emails,
    };

    // Create copies of citizen object to ensure they are compared by value
    const req = {
      params: { id: "123" },
      session: { citizen: { ...citizen } },
      body: { addressLine1: "changed", emailAddress: undefined },
    };
    const res = {
      locals: {},
      redirect: mockRedirect,
    };
    const next = jest.fn();

    await behaviourForPost()(req, res, next);

    expect(res.locals).toEqual({});
    expect(req).toEqual({
      body: { addressLine1: "changed", emailAddress: undefined },
      params: { id: "123" },
      session: {
        citizen: {
          id: "001",
          addressLine1: "changed",
          addressLine2: "add2",
          emailAddress: undefined,
          ...emails,
        },
      },
    });
    expect(mockUpdateCitizen).toBeCalledTimes(1);
    expect(mockUpdateCitizen).toBeCalledWith(req, citizen.id);
    expect(mockDeleteEmail).toBeCalledTimes(1);
    expect(mockDeleteEmail).toBeCalledWith(req, emails.emails[0].id);
    expect(next).not.toBeCalled();
    expect(mockRedirect).toBeCalledTimes(1);
    expect(mockRedirect).toBeCalledWith(
      "/test-context/exemption-information/123",
    );
  });

  test("calls next with error if there is an error whilst updating citizen", async () => {
    const mockRedirect = jest.fn();
    // Set return values for stubs
    isEmpty.mockReturnValue(true);
    mockUpdateCitizen.mockRejectedValue("rejected");

    const emails = {
      emails: [
        {
          emailAddress: "test@mail.com",
        },
      ],
    };
    const citizen = {
      id: "001",
      addressLine1: "add1",
      addressLine2: "add2",
      ...emails,
    };

    // Create copies of citizen object to ensure they are compared by value
    const req = {
      path: "/path",
      params: { id: "123" },
      session: { citizen: { ...citizen } },
      body: { addressLine1: "changed" },
    };
    const res = {
      locals: { citizen: { ...citizen } },
      redirect: () => mockRedirect,
    };
    const next = jest.fn();

    await behaviourForPost()(req, res, next);

    expect(res.locals).toEqual({
      citizen: { ...citizen },
    });
    expect(req).toEqual({
      path: "/path",
      body: { addressLine1: "changed" },
      params: { id: "123" },
      session: {
        citizen: {
          id: "001",
          addressLine1: "changed",
          addressLine2: "add2",
          ...emails,
        },
      },
    });
    expect(mockUpdateCitizen).toBeCalledTimes(1);
    expect(mockUpdateCitizen).toBeCalledWith(req, citizen.id);
    expect(mockDeleteEmail).not.toBeCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(
      wrapError({
        cause: "rejected",
        message: `Error on post when updating citizen ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
  });

  test("calls next with error if there is an error whilst deleting email", async () => {
    const mockRedirect = jest.fn();
    // Set return values for stubs
    isEmpty.mockReturnValue(true);
    mockDeleteEmail.mockRejectedValue("rejected");

    const emails = {
      emails: [
        {
          id: "123",
          emailAddress: "test@mail.com",
        },
      ],
    };
    const citizen = {
      id: "001",
      addressLine1: "add1",
      addressLine2: "add2",
      ...emails,
    };

    // Create copies of citizen object to ensure they are compared by value
    const req = {
      path: "/path",
      params: { id: "123" },
      session: { citizen: { ...citizen } },
      body: { addressLine1: "changed" },
    };
    const res = {
      locals: { citizen: { ...citizen } },
      redirect: () => mockRedirect,
    };
    const next = jest.fn();

    await behaviourForPost()(req, res, next);

    expect(res.locals).toEqual({
      citizen: { ...citizen },
    });
    expect(req).toEqual({
      path: "/path",
      body: { addressLine1: "changed" },
      params: { id: "123" },
      session: {
        citizen: {
          id: "001",
          addressLine1: "changed",
          addressLine2: "add2",
          ...emails,
        },
      },
    });
    expect(mockUpdateCitizen).toBeCalledTimes(1);
    expect(mockUpdateCitizen).toBeCalledWith(req, citizen.id);
    expect(mockDeleteEmail).toBeCalledTimes(1);
    expect(mockDeleteEmail).toBeCalledWith(req, emails.emails[0].id);
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(
      wrapError({
        cause: "rejected",
        message: `Error on post when deleting email ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
  });
});
