"use strict";

import axios from "axios";
import * as httpStatus from "http-status-codes";

const apiInstance = axios.create({});

const getSIDCookie = (response) => {
  const value = response.headers["set-cookie"]
    .toString()
    .match("exemption-telesales.sid=(.*?);")[1];
  return {
    key: "exemption-telesales.sid",
    value,
  };
};

const getCSRFCookie = (response) => {
  const value = response.headers["set-cookie"]
    .toString()
    .match("nhsbsa.x-csrf-token=(.*?);")[1];
  return {
    key: "nhsbsa.x-csrf-token",
    value,
  };
};

const getCSRFToken = (body) => body.match('"_csrf" value="(.*)"')[1];

const cookie = "isBrowserCookieEnabled=true;";
const getSIDCookieAndCSRFToken = (url) =>
  new Promise<string | any>((resolve, reject) => {
    const options = {
      url,
      headers: {
        Cookie: cookie,
      },
    };

    axios(options).then(
      (response) => {
        const sessionCookie = getSIDCookie(response);
        const csrfCookie = getCSRFCookie(response);
        resolve({
          sessionCookie: `${sessionCookie.key}=${sessionCookie.value}`,
          csrfCookie: `${csrfCookie.key}=${csrfCookie.value}`,
          csrfToken: getCSRFToken(response.data),
        });
      },
      (error) => {
        return reject(error);
      },
    );
  });

const postFormData = (url, form, sessionCookie, csrfCookie) =>
  new Promise<void>((resolve, reject) => {
    apiInstance
      .post(url, form, {
        maxRedirects: 0, // don't follow redirect because pa11y will do a page load on the next url
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Cookie: `${sessionCookie};isBrowserCookieEnabled=true;${csrfCookie}`,
        },
      })
      .then(
        () => {
          resolve();
        },
        (error) => {
          if (
            error.response.status === httpStatus.StatusCodes.MOVED_TEMPORARILY
          ) {
            resolve(error.response.headers.location);
          }
          return reject(error);
        },
      );
  });

const get = (url, cookie) =>
  new Promise((resolve, reject) => {
    axios
      .get(url, {
        headers: {
          Cookie: cookie,
        },
      })
      .then(
        (response) => {
          resolve(response.data);
        },
        (error) => {
          return reject(error);
        },
      );
  });

const postJsonData = (url, body) =>
  new Promise((resolve, reject) => {
    axios
      .post(url, body, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(
        () => {
          resolve(body);
        },
        (error) => {
          return reject(error);
        },
      );
  });

const performDelete = (url) =>
  new Promise<void>((resolve, reject) => {
    axios.delete(url).then(
      () => {
        resolve();
      },
      (error) => {
        return reject(error);
      },
    );
  });

export {
  getSIDCookieAndCSRFToken,
  postFormData,
  postJsonData,
  performDelete,
  get,
};
