import expect from "expect";
import { certificateClient } from "../../../../client/certificate-client";
import { productClient } from "../../../../client/product-client";
import { preferenceClient } from "../../../../client/preference-client";
import { wrapError } from "../../errors";
import { StatusCodes } from "http-status-codes";
import { Request, Response } from "express";
import { Session } from "express-session";
import { EXEMPTION_VIEW_URL } from "../../paths/paths";
import {
  Channel,
  Event,
  Preference,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/enums";
import { copyCertificateSessionDataToLocals } from "./edit-certificate-details";

const didValidationPass = jest.fn();
jest.mock("express-validator", () => ({
  validationResult: () => ({ isEmpty: didValidationPass }),
}));
jest.mock("../../../../client/certificate-client");
const patchCertificateRequestMock = jest.fn();
certificateClient.prototype.makeRequest = patchCertificateRequestMock;
jest.mock("../../../../client/product-client");
const endDateRequestMock = jest.fn();
productClient.prototype.makeRequest = endDateRequestMock;
jest.mock("../../../../client/preference-client");
const patchPreferenceRequestMock = jest.fn();
preferenceClient.prototype.makeRequest = patchPreferenceRequestMock;
const mockIsEligibleForEditing = jest.fn();
jest.mock("./../common/certificate-utils", () => ({
  isEligibleForEditing: mockIsEligibleForEditing,
}));
mockIsEligibleForEditing.mockReturnValue(true);
jest.mock("./edit-certificate-details", () => ({
  copyCertificateSessionDataToLocals: jest.fn((req, res) => {
    res.locals.certificateDetailsData = {};
  }),
}));
import { behaviourForPost } from "./behaviour-for-post";

const req = (
  {
    certificateId = "id",
    certificateStartDate = "2022-12-31",
    certificateFulfillment = Preference.EMAIL,
  } = {
    certificateId: "id",
    certificateStartDate: "2022-12-31",
    certificateFulfillment: Preference.EMAIL,
  },
): Request & { session: Session } =>
  ({
    path: "/test",
    session: {
      certificate: {
        id: certificateId,
        type: "certificate type",
        _meta: {
          channel: Channel.ONLINE,
        },
        preference: {
          id: "111",
          event: Event.ANY,
          preference: Preference.EMAIL,
        },
      },
    },
    params: {
      id: "HRT123",
    },
    body: {
      certificateStartDate,
      certificateFulfillment,
    },
  }) as unknown as Request;
const res = {
  locals: {},
  redirect: jest.fn(),
} as unknown as Response;
const next = jest.fn();

describe("behaviour for post", () => {
  beforeEach(() => {
    mockIsEligibleForEditing.mockReturnValue(true);
  });

  test("should invoke preference, product, certificate APIs and redirect when the start date is eligible for editing.", async () => {
    didValidationPass.mockReturnValue(true);
    endDateRequestMock.mockResolvedValue({ data: { endDate: "2024-11-30" } });
    patchCertificateRequestMock.mockResolvedValue({ data: "mockData" });
    const request = req({
      certificateId: "certificate-uuid",
      certificateStartDate: "2023-12-01",
      certificateFulfillment: Preference.EMAIL,
    });

    await behaviourForPost({})(request, res, next);

    expect(endDateRequestMock).toHaveBeenCalledTimes(1);
    expect(endDateRequestMock).toHaveBeenCalledWith({
      method: "GET",
      url: "v1/products/HRT_12m/end-date?startDate=2023-12-01",
    });
    expect(patchCertificateRequestMock).toHaveBeenCalledTimes(1);
    expect(patchCertificateRequestMock).toBeCalledWith(
      {
        method: "PATCH",
        url: "/v1/certificates/certificate-uuid",
        data: {
          startDate: "2023-12-01",
          endDate: "2024-11-30",
        },
        responseType: "json",
      },
      request.session,
    );
    expect(patchPreferenceRequestMock).toHaveBeenCalledTimes(1);
    expect(patchPreferenceRequestMock).toBeCalledWith(
      {
        method: "PATCH",
        url: "/v1/user-preference/111",
        data: {
          event: Event.ANY,
          preference: Preference.EMAIL,
        },
        responseType: "json",
      },
      request.session,
    );
    expect(res.redirect).toHaveBeenCalled();
    expect(res.redirect).toBeCalledWith(
      `/test-context${EXEMPTION_VIEW_URL}/HRT123`,
    );
  });

  test("should invoke preference API and redirect while bypassing calls to the products and certificate APIs when the start date is not eligible for editing.", async () => {
    didValidationPass.mockReturnValue(true);
    mockIsEligibleForEditing.mockReturnValue(false);
    endDateRequestMock.mockResolvedValue({ data: { endDate: "2024-11-30" } });
    patchCertificateRequestMock.mockResolvedValue({ data: "mockData" });
    const request = req({
      certificateId: "certificate-uuid",
      certificateStartDate: "2023-12-01",
      certificateFulfillment: Preference.EMAIL,
    });

    await behaviourForPost({})(request, res, next);

    expect(endDateRequestMock).not.toHaveBeenCalled();
    expect(patchCertificateRequestMock).not.toHaveBeenCalled();
    expect(patchPreferenceRequestMock).toHaveBeenCalledTimes(1);
    expect(patchPreferenceRequestMock).toBeCalledWith(
      {
        method: "PATCH",
        url: "/v1/user-preference/111",
        data: {
          event: Event.ANY,
          preference: Preference.EMAIL,
        },
        responseType: "json",
      },
      request.session,
    );
    expect(res.redirect).toBeCalledWith(
      `/test-context${EXEMPTION_VIEW_URL}/HRT123`,
    );
  });

  test("should not call certificate, products and preference APIs when there are validation errors", async () => {
    didValidationPass.mockReturnValue(false);

    await behaviourForPost({})(req(), res, next);

    expect(copyCertificateSessionDataToLocals).toHaveBeenCalledTimes(1);
    expect(copyCertificateSessionDataToLocals).toHaveBeenCalledWith(req(), res);
    expect(endDateRequestMock).not.toHaveBeenCalled();
    expect(patchCertificateRequestMock).not.toHaveBeenCalled();
    expect(patchPreferenceRequestMock).not.toHaveBeenCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });

  test("should copy submitted data into response locals when there are validation errors", async () => {
    didValidationPass.mockReturnValue(false);
    const request = req({ certificateStartDate: "2023-02-31" });
    request.body["certificateStartDate-day"] = "31";
    request.body["certificateStartDate-month"] = "02";
    request.body["certificateStartDate-year"] = "2023";

    await behaviourForPost({})(request, res, next);

    expect(res.locals).toMatchObject({
      certificateDetailsData: {
        certificateStartDate: "2023-02-31",
        "certificateStartDate-day": "31",
        "certificateStartDate-month": "02",
        "certificateStartDate-year": "2023",
        certificateFulfillment: Preference.EMAIL,
      },
    });
  });

  test("should return 500 when end date request fails", async () => {
    didValidationPass.mockReturnValue(true);
    patchPreferenceRequestMock.mockResolvedValue({});
    endDateRequestMock.mockRejectedValue(
      new Error("Axios error from end date query"),
    );

    await behaviourForPost({})(req(), res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(
      wrapError({
        cause: new Error("Axios error from end date query"),
        message: `Error posting /test`,
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  });

  test("should return 500 when certificate patch request fails", async () => {
    didValidationPass.mockReturnValue(true);
    patchPreferenceRequestMock.mockResolvedValue({});
    endDateRequestMock.mockResolvedValue({ data: { endDate: "2024-12-01" } });
    patchCertificateRequestMock.mockRejectedValue(
      new Error("Axios error from patch query"),
    );

    await behaviourForPost({})(req(), res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(
      wrapError({
        cause: new Error("Axios error from patch query"),
        message: `Error posting /test`,
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  });

  test("should return 500 when preference patch request fails", async () => {
    didValidationPass.mockReturnValue(true);
    patchPreferenceRequestMock.mockRejectedValue(
      new Error("Axios error from patch query"),
    );

    await behaviourForPost({})(req(), res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(
      wrapError({
        cause: new Error("Axios error from patch query"),
        message: `Error posting /test`,
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  });
});
