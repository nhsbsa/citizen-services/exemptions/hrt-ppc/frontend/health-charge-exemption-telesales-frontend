import {
  sanitizeAddress,
  sanitizeDateOfBirth,
  sanitizeEmailAddress,
} from "@nhsbsa/health-charge-exemption-common-frontend";

const sanitize = () => (req, res, next) => {
  req.body.firstName = req.body.firstName.trim();
  req.body.lastName = req.body.lastName.trim();
  req.body.nhsNumber = req.body.nhsNumber.trim();
  sanitizeAddress(req);
  sanitizeDateOfBirth(req);
  sanitizeEmailAddress(req);
  next();
};

export { sanitize };
