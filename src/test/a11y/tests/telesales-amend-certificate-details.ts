import { URLS } from "../paths";

const telesalesAmendCertificateJourney = [
  {
    url: URLS.SEARCH_HRT_RECORDS,
    formData: () => ({
      "dateOfBirth-day": "30",
      "dateOfBirth-month": "05",
      "dateOfBirth-year": "1920",
      dateOfBirth: "1920-05-30",
      lastName: "doe",
      postcode: "AA1 1AA",
    }),
  },
  {
    url: URLS.SEARCH_RESULTS,
  },
  {
    url: URLS.EXEMPTION_DETAIL,
  },
  {
    url: URLS.EDIT_CERTIFICATE_DETAILS,
    formData: () => {
      const [year, month, day] = new Date().toISOString().split(/[-T]/);
      return {
        "certificateStartDate-day": day,
        "certificateStartDate-month": month,
        "certificateStartDate-year": year,
      };
    },
  },
  {
    url: URLS.SEARCH_HRT_RECORDS,
  },
];

export { telesalesAmendCertificateJourney };
