/**
 * isUserAuthenticated(req) check if user is authenticated and redirects based on status
 * Cyper to be used while calling API's
 * call getUserCypher(req.session) to get cypher text for user when USE_AUTHENTICATION is enabled and set it into userCypher session variable.
 * set userCypher in session to LOCAL_USER_CYPHER when USE_AUTHENTICATION is disabled
 * **/

import { isUserAuthenticated } from "../../../../../common/azure-utils";
import { config } from "../../../../../config";
import { getUserCypher } from "../../../../../common/azure-utils";
import {
  SIGN_IN_URL,
  LOCAL_USER_CYPHER,
} from "../../../application/steps/common/constants";

const configureAuthentication = (req, res, next?) => {
  let authenticated;
  if (config.environment.USE_AUTHENTICATION) {
    authenticated = isUserAuthenticated(req);
    if (!authenticated) {
      // Set this in session before redirecting to azure so that the session cookie is set
      req.session.isAuthenticated = authenticated;
      return res.redirect(config.server.CONTEXT_PATH + SIGN_IN_URL);
    }
    req.session.userCypher = getUserCypher(req.session);
  } else {
    req.session.userCypher = LOCAL_USER_CYPHER;
  }
  if (next !== undefined) {
    next();
  }
};

export { configureAuthentication };
