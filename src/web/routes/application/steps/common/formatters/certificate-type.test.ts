import expect from "expect";
import { formatCertificateType } from "./certificate-type";
import { CertificateType } from "@nhsbsa/health-charge-exemption-common-frontend";

describe("formatCertificateType()", () => {
  test("throws error when type is undefined", () => {
    const result = () => formatCertificateType(undefined);

    expect(result).toThrowError(/A certificate type must be provided/);
  });

  test("replaces underscore with single space", () => {
    const result = formatCertificateType(CertificateType.HRT_PPC);

    expect(result).toEqual("HRT PPC");
  });
});
