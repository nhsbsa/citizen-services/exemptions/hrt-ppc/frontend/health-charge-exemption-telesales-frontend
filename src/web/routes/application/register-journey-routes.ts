import express, { NextFunction, Request, Response } from "express";
import nocache from "nocache";
import { registerCheckAnswersRoutes, registerCardPaymentRoutes } from "./steps";
import { configureGet } from "./flow-control/middleware/configure-get";
import { configurePost } from "./flow-control/middleware/configure-post";
import { configureSessionDetails } from "./flow-control/middleware/session-details";
import { handlePost } from "./flow-control/middleware/handle-post";
import { handleRequestForPath } from "./flow-control/middleware/handle-path-request";
import { handlePostRedirects } from "./flow-control/middleware//handle-post-redirects";
import { injectAdditionalHeaders } from "./flow-control/middleware/inject-headers";
import { renderView } from "./flow-control/middleware/render-view";
import { sanitize } from "./flow-control/middleware/sanitize";
import { escape } from "./flow-control/middleware/unescape-specific";
import { configureAuthentication } from "./flow-control/middleware/configure-authentication";

const middlewareNoop =
  () => (req: Request, res: Response, next: NextFunction) =>
    next();

export const handleOptionalMiddleware =
  (args) =>
  (operation, fallback = middlewareNoop) =>
    typeof operation === "undefined" ? fallback() : operation(...args);

const createRoute = (config, csrfProtection, journey, router) => (step) => {
  const { steps } = journey;
  // Make [config, journey, step] available as arguments to all optional middleware
  const optionalMiddleware = handleOptionalMiddleware([config, journey, step]);

  return router
    .route(step.path)
    .get(
      injectAdditionalHeaders,
      csrfProtection,
      configureSessionDetails(journey),
      configureAuthentication,
      configureGet(steps, step, journey, config),
      handleRequestForPath(journey, step),
      optionalMiddleware(step.behaviourForGet),
      renderView(step),
    )
    .post(
      injectAdditionalHeaders,
      csrfProtection,
      sanitize,
      escape(),
      configureSessionDetails(journey),
      configureAuthentication,
      configurePost(steps, step, journey),
      optionalMiddleware(step.sanitize),
      optionalMiddleware(step.validate),
      optionalMiddleware(step.behaviourForPost),
      handlePost(journey, step),
      handleRequestForPath(journey, step),
      handlePostRedirects(journey),
      renderView(step),
    );
};

export const registerJourneyRoutes =
  (config, csrfProtection, app) => (journey) => {
    const wizard = express.Router();
    journey.steps.forEach(createRoute(config, csrfProtection, journey, wizard));
    app.use(nocache());
    app.use(wizard);

    registerCheckAnswersRoutes(journey, app, csrfProtection, config);
    registerCardPaymentRoutes(journey, app, csrfProtection);
  };
