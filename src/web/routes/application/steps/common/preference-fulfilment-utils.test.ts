import {
  translateFulfilmentToText,
  deriveFulfilmentOnEvent,
} from "./preference-fulfilment-utils";
import {
  Event,
  Preference,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { formatCertificateFulfillment } from "./formatters/certificate-fulfillment";

jest.mock("../common/formatters/certificate-fulfillment", () => ({
  formatCertificateFulfillment: jest.fn(),
}));

afterEach(() => {
  jest.clearAllMocks();
});

describe("translateFulfilmentToText()", () => {
  test("translates and handles Event.ANY when preference is set to EMAIL", () => {
    const preference = Preference.EMAIL;
    const translate = "SomeTranslation";
    const mockFormattedValue = "Email";

    (formatCertificateFulfillment as jest.Mock).mockReturnValue(
      mockFormattedValue,
    );

    const result = translateFulfilmentToText(Event.ANY, preference, translate);

    expect(result).toEqual(mockFormattedValue);
    expect(formatCertificateFulfillment).toHaveBeenCalledWith(
      preference,
      translate,
    );
  });

  test("translates and handles Event.ANY when preference is set to POSTAL", () => {
    const preference = Preference.POSTAL;
    const translate = "SomeTranslation";
    const mockFormattedValue = "Post";

    (formatCertificateFulfillment as jest.Mock).mockReturnValue(
      mockFormattedValue,
    );

    const result = translateFulfilmentToText(Event.ANY, preference, translate);

    expect(result).toEqual(mockFormattedValue);
    expect(formatCertificateFulfillment).toHaveBeenCalledWith(
      preference,
      translate,
    );
  });

  test("translates and handles when Event is other than ANY", () => {
    const translate = "SomeTranslation";
    const mockFormattedValue = "MockFormattedValue";

    (formatCertificateFulfillment as jest.Mock).mockReturnValue(
      mockFormattedValue,
    );

    const result = translateFulfilmentToText(
      Event.REMINDER,
      Preference.EMAIL,
      translate,
    );

    expect(result).toEqual(mockFormattedValue);
    expect(formatCertificateFulfillment).toHaveBeenCalledWith(
      "NONE",
      translate,
    );
  });
});

describe("deriveFulfilmentOnEvent()", () => {
  test("handles Event.ANY without translation when preference is set to EMAIL", () => {
    const preference = Preference.EMAIL;
    const result = deriveFulfilmentOnEvent(Event.ANY, preference);

    expect(result).toEqual(preference);
    expect(formatCertificateFulfillment).not.toHaveBeenCalled();
  });

  test("handles Event.ANY without translation when preference is set to POSTAL", () => {
    const preference = Preference.POSTAL;
    const result = deriveFulfilmentOnEvent(Event.ANY, preference);

    expect(result).toEqual(preference);
    expect(formatCertificateFulfillment).not.toHaveBeenCalled();
  });

  test("handles without translation when Event is other than ANY", () => {
    const result = deriveFulfilmentOnEvent(Event.REMINDER, Preference.EMAIL);

    expect(result).toEqual("NONE");
    expect(formatCertificateFulfillment).not.toHaveBeenCalled();
  });
});
