/* eslint-disable @typescript-eslint/no-var-requires */
import expect from "expect";
import * as sinon from "sinon";
import * as states from "../states";
import {
  buildSessionForJourney,
  getNextAllowedPathForJourney,
} from "../test-utils/test-utils";

describe("handlePost() function", () => {
  afterEach(() => {
    jest.resetAllMocks();
    jest.resetModules();
  });

  test("should add errors and claim to locals if errors exist", () => {
    const errors = ["error"];

    jest.mock("express-validator", () => ({
      ...jest.requireActual("express-validator"),
      validationResult: () => ({
        isEmpty: () => false,
        array: () => errors,
      }),
    }));

    const { handlePost } = require("./handle-post");

    const steps = [
      { path: "/first", next: () => "/second" },
      { path: "/second" },
    ];
    const journey = { name: "telesales", steps };
    const step = steps[0];
    const claim = { firstName: "Joe", lastName: "Bloggs" };
    const req = { body: { lastName: "" } };
    const res = { locals: { claim, errors: {} } };
    const next = sinon.spy();

    handlePost(journey, step)(req, res, next);

    expect(res.locals.errors).toEqual(errors);
    expect(res.locals.claim).toEqual({ firstName: "Joe", lastName: "" });
    expect(next.called).toBe(true);
  });

  test("should add errors and custom form property to locals if errors exist", () => {
    const errors = ["error"];

    jest.mock("express-validator", () => ({
      ...jest.requireActual("express-validator"),
      validationResult: () => ({
        isEmpty: () => false,
        array: () => errors,
      }),
    }));

    const { handlePost } = require("./handle-post");

    const steps = [
      {
        path: "/first",
        next: () => "/second",
        customLocalsAndSessionFormProperty: () => "custom",
      },
      { path: "/second" },
    ];
    const journey = { name: "telesales", steps };
    const step = steps[0];
    const custom = { firstName: "Joe", lastName: "Bloggs" };
    const req = { body: { lastName: "" } };
    const res = { locals: { custom, errors: {} } };
    const next = sinon.spy();

    handlePost(journey, step)(req, res, next);

    expect(res.locals.errors).toEqual(errors);
    expect(res.locals.custom).toEqual({ firstName: "Joe", lastName: "" });
    expect(next.called).toBe(true);
  });

  test("adds claim body to the session if no errors exist", () => {
    jest.mock("express-validator", () => ({
      ...jest.requireActual("express-validator"),
      validationResult: () => ({
        isEmpty: () => true,
      }),
    }));
    const { handlePost } = require("./handle-post");

    const steps = [
      { path: "/first", next: () => "/second" },
      { path: "/second" },
    ];
    const journey = { name: "telesales", steps };
    const step = steps[0];
    const req = {
      session: {
        claim: {
          other: "claim data",
        },
      },
      body: {
        new: "claim data",
      },
    };

    const next = sinon.spy();

    const expected = {
      other: "claim data",
      new: "claim data",
    };

    handlePost(journey, step)(req, {}, next);
    expect(req.session.claim).toEqual(expected);
    expect(next.called).toBe(true);
  });

  test("adds custom form body to the session if no errors exist", () => {
    jest.mock("express-validator", () => ({
      ...jest.requireActual("express-validator"),
      validationResult: () => ({
        isEmpty: () => true,
      }),
    }));
    const { handlePost } = require("./handle-post");

    const steps = [
      {
        path: "/first",
        next: () => "/second",
        customLocalsAndSessionFormProperty: () => "custom",
      },
      { path: "/second" },
    ];
    const journey = { name: "telesales", steps };
    const step = steps[0];
    const req = {
      session: {
        custom: {
          other: "claim data",
        },
      },
      body: {
        new: "claim data",
      },
    };

    const next = sinon.spy();

    const expected = {
      other: "claim data",
      new: "claim data",
    };

    handlePost(journey, step)(req, {}, next);
    expect(req.session.custom).toEqual(expected);
    expect(next.called).toBe(true);
  });

  test("calls next() with error on error", () => {
    jest.mock("express-validator", () => {
      _: jest.requireActual("express-validator"),
        () => ({
          isEmpty: () => {
            throw new Error("error");
          },
          array: () => [],
        });
    });
    const { handlePost } = require("./handle-post");

    const steps = [
      { path: "/first", next: () => "/second" },
      { path: "/second" },
    ];
    const journey = { steps };
    const step = steps[0];
    const req = {
      session: {
        claim: {},
      },
      body: {},
    };

    const res = { locals: {} };
    const next = sinon.spy();

    handlePost(journey, step)(req, res, next);

    expect(next.calledWith(sinon.match.instanceOf(Error))).toBe(true);
  });

  test("adds next allowed step to session", () => {
    jest.mock("express-validator", () => ({
      ...jest.requireActual("express-validator"),
      validationResult: () => ({
        isEmpty: () => true,
      }),
    }));
    const { handlePost } = require("./handle-post");

    const steps = [
      { path: "/first", next: () => "/second" },
      { path: "/second" },
    ];
    const journey = {
      name: "telesales",
      steps,
      pathsInSequence: ["/first", "/second"],
    };
    const step = steps[0];

    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: "telesales",
          state: states.IN_PROGRESS,
          nextAllowedPath: "/first",
        }),
      },
      path: "/first",
    };

    const res = {};
    const next = () => {
      /* explicit empty function */
    };

    handlePost(journey, step)(req, res, next);

    expect(getNextAllowedPathForJourney("telesales", req)).toBe("/second");
  });

  test("should invalidate review if required by step", () => {
    const dispatch = jest.fn(() => {
      /*do nothing*/
    });
    const INVALIDATE_REVIEW = { INVALIDATE_REVIEW: "INVALIDATE_REVIEW" };
    const INCREMENT_NEXT_ALLOWED_PATH = {
      INCREMENT_NEXT_ALLOWED_PATH: "INCREMENT_NEXT_ALLOWED_PATH",
    };

    jest.mock("express-validator", () => ({
      ...jest.requireActual("express-validator"),
      validationResult: () => ({
        isEmpty: () => true,
      }),
    }));
    jest.mock("../state-machine", () => ({
      ...jest.requireActual("../state-machine"),
      stateMachine: { dispatch },
    }));
    jest.mock("../state-machine/actions", () => ({
      INVALIDATE_REVIEW,
      INCREMENT_NEXT_ALLOWED_PATH,
    }));
    const { handlePost } = require("./handle-post");

    const steps = [
      {
        path: "/first",
        next: () => "/second",
        shouldInvalidateReview: () => true,
      },
      { path: "/second" },
    ];
    const journey = { steps };
    const step = steps[0];

    const req = {
      session: {},
      path: "/first",
    };

    const res = {};
    const next = () => {
      /* explicit empty function */
    };

    handlePost(journey, step)(req, res, next);

    const expectedFirstCall = [
      { INVALIDATE_REVIEW: "INVALIDATE_REVIEW" },
      { path: "/first", session: { claim: {} } },
      { steps: steps },
    ];
    const expectedSecondCall = [
      { INCREMENT_NEXT_ALLOWED_PATH: "INCREMENT_NEXT_ALLOWED_PATH" },
      { path: "/first", session: { claim: {} } },
      { steps: steps },
    ];

    expect(dispatch.mock.calls.length).toBe(2);
    expect(dispatch.mock.calls[0]).toEqual(expectedFirstCall);
    expect(dispatch.mock.calls[1]).toEqual(expectedSecondCall);
  });
});
