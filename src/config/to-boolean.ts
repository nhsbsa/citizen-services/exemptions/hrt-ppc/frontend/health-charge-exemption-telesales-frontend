import { isNil } from "ramda";

const toBoolean = (value) =>
  isNil(value) ? false : value.toString().toLowerCase() === "true";

export { toBoolean };
