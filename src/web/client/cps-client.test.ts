import expect from "expect";
import * as AxiosLogger from "axios-logger";
import { cardPaymentsClient } from "./cps-client";

const config = {
  environment: {
    LOG_REQUESTS_AND_RESPONSES: true,
    OUTBOUND_API_TIMEOUT: 20000,
    CARD_PAYMENTS_API_URI: "baseURI",
    CARD_PAYMENTS_SERVICE_NAME: "hrttelephony",
  },
};

describe("cardPaymentsClient", () => {
  test("should set the correct configuration for apiInstance", () => {
    const { apiInstance } = new cardPaymentsClient(config);
    expect(apiInstance.defaults.timeout).toEqual(20000);
    expect(apiInstance.defaults.proxy).toEqual(false);
    expect(apiInstance.defaults.baseURL).toEqual(
      "baseURI/telephony-transactions/hrttelephony",
    );
    expect(apiInstance.defaults.headers.common).toEqual({
      Accept: "application/json, text/plain, */*",
    });
    expect(apiInstance.interceptors.request).toHaveProperty("handlers");
    expect(apiInstance.interceptors.request["handlers"].length).toBe(1);
    expect(
      apiInstance.interceptors.request["handlers"][0]["fulfilled"],
    ).toEqual(expect.any(Function));
    expect(apiInstance.interceptors.response).toHaveProperty("handlers");
    expect(apiInstance.interceptors.response["handlers"].length).toBe(1);
    expect(
      apiInstance.interceptors.response["handlers"][0]["fulfilled"],
    ).toEqual(AxiosLogger.responseLogger);
    expect(
      apiInstance.interceptors.response["handlers"][0]["rejected"],
    ).toEqual(AxiosLogger.errorLogger);
  });

  test("should set the correct default timeout for apiInstance", () => {
    const config = {
      environment: {},
    };
    const { apiInstance } = new cardPaymentsClient(config);
    expect(apiInstance.defaults.timeout).toEqual(30000);
  });

  test("should return the data into the response when creating payment", async () => {
    const api = new cardPaymentsClient(config);
    api.apiInstance.request = jest.fn().mockResolvedValue({ data: "data" });
    const response = await api.makeRequestCreatePayment({});
    expect(response).toEqual({ data: "data" });
  });
});
