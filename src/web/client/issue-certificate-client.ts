import axios, { AxiosInstance, AxiosResponse } from "axios";
import { addRequestAndLoggingInterceptors } from "./interceptors";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

export class issueCertificateClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL: appConfig.environment.ISSUE_CERTIFICATE_API_URI,
      headers: {
        common: {
          "x-api-key": appConfig.environment.ISSUE_CERTIFICATE_API_KEY,
          channel: Channel.STAFF,
        },
      },
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequest(config, req) {
    config.headers = {
      "user-id": req.session.userCypher,
      "correlation-id": req.session.locator,
    };

    const data: AxiosResponse<Exemptions, any> =
      await this.apiInstance.request(config);

    return data;
  }
}
