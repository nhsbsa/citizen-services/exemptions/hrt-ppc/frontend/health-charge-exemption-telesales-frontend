import expect from "expect";
import { toPounds } from "./currency";

test("toPounds() converts value in pence to pounds", () => {
  const expected = "4.25";
  const result = toPounds(425);
  expect(result).toBe(expected);
});
