import { EXEMPTION_VIEW_URL } from "../../paths/paths";
import {
  isNavigable,
  pageContent,
  behaviourForGet,
  exemptionDetail,
} from "./exemption-detail";

describe("isNavigable()", () => {
  it.each([[], undefined, ""])(
    "returns false when search results in session set to %p",
    (searchResult: any) => {
      const req = {
        params: { id: "HRT" },
        session: {
          searchResult: [searchResult],
        },
      };

      const result = isNavigable(req);

      expect(result).toBeFalsy();
    },
  );

  it.each([[{ reference: "HRT123" }]])(
    "returns true when search results in session set to %p",
    (searchResult: any) => {
      const req = {
        params: { id: "HRT123" },
        session: {
          searchResult: [searchResult],
        },
      };

      const result = isNavigable(req);

      expect(result).toBeTruthy();
    },
  );
});

describe("exemptionDetail", () => {
  test(`exemptionDetail should match expected outcomes`, () => {
    const expectedResults = {
      path: EXEMPTION_VIEW_URL + `/:id(HRT[A-Za-z0-9]+)/`,
      template: "exemption-detail",
      pageContent,
      isNavigable,
      behaviourForGet,
    };

    expect(exemptionDetail).toEqual(expectedResults);
  });
});
