import axios, { AxiosInstance, AxiosResponse } from "axios";
import { addRequestAndLoggingInterceptors } from "./interceptors";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

export class noteClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL: appConfig.environment.NOTES_API_URI,
      headers: {
        common: {
          "x-api-key": appConfig.environment.NOTES_API_KEY,
          channel: Channel.STAFF,
        },
      },
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequest(config, session) {
    config.headers = {
      "user-id": session.userCypher,
      "correlation-id": session.locator,
    };

    const data: AxiosResponse = await this.apiInstance.request(config);

    return data;
  }
}
