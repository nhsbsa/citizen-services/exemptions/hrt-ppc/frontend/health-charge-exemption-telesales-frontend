const mockIsEligibleForEditing = jest.fn();
import expect from "expect";
import {
  validate,
  validateCertificateStartDate,
  addDateToBody,
} from "./validate";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/test/apply-express-validation";
import { FieldValidationError } from "express-validator";
import { Request } from "express";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";

jest.mock("./../common/certificate-utils", () => ({
  isEligibleForEditing: mockIsEligibleForEditing,
}));

const res = jest.fn();
const next = jest.fn();

describe("validate", () => {
  let req: Request & { session: any };

  beforeEach(() => {
    req = {
      body: {},
      session: {
        certificate: {},
        citizen: {
          emails: [
            {
              emailAddress: "test@gmail.com",
            },
          ],
        },
      },
      t: (string) => string,
    } as unknown as Request & { session: any };
    mockIsEligibleForEditing.mockReturnValue(false);
  });

  function setRequestBodyDate(year: string, month: string, day: string) {
    req.body["certificateStartDate-day"] = day;
    req.body["certificateStartDate-month"] = month;
    req.body["certificateStartDate-year"] = year;
  }

  function setRequestBodyPreference(preference: string) {
    req.body.certificateFulfillment = preference;
  }

  test.each([
    ["2023-06-15", "2023-06-15"], // same day
    ["2023-04-01", "2023-04-30"], // up to a month in the past
    ["2023-05-31", "2023-06-30"], // up to a month in the future
  ])(
    "a start date of %s should be valid for certificates applied for on %s",
    (startDate: string, applicationDate: string) => {
      req.session.certificate.applicationDate = applicationDate;
      expect(validateCertificateStartDate(startDate, { req })).toBe(true);
    },
  );

  test.each([
    ["2023-03-31", "2023-04-01"], // before April 2023
    ["2023-04-01", "2023-05-02"], // more than a month before application
    ["2023-05-31", "2023-07-01"], // more than a month after application
  ])(
    "a start date of %s should be invalid (out of range) for certificates applied for on %s",
    (startDate: string, applicationDate: string) => {
      req.session.certificate.applicationDate = applicationDate;
      expect(() =>
        validateCertificateStartDate(startDate, { req }),
      ).toThrowError(/validation:startDateInvalidRange/);
    },
  );

  test("should throw a missing error for the empty string", () => {
    expect(() => validateCertificateStartDate("", { req })).toThrowError(
      /validation:missingStartDate/,
    );
  });

  test.each([
    "2023-02-29",
    "2023-02-299",
    "2023-10-EE",
    "2023-EE-10",
    "EEEE-10-10",
    "2023-EE-EE",
    "EEEE-10-EE",
    "EEEE-EE-10",
    "EEEE-EE-EE",
    "EE/10/2023",
    "10/EE/2023",
    "10/10/EEEE",
  ])("should throw a not real error for %s", (date: string) => {
    expect(() => validateCertificateStartDate(date, { req })).toThrowError(
      /validation:startDateNotReal/,
    );
  });

  test("verify that the custom check for real dates is being called when start date is eligible for editing.", async () => {
    mockIsEligibleForEditing.mockReturnValue(true);
    setRequestBodyDate("2023", "02", "30");
    setRequestBodyPreference(Preference.EMAIL);
    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("certificateStartDate");
    expect(errorOne.msg).toBe("validation:startDateNotReal");
  });

  test("verify that the custom check for real dates is not being called when start date is not eligible for editing.", async () => {
    setRequestBodyDate("2023", "02", "30");
    setRequestBodyPreference(Preference.EMAIL);
    const result = await applyExpressValidation(req, validate());

    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }
    expect(result.array().length).toBe(0);
  });

  it.each([null, undefined, ""])(
    "returns error when preference is set to %p",
    async (value: any) => {
      req.session.certificate.applicationDate = "2023-04-15";
      setRequestBodyDate("2023", "04", "15");
      setRequestBodyPreference(value);

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      const error = result.array()[0] as FieldValidationError;
      expect(result.array().length).toBe(1);
      expect(error.path).toBe("certificateFulfillment");
      expect(error.msg).toBe("validation:missingPreference");
    },
  );

  test("verify that the preference is set to Email and emailAddress is present in citizen object", async () => {
    req.session.certificate.applicationDate = "2023-04-15";
    req.session.citizen.emails[0] = undefined;
    req.body.certificateFulfillment = Preference.EMAIL;
    setRequestBodyDate("2023", "04", "15");
    setRequestBodyPreference(Preference.EMAIL);
    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("certificateFulfillment");
    expect(errorOne.msg).toBe(
      "validation:missingEmailAddressForEmailPrefrence",
    );
  });

  test("no validation error when all fields are valid", async () => {
    req.session.certificate.applicationDate = "2023-04-15";
    setRequestBodyDate("2023", "04", "15");
    setRequestBodyPreference(Preference.EMAIL);
    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }
    expect(result.array()).toEqual([]);
  });

  test("should add certificateStartDate to req.body when start date is eligible for editing", () => {
    mockIsEligibleForEditing.mockReturnValue(true);
    setRequestBodyDate("2023", "04", "15");
    addDateToBody(req, res, next);

    expect(req.body.certificateStartDate).toEqual("2023-04-15");
    expect(next).toHaveBeenCalled();
  });

  test("should not add certificateStartDate to req.body when start date is not eligible for editing", () => {
    addDateToBody(req, res, next);

    expect(req.body.certificateStartDate).toBeUndefined();
    expect(next).toHaveBeenCalled();
  });
});
