import { registerNoAccessError } from "./no-access";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";

const NO_ACCESS_URL = "/test-context/no-access";

const req = { t: (string) => string };
const app = {
  ...jest.requireActual("express"),
  get: jest.fn(),
  all: jest.fn(),
};
const res = { render: jest.fn() };

test(`registerNoAccessError() should redirect to ${NO_ACCESS_URL}`, () => {
  app.all.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res);
  });

  registerNoAccessError(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(
    1,
    NO_ACCESS_URL,
    configureAuthentication,
    expect.any(Function),
  );
});
