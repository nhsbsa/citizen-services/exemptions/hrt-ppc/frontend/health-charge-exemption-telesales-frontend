import session, { CookieOptions, SessionOptions } from "express-session";
import { path } from "ramda";
import { logger } from "../../logger/logger";
import { COOKIE_EXPIRES_MILLISECONDS } from "./cookie-settings";
import { createClient } from "redis";
import RedisStore from "connect-redis";
import { toBoolean } from "../../../config/to-boolean";

const useRedis = process.env.USE_REDIS || false;

const onClientError = (error) => {
  logger.error(`Error with redis session: ${error}`);
};

const onClientConnection = () => () => {
  logger.info("Redis client connected");
};

/**
 * By default the node_redis client will auto-reconnect when a connection is lost
 * but requests may come in during that time.
 */
const ensureSession = (req, res, next) => {
  if (!req.session) {
    const err = new Error("No session found");
    return next(err);
  }
  next();
};

const getSessionConfig = (config) => {
  const unsecureCookie =
    path(["environment", "USE_UNSECURE_COOKIE"], config) === true;
  const cookie: CookieOptions = {
    /**
     * Setting none for sameSite required for azure
     * https://learn.microsoft.com/en-us/azure/active-directory/develop/howto-handle-samesite-cookie-changes-chrome-browser
     */
    sameSite: unsecureCookie ? undefined : "none",
    secure: unsecureCookie ? false : true,
    maxAge: COOKIE_EXPIRES_MILLISECONDS,
  };

  const sessionConfig: SessionOptions = {
    secret: config.server.SESSION_SECRET,
    saveUninitialized: false,
    resave: false,
    rolling: true,
    name: config.server.SESSION_ID_NAME,
    cookie: cookie,
  };

  logger.info(`Using secure cookie: ${cookie.secure}`);

  return sessionConfig;
};

const initialiseSession = (onConnectCallback, config, app) => {
  const sessionConfig = getSessionConfig(config);
  if (toBoolean(useRedis) === true) {
    const client = createClient(config.redis);
    const store = new RedisStore({ client });
    (sessionConfig as any).store = store;

    (async () => {
      await client.connect();
    })();

    client.on("connect", onClientConnection());
    client.on("error", onClientError);
  }

  if (sessionConfig.cookie && sessionConfig.cookie.secure) {
    app.set("trust proxy", 1); // trust first proxy
  }

  app.use(session(sessionConfig));
  app.use(ensureSession);

  onConnectCallback();
};

export { ensureSession, getSessionConfig, initialiseSession };
