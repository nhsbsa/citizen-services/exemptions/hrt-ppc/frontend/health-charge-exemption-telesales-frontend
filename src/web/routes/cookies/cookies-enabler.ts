import { getErrorPageContent } from "../error-pages/error-page-content";

const getCookiesEnablerPage = (req, res) => {
  res.render("cookies-enabler", getErrorPageContent(req));
};

export { getCookiesEnablerPage };
