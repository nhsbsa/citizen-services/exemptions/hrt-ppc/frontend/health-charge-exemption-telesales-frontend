const mockFormatDateForDisplayFromDate = jest.fn();

import {
  getSectionFromEntity,
  formatAuditDescription,
  formatAuditType,
  formatAuditFromAndTo,
} from "./get-audit-detail-rows-utils";
import { Section } from "./enums";

jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  formatDateForDisplayFromDate: mockFormatDateForDisplayFromDate,
}));

import { formatCertificateFulfillment } from "../common/formatters/certificate-fulfillment";
import {
  PaymentStatus,
  Preference,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/enums";

jest.mock("../common/formatters/certificate-fulfillment", () => ({
  formatCertificateFulfillment: jest.fn(),
}));

describe("getSectionFromEntity", () => {
  test(`should return ${Section.CITIZEN_SECTION} for a valid citizen entity`, () => {
    const entity = "uk.nhs.nhsbsa.exemptions.citizen";
    expect(getSectionFromEntity(entity)).toBe(Section.CITIZEN_SECTION);
  });

  test(`should return ${Section.CERTIFICATE_SECTION} for a valid certificate entity`, () => {
    const entity = "uk.nhs.nhsbsa.exemptions.certificate";
    expect(getSectionFromEntity(entity)).toBe(Section.CERTIFICATE_SECTION);
  });

  test(`should return ${Section.CERTIFICATE_SECTION} for a valid preference entity`, () => {
    const entity = "uk.nhs.nhsbsa.exemptions.user.preference";
    expect(getSectionFromEntity(entity)).toBe(Section.CERTIFICATE_SECTION);
  });

  test(`should return ${Section.PAYMENT_SECTION} for a valid payment entity`, () => {
    const entity = "uk.nhs.nhsbsa.exemptions.payment";
    expect(getSectionFromEntity(entity)).toBe(Section.PAYMENT_SECTION);
  });

  test("should throw an error for an invalid entity", () => {
    const entity = "invalid.entity";
    expect(() => getSectionFromEntity(entity)).toThrow(
      `Unknown entity: ${entity}`,
    );
  });
});

describe("formatAuditType", () => {
  test(`should return the translation for ${Section.CITIZEN_SECTION}`, () => {
    const entity = "uk.nhs.nhsbsa.exemptions.citizen";
    const translate = {
      [Section.CITIZEN_SECTION]: "Citizen Translation",
    };

    const result = formatAuditType(entity, translate);

    expect(result).toBe("Citizen Translation");
  });

  test(`should return the translation for ${Section.CERTIFICATE_SECTION}`, () => {
    const entity = "uk.nhs.nhsbsa.exemptions.certificate";
    const translate = {
      [Section.CERTIFICATE_SECTION]: "Certificate Translation",
    };

    const result = formatAuditType(entity, translate);

    expect(result).toBe("Certificate Translation");
  });

  test(`should return the translation for ${Section.PAYMENT_SECTION}`, () => {
    const entity = "uk.nhs.nhsbsa.exemptions.payment";
    const translate = {
      [Section.PAYMENT_SECTION]: "Payment Translation",
    };

    const result = formatAuditType(entity, translate);

    expect(result).toBe("Payment Translation");
  });
});

describe("formatAuditDescription", () => {
  test("should return the update description when toValue is not null", () => {
    const entity = "uk.nhs.nhsbsa.exemptions.citizen";
    const fieldName = "fieldName";
    const toValue = "some value";
    const translate = {
      updated: "Updated",
      citizen: {
        fieldName: "Field Name Translation",
      },
    };

    const result = formatAuditDescription(
      entity,
      fieldName,
      toValue,
      translate,
    );

    expect(result).toBe("Updated Field Name Translation");
  });

  it.each(["", null, undefined])(
    "should return the delete description when toValue is %p",
    async (toValue) => {
      const entity = "uk.nhs.nhsbsa.exemptions.certificate";
      const fieldName = "fieldName";
      const translate = {
        removed: "Removed",
        certificate: {
          fieldName: "Field Name Translation",
        },
      };

      const result = formatAuditDescription(
        entity,
        fieldName,
        toValue,
        translate,
      );

      expect(result).toBe("Removed Field Name Translation");
    },
  );
});

describe("formatAuditFromAndTo", () => {
  test("should return a formatted date string for a valid date input", () => {
    const value = "2023-10-05T12:34:56Z";
    const formattedDate = "05/10/2023";
    const entity = "uk.nhs.nhsbsa.exemptions.certificate";
    const translate = {};
    mockFormatDateForDisplayFromDate.mockReturnValue(formattedDate);

    const result = formatAuditFromAndTo(value, entity, translate);

    expect(mockFormatDateForDisplayFromDate).toHaveBeenCalled();
    expect(mockFormatDateForDisplayFromDate).toHaveBeenCalledWith(
      new Date(value),
    );
    expect(result).toBe(formattedDate);
  });

  test("should return a formatted value for a entity which includes preference", () => {
    const value = Preference.EMAIL;
    const entity = "uk.nhs.nhsbsa.exemptions.user.preference";
    const translate = {};
    (formatCertificateFulfillment as jest.Mock).mockReturnValue("Email");

    const result = formatAuditFromAndTo(value, entity, translate);

    expect(result).toBe("Email");
    expect(mockFormatDateForDisplayFromDate).not.toHaveBeenCalled();
  });

  test("should return the value with class when entity includes payment and value is failed", () => {
    const value = PaymentStatus.FAILED;
    const entity = "uk.nhs.nhsbsa.exemptions.payment";
    const translate = {};

    const result = formatAuditFromAndTo(value, entity, translate);

    expect(result).toBe(
      `<span class="hce-telesales-error-color"> ${value} </span>`,
    );
    expect(mockFormatDateForDisplayFromDate).not.toHaveBeenCalled();
  });

  test("should return the value when entity includes payment and value is non-failed", () => {
    const value = PaymentStatus.SUCCESS;
    const entity = "uk.nhs.nhsbsa.exemptions.payment";
    const translate = {};

    const result = formatAuditFromAndTo(value, entity, translate);

    expect(result).toBe(value);
    expect(mockFormatDateForDisplayFromDate).not.toHaveBeenCalled();
  });

  test("should return value as is if none of the conditions match", () => {
    const value = "someInput";
    const entity = "uk.nhs.nhsbsa.exemptions.certificate";
    const translate = {};

    const result = formatAuditFromAndTo(value, entity, translate);

    expect(result).toBe(value);
    expect(mockFormatDateForDisplayFromDate).not.toHaveBeenCalled();
  });
});
