import expect from "expect";
const mockSanitizeAddress = jest.fn();
const mockSanitizeEmailAddress = jest.fn();
const mockSanitizeDateOfBirth = jest.fn();
jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  sanitizeAddress: mockSanitizeAddress,
  sanitizeDateOfBirth: mockSanitizeDateOfBirth,
  sanitizeEmailAddress: mockSanitizeEmailAddress,
}));
import { sanitize } from "./sanitize";

const next = jest.fn();

const request = {
  body: {
    firstName: "John",
    lastName: "Smith",
    nhsNumber: "4857773457",
  },
};
describe("sanitize()", () => {
  test("calls sanitizeDateOfBirth, sanitizeEmailAddress and sanitizeAddress", () => {
    sanitize()(request, {}, next);

    expect(mockSanitizeAddress).toHaveBeenCalledTimes(1);
    expect(mockSanitizeAddress).toHaveBeenCalledWith(request);
    expect(mockSanitizeDateOfBirth).toHaveBeenCalledTimes(1);
    expect(mockSanitizeDateOfBirth).toHaveBeenCalledWith(request);
    expect(mockSanitizeEmailAddress).toHaveBeenCalledTimes(1);
    expect(mockSanitizeEmailAddress).toHaveBeenCalledWith(request);
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
  });

  test("trims starting and ending spaces", () => {
    const req = {
      body: {
        firstName: "   John   ",
        lastName: "   Smith   ",
        nhsNumber: "   4857773457   ",
      },
    };

    sanitize()(req, {}, next);

    expect(req.body).toEqual({
      firstName: "John",
      lastName: "Smith",
      nhsNumber: "4857773457",
    });
    expect(mockSanitizeAddress).toHaveBeenCalledTimes(1);
    expect(mockSanitizeAddress).toHaveBeenCalledWith(request);
    expect(mockSanitizeDateOfBirth).toHaveBeenCalledTimes(1);
    expect(mockSanitizeDateOfBirth).toHaveBeenCalledWith(request);
    expect(mockSanitizeEmailAddress).toHaveBeenCalledTimes(1);
    expect(mockSanitizeEmailAddress).toHaveBeenCalledWith(request);
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
  });
});
