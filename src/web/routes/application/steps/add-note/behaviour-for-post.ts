import { validationResult } from "express-validator";
import { noteClient } from "../../../../client/note-client";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { getNotesRows } from "./get-notes-rows";
import { contextPath } from "../../paths/context-path";
import { EXEMPTION_VIEW_URL } from "../../paths/paths";

const behaviourForPost = (config) => async (req, res, next) => {
  await getNotesRows(req, res);

  if (!validationResult(req).isEmpty()) {
    return next();
  }

  const certificateId = req.session.certificate.id;
  const citizenId = req.session.certificate.citizenId;

  try {
    await new noteClient(config).makeRequest(
      {
        method: "POST",
        url: `/v1/notes?citizenId=${citizenId}&certificateId=${certificateId}`,
        data: {
          description: req.body.description,
        },
        responseType: "json",
      },
      req.session,
    );

    res.redirect(contextPath(`${EXEMPTION_VIEW_URL}/${req.params.id}`));
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

export { behaviourForPost };
