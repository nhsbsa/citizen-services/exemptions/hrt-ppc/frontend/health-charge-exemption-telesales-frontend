import axios, { AxiosInstance, AxiosResponse } from "axios";
import { addRequestAndLoggingInterceptors } from "./interceptors";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

export interface AuditResponseEntity {
  content: ContentEntity[];
}
export interface ContentEntity {
  changes?: ChangesEntity[] | null;
  commitMetadata: CommitMetadata;
}
export interface ChangesEntity {
  changeType: string;
  globalId: GlobalId;
  commitMetadata: CommitMetadata;
  property: string;
  propertyChangeType: string;
  left: string | null;
  right: string | null;
}
export interface GlobalId {
  entity: string;
  cdoId: string;
}
export interface CommitMetadata {
  author: string;
  properties?: PropertiesEntity[] | null;
  commitDate: string;
  commitDateInstant: string;
  id: number;
}
export interface PropertiesEntity {
  key: string;
  value: string;
}

export class compositionClient {
  apiInstance: AxiosInstance;

  constructor(appConfig) {
    this.apiInstance = axios.create({
      proxy: false,
      timeout: parseInt(appConfig.environment.OUTBOUND_API_TIMEOUT || "30000"),
      baseURL: appConfig.environment.COMPOSITION_API_URI,
      headers: {
        common: {
          "x-api-key": appConfig.environment.COMPOSITION_API_KEY,
          channel: Channel.STAFF,
        },
      },
    });
    addRequestAndLoggingInterceptors(appConfig, this.apiInstance);
  }

  public async makeRequest(config, session) {
    config.headers = {
      "user-id": session.userCypher,
      "correlation-id": session.locator,
    };

    const data: AxiosResponse = await this.apiInstance.request(config);

    return data;
  }
}
