import { postJsonData } from "../../request";
import { WIREMOCK_PRODUCT_MAPPING_URL } from "../paths";
import { createGetRetrieveEndDateMapping, SUCCESS } from "./mappings/mappings";

async function setupSuccessfulGetProductMapping() {
  await postJsonData(
    WIREMOCK_PRODUCT_MAPPING_URL,
    createGetRetrieveEndDateMapping(SUCCESS),
  );
}

export { setupSuccessfulGetProductMapping };
