import expect from "expect";
import { configureGet } from "./configure-get";

import { IN_PROGRESS, IN_REVIEW } from "../states";
import { CHECK_ANSWERS_URL } from "../../paths/paths";
import { buildSessionForJourney } from "../test-utils/test-utils";

const TELESALES = "telesales";

const TELESALES_JOURNEY = {
  name: TELESALES,
};

const stepOne = { path: "/first" };
const stepTwo = { path: "/second" };
const stepThree = { path: "/third" };
const steps = [stepOne, stepTwo, stepThree];

const res = {
  locals: {
    previous: null,
  },
  redirect: jest.fn(),
};

const config = {
  environment: {
    HRT_PPC_VALUE: 1570,
  },
};

test("configureGet() should return previous path in journey when state is IN_PROGRESS", () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };
  const next = jest.fn();

  configureGet(steps, stepTwo, TELESALES_JOURNEY, config)(req, res, next);
  expect(res.locals.previous).toBe(stepOne.path);
});

test("configureGet() should return check-answers path in journey when state is IN_REVIEW", () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
  };
  const next = jest.fn();

  configureGet(steps, stepTwo, TELESALES_JOURNEY, config)(req, res, next);
  expect(res.locals.previous).toBe(CHECK_ANSWERS_URL);
});

test("configureGet() should throw an error when HRT_PPC_VALUE is not set", () => {
  const config = {
    environment: {
      HRT_PPC_VALUE: undefined,
    },
  };

  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
  };
  const next = jest.fn();

  expect(() =>
    configureGet(steps, stepTwo, TELESALES_JOURNEY, config)(req, res, next),
  ).toThrowError("Price is not defined");
  expect(next).toBeCalledTimes(0);
});
