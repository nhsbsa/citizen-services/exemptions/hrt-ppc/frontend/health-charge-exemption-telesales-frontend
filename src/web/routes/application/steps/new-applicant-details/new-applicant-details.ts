import { config } from "../../../../../config";
import { toPounds } from "../../../../../common/currency";
import { isNilOrEmpty } from "../../../../../common/predicates";
import { sanitize } from "./sanitize";
import { validate } from "./validate";
import { behaviourForPost } from "./behaviour-for-post";
import { formatDateToEditableDate } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/formatters/dates";
import {
  DATE_OF_BIRTH_DAY_KEY,
  DATE_OF_BIRTH_MONTH_KEY,
  DATE_OF_BIRTH_YEAR_KEY,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";

const pageContent = ({ translate }) => ({
  title: translate("newApplicantDetails.title"),
  heading: translate("newApplicantDetails.heading"),
  addressLegend: translate("newApplicantDetails.addressLegend"),
  addressLineOneLabel: translate("newApplicantDetails.addressLineOneLabel"),
  addressLineTwoLabel: translate("newApplicantDetails.addressLineTwoLabel"),
  townOrCityLabel: translate("newApplicantDetails.townOrCityLabel"),
  postcodeLabel: translate("newApplicantDetails.postcodeLabel"),
  addressDetailsTitle: translate("newApplicantDetails.addressDetailsTitle"),
  addressDetailsHtml: translate("newApplicantDetails.addressDetailsHtml"),
  nameLegend: translate("newApplicantDetails.nameLegend"),
  firstNameLabel: translate("newApplicantDetails.firstNameLabel"),
  lastNameLabel: translate("newApplicantDetails.lastNameLabel"),
  contactLegend: translate("newApplicantDetails.contactLegend"),
  certificateStartDateLegend: translate(
    "newApplicantDetails.certificateStartDateLegend",
  ),
  certificateStartDateLabel: translate(
    "newApplicantDetails.certificateStartDateLabel",
  ),
  certificateStartDateHint: translate(
    "newApplicantDetails.certificateStartDateHint",
  ),
  telephoneNumberLabel: translate("newApplicantDetails.telephoneNumberLabel"),
  noteDescriptionLabel: translate("newApplicantDetails.noteDescriptionLabel"),
  dateOfBirthLegend: translate("newApplicantDetails.dateOfBirthLegend"),
  dateOfBirthLabel: translate("newApplicantDetails.dateOfBirthLabel"),
  emailLabel: translate("newApplicantDetails.emailLabel"),
  nhsNumberLabel: translate("newApplicantDetails.nhsNumberLabel"),
  nhsNumberLegend: translate("newApplicantDetails.nhsNumberLegend"),
  paymentLegend: translate("newApplicantDetails.paymentLegend"),
  paymentHeading: translate("newApplicantDetails.paymentHeading"),
  paymentHtml: translate("newApplicantDetails.paymentHtml", {
    ppcValue: toPounds(config.environment.HRT_PPC_VALUE),
  }),
  certificateFulfillmentLegend: translate(
    "newApplicantDetails.certificateFulfillmentLegend",
  ),
  certificateFulfillmentLabel: translate(
    "newApplicantDetails.certificateFulfillmentLabel",
  ),
  certificateFulfillmentEmailText: translate(
    "newApplicantDetails.certificateFulfillmentEmailText",
  ),
  certificateFulfillmentPostText: translate(
    "newApplicantDetails.certificateFulfillmentPostText",
  ),
  submitButtonText: translate("buttons:submit"),
  fulfillmentPostEnabed: config.features.FULFILLMENT_POST_ENABLED,
  telephoneEnabled: config.features.TELEPHONE_ENABLED,
});

const customLocalsAndSessionFormProperty = () => {
  return "newApplicant";
};

const behaviourForGet = () => (req, res, next) => {
  if (!res.locals.newApplicant) {
    if (req.session.newApplicant) {
      res.locals.newApplicant = req.session.newApplicant;
    } else {
      res.locals.newApplicant = {};
      res.locals.newApplicant.description = req.t(
        "newApplicantDetails.noteDescriptionContent",
      );
      res.locals.newApplicant.lastName =
        isNilOrEmpty(req.session.claim) ||
        isNilOrEmpty(req.session.claim.lastName)
          ? ""
          : req.session.claim.lastName;
      res.locals.newApplicant.dateOfBirth =
        isNilOrEmpty(req.session.claim) ||
        isNilOrEmpty(req.session.claim.dateOfBirth)
          ? ""
          : req.session.claim.dateOfBirth;
      res.locals.newApplicant.postcode =
        isNilOrEmpty(req.session.claim) ||
        isNilOrEmpty(req.session.claim.postcode)
          ? ""
          : req.session.claim.postcode;
      if (
        !isNilOrEmpty(req.session.claim) &&
        !isNilOrEmpty(req.session.claim.dateOfBirth)
      ) {
        const editableDate = formatDateToEditableDate(
          new Date(req.session.claim.dateOfBirth),
        );
        res.locals.newApplicant[DATE_OF_BIRTH_DAY_KEY] = editableDate.day;
        res.locals.newApplicant[DATE_OF_BIRTH_MONTH_KEY] = editableDate.month;
        res.locals.newApplicant[DATE_OF_BIRTH_YEAR_KEY] = editableDate.year;
      }
    }
  }
  next();
};

const newApplicantDetailsPage = {
  path: "/new-applicant-details",
  template: "new-applicant-details",
  embeddedMainJourney: true,
  sanitize,
  pageContent,
  behaviourForGet,
  validate,
  customLocalsAndSessionFormProperty,
  behaviourForPost,
};

export {
  newApplicantDetailsPage,
  pageContent,
  behaviourForGet,
  customLocalsAndSessionFormProperty,
};
