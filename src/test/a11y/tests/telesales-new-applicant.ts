import { URLS } from "../paths";
import { currentDate } from "./dates";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";

const telesalesNewApplicant = [
  {
    url: URLS.NEW_APPLICANT_DETAILS,
    formData: () => {
      const { day, month, year, iso } = currentDate();
      return {
        firstName: "John",
        lastName: "Doe",
        telephoneNumber: "07782345678",
        "certificateStartDate-day": `${day}`,
        "certificateStartDate-month": `${month}`,
        "certificateStartDate-year": `${year}`,
        certificateStartDate: iso,
        description: "my note",
        dateOfBirth: "1997-3-24",
        "dateOfBirth-day": "24",
        "dateOfBirth-month": "03",
        "dateOfBirth-year": 1997,
        nhsNumber: "9999999999",
        emailAddress: "email@test.com",
        addressLine1: "address one",
        addressLine2: "address two",
        townOrCity: "city",
        postcode: "NE15 8NY",
        certificateFulfillment: Preference.EMAIL,
      };
    },
  },
  {
    url: URLS.CHECK_ANSWERS,
    formData: () => ({}),
  },
  {
    url: URLS.CARD_PAYMENT,
    formData: () => ({}),
  },
];

export { telesalesNewApplicant };
