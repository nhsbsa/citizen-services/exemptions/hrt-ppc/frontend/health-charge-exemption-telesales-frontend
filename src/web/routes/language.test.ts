import expect from "expect";
import { getLanguageBase } from "./language";

test("getLanguageBase", async () => {
  expect(getLanguageBase("en-GB")).toBe("en");
  expect(getLanguageBase("us-CA")).toBe("us");
  expect(getLanguageBase("cy")).toBe("cy");
  expect(getLanguageBase.bind(null, "")).toThrowError(
    /language provided in the request is blank/,
  );
  expect(getLanguageBase.bind(null, undefined)).toThrowError(
    /language provided in the request is blank/,
  );
});
