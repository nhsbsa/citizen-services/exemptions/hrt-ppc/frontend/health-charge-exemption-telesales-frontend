import {
  registerCardPaymentRoutes,
  getCardPayments,
  pageContent,
} from "./card-payment";

const journey = {
  pathPrefix: "/your-path-prefix",
};

describe("registerCardPaymentRoutes", () => {
  test("should register the card payment route correctly", () => {
    const mockGet = jest.fn();
    const app = {
      route: jest.fn(() => ({
        get: mockGet,
      })),
    };

    const csrfProtection = jest.fn();

    registerCardPaymentRoutes(journey, app, csrfProtection);

    expect(app.route).toHaveBeenCalledWith("/your-path-prefix/payment-ref");
    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith(
      expect.any(Function),
      expect.any(Function),
      csrfProtection,
      expect.any(Function),
    );
  });
});

describe("pageContent", () => {
  test("should return an object with the correct properties", () => {
    const translate = jest.fn((key) => `Translated: ${key}`);
    const req = {
      session: {
        cardPaymentDetails: {
          pin: "1234",
        },
      },
    };

    const result = pageContent({ translate, req, journey });

    expect(result).toEqual({
      title: "Translated: cardPayment.title",
      heading: "Translated: cardPayment.heading",
      searchBoxHeading: "Translated: cardPayment.searchBoxHeading",
      paragraphOne: "Translated: cardPayment.paragraphOne",
      pin: "1234",
      backText: "Translated: buttons:back",
      submitText: "Translated: buttons:submit",
      previous: "/your-path-prefix/check-their-answers",
    });
  });
});

describe("getCardPayments", () => {
  test('should render the "card-payment" template with the correct data', () => {
    const req = {
      t: jest.fn((key) => `Translated: ${key}`),
      session: {
        cardPaymentDetails: {
          pin: "1234",
        },
      },
    };
    const res = {
      render: jest.fn(),
    };

    const handler = getCardPayments(journey);
    handler(req, res);

    expect(res.render).toHaveBeenCalledWith("card-payment", {
      title: "Translated: cardPayment.title",
      heading: "Translated: cardPayment.heading",
      searchBoxHeading: "Translated: cardPayment.searchBoxHeading",
      paragraphOne: "Translated: cardPayment.paragraphOne",
      pin: "1234",
      backText: "Translated: buttons:back",
      submitText: "Translated: buttons:submit",
      previous: "/your-path-prefix/check-their-answers",
    });
  });
});
