import expect from "expect";
import * as AxiosLogger from "axios-logger";
import { preferenceClient } from "./preference-client";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

const appConfig = {
  environment: {
    LOG_REQUESTS_AND_RESPONSES: true,
    OUTBOUND_API_TIMEOUT: 20000,
    PREFERENCE_API_URI: "baseURI",
    PREFERENCE_API_KEY: "test-key",
  },
};

describe("preferenceClient()", () => {
  test("should set the correct configuration for apiInstance", () => {
    const { apiInstance } = new preferenceClient(appConfig);
    expect(apiInstance.defaults.timeout).toEqual(20000);
    expect(apiInstance.defaults.proxy).toEqual(false);
    expect(apiInstance.defaults.baseURL).toEqual("baseURI");
    expect(apiInstance.defaults.headers.common).toEqual({
      Accept: "application/json, text/plain, */*",
      "x-api-key": "test-key",
      channel: Channel.STAFF,
    });
    expect(apiInstance.interceptors.request).toHaveProperty("handlers");
    expect(apiInstance.interceptors.request["handlers"].length).toBe(1);
    expect(
      apiInstance.interceptors.request["handlers"][0]["fulfilled"],
    ).toEqual(expect.any(Function));
    expect(apiInstance.interceptors.response).toHaveProperty("handlers");
    expect(apiInstance.interceptors.response["handlers"].length).toBe(1);
    expect(
      apiInstance.interceptors.response["handlers"][0]["fulfilled"],
    ).toEqual(AxiosLogger.responseLogger);
    expect(
      apiInstance.interceptors.response["handlers"][0]["rejected"],
    ).toEqual(AxiosLogger.errorLogger);
  });

  test("should set the correct default timeout for apiInstance", () => {
    const appConfig = { environment: {} };
    const { apiInstance } = new preferenceClient(appConfig);
    expect(apiInstance.defaults.timeout).toEqual(30000);
  });

  test("should return the data into the response", async () => {
    const api = new preferenceClient(appConfig);
    const config = {};
    api.apiInstance.request = jest.fn().mockResolvedValue({ data: "data" });
    const response = await api.makeRequest(config, {
      locator: "test",
      userCypher: "CYPHER",
    });
    expect(config).toEqual({
      headers: { "correlation-id": "test", "user-id": "CYPHER" },
    });
    expect(response).toEqual({ data: "data" });
  });
});
