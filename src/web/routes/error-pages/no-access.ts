import { renderErrorPageContent } from "./error-page-content";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";

const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const registerNoAccessError = (app) => {
  app.get(
    `${CONTEXT_PATH}/no-access`,
    configureAuthentication,
    renderErrorPageContent("no-access"),
  );
};

export { registerNoAccessError };
