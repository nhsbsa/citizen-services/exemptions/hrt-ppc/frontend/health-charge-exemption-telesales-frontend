import {
  CertificateStatus,
  AddressType,
  CertificateType,
} from "@nhsbsa/health-charge-exemption-common-frontend";

export default {
  status: 200,
  jsonBody: {
    exemptions: {
      "hrt-ppcs": [
        {
          citizens: [
            {
              id: "c0a8000a-86da-1e45-8186-ee98c395000f",
              firstName: "John",
              lastName: "Smith",
              dateOfBirth: "1997-03-24",
              addresses: [
                {
                  id: "c0a8000a-86da-1e45-8186-ee98c39c0010",
                  addressLine1: "Stella House",
                  addressLine2: "Goldcrest Way",
                  townOrCity: "Newcastle upon Tyne",
                  country: "England",
                  postcode: "NE15 8NY",
                  type: AddressType.POSTAL,
                },
              ],
              certificates: [
                {
                  id: "c0a8000a-86da-1e6a-8186-ee9a40dd000f",
                  reference: "HRTC50ABAA9",
                  type: CertificateType.HRT_PPC,
                  status: CertificateStatus.ACTIVE,
                  startDate: "2022-12-12",
                  endDate: "2099-12-12",
                },
              ],
            },
          ],
        },
      ],
      others: [],
    },
  },
  headers: {
    "Content-Type": "application/json",
  },
};
