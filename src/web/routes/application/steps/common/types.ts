export type NotesDetailsRowList = {
  key: {
    text: string;
  };
  value: {
    text: string;
  };
};

export type CertificateDetailsRowList = {
  key: {
    text: string;
  };
  value: {
    text: string;
  };
};

export type ApplicationDetailsRowList = {
  key: {
    text: string;
  };
  value: {
    text?: string;
    html?: string;
  };
};

export type ErrorObject = {
  text: string;
  href: string;
  attributes: {
    id: string;
  };
};
