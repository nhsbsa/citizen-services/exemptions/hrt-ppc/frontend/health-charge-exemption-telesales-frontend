import { path } from "ramda";
import { toBoolean } from "./to-boolean";
import * as dotEnv from "dotenv";

dotEnv.config();

export const environment = {
  APP_BASE_URL: process.env.APP_BASE_URL,
  NODE_ENV: process.env.NODE_ENV,
  USE_UNSECURE_COOKIE: path(["env", "USE_UNSECURE_COOKIE"], process) === "true",
  DATADOG_APM_ENABLED: path(["env", "DATADOG_APM_ENABLED"], process) === "true",
  DATADOG_HOST_URL: process.env.DATADOG_HOST_URL,
  GA_TRACKING_ID: process.env.GA_TRACKING_ID,
  LOG_LEVEL: process.env.LOG_LEVEL || "debug",
  LOGGING_FILE_NAME: process.env.LOGGING_FILE_NAME || "application-%DATE%.log",
  LOGGING_FILE_MAX_SIZE: process.env.LOGGING_FILE_MAX_SIZE || "50m",
  LOGGING_FILE_MAX_FILES: process.env.LOGGING_FILE_MAX_FILES || "28d",
  SERVICE_AVAILABLE_DATE: process.env.SERVICE_AVAILABLE_DATE,
  OVERVIEW_URL: "/",
  APP_VERSION: process.env.APP_VERSION || "unknown",
  GOOGLE_ANALYTICS_URI: process.env.GOOGLE_ANALYTICS_URI,
  CONTACT_PREFERENCE: process.env.CONTACT_PREFERENCE,
  CONTACT_TELEPHONE: process.env.CONTACT_TELEPHONE,
  CONTACT_EMAIL: process.env.CONTACT_EMAIL,
  HRT_PPC_VALUE: undefined,
  HRT_PPC_VALUE_UPDATE_TIME: undefined,
  PRESCRIPTION_VALUE: process.env.PRESCRIPTION_VALUE,
  OUTBOUND_API_TIMEOUT: process.env.OUTBOUND_API_TIMEOUT,
  SEARCH_API_URI: process.env.SEARCH_API_URI,
  SEARCH_API_KEY: process.env.SEARCH_API_KEY,
  SEARCH_PAST_EXEMPTIONS_PERIOD:
    process.env.SEARCH_PAST_EXEMPTIONS_PERIOD || 24,
  SEARCH_RESULTS_PAGE_SIZE: Number(process.env.SEARCH_RESULTS_PAGE_SIZE) || 10,
  PAYMENT_API_URI: process.env.PAYMENT_API_URI,
  PAYMENT_API_KEY: process.env.PAYMENT_API_KEY,
  LOG_REQUESTS_AND_RESPONSES:
    toBoolean(process.env.LOG_REQUESTS_AND_RESPONSES) || toBoolean(false),
  MSALCONFIG: {
    auth: {
      clientId: `${process.env.CLIENT_ID}`,
      authority: `${process.env.CLOUD_INSTANCE}${process.env.TENANT_ID}`,
      clientSecret: process.env.CLIENT_SECRET,
    },
  },
  REDIRECT_URI: process.env.REDIRECT_URI,
  POST_LOGOUT_REDIRECT_URI: process.env.POST_LOGOUT_REDIRECT_URI,
  USE_AUTHENTICATION:
    toBoolean(process.env.USE_AUTHENTICATION) || toBoolean(false),
  CITIZEN_API_URI: process.env.CITIZEN_API_URI,
  CITIZEN_API_KEY: process.env.CITIZEN_API_KEY,
  CERTIFICATE_API_URI: process.env.CERTIFICATE_API_URI,
  CERTIFICATE_API_KEY: process.env.CERTIFICATE_API_KEY,
  NOTES_API_URI: process.env.NOTES_API_URI,
  NOTES_API_KEY: process.env.NOTES_API_KEY,
  PRODUCT_API_URI: process.env.PRODUCT_API_URI,
  PRODUCT_API_KEY: process.env.PRODUCT_API_KEY,
  REISSUE_API_URI: process.env.REISSUE_API_URI,
  REISSUE_API_KEY: process.env.REISSUE_API_KEY,
  ISSUE_CERTIFICATE_API_URI: process.env.ISSUE_CERTIFICATE_API_URI,
  ISSUE_CERTIFICATE_API_KEY: process.env.ISSUE_CERTIFICATE_API_KEY,
  CARD_PAYMENTS_API_URI: process.env.CARD_PAYMENTS_API_URI,
  CARD_PAYMENTS_SERVICE_NAME: process.env.CARD_PAYMENTS_SERVICE_NAME,
  CARD_PAYMENTS_USE_MOCK:
    toBoolean(process.env.CARD_PAYMENTS_USE_MOCK) || toBoolean(false),
  PREFERENCE_API_URI: process.env.PREFERENCE_API_URI,
  PREFERENCE_API_KEY: process.env.PREFERENCE_API_KEY,
  COMPOSITION_API_URI: process.env.COMPOSITION_API_URI,
  COMPOSITION_API_KEY: process.env.COMPOSITION_API_KEY,
};
