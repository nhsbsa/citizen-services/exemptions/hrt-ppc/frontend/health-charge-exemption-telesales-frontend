import { registerCheckAnswersRoutes } from "./check-answers/check-answers";
import { registerCardPaymentRoutes } from "./card-payment/card-payment";
import { searchNoResults } from "./no-results";
import { searchResultsFound } from "./search-results";
import { searchRecords } from "./search-records";
import { newApplicantDetailsPage } from "./new-applicant-details";
import { exemptionDetail } from "./exemption-detail";
import { addNote } from "./add-note";
import { editPersonalDetails } from "./edit-personal-details";
import { editCertificateDetails } from "./edit-certificate-details";
import { reissue } from "./reissue";

export {
  searchRecords,
  searchNoResults,
  searchResultsFound,
  exemptionDetail,
  editPersonalDetails,
  editCertificateDetails,
  newApplicantDetailsPage,
  addNote,
  registerCheckAnswersRoutes,
  registerCardPaymentRoutes,
  reissue,
};
