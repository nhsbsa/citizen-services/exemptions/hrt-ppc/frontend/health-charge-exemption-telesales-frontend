import { logger, logWithMessageAndRequestDetails } from "./logger";
import { winstonLogger } from "./winston-logger";
import { REQUEST_ID_HEADER } from "../../web/server/headers";

afterEach(() => {
  jest.resetAllMocks();
});

const sampleRequest = {
  session: {
    locator: "246857000",
  },
  headers: {
    [REQUEST_ID_HEADER]: "123456",
  },
};

test("logger() logs info message correctly", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "info");

  logger.info("some message");

  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    req: undefined,
    version: "example-version",
  });
});

test("logger() logs info message correctly with locator and request id", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "info");

  logger.info("some message", sampleRequest);
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    version: "example-version",
    req: sampleRequest,
  });
});

test("logger() logs info message correctly with locator, request id and meta", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "info");

  const meta = { metaExample: "test" };

  logger.info("some message", sampleRequest, meta);
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    version: "example-version",
    ...meta,
    req: sampleRequest,
  });
});

test("logger() logs info message correctly with missing locator and request id", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "info");

  const sampleRequest = {
    session: {
      locator: null,
    },
    headers: {
      [REQUEST_ID_HEADER]: null,
    },
  };

  logger.info("some message", sampleRequest);
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    version: "example-version",
    req: undefined,
  });
});

test("logger() logs error message correctly", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "error");

  logger.error("some message");
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    req: undefined,
    version: "example-version",
  });
});

test("logger() logs warn message correctly", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "warn");

  logger.warn("some message");
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    req: undefined,
    version: "example-version",
  });
});

test("logger() logs debug message correctly", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "debug");

  logger.debug("some message");
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    req: undefined,
    version: "example-version",
  });
});

test("logWithMessageAndRequestDetails() logs debug message with no req or meta", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "debug");

  logWithMessageAndRequestDetails(winstonLogger.debug, "some message");
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    req: undefined,
    version: "example-version",
  });
});

test("logWithMessageAndRequestDetails() logs debug message with req only containing locator", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "debug");

  const sampleRequest = {
    session: {
      locator: "246857000",
    },
  };

  logWithMessageAndRequestDetails(
    winstonLogger.debug,
    "some message",
    sampleRequest,
  );
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    req: undefined,
    version: "example-version",
  });
});

test("logWithMessageAndRequestDetails() logs debug message with req containing locator and request-id", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "debug");

  logWithMessageAndRequestDetails(
    winstonLogger.debug,
    "some message",
    sampleRequest,
  );
  expect(winstonLoggerSpy).toBeCalledWith("some message", {
    req: sampleRequest,
    version: "example-version",
  });
});

test("logWithMessageAndRequestDetails() logs debug message with req, meta and certificate details in session", () => {
  const winstonLoggerSpy = jest.spyOn(winstonLogger, "debug");

  sampleRequest.session["issuedCertificateDetails"] = { id: "certId" };
  const meta = { tagMeta: "metaAdditional" };

  const expected = {
    certificate: {
      id: "certId",
    },
    req: {
      headers: {
        "x-request-id": "123456",
      },
      session: {
        locator: "246857000",
      },
    },
    tagMeta: "metaAdditional",
    version: "example-version",
  };

  logWithMessageAndRequestDetails(
    winstonLogger.debug,
    "some message",
    sampleRequest,
    meta,
  );
  expect(winstonLoggerSpy).toBeCalledWith("some message", expected);
});
