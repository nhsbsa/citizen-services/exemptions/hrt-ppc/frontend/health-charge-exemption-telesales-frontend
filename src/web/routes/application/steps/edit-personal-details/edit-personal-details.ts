import {
  isNilOrEmpty,
  notIsUndefinedOrNullOrEmpty,
} from "../../../../../common/predicates";
import {
  EDIT_PERSONAL_DETAILS_URL,
  EXEMPTION_VIEW_URL,
} from "../../paths/paths";
import { formatDateToEditableDate } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/formatters/dates";
import { findRecordInSearchResults } from "../common/search-utils";
import { sanitize } from "./sanitize";
import { validate } from "./validate";
import { isActiveAndHasNotExpired } from "../common/certificate-utils";
import { behaviourForPost } from "./behaviour-for-post";

const pageContent = ({ translate, req }) => ({
  title: translate("editPersonalDetails.title"),
  heading: translate("editPersonalDetails.heading"),
  warningCalloutHeading: translate("editPersonalDetails.warningCalloutHeading"),
  warningCalloutHTML: translate("editPersonalDetails.warningCalloutHTML"),
  summaryCardTitle: translate("editPersonalDetails.summaryCardTitle"),
  firstNameLabel: translate("editPersonalDetails.firstNameLabel"),
  lastNameLabel: translate("editPersonalDetails.lastNameLabel"),
  addressLineOneLabel: translate("editPersonalDetails.addressLineOneLabel"),
  addressLineTwoLabel: translate("editPersonalDetails.addressLineTwoLabel"),
  townLabel: translate("editPersonalDetails.townLabel"),
  postcodeLabel: translate("editPersonalDetails.postcodeLabel"),
  dateOfBirthLabel: translate("editPersonalDetails.dateOfBirthLabel"),
  emailAddressLabel: translate("editPersonalDetails.emailAddressLabel"),
  nhsNumberLabel: translate("editPersonalDetails.nhsNumberLabel"),
  submitButtonText: translate("buttons:submit"),
  previous: `${EXEMPTION_VIEW_URL}/${req.params.id}`,
});

/**
 * isNavigable will check if the search result exists in session and then try and get the record by certificate reference,
 * when found check if certificate exists in session & the certificate reference matches with the url param & that the status is ACTIVE
 * @param req the request
 * @returns true if navigable otherwise false
 */
const isNavigable = (req) => {
  return !!(
    notIsUndefinedOrNullOrEmpty(findRecordInSearchResults(req)) &&
    isActiveAndHasNotExpired(req.session.certificate) &&
    req.session.certificate.reference === req.params.id
  );
};

const behaviourForGet = () => (req, res, next) => {
  const citizen = req.session.citizen;
  res.locals.citizen = {
    firstName: citizen.firstName,
    lastName: citizen.lastName,
    dateOfBirth: citizen.dateOfBirth,
    emailAddress: isNilOrEmpty(citizen.emails)
      ? ""
      : citizen.emails[0].emailAddress,
    nhsNumber: citizen.nhsNumber,
    addressLine1: citizen.addresses[0].addressLine1,
    addressLine2: citizen.addresses[0].addressLine2,
    townOrCity: citizen.addresses[0].townOrCity,
    postcode: citizen.addresses[0].postcode,
  };
  const editableDate = formatDateToEditableDate(new Date(citizen.dateOfBirth));
  res.locals.citizen["dateOfBirth-day"] = editableDate.day;
  res.locals.citizen["dateOfBirth-month"] = editableDate.month;
  res.locals.citizen["dateOfBirth-year"] = editableDate.year;

  next();
};

const editPersonalDetails = {
  path: `/:id(HRT[A-Za-z0-9]+)${EDIT_PERSONAL_DETAILS_URL}`,
  template: "edit-personal-details",
  pageContent,
  isNavigable,
  behaviourForGet,
  behaviourForPost,
  sanitize,
  validate,
};

export { editPersonalDetails, pageContent, behaviourForGet, isNavigable };
