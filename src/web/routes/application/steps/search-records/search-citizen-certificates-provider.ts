import { searchClient } from "../../../../client/search-client";
import { logger } from "../../../../logger/logger";
import { config } from "../../../../../config";
import {
  isNilOrEmpty,
  notIsUndefinedOrNullOrEmpty,
} from "../../../../../common/predicates";
import moment from "moment";
import Redactyl from "redactyl.js";
import { sort } from "fast-sort";

const redactyl = new Redactyl({
  properties: ["firstName", "lastName", "dateOfBirth", "addresses"],
});

const makeSearchRequest = async (req) => {
  const client = new searchClient(config);
  const body = createRequestBody(req);
  const response = await client.makeRequest(
    {
      method: "POST",
      url: "/v1/hrt-exemptions/match",
      data: body,
      responseType: "json",
    },
    req,
  );
  return response;
};

const createRequestBody = (req) => {
  const postcode = req.body.sanitizedPostcode;
  const lastName = req.body.lastName;
  const dateOfBirth = req.body.dateOfBirth;
  const startDate = moment()
    .subtract(config.environment.SEARCH_PAST_EXEMPTIONS_PERIOD, "month")
    .format("YYYY-MM-DD");

  const request = {
    citizen: { undefined, address: {} },
    certificate: {
      startDate: startDate,
    },
  };
  if (!isNilOrEmpty(postcode)) {
    request["citizen"]["address"]["postcode"] = postcode;
  }
  if (!isNilOrEmpty(lastName)) {
    request["citizen"]["lastName"] = lastName;
  }
  if (!isNilOrEmpty(dateOfBirth)) {
    request["citizen"]["dateOfBirth"] = dateOfBirth;
  }

  return request;
};

const searchExemptions = async (req) => {
  const response = await makeSearchRequest(req);

  const data: any = response.data;

  logger.info(
    `Exemption search result: ` + JSON.stringify(redactyl.redact(data)),
    req,
  );

  const hrtPpcs = data.exemptions["hrt-ppcs"];
  const others = data.exemptions["others"];

  // Filter out any citizens with no certificates
  const filterCitizensWithEmptyCertificate = (exemption: Exemption): void => {
    exemption.citizens = exemption.citizens.filter(
      (citizen) => !isNilOrEmpty(citizen.certificates),
    );
  };

  // Filter out any empty citizens
  const filterEmptyCitizens = (exemption: Exemption): boolean =>
    !isNilOrEmpty(exemption.citizens);

  hrtPpcs.forEach(filterCitizensWithEmptyCertificate);
  others.forEach(filterCitizensWithEmptyCertificate);
  const filteredHrtPpcs: Exemption[] = hrtPpcs.filter(filterEmptyCitizens);
  const filteredOthers: Exemption[] = others.filter(filterEmptyCitizens);

  // There will always only be one 'citizens' object in the array. Workaround until response is updated
  const exemptionsHrt = filteredHrtPpcs[0];
  const exemptionsOther = filteredOthers[0];

  // Here we concatenate both citizens arrays into one if its defined and not empty
  let combinedCitizens: Citizen[] = [];
  if (notIsUndefinedOrNullOrEmpty(exemptionsHrt)) {
    combinedCitizens = combinedCitizens.concat(exemptionsHrt.citizens);
  }
  if (notIsUndefinedOrNullOrEmpty(exemptionsOther)) {
    // To enable showing other exemptions in future release
    // combinedCitizens = combinedCitizens.concat(exemptionsOther.citizens);
  }

  if (!isNilOrEmpty(combinedCitizens)) {
    /**
     * Here we will transform so that the certificate elements are in root and citizen as a sub-element
     * to make it simpler for sorting
     */
    const transformedCertificates: CertificateTransformed[] = [];
    combinedCitizens.map((citizen) => {
      citizen.certificates.map((cert) => {
        const transformed: CertificateTransformed = {
          ...cert,
          citizen: citizen,
        };
        delete transformed.citizen["certificates"];
        transformedCertificates.push(transformed);
      });
    });

    const sorted = sort(transformedCertificates).by([
      { desc: (u) => u.endDate },
    ]);

    return sorted;
  }

  return undefined;
};

export { createRequestBody, searchExemptions };
