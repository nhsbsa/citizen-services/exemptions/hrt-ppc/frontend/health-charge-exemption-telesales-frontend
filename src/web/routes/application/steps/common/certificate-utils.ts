import moment from "moment";
import {
  Event,
  CertificateStatus,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/enums";

const isActiveCertificate = (certificate: Certificate) =>
  certificate.status === CertificateStatus.ACTIVE;

const hasExpired = (certificate) =>
  moment(certificate.endDate).isBefore(moment());

const hasApplicationDateLessThanOneMonthAgo = (certificate) =>
  moment(certificate.applicationDate).isSameOrAfter(
    moment().subtract(1, "months").format("YYYY-MM-DD"),
  );

const hasAnyPreference = (certificate) =>
  certificate.preference.event === Event.ANY;

/**
 * Checks for:
 * - a valid certificate param.
 * - an ACTIVE certificate status.
 * - an end date (expiry) in the future.
 * @param certificate a certificate object
 * @returns true if each condition is met else false
 */
export const isActiveAndHasNotExpired = (certificate?): boolean =>
  !!certificate && isActiveCertificate(certificate) && !hasExpired(certificate);

/**
 * Checks for:
 * - a valid certificate param.
 * - an ACTIVE certificate status.
 * - an end date (expiry) in the future.
 * - the application date is within the last calendar month.
 * @param certificate a certificate object
 * @returns true if each condition is met else false
 */
export const isEligibleForEditing = (certificate?) =>
  isActiveAndHasNotExpired(certificate) &&
  hasApplicationDateLessThanOneMonthAgo(certificate);

/**
 * Checks for:
 * - a valid certificate param.
 * - an ACTIVE certificate status.
 * - an end date (expiry) in the future.
 * - the application has certificate preference as ANY.
 * @param certificate a certificate object
 * @returns true if each condition is met else false
 */
export const isEligibleForReissue = (certificate?) =>
  isActiveAndHasNotExpired(certificate) && hasAnyPreference(certificate);
