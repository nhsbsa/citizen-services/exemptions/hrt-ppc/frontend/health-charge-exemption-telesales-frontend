import expect from "expect";
import {
  searchExemptions,
  createRequestBody,
} from "./search-citizen-certificates-provider";
import * as exemptionsNoMatch from "../../../../../__mocks__/api-responses/search-api/exemptions-no-match.json";
import * as exemptionsMixedNotSorted from "../../../../../__mocks__/api-responses/search-api/exemptions-mixed-sorting.json";
import * as exemptionsMixedExpiredAndActive from "../../../../../__mocks__/api-responses/search-api/exemptions-mixed-expired-and-active.json";
import * as exemptionsHrtCertificatesEmpty from "../../../../../__mocks__/api-responses/search-api/exemptions-hrt-certificates-empty.json";
import * as exemptionsOtherCertificatesEmpty from "../../../../../__mocks__/api-responses/search-api/exemptions-others-certificates-empty.json";
import * as exemptionsHrtCitizensEmpty from "../../../../../__mocks__/api-responses/search-api/exemptions-hrt-citizens-empty.json";
import * as exemptionsOtherCitizensEmpty from "../../../../../__mocks__/api-responses/search-api/exemptions-others-citizens-empty.json";
import {
  CertificateStatus,
  AddressType,
  CertificateType,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { searchClient } from "../../../../client/search-client";
import moment from "moment";
jest.mock("../../../../client/search-client");

const todayMinusOneMonth = moment().subtract("1", "month").format("YYYY-MM-DD");

const req = {
  body: {
    lastName: "Lname",
    dateOfBirth: "2022-01-01",
    sanitizedPostcode: "NE15 8NY",
  },
};

const expectedCitizenAndCertificate = {
  citizen: {
    address: { postcode: "NE15 8NY" },
    lastName: "Lname",
    dateOfBirth: "2022-01-01",
  },
  certificate: {
    startDate: todayMinusOneMonth,
  },
};

const makeRequestMock = jest.fn();

const expectedMockRequestCall = {
  method: "POST",
  url: "/v1/hrt-exemptions/match",
  data: expectedCitizenAndCertificate,
  responseType: "json",
};

beforeEach(() => {
  jest.clearAllMocks();
  searchClient.prototype.makeRequest = makeRequestMock;
});

describe("createRequestBody()", () => {
  test("formats and populates the request body when all search fields provided", () => {
    const req = {
      body: {
        lastName: "Lname",
        dateOfBirth: "2022-01-01",
        sanitizedPostcode: "NE15 8NY",
      },
    };
    const result = createRequestBody(req);

    const expectedCitizenAndCertificate = {
      citizen: {
        address: { postcode: "NE15 8NY" },
        lastName: "Lname",
        dateOfBirth: "2022-01-01",
      },
      certificate: {
        startDate: todayMinusOneMonth,
      },
    };
    expect(result).toEqual(expectedCitizenAndCertificate);
  });

  test("formats and populates the request body when search fields missing lastName", () => {
    const req = {
      body: {
        dateOfBirth: "2022-01-01",
        sanitizedPostcode: "NE15 8NY",
      },
    };
    const result = createRequestBody(req);

    const expectedCitizenAndCertificate = {
      citizen: {
        address: { postcode: "NE15 8NY" },
        dateOfBirth: "2022-01-01",
      },
      certificate: {
        startDate: todayMinusOneMonth,
      },
    };
    expect(result).toEqual(expectedCitizenAndCertificate);
  });

  test("formats and populates the request body when search fields missing dateOfBirth", () => {
    const req = {
      body: {
        lastName: "Lname",
        sanitizedPostcode: "NE15 8NY",
      },
    };
    const result = createRequestBody(req);

    const expectedCitizenAndCertificate = {
      citizen: {
        address: { postcode: "NE15 8NY" },
        lastName: "Lname",
      },
      certificate: {
        startDate: todayMinusOneMonth,
      },
    };
    expect(result).toEqual(expectedCitizenAndCertificate);
  });

  test("formats and populates the request body when search fields missing postcode", () => {
    const req = {
      body: {
        lastName: "Lname",
        dateOfBirth: "2022-01-01",
      },
    };
    const result = createRequestBody(req);

    const expectedCitizenAndCertificate = {
      citizen: {
        address: {},
        lastName: "Lname",
        dateOfBirth: "2022-01-01",
      },
      certificate: {
        startDate: todayMinusOneMonth,
      },
    };
    expect(result).toEqual(expectedCitizenAndCertificate);
  });

  test("formats and populates the request body when search fields only has lastName", () => {
    const req = {
      body: {
        lastName: "Lname",
      },
    };
    const result = createRequestBody(req);

    const expectedCitizenAndCertificate = {
      citizen: {
        address: {},
        lastName: "Lname",
      },
      certificate: {
        startDate: todayMinusOneMonth,
      },
    };
    expect(result).toEqual(expectedCitizenAndCertificate);
  });
});

describe("searchExemptions()", () => {
  test("should return undefined when `hrt-ppcs` and `others` empty", async () => {
    makeRequestMock.mockResolvedValue({ data: exemptionsNoMatch });

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toBeUndefined();
  });

  test("should return results when there is both a hrt and other certificate not expired/ expiring", async () => {
    const expected = [
      {
        citizen: {
          addresses: [
            {
              addressLine1: "Stella House",
              addressLine2: "Goldcrest Way",
              country: "England",
              id: "c0a80005-8617-1c07-8186-17fe5f1200f8",
              postcode: "NE15 8NY",
              townOrCity: "Newcastle upon Tyne",
              type: AddressType.POSTAL,
            },
          ],
          dateOfBirth: "1997-03-24",
          firstName: "HRT",
          id: "c0a80005-859c-1146-8185-b08e84980012",
          lastName: "MIXED",
        },
        endDate: "2099-03-31",
        id: "c0a80005-8687-1c06-8186-87513fbc0000",
        reference: "HRT7DDB232E",
        startDate: "2022-03-31",
        status: CertificateStatus.ACTIVE,
        type: CertificateType.HRT_PPC,
      },
    ];
    makeRequestMock.mockResolvedValue({
      data: exemptionsMixedExpiredAndActive,
    });

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toStrictEqual(expected);
  });

  test("should return results sorted by certificate endDate when there is a mixture of hrt and other certificates", async () => {
    const expected = [
      {
        citizen: {
          addresses: [
            {
              addressLine1: "Stella House",
              addressLine2: "Goldcrest Way",
              country: "England",
              id: "c0a80005-8617-1c07-8186-17fe5f1200f8",
              postcode: "NE15 8NY",
              townOrCity: "Newcastle upon Tyne",
              type: AddressType.POSTAL,
            },
          ],
          dateOfBirth: "1997-03-24",
          firstName: "Efg",
          id: "c0a80005-859c-1146-8185-b08e73b00003",
          lastName: "hrt",
        },
        endDate: "2023-01-12",
        id: "c0a80005-8687-1c06-8186-87513fbc0001",
        reference: "HRT98CDBBF8",
        startDate: "2022-01-12",
        status: CertificateStatus.ACTIVE,
        type: CertificateType.HRT_PPC,
      },
      {
        citizen: {
          addresses: [
            {
              addressLine1: "Stella House",
              addressLine2: "Goldcrest Way",
              country: "England",
              id: "c0a80005-8617-1c07-8186-17fe5f1200f9",
              postcode: "NE16 8NY",
              townOrCity: "Newcastle upon Tyne",
              type: AddressType.POSTAL,
            },
          ],
          dateOfBirth: "1996-03-24",
          firstName: "Abcd",
          id: "c0a80005-859c-1146-8185-b08e73b00004",
          lastName: "hrt",
        },
        endDate: "2023-01-12",
        id: "c0a80005-8687-1c06-8186-87513fbc0003",
        reference: "HRT98CDBBF6",
        startDate: "2022-01-12",
        status: CertificateStatus.ACTIVE,
        type: CertificateType.HRT_PPC,
      },
      {
        citizen: {
          addresses: [
            {
              addressLine1: "Stella House",
              addressLine2: "Goldcrest Way",
              country: "England",
              id: "c0a80005-8617-1c07-8186-17fe5f1200f8",
              postcode: "NE15 8NY",
              townOrCity: "Newcastle upon Tyne",
              type: AddressType.POSTAL,
            },
          ],
          dateOfBirth: "1997-03-24",
          firstName: "Efg",
          id: "c0a80005-859c-1146-8185-b08e73b00003",
          lastName: "hrt",
        },
        endDate: "2021-01-12",
        id: "c0a80005-8687-1c06-8186-87513fbc0000",
        reference: "HRT98CDBBF9",
        startDate: "2020-01-12",
        status: "EXPIRED",
        type: CertificateType.HRT_PPC,
      },
      {
        citizen: {
          addresses: [
            {
              addressLine1: "Stella House",
              addressLine2: "Goldcrest Way",
              country: "England",
              id: "c0a80005-8617-1c07-8186-17fe5f1200f9",
              postcode: "NE16 8NY",
              townOrCity: "Newcastle upon Tyne",
              type: AddressType.POSTAL,
            },
          ],
          dateOfBirth: "1996-03-24",
          firstName: "Abcd",
          id: "c0a80005-859c-1146-8185-b08e73b00004",
          lastName: "hrt",
        },
        endDate: "2021-01-12",
        id: "c0a80005-8687-1c06-8186-87513fbc0002",
        reference: "HRT98CDBBF7",
        startDate: "2020-01-12",
        status: "EXPIRED",
        type: CertificateType.HRT_PPC,
      },
      {
        citizen: {
          addresses: [
            {
              addressLine1: "Stella House",
              addressLine2: "Goldcrest Way",
              country: "England",
              id: "c0a80005-8617-1c07-8186-17fe5f1200f9",
              postcode: "NE16 8NY",
              townOrCity: "Newcastle upon Tyne",
              type: AddressType.POSTAL,
            },
          ],
          dateOfBirth: "1996-03-24",
          firstName: "Abcd",
          id: "c0a80005-859c-1146-8185-b08e73b00004",
          lastName: "hrt",
        },
        endDate: "2019-01-12",
        id: "c0a80005-8687-1c06-8186-87513fbc0003",
        reference: "HRT98CDBBF5",
        startDate: "2018-01-12",
        status: "EXPIRED",
        type: CertificateType.HRT_PPC,
      },
    ];
    makeRequestMock.mockResolvedValue({ data: exemptionsMixedNotSorted });

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toStrictEqual(expected);
  });

  test("should return undefined when the `hrt-ppcs[].certificates` is empty", async () => {
    makeRequestMock.mockResolvedValue({ data: exemptionsHrtCertificatesEmpty });

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toBeUndefined();
  });

  test("should return undefined when the `others[].certificates` is empty", async () => {
    makeRequestMock.mockResolvedValue({
      data: exemptionsOtherCertificatesEmpty,
    });

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toBeUndefined();
  });

  test("should return undefined when the `hrt-ppcs[].citizens` is empty", async () => {
    makeRequestMock.mockResolvedValue({ data: exemptionsHrtCitizensEmpty });

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toBeUndefined();
  });

  test("should return undefined when the `others[].citizens` is empty", async () => {
    makeRequestMock.mockResolvedValue({ data: exemptionsOtherCitizensEmpty });

    const result = await searchExemptions(req);

    expect(makeRequestMock).toBeCalledTimes(1);
    expect(makeRequestMock).toBeCalledWith(expectedMockRequestCall, req);
    expect(result).toBeUndefined();
  });
});
