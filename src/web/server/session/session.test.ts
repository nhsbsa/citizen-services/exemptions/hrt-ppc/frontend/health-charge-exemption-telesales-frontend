import expect from "expect";
import * as sinon from "sinon";

import { ensureSession, getSessionConfig } from "./session";

test("ensureSession should throw an error if session does not exist", () => {
  const req = {};
  const res = {};
  const next = sinon.spy();

  ensureSession(req, res, next);

  expect(next.calledWith(sinon.match.instanceOf(Error))).toBe(true);
});

test("ensureSession should not throw an error if session does not exist", () => {
  const req = { session: {} };
  const res = {};
  const next = sinon.spy();

  ensureSession(req, res, next);

  expect(next.calledWith()).toBe(true);
});

test("getSessionConfig", () => {
  const cookieMaxAge = 30 * 60 * 1000;

  const configDevelopment = {
    server: {
      SESSION_SECRET: "password123",
      SESSION_ID_NAME: "session.sid",
    },
    environment: {
      USE_UNSECURE_COOKIE: true,
      maxAge: cookieMaxAge,
    },
  };

  const configProduction = {
    server: {
      SESSION_SECRET: "password123",
      SESSION_ID_NAME: "session.sid",
    },
  };

  const expectedLocal = {
    secret: "password123",
    saveUninitialized: false,
    resave: false,
    rolling: true,
    name: "session.sid",
    cookie: {
      secure: false,
      maxAge: cookieMaxAge,
    },
  };

  const expectedProduction = {
    ...expectedLocal,
    cookie: {
      sameSite: "none",
      secure: true,
      maxAge: cookieMaxAge,
    },
  };

  const resultDevelopment = getSessionConfig(configDevelopment);
  const resultProduction = getSessionConfig(configProduction);

  expect(resultDevelopment).toEqual(expectedLocal);
  expect(resultProduction).toEqual(expectedProduction);
});
