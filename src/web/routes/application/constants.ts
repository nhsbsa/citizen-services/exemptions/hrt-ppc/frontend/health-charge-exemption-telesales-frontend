import {
  ADDRESS_LINE_1_KEY,
  ADDRESS_LINE_2_KEY,
  TOWN_OR_CITY_KEY,
  POSTCODE_KEY,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";

// Journey names
export const SEARCH_JOURNEY_NAME: string = "search";
export const NEW_APP_JOURNEY_NAME: string = "newApplicant";

// Certificate
export const ACTIVE_STATUS: string = "ACTIVE";

// Validation related
export const NOTE_DESCRIPTION_MAX_LENGTH: number = 1000;
export const FIRSTNAME_MAX_LENGTH: number = 50;
export const LASTNAME_MAX_LENGTH: number = 50;
export const NAME_PATTERN: RegExp =
  /^(?!.*[\u00D7\u00F7])[A-Za-z\u1E00-\u1EFF\u0100-\u017F\u00c0-\u00ff\s\-']*$/;

export const ADDRESS_KEYS = [
  ADDRESS_LINE_1_KEY,
  ADDRESS_LINE_2_KEY,
  TOWN_OR_CITY_KEY,
  POSTCODE_KEY,
];
