import { postJsonData } from "../../request";
import { WIREMOCK_PAYMENT_MAPPING_URL } from "../paths";
import { createNewPaymentSuccessMapping } from "./mappings/mappings";

async function setupSuccessfulNewPaymentMapping() {
  await postJsonData(
    WIREMOCK_PAYMENT_MAPPING_URL,
    createNewPaymentSuccessMapping(),
  );
}

export { setupSuccessfulNewPaymentMapping };
