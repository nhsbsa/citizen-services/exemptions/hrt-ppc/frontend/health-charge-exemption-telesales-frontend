import expect from "expect";
import { searchNoResults, pageContent, isNavigable } from "./no-results";

test(`searchNoResults should match expected outcomes`, () => {
  const expectedResults = {
    path: "/no-results",
    template: "no-results",
    pageContent,
    isNavigable,
  };

  expect(searchNoResults).toEqual(expectedResults);
});

test(`pageContent() should return expected results`, () => {
  const translate = (string) => string;
  const expected = {
    title: translate("noResults.title"),
    heading: translate("noResults.heading"),
    searchBoxHeading: translate("noResults.searchBoxHeading"),
    paragraphOne: translate("noResults.paragraphOne"),
    newApplicationButtonText: translate("buttons:newApplication"),
    previous: false,
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

it.each([[], undefined, ""])(
  `isNavigable() returns true when search results in session set to %p`,
  (searchResult: any) => {
    const req = {
      session: {
        searchResult: searchResult,
      },
    };

    const result = isNavigable(req);

    expect(result).toBeTruthy();
  },
);

it.each([[{ data: "something" }]])(
  `isNavigable() returns false when search results in session set to %p`,
  (searchResult: any) => {
    const req = {
      session: {
        searchResult: searchResult,
      },
    };

    const result = isNavigable(req);

    expect(result).toBeFalsy();
  },
);
