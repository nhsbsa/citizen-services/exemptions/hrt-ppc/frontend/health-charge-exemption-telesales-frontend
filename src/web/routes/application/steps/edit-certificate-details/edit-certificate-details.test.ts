const mockIsEligibleForEditing = jest.fn();
jest.mock("./../common/certificate-utils", () => ({
  isEligibleForEditing: mockIsEligibleForEditing,
}));

import {
  copyCertificateSessionDataToLocals,
  editCertificateDetails,
  isNavigable,
  pageContent,
} from "./edit-certificate-details";
import moment from "moment";
import { Request, Response } from "express";
import {
  Event,
  Preference,
  CertificateStatus,
  Channel,
} from "@nhsbsa/health-charge-exemption-common-frontend";

describe("edit certificate details", () => {
  const validReq = {
    params: {
      id: "HRTsomething",
    },
    session: {
      certificate: {
        status: CertificateStatus.ACTIVE,
        applicationDate: moment().subtract(2, "weeks").format("YYYY-MM-DD"),
        reference: "HRTsomething",
      },
      searchResult: [
        {
          reference: "HRTsomething",
        },
      ],
    },
  } as unknown as Request;

  const translate = jest.fn(
    (key) =>
      ({
        "editCertificateDetails.title": "title",
        "editCertificateDetails.heading": "heading",
        "preferencesText.email": "Email",
        "preferencesText.post": "Post",
        "preferencesText.none": "None",
        "editCertificateDetails.warningCalloutHeading":
          "warning callout heading",
        "editCertificateDetails.warningCalloutHTML": "warning callout HTML",
        "editCertificateDetails.summaryCardTitle": "summary card title",
        "editCertificateDetails.certificateNumberLabel":
          "certificate number label",
        "editCertificateDetails.certificateTypeLabel": "certificate type label",
        "editCertificateDetails.certificateStatusLabel":
          "certificate status label",
        "editCertificateDetails.certificateChannelLabel":
          "certificate channel label",
        "editCertificateDetails.certificateStartLabel":
          "certificate start label",
        "editCertificateDetails.certificateExpiryLabel":
          "certificate expiry label",
        "editCertificateDetails.applicationDateLabel": "application date label",
        "editCertificateDetails.certificateFulfillmentLabel":
          "certificate fulfillment label",
        "editCertificateDetails.certificateFulfillmentEmailText":
          "certificate fulfillment email text",
        "editCertificateDetails.certificateFulfillmentPostText":
          "certificate fulfillment post text",
        "buttons:submit": "submit button",
      })[key],
  );

  describe("editCertificateDetails", () => {
    test("has the right contents", () => {
      expect(editCertificateDetails).toEqual({
        path: "/:id(HRT[A-Za-z0-9]+)/edit-certificate-details",
        template: "edit-certificate-details",
        pageContent: expect.any(Function),
        isNavigable: expect.any(Function),
        behaviourForGet: expect.any(Function),
        behaviourForPost: expect.any(Function),
        sanitize: expect.any(Function),
        validate: expect.any(Function),
      });
    });
  });

  describe("pageContent()", () => {
    test("returns expected translations", () => {
      const req = { params: { id: "ID" } };

      const content = pageContent({ translate, req });

      expect(content).toEqual({
        title: "title",
        heading: "heading",
        preferencesText: {
          email: "Email",
          post: "Post",
          none: "None",
        },
        warningCalloutHeading: "warning callout heading",
        warningCalloutHTML: "warning callout HTML",
        summaryCardTitle: "summary card title",
        certificateNumberLabel: "certificate number label",
        certificateTypeLabel: "certificate type label",
        certificateStatusLabel: "certificate status label",
        certificateChannelLabel: "certificate channel label",
        certificateStartLabel: "certificate start label",
        certificateExpiryLabel: "certificate expiry label",
        applicationDateLabel: "application date label",
        certificateFulfillmentLabel: "certificate fulfillment label",
        certificateFulfillmentEmailText: "certificate fulfillment email text",
        certificateFulfillmentPostText: "certificate fulfillment post text",
        submitButtonText: "submit button",
        previous: "/exemption-information/ID",
      });
    });
  });

  describe("isNavigable()", () => {
    test("true when certificate is active and has correct ID", () => {
      expect(isNavigable(validReq)).toEqual(true);
    });

    test("false when no record in the request", () => {
      const req = { ...validReq, session: {} } as unknown as Request;

      const navigable = isNavigable(req);

      expect(navigable).toEqual(false);
    });

    test("false when certificate in the session doesn't match the id of the request", () => {
      const req = { ...validReq } as unknown as Request;
      req.session["certificate"].reference = req.params.id + "diff";

      const navigable = isNavigable(req);

      expect(navigable).toEqual(false);
    });
  });

  describe("copyCertificateSessionDataToLocals()", () => {
    const res = { locals: {} } as Response;
    const req = {
      params: {
        id: "ID",
      },
      session: {
        certificate: {
          reference: "HRTReference",
          type: "certificate_type",
          status: CertificateStatus.ACTIVE,
          _meta: {
            channel: Channel.ONLINE,
          },
          startDate: "2013-12-31",
          endDate: "2014-12-31",
          applicationDate: "2013-12-25",
          preference: {
            event: Event.ANY,
            preference: Preference.EMAIL,
          },
        },
      },
    } as unknown as Request;
    test("should put certificate data from the request session into the response locals when start date is eligible for editing", () => {
      mockIsEligibleForEditing.mockReturnValue(true);

      copyCertificateSessionDataToLocals(req, res);

      expect(res.locals.certificateDetailsData).toEqual({
        reference: "HRTReference",
        type: '<strong class="nhsuk-tag nhsuk-tag--green">certificate type</strong>',
        status: CertificateStatus.ACTIVE,
        channel: Channel.ONLINE,
        startDate: "31/12/2013",
        endDate: "31/12/2014",
        applicationDate: "25/12/2013",
        certificateFulfillment: Preference.EMAIL,
        isStartDateEligibleForEditing: true,
      });
    });

    test("should put certificate data from the request session into the response locals when start date is not eligible for editing", () => {
      mockIsEligibleForEditing.mockReturnValue(false);

      copyCertificateSessionDataToLocals(req, res);

      expect(res.locals.certificateDetailsData).toEqual({
        reference: "HRTReference",
        type: '<strong class="nhsuk-tag nhsuk-tag--green">certificate type</strong>',
        status: CertificateStatus.ACTIVE,
        channel: Channel.ONLINE,
        startDate: "31/12/2013",
        endDate: "31/12/2014",
        applicationDate: "25/12/2013",
        certificateFulfillment: Preference.EMAIL,
        isStartDateEligibleForEditing: false,
      });
    });
  });
});
