import expect from "expect";
import { toErrorList, getErrorForField, camelToKebabCase } from "./filters";

const firstNameTooLong = "First name is too long";
const missingLastName = "Missing last name";

const errors = [
  {
    path: "firstName",
    msg: firstNameTooLong,
  },
  {
    path: "lastName",
    msg: missingLastName,
  },
];

test("toErrorList", () => {
  const expected = [
    {
      text: firstNameTooLong,
      href: "#first-name",
      attributes: {
        id: "error-link-first-name",
      },
    },
    {
      text: missingLastName,
      href: "#last-name",
      attributes: {
        id: "error-link-last-name",
      },
    },
  ];

  const errorList = toErrorList(errors);

  expect(errorList).toEqual(expected);
});

test("getErrorForField", () => {
  const result = getErrorForField(errors, "middleName");
  expect(result).toBe(null);
});

test("getErrorForField", () => {
  const result = getErrorForField(errors, "lastName");

  const expected = {
    text: missingLastName,
  };

  expect(result).toEqual(expected);
});

test("getErrorForField", () => {
  const result = getErrorForField(undefined, "lastName");
  expect(result).toEqual(null);
});

test("camelToKebabCase", () => {
  const result = camelToKebabCase("personsFirstName");
  expect(result).toEqual("persons-first-name");
});

test("camelToKebabCase ending in a number", () => {
  const result = camelToKebabCase("addressLine1");
  expect(result).toEqual("address-line-1");
});
