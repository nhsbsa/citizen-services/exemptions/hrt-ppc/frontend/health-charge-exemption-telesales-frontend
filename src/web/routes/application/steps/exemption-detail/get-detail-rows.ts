import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import { formatCertificateType } from "../common/formatters/certificate-type";
import {
  formatDateForDisplayFromDate,
  formatDateTimeForDisplayFromDate,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/formatters/dates";
import { getTypeTag } from "../common/type-tag";
import { translateFulfilmentToText } from "../common/preference-fulfilment-utils";
import {
  formatAuditType,
  formatAuditDescription,
  formatAuditFromAndTo,
} from "./get-audit-detail-rows-utils";
import {
  AuditResponseEntity,
  ChangesEntity,
} from "../../../../client/composition-client";
import {
  AUDIT_CITIZEN_ENTITY,
  AUDIT_INITITAL_VALUE_CHANGE_TYPE,
} from "../common/constants";

interface AuditItem {
  header: string;
  html: string;
}

const buildPersonalDetailRows = (citizenData, translate) => {
  const addresses = citizenData.addresses;
  const emails = citizenData.emails;
  const translationKeyPrefix = translate.tables.personalDetails.rows;
  return [
    {
      key: { text: translationKeyPrefix.name },
      value: {
        text: citizenData.firstName + " " + citizenData.lastName,
      },
    },
    {
      key: { text: translationKeyPrefix.address },
      value: {
        html:
          addresses === undefined
            ? ""
            : `${addresses[0].addressLine1 || ""} <br>
              ${addresses[0].addressLine2 || ""} <br>
              ${addresses[0].townOrCity || ""} <br>
              ${addresses[0].postcode || ""}`,
      },
    },
    {
      key: { text: translationKeyPrefix.dateOfBirth },
      value: {
        text: formatDateForDisplayFromDate(new Date(citizenData.dateOfBirth)),
      },
    },
    {
      key: { text: translationKeyPrefix.email },
      value: {
        text: notIsUndefinedOrNullOrEmpty(emails)
          ? `${emails[0].emailAddress || ""}`
          : "",
      },
    },
    {
      key: { text: translationKeyPrefix.nhsNumber },
      value: {
        text: citizenData.nhsNumber ?? "",
      },
    },
  ];
};

const buildCertificateDetailRows = (certificateData, translate) => {
  const translationKeyPrefix = translate.tables.certificateDetails.rows;
  return [
    {
      key: { text: translationKeyPrefix.reference },
      value: {
        text: certificateData.reference,
      },
    },
    {
      key: { text: translationKeyPrefix.type },
      value: { html: getTypeTag(formatCertificateType(certificateData.type)) },
    },
    {
      key: { text: translationKeyPrefix.status },
      value: { text: certificateData.status },
    },
    {
      key: { text: translationKeyPrefix.channel },
      value: { text: certificateData._meta.channel },
    },
    {
      key: { text: translationKeyPrefix.fulfillment },
      value: {
        text: translateFulfilmentToText(
          certificateData.preference.event,
          certificateData.preference.preference,
          translate,
        ),
      },
    },
    {
      key: { text: translationKeyPrefix.startDate },
      value: {
        text: formatDateForDisplayFromDate(new Date(certificateData.startDate)),
      },
    },
    {
      key: { text: translationKeyPrefix.endDate },
      value: {
        text: formatDateForDisplayFromDate(new Date(certificateData.endDate)),
      },
    },
    {
      key: { text: translationKeyPrefix.applicationDate },
      value: {
        text: formatDateForDisplayFromDate(
          new Date(certificateData.applicationDate),
        ),
      },
    },
  ];
};

const getNotesHeadings = (translate) => {
  const translationKeyPrefix = translate.tables.notes.rows;
  return [
    { text: translationKeyPrefix.date },
    { text: translationKeyPrefix.time },
    { text: translationKeyPrefix.agent },
    { text: translationKeyPrefix.description },
  ];
};

const buildNoteTableRows = (notesData, translate) => {
  const translationKeyPrefix = translate.tables.notes.rows;
  // the notesData is from session and mutable, when reversing it will update the session
  // to avoid this we clone the object then reverse
  const notes = [...notesData._embedded.notes].reverse();

  // return empty [] when no records
  const notesList: any = [];
  for (let i = 0; i < notes.length; i++) {
    const note = [
      {
        header: translationKeyPrefix.date,
        text: formatDateForDisplayFromDate(
          new Date(notes[i]._meta.createdTimestamp),
        ),
      },
      {
        header: translationKeyPrefix.time,
        text: formatDateTimeForDisplayFromDate(
          new Date(notes[i]._meta.createdTimestamp),
        ),
      },
      {
        header: translationKeyPrefix.agent,
        text: notes[i]._meta.createdBy,
      },
      {
        header: translationKeyPrefix.description,
        text: notes[i].description,
      },
    ];
    notesList.push(note);
  }
  return notesList;
};

const buildNotesRows = (notesData, translate) => {
  return buildNoteTableRows(notesData, translate);
};

const getRowsForCertificateDetailsTable = (certificateData, localisation) => {
  return buildCertificateDetailRows(certificateData, localisation) || [];
};

const getRowsForPersonalDetailsTable = (citizenData, localisation) => {
  return buildPersonalDetailRows(citizenData, localisation) || [];
};

const getRowsForNotesTable = (notesData, localisation) => {
  return buildNotesRows(notesData, localisation);
};

const constructAuditData = async (auditData: AuditResponseEntity) => {
  const constructedAuditData: ChangesEntity[] = [];
  let lastIndexCitizenInitial;
  for (let i = 0; i < auditData.content.length; i++) {
    const content = auditData.content[i];
    content.changes?.forEach((change) => {
      if (
        change.globalId.entity.toLowerCase().includes(AUDIT_CITIZEN_ENTITY) &&
        change.changeType === AUDIT_INITITAL_VALUE_CHANGE_TYPE
      ) {
        lastIndexCitizenInitial = i;
      }
    });
  }
  if (notIsUndefinedOrNullOrEmpty(lastIndexCitizenInitial)) {
    auditData.content.splice(lastIndexCitizenInitial);
  }

  auditData.content.forEach((item) => {
    if (item.changes) {
      // Adds objects only when the entity is a non-citizen and the change type is non-initial.
      // Adds objects when the entity is a citizen.
      const isNotInitialValueChange = (change) => change.changeType !== AUDIT_INITITAL_VALUE_CHANGE_TYPE;
      const isNotCitizenEntity = (change) => !change.globalId.entity.toLowerCase().includes(AUDIT_CITIZEN_ENTITY);
      const isCitizenEntity = (change) => change.globalId.entity.toLowerCase().includes(AUDIT_CITIZEN_ENTITY);

      const filteredChanges = item.changes.filter((change) => {
        return (isNotInitialValueChange(change) && isNotCitizenEntity(change)) || isCitizenEntity(change);
      });
      
      constructedAuditData.push(...filteredChanges);
    }
  });
  return constructedAuditData;
};

const buildAuditHistoryRows = async (
  auditData: AuditResponseEntity,
  translate,
) => {
  const translationKeyPrefix = translate.tables.auditHistory;
  const auditList: AuditItem[][] = [];
  const constructedAuditData = await constructAuditData(auditData);
  for (let i = 0; i < constructedAuditData.length; i++) {
    const audit: AuditItem[] = [
      {
        header: translationKeyPrefix.rows.date,
        html: formatDateForDisplayFromDate(
          new Date(constructedAuditData[i].commitMetadata.commitDate),
        ),
      },
      {
        header: translationKeyPrefix.rows.type,
        html: formatAuditType(
          constructedAuditData[i].globalId.entity,
          translationKeyPrefix.type,
        ),
      },
      {
        header: translationKeyPrefix.rows.from,
        html: formatAuditFromAndTo(
          constructedAuditData[i].left,
          constructedAuditData[i].globalId.entity,
          translationKeyPrefix,
        ),
      },
      {
        header: translationKeyPrefix.rows.to,
        html: formatAuditFromAndTo(
          constructedAuditData[i].right,
          constructedAuditData[i].globalId.entity,
          translationKeyPrefix,
        ),
      },
      {
        header: translationKeyPrefix.rows.description,
        html: formatAuditDescription(
          constructedAuditData[i].globalId.entity,
          constructedAuditData[i].property,
          constructedAuditData[i].right,
          translationKeyPrefix.description,
        ),
      },
    ];
    auditList.push(audit);
  }
  return auditList;
};

const getAuditHistoryHeadings = (translate) => {
  const translationKeyPrefix = translate.tables.auditHistory.rows;
  return [
    { text: translationKeyPrefix.date },
    { text: translationKeyPrefix.type },
    { text: translationKeyPrefix.from },
    { text: translationKeyPrefix.to },
    { text: translationKeyPrefix.description },
  ];
};

const getRowsForAuditHistoryTable = (
  auditData: AuditResponseEntity,
  localisation,
) => {
  return buildAuditHistoryRows(auditData, localisation) || [];
};

export {
  getRowsForCertificateDetailsTable,
  getRowsForPersonalDetailsTable,
  getRowsForNotesTable,
  getNotesHeadings,
  getRowsForAuditHistoryTable,
  getAuditHistoryHeadings,
  constructAuditData,
};
