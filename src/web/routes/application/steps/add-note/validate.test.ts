import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/test/apply-express-validation";
import { FieldValidationError } from "express-validator";

describe("validation middleware", () => {
  it.each([null, undefined, ""])(
    "throws errors when description field is %p",
    async (description: null | undefined | string) => {
      const req = {
        body: {
          description: description,
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      const errorOne = result.array()[0] as FieldValidationError;

      expect(result.array().length).toBe(1);
      expect(errorOne.path).toBe("description");
      expect(errorOne.msg).toBe("validation:missingDescription");
    },
  );

  test("throws errors when description field exceeds min length", async () => {
    const req = {
      body: {
        description: `LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum
          LorumIpsum`,
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("description");
    expect(errorOne.msg).toBe("validation:descriptionTooLong");
  });

  test("does not throw error when description is valid", async () => {
    const req = {
      body: {
        description:
          "LorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsumLorumIpsum",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(0);
  });
});
