const mockGeneratePkceCodes = {
  verifier: jest.fn(),
  challenge: jest.fn(),
};
const mockGetAuthCode = jest.fn();

const mockConfidentialClientApplication = jest.fn().mockImplementation(() => {
  return {
    generatePkceCodes: () => mockGeneratePkceCodes,
    getAuthCodeUrl: mockGetAuthCode,
  };
});
const mockCryptoProvider = jest.fn().mockImplementation(() => {
  return { generatePkceCodes: () => mockGeneratePkceCodes };
});

import { redirectToAuthCodeUrl } from "./auth";
import { environment } from "../../../config/environment";

jest.mock("@azure/msal-node", () => ({
  ConfidentialClientApplication: mockConfidentialClientApplication,
  CryptoProvider: mockCryptoProvider,
}));

const mockReq = {
  session: {},
};

const mockRes = {
  redirect: jest.fn(),
};

const mockNext = jest.fn();

beforeEach(() => {
  jest.resetAllMocks();
  jest.clearAllMocks();
  jest.resetModules();
});

describe("redirectToAuthCodeUrl", () => {
  test("should redirect to auth code url with pkce codes and request parameters in session", async () => {
    mockGetAuthCode.mockReturnValue("url.com");
    const authCodeUrlRequestParams = { scope: "openid profile" };
    const authCodeRequestParams = {
      authority: "https://login.microsoftonline.com",
    };

    await redirectToAuthCodeUrl(
      mockReq,
      mockRes,
      mockNext,
      authCodeUrlRequestParams,
      authCodeRequestParams,
    );

    expect(mockReq.session["pkceCodes"]).toEqual({
      challengeMethod: "S256",
      verifier: mockGeneratePkceCodes.verifier,
      challenge: mockGeneratePkceCodes.challenge,
    });
    expect(mockReq.session["authCodeUrlRequest"]).toEqual({
      redirectUri: environment.REDIRECT_URI,
      responseMode: "form_post",
      codeChallenge: mockGeneratePkceCodes.challenge,
      codeChallengeMethod: "S256",
      ...authCodeUrlRequestParams,
    });
    expect(mockReq.session["authCodeRequest"]).toEqual({
      redirectUri: environment.REDIRECT_URI,
      code: "",
      ...authCodeRequestParams,
    });
    expect(mockRes.redirect).toHaveBeenCalled();
    expect(mockRes.redirect).toBeCalledWith("url.com");
  });

  test("should call next with error if there`s an error while getting auth code url", async () => {
    const authCodeUrlRequestParams = { scope: "openid profile" };
    const authCodeRequestParams = {
      authority: "https://login.microsoftonline.com",
    };
    const mockError = new Error("Auth code url could not be obtained");

    mockGetAuthCode.mockRejectedValue(mockError);

    await redirectToAuthCodeUrl(
      mockReq,
      mockRes,
      mockNext,
      authCodeUrlRequestParams,
      authCodeRequestParams,
    );

    expect(mockNext).toHaveBeenCalledWith(mockError);
  });
});
