import * as httpStatus from "http-status-codes";
import { logger } from "../logger/logger";

const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const toErrorLogMessage = (error) => `${error.toString()}\n${error.stack}`;

const logErrors = (err, req, res, next) => {
  logger.error(toErrorLogMessage(err), req);
  next(err);
};

const errorHandler = (err, req, res, next) => {
  const statusCode = err.statusCode || httpStatus.INTERNAL_SERVER_ERROR;
  res.status(statusCode);

  switch (statusCode) {
    case httpStatus.REQUEST_TIMEOUT:
      res.redirect(`${CONTEXT_PATH}/timed-out`);
      break;
    case httpStatus.FORBIDDEN:
      res.redirect(`${CONTEXT_PATH}/no-access`);
      break;
    default:
      res.redirect(`${CONTEXT_PATH}/problem-with-service`);
  }
  next();
};

const registerErrorHandlers = (app) => {
  app.use(logErrors);
  app.use(errorHandler);
};

export { errorHandler, registerErrorHandlers };
