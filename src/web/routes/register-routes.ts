import { registerCookieRoutes } from "./cookies/register-cookie-routes";
import { getLanguageBase } from "./language";
import { registerStartRoute } from "./start/start";
import { registerJourneys } from "./application/register-journeys";
import { JOURNEYS } from "./application/journey-definitions";
import { registerProblemWithServiceError } from "./error-pages/problem-with-service";
import { registerPageNotFoundRoute } from "./error-pages/page-not-found";
import { registerNoAccessError } from "./error-pages/no-access";
import { registerAuthRoutes } from "./azure-routes/auth-routes";
import { getUserName } from "../../common/azure-utils";
import {
  SIGN_OUT_URL,
  LOCAL_SIGN_OUT_URL,
  LOCAL_USER_NAME,
} from "../routes/application/steps/common/constants";
const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const setCommonTemplateValues = (req, res, next) => {
  res.locals.htmlLang = req.language;
  res.locals.language = getLanguageBase(req.language);
  res.locals.cookieLinkName = req.t("cookies.linkName");
  res.locals.privacyNoticeLinkName = req.t("privacyNotice.linkName");
  res.locals.footerAccessibilityLink = req.t("footer.accessibilityLink");
  res.locals.footerContactUsLink = req.t("footer.contactUsLink");
  res.locals.footerCookiesLink = req.t("footer.cookiesLink");
  res.locals.footerPrivacyLink = req.t("footer.privacyLink");
  res.locals.footerTermsLink = req.t("footer.termsLink");
  res.locals.phaseBannerHtml = req.t("phaseBanner.html");
  res.locals.back = req.t("back");
  res.locals.serviceName = req.t("header.serviceName");
  res.locals.serviceDescription = req.t("header.serviceDescription");
  res.locals.signoutText = req.t("header.signoutText");
  (res.locals.backButtonText = req.t("buttons:back")),
    (res.locals.cancelButtonText = req.t("buttons:cancel")),
    (res.locals.username =
      getUserName(req.session) === undefined
        ? LOCAL_USER_NAME
        : getUserName(req.session));
  res.locals.signoutUrl =
    getUserName(req.session) === undefined
      ? LOCAL_SIGN_OUT_URL
      : CONTEXT_PATH + SIGN_OUT_URL;
  next();
};

const registerRoutes = (config, app) => {
  app.use(setCommonTemplateValues);
  registerAuthRoutes(app);
  registerJourneys(JOURNEYS)(config, app);
  registerCookieRoutes(app);
  registerStartRoute(app);
  registerProblemWithServiceError(app);
  registerNoAccessError(app);
  // Page not found route should always be registered last as it is a catch all route
  registerPageNotFoundRoute(app);
};

export { registerRoutes, setCommonTemplateValues };
