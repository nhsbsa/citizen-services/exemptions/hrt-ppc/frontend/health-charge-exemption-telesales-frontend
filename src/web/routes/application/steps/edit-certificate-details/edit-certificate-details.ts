import {
  EDIT_CERTIFICATE_DETAILS_URL,
  EXEMPTION_VIEW_URL,
} from "../../paths/paths";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import { findRecordInSearchResults } from "../common/search-utils";
import { isEligibleForEditing } from "../common/certificate-utils";
import { behaviourForGet } from "./behaviour-for-get";
import { behaviourForPost } from "./behaviour-for-post";
import { validate } from "./validate";
import { sanitize } from "./sanitize";
import { Request, Response } from "express";
import { getTypeTag } from "../common/type-tag";
import { formatCertificateType } from "../common/formatters/certificate-type";
import { formatDateForDisplayFromDate } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/formatters/dates";
import { deriveFulfilmentOnEvent } from "../common/preference-fulfilment-utils";

const pageContent = ({ translate, req }) => ({
  title: translate("editCertificateDetails.title"),
  heading: translate("editCertificateDetails.heading"),
  preferencesText: {
    email: translate("preferencesText.email"),
    post: translate("preferencesText.post"),
    none: translate("preferencesText.none"),
  },
  warningCalloutHeading: translate(
    "editCertificateDetails.warningCalloutHeading",
  ),
  warningCalloutHTML: translate("editCertificateDetails.warningCalloutHTML"),
  summaryCardTitle: translate("editCertificateDetails.summaryCardTitle"),
  certificateNumberLabel: translate(
    "editCertificateDetails.certificateNumberLabel",
  ),
  certificateTypeLabel: translate(
    "editCertificateDetails.certificateTypeLabel",
  ),
  certificateStatusLabel: translate(
    "editCertificateDetails.certificateStatusLabel",
  ),
  certificateChannelLabel: translate(
    "editCertificateDetails.certificateChannelLabel",
  ),
  certificateStartLabel: translate(
    "editCertificateDetails.certificateStartLabel",
  ),
  certificateExpiryLabel: translate(
    "editCertificateDetails.certificateExpiryLabel",
  ),
  applicationDateLabel: translate(
    "editCertificateDetails.applicationDateLabel",
  ),
  certificateFulfillmentLabel: translate(
    "editCertificateDetails.certificateFulfillmentLabel",
  ),
  certificateFulfillmentEmailText: translate(
    "editCertificateDetails.certificateFulfillmentEmailText",
  ),
  certificateFulfillmentPostText: translate(
    "editCertificateDetails.certificateFulfillmentPostText",
  ),
  submitButtonText: translate("buttons:submit"),
  previous: `${EXEMPTION_VIEW_URL}/${req.params.id}`,
});

const isNavigable = (req: Request) => {
  return (
    notIsUndefinedOrNullOrEmpty(findRecordInSearchResults(req)) &&
    req.session.certificate?.reference === req.params.id
  );
};

const copyCertificateSessionDataToLocals = (req: Request, res: Response) => {
  const certificateData = req.session.certificate;
  res.locals.certificateDetailsData = {
    reference: certificateData.reference,
    type: getTypeTag(formatCertificateType(certificateData.type)),
    status: certificateData.status,
    channel: certificateData._meta.channel,
    startDate: formatDateForDisplayFromDate(
      new Date(certificateData.startDate),
    ),
    endDate: formatDateForDisplayFromDate(new Date(certificateData.endDate)),
    applicationDate: formatDateForDisplayFromDate(
      new Date(certificateData.applicationDate),
    ),
    certificateFulfillment: deriveFulfilmentOnEvent(
      certificateData["preference"].event,
      certificateData["preference"].preference,
    ),
    isStartDateEligibleForEditing: isEligibleForEditing(certificateData),
  };
};

const editCertificateDetails = {
  path: `/:id(HRT[A-Za-z0-9]+)${EDIT_CERTIFICATE_DETAILS_URL}`,
  template: "edit-certificate-details",
  pageContent,
  isNavigable,
  behaviourForGet,
  behaviourForPost,
  sanitize,
  validate,
};

export {
  editCertificateDetails,
  pageContent,
  isNavigable,
  behaviourForGet,
  behaviourForPost,
  copyCertificateSessionDataToLocals,
};
