import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import { EXEMPTION_VIEW_URL } from "../../paths/paths";
import { findRecordInSearchResults } from "../common/search-utils";
import { behaviourForGet } from "./behaviour-for-get";
import { pageContent } from "./page-content";

const isNavigable = (req) => {
  return notIsUndefinedOrNullOrEmpty(findRecordInSearchResults(req));
};

const exemptionDetail = {
  // ID: context/page_url/HRT1D612A3C
  path: `${EXEMPTION_VIEW_URL}/:id(HRT[A-Za-z0-9]+)/`,
  template: "exemption-detail",
  pageContent,
  isNavigable,
  behaviourForGet,
};

export { exemptionDetail, behaviourForGet, pageContent, isNavigable };
