const mockSessionDestroyButKeepLoggedIn = jest.fn();

import expect from "expect";
import {
  getPageNotFoundErrorPage,
  registerPageNotFoundRoute,
} from "./page-not-found";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";
jest.mock("../application/steps/common/session-destroy");

import { injectAdditionalHeaders } from "../application/flow-control/middleware/inject-headers";
import { getErrorPageContent } from "./error-page-content";
jest.mock("../application/flow-control/middleware/inject-headers");

jest.mock("../application/steps/common/session-destroy", () => ({
  sessionDestroyButKeepLoggedIn: mockSessionDestroyButKeepLoggedIn,
}));

const PAGE_NOT_FOUND = "/test-context/page-not-found";
const STATUS_CODE = 404;

const req = {
  t: (string) => string,
};
const app = {
  ...jest.requireActual("express"),
  get: jest.fn(),
  all: jest.fn(),
};

const res = {
  redirect: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};

test(`registerPageNotFoundRoute() should redirect to ${PAGE_NOT_FOUND} with response status as ${STATUS_CODE}`, () => {
  app.all.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res);
  });

  registerPageNotFoundRoute(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(
    1,
    PAGE_NOT_FOUND,
    configureAuthentication,
    expect.anything(),
  );
  expect(res.status).toBeCalledWith(STATUS_CODE);
  expect(res.redirect).toBeCalledWith(PAGE_NOT_FOUND);
});

test(`getPageNotFoundErrorPage() should call sessionDestroy function and render the page`, () => {
  app.all.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res);
  });

  getPageNotFoundErrorPage(req, res);

  expect(mockSessionDestroyButKeepLoggedIn).toBeCalled();
  expect(mockSessionDestroyButKeepLoggedIn).toBeCalledWith(req);
  expect(injectAdditionalHeaders).toHaveBeenCalledTimes(1);
  expect(res.render).toBeCalledWith(
    "error-pages/page-not-found",
    getErrorPageContent(req),
  );
});
