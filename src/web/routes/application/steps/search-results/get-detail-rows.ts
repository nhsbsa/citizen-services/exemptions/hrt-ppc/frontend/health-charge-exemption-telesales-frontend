import { contextPath } from "../../paths/context-path";
import { EXEMPTION_VIEW_URL } from "../../paths/paths";
import { formatCertificateType } from "../common/formatters/certificate-type";
import { formatDateForDisplayFromDate } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/formatters/dates";

const buildRow = (certificate: CertificateTransformed, translate) => {
  const addresses = certificate.citizen.addresses;
  return [
    {
      header: translate.tableHeadingOne,
      html: generateCertificateLink(certificate),
    },
    {
      header: translate.tableHeadingTwo,
      text: certificate.citizen.firstName + " " + certificate.citizen.lastName,
    },
    {
      header: translate.tableHeadingThree,
      text:
        addresses === undefined
          ? ""
          : `${addresses[0].addressLine1 || ""} ${
              addresses[0].addressLine2 || ""
            } ${addresses[0].townOrCity || ""}`,
    },
    {
      header: translate.tableHeadingFour,
      text: addresses === undefined ? "" : addresses[0].postcode,
    },
    {
      header: translate.tableHeadingFive,
      text: formatDateForDisplayFromDate(
        new Date(certificate.citizen.dateOfBirth),
      ),
    },
    {
      header: translate.tableHeadingSix,
      text: formatCertificateType(certificate.type),
    },
    {
      header: translate.tableHeadingSeven,
      text: certificate.status,
    },
    {
      header: translate.tableHeadingEight,
      text: formatDateForDisplayFromDate(new Date(certificate.startDate)),
    },
    {
      header: translate.tableHeadingNine,
      text: formatDateForDisplayFromDate(new Date(certificate.endDate)),
    },
  ];
};

function generateCertificateLink(certificate: CertificateTransformed) {
  const url = contextPath(`${EXEMPTION_VIEW_URL}/${certificate.reference}`);
  return `<a role="button" href=${url} id="cert-link-${certificate.reference}">${certificate.reference}</a>`;
}

const getCertificateAndCitizenDetailsRows = (results, localisation) => {
  const rows: any = [];

  for (let i = 0; i < results.length; i++) {
    const row = buildRow(results[i], localisation);
    rows.push(row);
  }
  return rows;
};

export { getCertificateAndCitizenDetailsRows, buildRow };
