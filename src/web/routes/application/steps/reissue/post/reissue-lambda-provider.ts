import { config } from "../../../../../../config";
import { reissueClient } from "../../../../../client/reissue-client";

const makeReissuePostRequest = async (session) => {
  const client = new reissueClient(config);
  const body = createRequestBody(session);
  const response = await client.makeRequest(
    {
      method: "POST",
      url: "/v1/reissue",
      data: body,
      responseType: "json",
    },
    session,
  );
  return response;
};

const createRequestBody = (session) => {
  const citizenId = session.citizen.id;
  const certificate: Certificate = session.certificate;
  const request = {
    citizenId: citizenId,
    certificate: {
      id: certificate.id,
      reference: certificate.reference,
      type: certificate.type,
      startDate: certificate.startDate,
      endDate: certificate.endDate,
    },
  };

  return request;
};

export { createRequestBody, makeReissuePostRequest };
