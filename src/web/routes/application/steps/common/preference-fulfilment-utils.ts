import { NONE } from "./constants";
import {
  Event,
  Preference,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { formatCertificateFulfillment } from "./formatters/certificate-fulfillment";

export function translateFulfilmentToText(
  event: Event,
  preference: Preference,
  translate: string,
): string {
  if (event === Event.ANY) {
    return formatCertificateFulfillment(preference, translate);
  }
  return formatCertificateFulfillment(NONE, translate);
}

export function deriveFulfilmentOnEvent(
  event: Event,
  preference: Preference,
): string {
  if (event === Event.ANY) {
    return preference.toString();
  } else {
    return NONE;
  }
}
