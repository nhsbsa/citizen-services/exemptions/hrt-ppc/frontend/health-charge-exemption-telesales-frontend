import { config } from "../../../../../config";
import { contextPath } from "../../paths/context-path";
import { searchResultsFound } from "./search-results";

const PAGE_SIZE = config.environment.SEARCH_RESULTS_PAGE_SIZE;

const buildPageSection = (currentPage: number, pageNumber: number) => {
  return {
    text: `${pageNumber}`,
    href: `${contextPath(searchResultsFound.path)}?page=${pageNumber}`,
    selected: Number(currentPage) === Number(pageNumber) ? true : false,
  };
};

const getPagination = (
  currentPage: number,
  count,
  totalPages,
  localisation,
) => {
  const items: any = [];

  for (let pageNumber = 1; pageNumber < totalPages + 1; pageNumber++) {
    const row = buildPageSection(currentPage, pageNumber);
    items.push(row);
  }

  const startPageOneIrrelevant = (currentPage - 1) * PAGE_SIZE + 1;
  const start = currentPage === 1 ? 1 : startPageOneIrrelevant;
  const endLastPageIrrelevant = currentPage * PAGE_SIZE;
  const end = endLastPageIrrelevant > count ? count : endLastPageIrrelevant;

  const currentPageAsNumber = Number(currentPage);
  const previous =
    currentPageAsNumber !== 1
      ? {
          text: localisation.paginationPrevious,
          href: `${contextPath(searchResultsFound.path)}?page=${
            currentPage - 1
          }`,
        }
      : undefined;

  const next =
    currentPage < totalPages
      ? {
          text: localisation.paginationNext,
          href: `${contextPath(searchResultsFound.path)}?page=${
            currentPageAsNumber + Number(1)
          }`,
        }
      : undefined;

  return {
    items: items,
    results: {
      count: count,
      from: start,
      to: end,
      text: localisation.paginationResults,
    },
    previous: previous,
    next: next,
  };
};

export { getPagination, buildPageSection };
