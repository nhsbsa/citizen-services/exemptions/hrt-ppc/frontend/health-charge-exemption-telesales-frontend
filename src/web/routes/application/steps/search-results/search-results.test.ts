import { IN_PROGRESS, VIEW } from "../../flow-control/states";
import {
  buildSessionForJourney,
  getStateForJourney,
} from "../../flow-control/test-utils/test-utils";
import {
  CertificateStatus,
  CertificateType,
} from "@nhsbsa/health-charge-exemption-common-frontend";

const config = { server: { CONTEXT_PATH: "/test-context" } };
const journey = { name: "search" };
const exampleRows = [{ row: "data" }];
const firstResult = {
  reference: "1",
  type: CertificateType.HRT_PPC,
  status: CertificateStatus.PENDING,
  startDate: new Date(),
  endDate: new Date(),
  citizen: {
    firstName: "Fname1",
    lastName: "Lname1",
    dateOfBirth: new Date(),
  },
};
const secondResult = {
  reference: "2",
  type: CertificateType.HRT_PPC,
  status: CertificateStatus.PENDING,
  startDate: new Date(),
  endDate: new Date(),
  citizen: {
    firstName: "Fname2",
    lastName: "Lname2",
    dateOfBirth: new Date(),
  },
};
const thirdResult = {
  reference: "3",
  type: CertificateType.HRT_PPC,
  status: CertificateStatus.PENDING,
  startDate: new Date(),
  endDate: new Date(),
  citizen: {
    firstName: "Fname3",
    lastName: "Lname3",
    dateOfBirth: new Date(),
  },
};

const mockGetCertificateAndCitizenDetailsRows = jest
  .fn()
  .mockReturnValue(exampleRows);
const mockGetPagination = jest.fn().mockReturnValue("data");

import { behaviourForGet, pageContent, isNavigable } from "./search-results";

jest.mock("./get-detail-rows", () => ({
  getCertificateAndCitizenDetailsRows: mockGetCertificateAndCitizenDetailsRows,
}));
jest.mock("./get-pagination", () => ({
  getPagination: mockGetPagination,
}));

beforeEach(() => {
  jest.clearAllMocks();
});

describe("pageContent()", () => {
  test(`should return expected results`, () => {
    const translate = (string) => string;
    const expected = {
      title: translate("searchResults.title"),
      heading: translate("searchResults.heading"),
      searchBoxHeading: translate("searchResults.searchBoxHeading"),
      paragraphOne: translate("searchResults.paragraphOne"),
      tableHeadingOne: translate("searchResults.tableHeadingOne"),
      tableHeadingTwo: translate("searchResults.tableHeadingTwo"),
      tableHeadingThree: translate("searchResults.tableHeadingThree"),
      tableHeadingFour: translate("searchResults.tableHeadingFour"),
      tableHeadingFive: translate("searchResults.tableHeadingFive"),
      tableHeadingSix: translate("searchResults.tableHeadingSix"),
      tableHeadingSeven: translate("searchResults.tableHeadingSeven"),
      tableHeadingEight: translate("searchResults.tableHeadingEight"),
      tableHeadingNine: translate("searchResults.tableHeadingNine"),
      paginationPrevious: translate("previous"),
      paginationNext: translate("next"),
      paginationResults: translate("results"),
      newApplicationButtonText: translate("buttons:newApplication"),
      previous: false,
    };
    const result = pageContent({ translate });

    expect(result).toEqual(expected);
  });
});

describe("behaviourForGet()", () => {
  it.each([undefined, ""])(
    `sets state to ${VIEW} and sets page 1 results when page query param is %p`,
    (page: string | undefined) => {
      const resultArray = [firstResult];
      const req = {
        query: { page: page },
        t: (string) => `${string}`,
        session: {
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          searchResult: resultArray,
        },
      };

      const res = { locals: {} };
      const next = jest.fn();
      const expectedState = VIEW;

      behaviourForGet(config, journey)(req, res, next);

      expect(mockGetCertificateAndCitizenDetailsRows).toBeCalledWith(
        [firstResult],
        expect.anything(),
      );
      expect(res).toEqual({
        locals: {
          results: exampleRows,
          totalCount: 1,
          totalPages: 1,
          pagination: "data",
        },
      });
      expect(next).toBeCalledTimes(1);
      expect(getStateForJourney(journey.name, req)).toBe(expectedState);
    },
  );

  test(`sets state to ${VIEW} and sets page 1 results when page query param with 1`, () => {
    const resultArray = [firstResult, secondResult, thirdResult];
    const req = {
      query: { page: 1 },
      t: (string) => `${string}`,
      session: {
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
        searchResult: resultArray,
      },
    };

    const res = { locals: {} };
    const next = jest.fn();
    const expectedState = VIEW;

    behaviourForGet(config, journey)(req, res, next);

    expect(mockGetCertificateAndCitizenDetailsRows).toBeCalledWith(
      [firstResult, secondResult],
      expect.anything(),
    );
    expect(res).toEqual({
      locals: {
        results: exampleRows,
        totalCount: 3,
        totalPages: 2,
        pagination: "data",
      },
    });
    expect(next).toBeCalledTimes(1);
    expect(getStateForJourney(journey.name, req)).toBe(expectedState);
  });

  test(`sets state to ${VIEW} and sets page 2 results when page query param with 2`, () => {
    const resultArray = [firstResult, secondResult, thirdResult];
    const req = {
      query: { page: 2 },
      t: (string) => `${string}`,
      session: {
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
        searchResult: resultArray,
      },
    };

    const res = { locals: {} };
    const next = jest.fn();
    const expectedState = VIEW;

    behaviourForGet(config, journey)(req, res, next);

    expect(mockGetCertificateAndCitizenDetailsRows).toBeCalledWith(
      [thirdResult],
      expect.anything(),
    );
    expect(res).toEqual({
      locals: {
        results: exampleRows,
        totalCount: 3,
        totalPages: 2,
        pagination: "data",
      },
    });
    expect(next).toBeCalledTimes(1);
    expect(getStateForJourney(journey.name, req)).toBe(expectedState);
  });

  test(`sets state to ${VIEW} and redirects to first page when query param is more than total pages`, () => {
    const resultArray = [firstResult];
    const req = {
      query: { page: 2 },
      t: (string) => `${string}`,
      session: {
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
        searchResult: resultArray,
      },
    };

    const res = {
      redirect: jest.fn(),
      end: jest.fn(),
    };
    const next = jest.fn();
    const expectedState = VIEW;

    behaviourForGet(config, journey)(req, res, next);

    expect(mockGetCertificateAndCitizenDetailsRows).not.toBeCalled();
    expect(res.redirect).toBeCalledWith(
      config.server.CONTEXT_PATH + "/search-results?page=1",
    );
    expect(res.end).toBeCalledTimes(1);
    expect(next).not.toBeCalled();
    expect(getStateForJourney(journey.name, req)).toBe(expectedState);
  });

  it.each(["abcd", "a b c", "  ", " ", "@!@££*£*£test"])(
    `sets state to ${VIEW} and redirects to first page when query param is %p`,
    (page: string) => {
      const resultArray = [firstResult];
      const req = {
        query: { page: page },
        t: (string) => `${string}`,
        session: {
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          searchResult: resultArray,
        },
      };

      const res = {
        redirect: jest.fn(),
        end: jest.fn(),
      };
      const next = jest.fn();
      const expectedState = VIEW;

      behaviourForGet(config, journey)(req, res, next);

      expect(mockGetCertificateAndCitizenDetailsRows).not.toBeCalled();
      expect(res.redirect).toBeCalledWith(
        config.server.CONTEXT_PATH + "/search-results?page=1",
      );
      expect(res.end).toBeCalledTimes(1);
      expect(next).not.toBeCalled();
      expect(getStateForJourney(journey.name, req)).toBe(expectedState);
    },
  );

  test(`sets state to ${VIEW} and redirects to first page when query param is 0`, () => {
    const resultArray = [firstResult];
    const req = {
      query: { page: -1 },
      t: (string) => `${string}`,
      session: {
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
        searchResult: resultArray,
      },
    };

    const res = {
      redirect: jest.fn(),
      end: jest.fn(),
    };
    const next = jest.fn();
    const expectedState = VIEW;

    behaviourForGet(config, journey)(req, res, next);

    expect(mockGetCertificateAndCitizenDetailsRows).not.toBeCalled();
    expect(res.redirect).toBeCalledWith(
      config.server.CONTEXT_PATH + "/search-results?page=1",
    );
    expect(res.end).toBeCalledTimes(1);
    expect(next).not.toBeCalled();
    expect(getStateForJourney(journey.name, req)).toBe(expectedState);
  });
});

describe("isNavigable()", () => {
  it.each([[], undefined, ""])(
    `returns false when search results in session set to %p`,
    (searchResult: unknown) => {
      const req = {
        session: {
          searchResult: searchResult,
        },
      };

      const result = isNavigable(req);

      expect(result).toBeFalsy();
    },
  );

  it.each([[{ data: "something" }]])(
    `returns true when search results in session set to %p`,
    (searchResult: unknown) => {
      const req = {
        session: {
          searchResult: searchResult,
        },
      };

      const result = isNavigable(req);

      expect(result).toBeTruthy();
    },
  );
});
