import expect from "expect";
import { states } from "../../flow-control";
import { stateMachine } from "../../flow-control/state-machine";
import { VIEW } from "../../flow-control/states";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";

const isEmpty = jest.fn();
const mockSearch = jest.fn();

import {
  searchRecords,
  pageContent,
  sanitize,
  validate,
  behaviourForPost,
} from "./search-records";

jest.mock("./post", () => ({
  search: mockSearch,
}));

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

jest.mock("../../../../../config", () => ({
  config: {
    environment: {
      HRT_PPC_VALUE: 1930,
    },
  },
}));

test(`searchRecords should match expected outcomes`, () => {
  const expectedResults = {
    path: "/search-hrt-records",
    template: "search-hrt-records",
    pageContent,
    sanitize,
    validate,
    behaviourForPost,
  };

  expect(searchRecords).toEqual(expectedResults);
});

test(`pageContent() should return expected results`, () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("start.title"),
    heading: translate("start.heading"),
    warningCalloutHeading: translate(
      "start.warningCallout.warningCalloutHeading",
    ),
    warningCalloutHTML: translate("start.warningCallout.warningCalloutHTML", {
      ppcValue: "£19.30",
    }),
    accessKeyButtonText: translate("buttons:accessKey"),
    accessKeyLink: "/test-context/search-hrt-records",
    searchBoxHeading: translate("start.searchBoxHeading"),
    lastNameLabel: translate("start.lastNameLabel"),
    dateOfBirthLabel: translate("start.dateOfBirthLabel"),
    hint: translate("start.hint"),
    dayLabel: translate("start.dayLabel"),
    monthLabel: translate("start.monthLabel"),
    yearLabel: translate("start.yearLabel"),
    postcodeLabel: translate("start.postcodeLabel"),
    submitButtonText: translate("buttons:submit"),
    cancelButtonText: translate("buttons:cancel"),
    previous: false,
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

test("behaviourForPost() does not call search on validation error", async () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(false);
  const res = {};

  const req = {
    session: {
      searchResult: undefined,
      ...buildSessionForJourney({
        journeyName: "search",
        state: states.IN_PROGRESS,
        nextAllowedPath: "/next-path",
      }),
    },
  };
  const next = jest.fn();
  const config = {};
  const journey = { name: "search" };

  await behaviourForPost(config, journey)(req, res, next);

  expect(mockSearch).not.toBeCalled();
  expect(next).toBeCalledTimes(1);
  expect(req.session.searchResult).toBeUndefined();
  expect(stateMachine.getState(req, journey)).toEqual(states.IN_PROGRESS);
});

test("behaviourForPost() sets the search results in session and state to COMPLETED_SEARCH", async () => {
  // Set return values for stubs
  isEmpty.mockReturnValue(true);
  const res = {};
  const searchResult = { date: "dateTest" };
  mockSearch.mockResolvedValue(searchResult);

  const journey = { name: "search" };
  const req = {
    session: {
      searchResult: undefined,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: VIEW,
        nextAllowedPath: undefined,
      }),
    },
  };
  const next = jest.fn();
  const config = {};

  await behaviourForPost(config, journey)(req, res, next);

  expect(mockSearch).toBeCalledTimes(1);
  expect(mockSearch).toBeCalledWith(req, res, next);
  expect(next).toBeCalledTimes(1);
  expect(req.session.searchResult).toEqual({ date: "dateTest" });
  expect(stateMachine.getState(req, journey)).toEqual(states.COMPLETED_SEARCH);
});
