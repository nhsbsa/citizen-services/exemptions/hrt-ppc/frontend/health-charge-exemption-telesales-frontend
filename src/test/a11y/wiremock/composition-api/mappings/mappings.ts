const SUCCESS = "success";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";
import v1AuditHistory from "./v1-get-audit-history";

const RESPONSE_MAP = {
  [SUCCESS]: v1AuditHistory,
};

const createGetAuditHistorySuccessMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "POST",
      url: `/v1/exemptions/audit`,
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
        channel: {
          equalTo: Channel.STAFF,
        },
        "user-id": {
          equalTo: "LOCAL_USER_CYPHER",
        },
        "correlation-id": {
          matches: "([A-Za-z0-9_-])+",
        },
      },
    },
    response: RESPONSE_MAP[scenario],
  });

export { createGetAuditHistorySuccessMapping, SUCCESS };
