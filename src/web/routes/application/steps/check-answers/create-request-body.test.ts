import expect from "expect";
import moment from "moment";
import {
  createIssueCertificateRequestBody,
  createPaymentRequestBody,
  createCardPaymentRequestBody,
} from "./create-request-body";
import {
  Preference,
  CertificateType,
  PaymentMethod,
} from "@nhsbsa/health-charge-exemption-common-frontend";

jest.mock("../../../../../config", () => ({
  config: {
    environment: {
      HRT_PPC_VALUE: 1930,
    },
  },
}));

jest.mock("moment", () => {
  const mockMoment = jest.fn(() => ({
    toDate: jest.fn(() => new Date("2023-08-21")),
  }));

  return mockMoment;
});

const requestBody = {
  citizen: {
    firstName: "fname",
    lastName: "lname",
    dateOfBirth: "1995-01-01",
    nhsNumber: "9898989898",
    addresses: [
      {
        addressLine1: "address one",
        addressLine2: "address two",
        townOrCity: "address town",
        postcode: "NE15 8NY",
      },
    ],
    emails: [
      {
        emailAddress: "email@test.com",
      },
    ],
    telephones: [
      {
        phoneNumber: "7999999999",
      },
    ],
  },
  certificate: {
    type: CertificateType.HRT_PPC,
    duration: "P12M",
    cost: 1930,
    startDate: "2023-04-19",
    endDate: "2024-09-13",
    applicationDate: moment("2023-08-21T00:00:00.000Z").toDate(),
  },
  userPreference: Preference.POSTAL,
};

const requestBodyWithoutEmail = {
  citizen: {
    firstName: "fname",
    lastName: "lname",
    dateOfBirth: "1995-01-01",
    nhsNumber: "9898989898",
    addresses: [
      {
        addressLine1: "address one",
        addressLine2: "address two",
        townOrCity: "address town",
        postcode: "NE15 8NY",
      },
    ],
    telephones: [
      {
        phoneNumber: "7999999999",
      },
    ],
  },
  certificate: {
    type: CertificateType.HRT_PPC,
    duration: "P12M",
    cost: 1930,
    startDate: "2023-04-19",
    endDate: "2024-09-13",
    applicationDate: moment("2023-08-21T00:00:00.000Z").toDate(),
  },
  userPreference: Preference.POSTAL,
};

const requestBodyWithoutPhoneNumber = {
  citizen: {
    firstName: "fname",
    lastName: "lname",
    dateOfBirth: "1995-01-01",
    nhsNumber: "9898989898",
    addresses: [
      {
        addressLine1: "address one",
        addressLine2: "address two",
        townOrCity: "address town",
        postcode: "NE15 8NY",
      },
    ],
    emails: [
      {
        emailAddress: "email@test.com",
      },
    ],
  },
  certificate: {
    type: CertificateType.HRT_PPC,
    duration: "P12M",
    cost: 1930,
    startDate: "2023-04-19",
    endDate: "2024-09-13",
    applicationDate: moment("2023-08-21T00:00:00.000Z").toDate(),
  },
  userPreference: Preference.POSTAL,
};

const requestBodyWithoutAddressLineTwo = {
  citizen: {
    firstName: "fname",
    lastName: "lname",
    dateOfBirth: "1995-01-01",
    nhsNumber: "9898989898",
    addresses: [
      {
        addressLine1: "address one",
        townOrCity: "address town",
        postcode: "NE15 8NY",
      },
    ],
    emails: [
      {
        emailAddress: "email@test.com",
      },
    ],
    telephones: [
      {
        phoneNumber: "7999999999",
      },
    ],
  },
  certificate: {
    type: CertificateType.HRT_PPC,
    duration: "P12M",
    cost: 1930,
    startDate: "2023-04-19",
    endDate: "2024-09-13",
    applicationDate: moment("2023-08-21T00:00:00.000Z").toDate(),
  },
  userPreference: Preference.POSTAL,
};

const requestBodyWithoutNhsNumber = {
  citizen: {
    firstName: "fname",
    lastName: "lname",
    dateOfBirth: "1995-01-01",
    addresses: [
      {
        addressLine1: "address one",
        addressLine2: "address two",
        townOrCity: "address town",
        postcode: "NE15 8NY",
      },
    ],
    emails: [
      {
        emailAddress: "email@test.com",
      },
    ],
    telephones: [
      {
        phoneNumber: "7999999999",
      },
    ],
  },
  certificate: {
    type: CertificateType.HRT_PPC,
    duration: "P12M",
    cost: 1930,
    startDate: "2023-04-19",
    endDate: "2024-09-13",
    applicationDate: moment("2023-08-21T00:00:00.000Z").toDate(),
  },
  userPreference: Preference.POSTAL,
};
describe("createIssueCertificateRequestBody()", () => {
  it.each([
    ["email@test.com", requestBody],
    ["", requestBodyWithoutEmail],
    [null, requestBodyWithoutEmail],
    [undefined, requestBodyWithoutEmail],
  ])(
    "should return request body in correct format when emailAddress is set to %p",
    (emailAddress, expected) => {
      const req = {
        session: {
          newApplicant: {
            firstName: "fname",
            lastName: "lname",
            dateOfBirth: "1995-01-01",
            nhsNumber: "9898989898",
            addressLine1: "address one",
            addressLine2: "address two",
            sanitizedPostcode: "NE15 8NY",
            townOrCity: "address town",
            emailAddress: emailAddress,
            telephoneNumber: "7999999999",
            certificateStartDate: "2023-04-19",
            certificateEndDate: "2024-09-13",
            certificateFulfillment: Preference.POSTAL,
          },
        },
      };

      const result = createIssueCertificateRequestBody(req.session);

      expect(result).toEqual(expected);
    },
  );

  it.each([
    ["7999999999", requestBody],
    ["", requestBodyWithoutPhoneNumber],
    [null, requestBodyWithoutPhoneNumber],
    [undefined, requestBodyWithoutPhoneNumber],
  ])(
    "should return request body in correct format when phoneNumber is set to %p",
    (phoneNumber, expected) => {
      const req = {
        session: {
          newApplicant: {
            firstName: "fname",
            lastName: "lname",
            dateOfBirth: "1995-01-01",
            nhsNumber: "9898989898",
            addressLine1: "address one",
            addressLine2: "address two",
            sanitizedPostcode: "NE15 8NY",
            townOrCity: "address town",
            emailAddress: "email@test.com",
            telephoneNumber: phoneNumber,
            certificateStartDate: "2023-04-19",
            certificateEndDate: "2024-09-13",
            certificateFulfillment: Preference.POSTAL,
          },
        },
      };

      const result = createIssueCertificateRequestBody(req.session);

      expect(result).toEqual(expected);
    },
  );

  it.each([
    ["9898989898", requestBody],
    ["", requestBodyWithoutNhsNumber],
    [null, requestBodyWithoutNhsNumber],
    [undefined, requestBodyWithoutNhsNumber],
  ])(
    "should return request body in correct format when nhs number is set to %p",
    (nhsNumber, expected) => {
      const req = {
        session: {
          newApplicant: {
            firstName: "fname",
            lastName: "lname",
            dateOfBirth: "1995-01-01",
            nhsNumber: nhsNumber,
            addressLine1: "address one",
            addressLine2: "address two",
            sanitizedPostcode: "NE15 8NY",
            townOrCity: "address town",
            emailAddress: "email@test.com",
            telephoneNumber: "7999999999",
            certificateStartDate: "2023-04-19",
            certificateEndDate: "2024-09-13",
            certificateFulfillment: Preference.POSTAL,
          },
        },
      };

      const result = createIssueCertificateRequestBody(req.session);

      expect(result).toEqual(expected);
    },
  );

  it.each([
    ["address two", requestBody],
    ["", requestBodyWithoutAddressLineTwo],
    [null, requestBodyWithoutAddressLineTwo],
    [undefined, requestBodyWithoutAddressLineTwo],
  ])(
    "should return request body in correct format when addressLine2 is set to %p",
    (addressLine2, expected) => {
      const req = {
        session: {
          newApplicant: {
            firstName: "fname",
            lastName: "lname",
            dateOfBirth: "1995-01-01",
            nhsNumber: "9898989898",
            addressLine1: "address one",
            addressLine2: addressLine2,
            sanitizedPostcode: "NE15 8NY",
            townOrCity: "address town",
            emailAddress: "email@test.com",
            telephoneNumber: "7999999999",
            certificateStartDate: "2023-04-19",
            certificateEndDate: "2024-09-13",
            certificateFulfillment: Preference.POSTAL,
          },
        },
      };

      const result = createIssueCertificateRequestBody(req.session);

      expect(result).toEqual(expected);
    },
  );
});

describe("createPaymentRequestBody()", () => {
  test("formats and populates the request payment body", () => {
    const expected = {
      amount: 1930,
      date: moment("2023-08-21T00:00:00.000Z").toDate(),
      method: PaymentMethod.CARD,
    };
    const result = createPaymentRequestBody();

    expect(result).toEqual(expected);
  });
});

describe("createCardPaymentRequestBody()", () => {
  test("formats and populates the request card payments body", () => {
    const req = {
      session: {
        issuedCertificateDetails: {
          certificateReference: "test-reference",
        },
        paymentDetails: {
          transactionId: "test-trasaction",
        },
      },
    };

    const expected = {
      reference: req.session.issuedCertificateDetails.certificateReference,
      transactionId: req.session.paymentDetails.transactionId,
      amount: 1930,
      serviceDescription: "Twelve Months single payment",
    };
    const result = createCardPaymentRequestBody(req.session);

    expect(result).toEqual(expected);
  });
});
