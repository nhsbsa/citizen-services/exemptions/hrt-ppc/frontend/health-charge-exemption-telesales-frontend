import expect from "expect";
import {
  buildSessionForJourney,
  getStateForJourney,
  getNextAllowedPathForJourney,
} from "./test-utils";
import { JOURNEYS_KEY, STATE_KEY, NEXT_ALLOWED_PATH_KEY } from "../keys";

test("buildSessionForJourney() should build the correct journey status for supplied values", () => {
  const result = buildSessionForJourney({
    journeyName: "telesales",
    state: "IN_PROGRESS",
    nextAllowedPath: "/name",
  });

  const expected = {
    [JOURNEYS_KEY]: {
      telesales: {
        [STATE_KEY]: "IN_PROGRESS",
        [NEXT_ALLOWED_PATH_KEY]: "/name",
      },
    },
  };

  expect(result).toEqual(expected);
});

test("getStateForJourney() gets the state for the correct journey", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        telesales: {
          [STATE_KEY]: "IN_PROGRESS",
          [NEXT_ALLOWED_PATH_KEY]: "/name",
        },
        reportChange: {
          [STATE_KEY]: "IN_REVIEW",
          [NEXT_ALLOWED_PATH_KEY]: "/email-address",
        },
      },
    },
  };

  const result = getStateForJourney("telesales", req);
  expect(result).toBe("IN_PROGRESS");
});

test("getNextAllowedPathForJourney() gets the next allowed path for the correct journey", () => {
  const req = {
    session: {
      [JOURNEYS_KEY]: {
        telesales: {
          [STATE_KEY]: "IN_PROGRESS",
          [NEXT_ALLOWED_PATH_KEY]: "/name",
        },
        reportChange: {
          [STATE_KEY]: "IN_REVIEW",
          [NEXT_ALLOWED_PATH_KEY]: "/email-address",
        },
      },
    },
  };

  const result = getNextAllowedPathForJourney("telesales", req);
  expect(result).toBe("/name");
});
