import { isUndefined, isString } from "../../../../common/predicates";

const isInvalidSegment = (segment) =>
  !isString(segment) || !segment.startsWith("/");

const contextPath = (path) => {
  const CONTEXT_PATH = process.env.CONTEXT_PATH || "";
  if (isInvalidSegment(path)) {
    throw new Error(
      `Invalid path "${path}". Path must be a string starting with "/"`,
    );
  }

  if (isUndefined(CONTEXT_PATH)) {
    throw new Error(`Undefined context path "${CONTEXT_PATH}"`);
  }

  return `${CONTEXT_PATH}${path}`;
};

export { contextPath };
