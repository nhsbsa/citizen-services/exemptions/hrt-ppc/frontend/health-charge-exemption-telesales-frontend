import {
  renderErrorPageContent,
  getErrorPageContent,
} from "./error-page-content";

export const expectedProblemWithServiceTranslation = {
  title: "errors:problemWithTheService.title",
  heading: "errors:problemWithTheService.heading",
  paragraphOne: "errors:problemWithTheService.paragraphOne",
  paragraphTwo: "errors:problemWithTheService.paragraphTwo",
};

const errorUrls = [
  "problem-with-service",
  "page-not-found",
  "timed-out",
  "cookies-enabler",
];

describe("getErrorPageContent() should call res.render with page url and correct page content", () => {
  const req = { t: (string) => string };
  const res = { render: jest.fn() };
  test.each(errorUrls)(
    "should render %s error page with expected translation content",
    (url) => {
      renderErrorPageContent(url)(req, res);

      expect(res.render).toBeCalledTimes(1);
      expect(res.render).toBeCalledWith(
        "error-pages/" + url,
        expectedProblemWithServiceTranslation,
      );
    },
  );

  it("getErrorPageContent() should return correct array of content", () => {
    const expectedContent = {
      title: req.t("errors:problemWithTheService.title"),
      heading: req.t("errors:problemWithTheService.heading"),
      paragraphOne: req.t("errors:problemWithTheService.paragraphOne"),
      paragraphTwo: req.t("errors:problemWithTheService.paragraphTwo"),
    };

    const result = getErrorPageContent(req);

    expect(result).toStrictEqual(expectedContent);
  });
});
