import { check } from "express-validator";
import {
  entirePermittedStartDateRange,
  toDateString,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import {
  CERT_START_DATE_DAY,
  CERT_START_DATE_MONTH,
  CERT_START_DATE_YEAR,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";
import {
  validateCertificateStartDate as validateCertificateStartDateCommon,
  validateCertificateStartDateDay,
  validateCertificateStartDateMonth,
  validateCertificateStartDateYear,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/certificate-start-date/validate";
import moment from "moment";
import { isUndefined } from "../../../../../common/predicates";
import { translateValidationMessage } from "../common/translate-validation-message";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";
import { isEligibleForEditing } from "../common/certificate-utils";

const addDateToBody = (req, res, next) => {
  if (isEligibleForEditing(req.session.certificate)) {
    req.body.certificateStartDate = toDateString(
      req.body[CERT_START_DATE_DAY],
      req.body[CERT_START_DATE_MONTH],
      req.body[CERT_START_DATE_YEAR],
    );
  }
  next();
};

const validateCertificateStartDate = (certificateStartDate, { req }) => {
  const permittedStartDateRange = entirePermittedStartDateRange(
    moment(req.session.certificate.applicationDate).toDate(),
  );
  return validateCertificateStartDateCommon(
    certificateStartDate,
    permittedStartDateRange,
    { req },
  );
};

const validate = () => [
  addDateToBody,
  check("certificateStartDate")
    .if((value, { req }) => isEligibleForEditing(req.session.certificate))
    .custom(validateCertificateStartDate)
    .optional(),
  check(CERT_START_DATE_DAY)
    .if((value, { req }) => isEligibleForEditing(req.session.certificate))
    .custom(validateCertificateStartDateDay),
  check(CERT_START_DATE_MONTH)
    .if((value, { req }) => isEligibleForEditing(req.session.certificate))
    .custom(validateCertificateStartDateMonth),
  check(CERT_START_DATE_YEAR)
    .if((value, { req }) => isEligibleForEditing(req.session.certificate))
    .custom(validateCertificateStartDateYear),

  check("certificateFulfillment")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingPreference"))
    .custom((value, { req }) => {
      if (
        value == Preference.EMAIL &&
        isUndefined(req.session.citizen.emails[0])
      ) {
        throw new Error(
          req.t("validation:missingEmailAddressForEmailPrefrence"),
        );
      }
      return true;
    }),
];

export { addDateToBody, validateCertificateStartDate, validate };
