import { formatDateToEditableDate } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/formatters/dates";
import {
  CERT_START_DATE_DAY,
  CERT_START_DATE_MONTH,
  CERT_START_DATE_YEAR,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";
import { copyCertificateSessionDataToLocals } from "./edit-certificate-details";

export const behaviourForGet = () => (req, res, next) => {
  copyCertificateSessionDataToLocals(req, res);
  const certificateData = req.session.certificate;
  const editableDate = formatDateToEditableDate(
    new Date(certificateData.startDate),
  );
  res.locals.certificateDetailsData[CERT_START_DATE_DAY] = editableDate.day;
  res.locals.certificateDetailsData[CERT_START_DATE_MONTH] = editableDate.month;
  res.locals.certificateDetailsData[CERT_START_DATE_YEAR] = editableDate.year;
  next();
};
