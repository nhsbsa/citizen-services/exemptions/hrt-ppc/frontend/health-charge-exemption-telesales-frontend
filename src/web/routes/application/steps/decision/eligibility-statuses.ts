export enum EligibilityStatus {
  ELIGIBLE = "ELIGIBLE",
  DUPLICATE = "DUPLICATE",
  INELIGIBLE = "INELIGIBLE",
}
