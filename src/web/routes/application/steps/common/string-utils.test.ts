import { capitalizeFirstLetter } from "./string-utils";

describe("capitalizeFirstLetter", () => {
  test("should capitalize the first letter of a string", () => {
    const result = capitalizeFirstLetter("hello");
    expect(result).toBe("Hello");
  });

  test("should throw error for undefined input", () => {
    expect(() => capitalizeFirstLetter(undefined)).toThrow("Invalid string");
  });

  test("should throw error for numeric input", () => {
    expect(() => capitalizeFirstLetter(123)).toThrow("Invalid string");
  });

  test("should handle empty string", () => {
    const result = capitalizeFirstLetter("");
    expect(result).toBe("");
  });

  test("should handle string with only one character", () => {
    const result = capitalizeFirstLetter("a");
    expect(result).toBe("A");
  });
});
