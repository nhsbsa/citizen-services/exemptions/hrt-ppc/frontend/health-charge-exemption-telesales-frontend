const NO_MATCH = "no_match";
const ACTIVE_MATCH = "active_match";
import v1NoMatch from "./v1-no-match";
import v1ActiveMatch from "./v1-active-match";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

const RESPONSE_MAP = {
  [NO_MATCH]: v1NoMatch,
  [ACTIVE_MATCH]: v1ActiveMatch,
};

const createExemptionsSearchMapping = (scenario = NO_MATCH) =>
  JSON.stringify({
    request: {
      method: "POST",
      url: `/v1/hrt-exemptions/match`,
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
        channel: {
          equalTo: Channel.STAFF,
        },
        "user-id": {
          equalTo: "LOCAL_USER_CYPHER",
        },
      },
    },
    response: RESPONSE_MAP[scenario],
  });

export { createExemptionsSearchMapping, NO_MATCH, ACTIVE_MATCH };
