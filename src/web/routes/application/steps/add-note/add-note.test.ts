import expect from "expect";
import {
  pageContent,
  addNote,
  validate,
  isNavigable,
  behaviourForPost,
  behaviourForGet,
} from "./add-note";
import moment from "moment";
import * as notIsUndefinedOrNullOrEmpty from "../../../../../common/predicates";
import { CertificateStatus } from "@nhsbsa/health-charge-exemption-common-frontend";
import { getNotesRows } from "./get-notes-rows";
jest.mock("./get-notes-rows", () => ({
  getNotesRows: jest.fn(),
}));

const req = {
  t: (string) => string,
  session: {
    certificate: { reference: "HRT123" },
  },
  params: {
    id: "HRT123",
  },
};
const isEmpty = jest.fn();
const res = {
  redirect: jest.fn(),
};
const next = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

beforeEach(() => {
  jest.clearAllMocks();
});

test("pageContent() should match expected translated values", () => {
  const translate = (string, object?) => `${string}${object}`;
  const result = pageContent({ translate, req });

  const expected = {
    title: translate("addNote.title"),
    heading: translate("addNote.heading"),
    tableHeading: translate("addNote.tableHeading"),
    addNoteLabel: translate("addNote.addNoteLabel"),
    submitButtonText: translate("buttons:submit"),
    backButtonText: translate("buttons:back"),
    backButtonUrl: "/test-context/exemption-information/HRT123",
    previous: false,
    tables: {
      notes: {
        rows: {
          date: translate("addNote.tables.notes.rows.date"),
          time: translate("addNote.tables.notes.rows.time"),
          agent: translate("addNote.tables.notes.rows.agent"),
          description: translate("addNote.tables.notes.rows.description"),
        },
      },
    },
  };

  expect(result).toEqual(expected);
  expect(next).not.toBeCalled();
});

test("behaviourForGet() should call getNotesRows() and next()", async () => {
  await behaviourForGet()(req, res, next);

  expect(getNotesRows).toBeCalledTimes(1);
  expect(getNotesRows).toBeCalledWith(req, res);
  expect(next).toBeCalledTimes(1);
});

test("addNote should match expected outcomes", () => {
  const expected = {
    path: "/:id(HRT[A-Za-z0-9]+)/add-note",
    template: "add-note",
    isNavigable,
    pageContent,
    validate,
    behaviourForPost,
    behaviourForGet,
  };

  expect(addNote).toEqual(expected);
});

describe("isNavigable()", () => {
  test("returns true when all conditions are met", () => {
    const mockReq = {
      session: {
        certificate: {
          reference: "123",
          status: CertificateStatus.ACTIVE,
          startDate: moment().subtract(1, "day").toISOString(),
          endDate: moment().add(1, "day").toISOString(),
        },
      },
      params: { id: "123" },
    };

    jest
      .spyOn(notIsUndefinedOrNullOrEmpty, "notIsUndefinedOrNullOrEmpty")
      .mockReturnValue(true);

    const result = isNavigable(mockReq);
    expect(result).toBeTruthy();
  });

  test("returns false when findRecordInSearchResults returns undefined", () => {
    const mockReq = {
      session: {
        certificate: {
          reference: "123",
          status: CertificateStatus.ACTIVE,
          endDate: moment().add(1, "day").toISOString(),
        },
      },
      params: { id: undefined },
    };

    jest.mock("../common/search-utils").fn().mockReturnValue(undefined);

    const result = isNavigable(mockReq);
    expect(result).toBeFalsy();
  });

  test("returns false when notIsUndefinedOrNullOrEmpty returns false", () => {
    const mockReq = {
      session: {
        certificate: {
          reference: "123",
          status: CertificateStatus.ACTIVE,
          endDate: moment().add(1, "day").toISOString(),
        },
      },
      params: { id: undefined },
    };

    jest
      .spyOn(notIsUndefinedOrNullOrEmpty, "notIsUndefinedOrNullOrEmpty")
      .mockReturnValue(false);

    const result = isNavigable(mockReq);
    expect(result).toBeFalsy();
  });

  test("returns false when session.certificate.reference is undefined", () => {
    const mockReq = {
      session: {
        certificate: {
          reference: {},
        },
      },
      params: { id: "123" },
    };

    jest
      .spyOn(notIsUndefinedOrNullOrEmpty, "notIsUndefinedOrNullOrEmpty")
      .mockReturnValue(true);

    const result = isNavigable(mockReq);
    expect(result).toBeFalsy();
  });

  test("returns false when certificate reference does not match params.id", () => {
    const mockReq = {
      session: {
        certificate: {
          reference: "456",
          status: CertificateStatus.ACTIVE,
          endDate: moment().add(1, "day").toISOString(),
        },
      },
      params: { id: "123" },
    };

    jest
      .spyOn(notIsUndefinedOrNullOrEmpty, "notIsUndefinedOrNullOrEmpty")
      .mockReturnValue(true);

    const result = isNavigable(mockReq);
    expect(result).toBeFalsy();
  });
});
