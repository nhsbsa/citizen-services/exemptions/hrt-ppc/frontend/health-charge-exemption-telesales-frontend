import expect from "expect";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/test/apply-express-validation";
import { FieldValidationError } from "express-validator";

const mockValidateAddress = jest.fn();
const mockValidateDateOfBirth = jest.fn();
const mockValidateDateOfBirthIsEmpty = jest.fn();
const mockValidateDateOfBirthAgeCap = jest.fn();
const mockValidateDateOfBirthDay = jest.fn();
const mockValidateDateOfBirthMonth = jest.fn();
const mockValidateDateOfBirthYear = jest.fn();
const mockValidateEmailAddress = jest.fn();
const addDateToBody = jest.fn();

import { validate } from "./validate";

jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  addDateToBody: addDateToBody,
  validateAddress: mockValidateAddress,
  validateDateOfBirth: mockValidateDateOfBirth,
  validateDateOfBirthIsEmpty: mockValidateDateOfBirthIsEmpty,
  validateDateOfBirthAgeCap: mockValidateDateOfBirthAgeCap,
  validateDateOfBirthDay: mockValidateDateOfBirthDay,
  validateDateOfBirthMonth: mockValidateDateOfBirthMonth,
  validateDateOfBirthYear: mockValidateDateOfBirthYear,
  validateEmailAddress: mockValidateEmailAddress,
  Preference: {
    EMAIL: "mock-pref-email",
  },
  Event: {
    ANY: "mock-event-any",
  },
}));

let request;

beforeEach(() => {
  request = {
    body: {
      firstName: "John",
      lastName: "Smith",
      dateOfBirth: "1997-03-24",
      nhsNumber: "4857773457",
      "dateOfBirth-day": "24",
      "dateOfBirth-month": "03",
      "dateOfBirth-year": 1997,
      emailAddress: "email@test.com",
    },
    session: {
      certificate: {
        preference: {
          event: "mock-event-any",
          preference: "mock-pref-email",
        },
      },
    },
  };

  mockValidateAddress.mockReturnValue(true);
  mockValidateDateOfBirth.mockReturnValue(true);
  mockValidateDateOfBirthDay.mockReturnValue(true);
  mockValidateDateOfBirthMonth.mockReturnValue(true);
  mockValidateDateOfBirthYear.mockReturnValue(true);
  mockValidateDateOfBirthIsEmpty.mockReturnValue(true);
  mockValidateDateOfBirthAgeCap.mockReturnValue(true);
  mockValidateEmailAddress.mockReturnValue(true);
});

describe("validate first name", () => {
  it.each([null, undefined, ""])(
    "throws errors when firstName field is %p",
    async (firstName: null | undefined | string) => {
      request.body.firstName = firstName;

      const result = await applyExpressValidation(request, validate());

      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      const errorOne = result.array()[0] as FieldValidationError;

      expect(result.array().length).toBe(1);
      expect(errorOne.path).toBe("firstName");
      expect(errorOne.msg).toBe("validation:missingFirstName");
    },
  );

  test("errors for field is invalid", async () => {
    request.body.firstName = "L1$a";

    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("firstName");
    expect(errorOne.msg).toBe("validation:patternFirstName");
  });

  test("errors for field exceeds min length", async () => {
    request.body.firstName =
      "DavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavidDavid";

    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("firstName");
    expect(errorOne.msg).toBe("validation:firstNameTooLong");
  });
});

describe("validate last name", () => {
  it.each([null, undefined, ""])(
    "throws errors when lastName field is %p",
    async (lastName: null | undefined | string) => {
      request.body.lastName = lastName;

      const result = await applyExpressValidation(request, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      const errorOne = result.array()[0] as FieldValidationError;

      expect(result.array().length).toBe(1);
      expect(errorOne.path).toBe("lastName");
      expect(errorOne.msg).toBe("validation:missingLastName");
    },
  );

  test("errors for field is invalid", async () => {
    request.body.lastName = "$m1th";

    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("lastName");
    expect(errorOne.msg).toBe("validation:patternLastName");
  });

  test("errors for field exceeds min length", async () => {
    request.body.lastName =
      "CamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonC";

    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("lastName");
    expect(errorOne.msg).toBe("validation:lastNameTooLong");
  });
});

describe("validate date of birth", () => {
  test("returns error when dateOfBirth field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirth");
    mockValidateDateOfBirth.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirth");
  });

  test("returns error when dateOfBirth-day field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthDay");
    mockValidateDateOfBirthDay.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-day");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthDay");
  });

  test("returns error when dateOfBirth-month field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthMonth");
    mockValidateDateOfBirthMonth.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-month");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthMonth");
  });

  test("returns error when dateOfBirth-year field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthYear");
    mockValidateDateOfBirthYear.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-year");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthYear");
  });

  test("calls addDateToBody", async () => {
    await applyExpressValidation(request, validate());

    expect(addDateToBody).toHaveBeenCalledTimes(1);
  });
});

describe("validate nhs number", () => {
  it.each([
    ["9992158107", 0],
    ["9999999999", 0],
    ["9979989998", 0],
    ["9979989696", 0],
    ["1123443986", 1],
    ["@!@££*£*£test", 1],
    ["9999999999999999999999", 1],
    ["9999", 1],
  ])(
    "check %p is valid nhs number expecting %p validation errors",
    async (nhsNumber: string, errorCount: number) => {
      request.body.nhsNumber = nhsNumber;

      const result = await applyExpressValidation(request, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(errorCount);
      if (errorCount > 0) {
        const errorOne = result.array()[0] as FieldValidationError;
        expect(errorOne.path).toBe("nhsNumber");
        expect(errorOne.msg).toBe("validation:invalidNhsNumber");
      }
    },
  );

  it.each(["", null, undefined])(
    "passes validation when nhs number is %p",
    async (nhsNumber: null | undefined | string) => {
      request.body.nhsNumber = nhsNumber;

      const result = await applyExpressValidation(request, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      expect(result.array().length).toBe(0);
    },
  );
});

describe("validate email address", () => {
  it.each([null, undefined, ""])(
    "No errors when derived certificate fulfillment is other than 'EMAIL' and email-address field is %p",
    async (emailAddress: null | undefined | string) => {
      request.body.emailAddress = emailAddress;
      request.session.certificate.preference.preference =
        "mock-pref-something-else";

      const result = await applyExpressValidation(request, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      result.array()[0] as FieldValidationError;

      expect(result.array().length).toBe(0);
    },
  );

  it.each([null, undefined, ""])(
    "throws errors when derived certificate fulfillment is 'EMAIL' and email-address field is %p",
    async (emailAddress: null | undefined | string) => {
      request.body.emailAddress = emailAddress;

      const result = await applyExpressValidation(request, validate());
      if (result === undefined) {
        throw new Error("async result is undefined after waiting");
      }

      const errorOne = result.array()[0] as FieldValidationError;

      expect(result.array().length).toBe(1);
      expect(errorOne.path).toBe("emailAddress");
      expect(errorOne.msg).toBe("validation:invalidEmailAddressDeletion");
    },
  );
});
describe("validate", () => {
  test("calls validateAddress", async () => {
    await applyExpressValidation(request, validate());

    expect(mockValidateAddress).toHaveBeenCalledTimes(1);
    expect(mockValidateAddress).toHaveBeenCalledWith();
  });

  test("calls validateEmailAddress", async () => {
    await applyExpressValidation(request, validate());

    expect(mockValidateEmailAddress).toHaveBeenCalledTimes(1);
    expect(mockValidateEmailAddress).toHaveBeenCalledWith();
  });
});

test("no validation error when all fields are valid", async () => {
  const result = await applyExpressValidation(request, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});
