import { partial } from "ramda";
import { COMPLETED, IN_PROGRESS } from "../states";
import {
  buildSessionForJourney,
  getStateForJourney,
} from "../test-utils/test-utils";
import {
  handleStateDestroySession,
  handleSessionDestruction,
  sessionDestroyCallback,
} from "./handle-get";

const journey = { name: "telesales" };

test("handleStateDestroySession() should set state to COMPLETE then destroy current session", () => {
  const getStateForTelesalesJourney = partial(getStateForJourney, [
    "telesales",
  ]);
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: "telesales",
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
      destroy: jest.fn(),
      claim: {
        firstName: "Some",
        lastName: "Data",
      },
    },
  };
  const res = jest.fn();
  const next = jest.fn();

  handleStateDestroySession(journey)(req, res, next);

  expect(getStateForTelesalesJourney(req)).toBe(COMPLETED);
  expect(req.session.destroy).toBeCalledTimes(1);
  expect(req.session.destroy).toBeCalledWith(sessionDestroyCallback);
  expect(next).toBeCalledTimes(1);
});

test("handleStateDestroySession() should ignore logic if journeys undefined (page re-submit)", () => {
  const req = {
    session: {
      journeys: undefined,
      destroy: jest.fn(),
    },
  };
  const res = jest.fn();
  const next = jest.fn();

  handleStateDestroySession(journey)(req, res, next);

  expect(req.session.destroy).not.toBeCalled();
  expect(next).toBeCalledTimes(1);
});

test("handleSessionDestruction() should destroy session", () => {
  const req = {
    session: {
      journeys: journey,
      destroy: jest.fn(),
      claim: {
        firstName: "Some",
        lastName: "Data",
      },
    },
  };

  handleSessionDestruction(req);

  expect(req.session.destroy).toBeCalledTimes(1);
  expect(req.session.destroy).toBeCalledWith(sessionDestroyCallback);
});
