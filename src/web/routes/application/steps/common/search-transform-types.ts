// eslint-disable-next-line @typescript-eslint/no-unused-vars
type ExemptionTransormed = {
  certificates: Array<CertificateTransformed>;
};

type CitizenTransormed = {
  id?: string;
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  addresses?: Array<Address>;
};

type CertificateTransformed = {
  id?: string;
  reference: string;
  type: string;
  status: string;
  startDate: Date;
  endDate: Date;
  citizen: CitizenTransormed;
};
