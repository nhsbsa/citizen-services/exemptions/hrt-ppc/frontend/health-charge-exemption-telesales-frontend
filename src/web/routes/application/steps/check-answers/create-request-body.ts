import moment from "moment";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import {
  CertificateType,
  PaymentMethod,
  Duration,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import {
  PaymentRequest,
  IssueCertificateRequest,
  CardPaymentRequest,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/types";
import { config } from "../../../../../config";

const createIssueCertificateRequestBody = (
  session,
): IssueCertificateRequest => {
  const applicantDetails = session.newApplicant;
  return {
    citizen: {
      firstName: applicantDetails.firstName,
      lastName: applicantDetails.lastName,
      dateOfBirth: applicantDetails.dateOfBirth,
      ...(notIsUndefinedOrNullOrEmpty(applicantDetails.nhsNumber) && {
        nhsNumber: applicantDetails.nhsNumber,
      }),
      addresses: [
        {
          addressLine1: applicantDetails.addressLine1,
          townOrCity: applicantDetails.townOrCity,
          postcode: applicantDetails.sanitizedPostcode,
          ...(notIsUndefinedOrNullOrEmpty(applicantDetails.addressLine2) && {
            addressLine2: applicantDetails.addressLine2,
          }),
        },
      ],
      ...(notIsUndefinedOrNullOrEmpty(applicantDetails.emailAddress) && {
        emails: [
          {
            emailAddress: applicantDetails.emailAddress,
          },
        ],
      }),
      ...(notIsUndefinedOrNullOrEmpty(applicantDetails.telephoneNumber) && {
        telephones: [
          {
            phoneNumber: applicantDetails.telephoneNumber,
          },
        ],
      }),
    },
    certificate: {
      type: CertificateType.HRT_PPC,
      duration: Duration.P12M,
      cost: config.environment.HRT_PPC_VALUE,
      startDate: applicantDetails.certificateStartDate,
      endDate: applicantDetails.certificateEndDate,
      applicationDate: moment().toDate(),
    },
    userPreference: applicantDetails.certificateFulfillment,
  };
};

const createPaymentRequestBody = (): PaymentRequest => {
  return {
    amount: config.environment.HRT_PPC_VALUE,
    date: moment().toDate(),
    method: PaymentMethod.CARD,
  };
};

const createCardPaymentRequestBody = (session): CardPaymentRequest => {
  return {
    reference: session.issuedCertificateDetails.certificateReference,
    transactionId: session.paymentDetails.transactionId,
    amount: config.environment.HRT_PPC_VALUE,
    serviceDescription: "Twelve Months single payment",
  };
};

export {
  createIssueCertificateRequestBody,
  createPaymentRequestBody,
  createCardPaymentRequestBody,
};
