import { config } from "../../../../../config";
import { citizenClient } from "../../../../client/citizen-client";

const deleteEmail = async (req, emailId) => {
  const client = new citizenClient(config);
  const response = await client.makeRequest(
    {
      method: "DELETE",
      url: "/v1/emails/" + emailId,
      responseType: "json",
    },
    req.session,
  );
  return response;
};

export { deleteEmail };
