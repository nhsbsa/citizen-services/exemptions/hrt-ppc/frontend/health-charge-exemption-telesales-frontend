import { createLogger, format, transports } from "winston";
import { config } from "../../config";
import { formatLog } from "./format";

const { combine, timestamp } = format;

const TIMESTAMP_FORMAT = "HH:mm:ss.SSS";

function buildTransports() {
  if (config.environment.NODE_ENV === "local") {
    return [
      new transports.Console({
        format: combine(
          timestamp({
            format: TIMESTAMP_FORMAT,
          }),
          formatLog,
        ),
      }),
    ];
  } else {
    return [
      new transports.Console({
        format: combine(format.prettyPrint(), format.json()),
      }),
    ];
  }
}

const winstonLogger = createLogger({
  level: config.environment.LOG_LEVEL,
  transports: buildTransports(),
  defaultMeta: { service: "health-charge-exemption-telesales-frontend" },
  exitOnError: true,
});

export { winstonLogger };
