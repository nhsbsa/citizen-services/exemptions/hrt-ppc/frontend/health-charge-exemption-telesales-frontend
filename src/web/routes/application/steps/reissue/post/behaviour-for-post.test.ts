import { COMPLETED, VIEW } from "../../../flow-control/states";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";

const mockReissue = jest.fn();
const mockStateMachine = { setState: jest.fn() };

import { behaviourForPost } from "./behaviour-for-post";

jest.mock("./post", () => ({
  reissue: mockReissue,
}));

jest.mock("../../../flow-control/state-machine/state-machine", () => ({
  stateMachine: mockStateMachine,
}));

beforeEach(() => {
  jest.clearAllMocks();
});

describe("behaviourForPost()", () => {
  test("should call reissue() and next() and set the state machine to COMPLETE", async () => {
    const journey = { name: "search" };
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: VIEW,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const config = {};
    const next = jest.fn();

    await behaviourForPost(config, journey)(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
    expect(mockReissue).toBeCalledTimes(1);
    expect(mockReissue).toBeCalledWith(req, res, next);
    expect(mockStateMachine.setState).toBeCalledTimes(1);
    expect(mockStateMachine.setState).toBeCalledWith(COMPLETED, req, journey);
  });
});
