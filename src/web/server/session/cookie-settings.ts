const COOKIE_EXPIRES_MILLISECONDS = 30 * 60 * 1000;

export { COOKIE_EXPIRES_MILLISECONDS };
