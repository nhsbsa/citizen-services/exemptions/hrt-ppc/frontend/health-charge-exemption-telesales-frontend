import { reissueClient } from "../../../../../client/reissue-client";

const mockReissueClient = jest.fn();

beforeEach(() => {
  jest.clearAllMocks();
  reissueClient.prototype.makeRequest = mockReissueClient;

  mockReissueClient.mockResolvedValue({ data: "some data" });
});

import {
  createRequestBody,
  makeReissuePostRequest,
} from "./reissue-lambda-provider";
import { CertificateType } from "@nhsbsa/health-charge-exemption-common-frontend";

const expectedBody = {
  citizenId: "123",
  certificate: {
    id: "1234",
    reference: "ref",
    type: CertificateType.HRT_PPC,
    startDate: "",
    endDate: "",
  },
};

const expectedReissueCall = {
  data: expectedBody,
  method: "POST",
  responseType: "json",
  url: "/v1/reissue",
};

describe("makeReissuePostRequest()", () => {
  test("should return a value when makeRequest is called on the client", async () => {
    const session = {
      citizen: { id: "123" },
      certificate: {
        id: "1234",
        reference: "ref",
        type: CertificateType.HRT_PPC,
        startDate: "",
        endDate: "",
      },
    };

    const result = await makeReissuePostRequest(session);

    expect(result).toEqual({ data: "some data" });
    expect(mockReissueClient).toBeCalledTimes(1);
    expect(mockReissueClient).toBeCalledWith(expectedReissueCall, session);
  });
});

describe("createRequestBody()", () => {
  test("should set each body var correctly", () => {
    const session = {
      citizen: { id: "123" },
      certificate: {
        id: "1234",
        reference: "ref",
        type: CertificateType.HRT_PPC,
        startDate: "",
        endDate: "",
      },
    };

    const result = createRequestBody(session);

    expect(result).toEqual({
      citizenId: "123",
      certificate: {
        id: "1234",
        reference: "ref",
        type: CertificateType.HRT_PPC,
        startDate: "",
        endDate: "",
      },
    });
  });
});
