import { postJsonData } from "../../request";
import { WIREMOCK_CERTIFICATE_MAPPING_URL } from "../paths";
import {
  createGetCertificateByIdSuccessMapping,
  SUCCESS,
} from "./mappings/mappings";

async function setupSuccessfulGetCertificateMapping() {
  await postJsonData(
    WIREMOCK_CERTIFICATE_MAPPING_URL,
    createGetCertificateByIdSuccessMapping(SUCCESS),
  );
}

export { setupSuccessfulGetCertificateMapping };
