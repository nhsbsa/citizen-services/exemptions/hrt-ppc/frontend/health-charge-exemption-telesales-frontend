import expect from "expect";
import { applyExpressValidation } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/common/test/apply-express-validation";
import { FieldValidationError } from "express-validator";

const mockValidateDateOfBirth = jest.fn();
const mockValidateDateOfBirthDay = jest.fn();
const mockValidateDateOfBirthMonth = jest.fn();
const mockValidateDateOfBirthYear = jest.fn();
const addDateToBody = jest.fn();

import { validate, checkForInput } from "./validate";

jest.mock(
  "@nhsbsa/health-charge-exemption-common-frontend/dist/src/date-of-birth",
  () => ({
    addDateToBody: addDateToBody,
    validateDateOfBirth: mockValidateDateOfBirth,
    validateDateOfBirthDay: mockValidateDateOfBirthDay,
    validateDateOfBirthMonth: mockValidateDateOfBirthMonth,
    validateDateOfBirthYear: mockValidateDateOfBirthYear,
  }),
);

let request;

beforeEach(() => {
  request = {
    t: (string) => string,
    body: {
      lastName: "Smith",
      dateOfBirth: "1997-03-24",
      sanitizedPostcode: "NE15 8NY",
      postcode: "NE15 8NY",
      "dateOfBirth-day": "24",
      "dateOfBirth-month": "03",
      "dateOfBirth-year": "1997",
    },
  };

  mockValidateDateOfBirth.mockReturnValue(true);
  mockValidateDateOfBirthDay.mockReturnValue(true);
  mockValidateDateOfBirthMonth.mockReturnValue(true);
  mockValidateDateOfBirthYear.mockReturnValue(true);
});

test("checkForInput() returns false when all fields are empty", () => {
  const req = {
    body: {
      lastName: "",
      dateOfBirth: "",
      postcode: "",
    },
  };

  const retValue = checkForInput(null, { req });
  expect(retValue).toBe(false);
});

describe("validate date of birth", () => {
  test("returns error when dateOfBirth field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirth");
    mockValidateDateOfBirth.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirth");
  });

  test("returns error when dateOfBirth-day field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthDay");
    mockValidateDateOfBirthDay.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-day");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthDay");
  });

  test("returns error when dateOfBirth-month field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthMonth");
    mockValidateDateOfBirthMonth.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-month");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthMonth");
  });

  test("returns error when dateOfBirth-year field is invalid", async () => {
    const error = new Error("validation:invalidDateOfBirthYear");
    mockValidateDateOfBirthYear.mockRejectedValue(error);
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(1);
    const errorOne = result.array()[0] as FieldValidationError;
    expect(errorOne.path).toBe("dateOfBirth-year");
    expect(errorOne.msg).toBe("validation:invalidDateOfBirthYear");
  });

  test("calls addDateToBody", async () => {
    await applyExpressValidation(request, validate());

    expect(addDateToBody).toHaveBeenCalledTimes(1);
  });
});

describe("validate last name", () => {
  test("returns error when lastName field is invalid", async () => {
    request.body.lastName = "$m1th";

    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("lastName");
    expect(errorOne.msg).toBe("validation:patternLastName");
  });

  test("returns error when lastName field exceeds min length", async () => {
    request.body.lastName =
      "CamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonCamaroonC";

    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("lastName");
    expect(errorOne.msg).toBe("validation:lastNameTooLong");
  });

  test("No validation error when lastName field is valid", async () => {
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(0);
  });
});

describe("validate postcode", () => {
  test("returns error when postcode field is invalid", async () => {
    request.body.postcode = "1@wertye";
    request.body.sanitizedPostcode = "1@WE RTYE";

    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const errorOne = result.array()[0] as FieldValidationError;

    expect(result.array().length).toBe(1);
    expect(errorOne.path).toBe("postcode");
    expect(errorOne.msg).toBe("validation:invalidPostcode");
  });

  test("No validation error when postcode field is valid", async () => {
    const result = await applyExpressValidation(request, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    expect(result.array().length).toBe(0);
  });
});
