import { URLS } from "../paths";

const telesalesPageNotFound = [
  {
    url: URLS.PAGE_NOT_FOUND,
    formData: () => ({}),
  },
];
const telesalesProblemWithService = [
  {
    url: URLS.SEARCH_HRT_RECORDS,
    formData: () => ({}),
  },
  {
    url: URLS.PROBLEM_WITH_SERVICE,
    formData: () => ({}),
  },
];
export { telesalesPageNotFound, telesalesProblemWithService };
