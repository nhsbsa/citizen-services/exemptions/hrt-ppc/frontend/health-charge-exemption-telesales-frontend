import { parsePhoneNumberFromString } from "libphonenumber-js";
import { path } from "ramda";
import {
  sanitizeAddress,
  sanitizeDateOfBirth,
  sanitizeEmailAddress,
  sanitizeStartDate,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";

function getPhoneNumber(phoneNumber) {
  if (notIsUndefinedOrNullOrEmpty(phoneNumber)) {
    const parsedNumber = parsePhoneNumberFromString(phoneNumber, "GB");
    return path(["number"], parsedNumber);
  }
  return undefined;
}

const sanitize = () => (req, res, next) => {
  req.body.formattedPhoneNumber = getPhoneNumber(req.body.telephoneNumber);
  sanitizeAddress(req);
  sanitizeDateOfBirth(req);
  sanitizeEmailAddress(req);
  sanitizeStartDate(req);
  next();
};

export { sanitize, getPhoneNumber };
