import expect from "expect";
import { stateMachine } from "../flow-control/state-machine";
import { getPreviousNavigablePath, getPreviousPath } from "./get-previous-path";

jest.mock("../flow-control/state-machine", () => ({
  stateMachine: {
    getCurrentPath: jest.fn(),
  },
}));

beforeEach(() => {
  jest.clearAllMocks();
});

test("getPreviousNavigablePath() returns the path for previous step if isNavigable function does not exist", () => {
  const steps = [{ path: "/first" }, { path: "/second" }];
  const result = getPreviousNavigablePath(steps, 1, {});

  expect(result).toBe("/first");
});

test("getPreviousNavigablePath() throws error if index is zero", () => {
  const steps = [{ path: "/first" }, { path: "/second" }];
  const result = () => getPreviousNavigablePath(steps, 0, {});

  expect(result).toThrowError(/No allowed back route found/);
});

test("getPreviousNavigablePath() returns the path for previous step if isNavigable function exists and returns true", () => {
  const steps = [
    { path: "/first", isNavigable: () => true },
    { path: "/second" },
  ];
  const result = getPreviousNavigablePath(steps, 1, {});

  expect(result).toBe("/first");
});

test("getPreviousNavigablePath() returns the path for the first step if isNavigable function exists and returns false for second step", () => {
  const steps = [
    { path: "/first" },
    { path: "/second", isNavigable: () => false },
    { path: "/third" },
  ];
  const result = getPreviousNavigablePath(steps, 2, {});

  expect(result).toBe("/first");
});

test("getPreviousNavigablePath() passes session to isNavigable function", () => {
  const session = { isNavigable: true };
  const steps = [
    { path: "/first", isNavigable: () => true },
    { path: "/second" },
  ];
  const result = getPreviousNavigablePath(steps, 1, session);

  expect(result).toBe("/first");
});

test("getPreviousNavigablePath() throws an error when no previous steps are allowed", () => {
  const steps = [
    { path: "/first", isNavigable: () => false },
    { path: "/second", isNavigable: () => false },
    { path: "/third" },
  ];
  const result = () => getPreviousNavigablePath(steps, 2, {});

  expect(result).toThrowError(/No allowed back route found/);
});

test("getPreviousNavigablePath() throws an error if isNavigable() exists but is not a function", () => {
  const steps = [
    { path: "/first", isNavigable: "not-a-function" },
    { path: "/second" },
  ];
  const result = () => getPreviousNavigablePath(steps, 1, {});

  expect(result).toThrowError(
    /isNavigable must be a function for step {"path":"\/first","isNavigable":"not-a-function"}/,
  );
});

test("getPreviousNavigablePath() throws an error if isNavigable() does not return a boolean", () => {
  const steps = [
    { path: "/first", isNavigable: () => "not-a-boolean" },
    { path: "/second" },
  ];
  const result = () => getPreviousNavigablePath(steps, 1, {});

  expect(result).toThrowError(
    /isNavigable must return a boolean for step {"path":"\/first"}/,
  );
});

test("getPreviousNavigablePath() returns the back function if back is defined in the step", () => {
  const session = { isNavigable: true };
  const request = { session: { session } };
  const steps = [
    { path: "/first", isNavigable: () => true, back: () => "some-other" },
    { path: "/second" },
  ];
  const result = getPreviousNavigablePath(steps, 1, request);

  expect(result).toBe("some-other");
});

test("getPreviousPath() throws an error if step does not exist in list of steps", () => {
  const steps = [{ path: "/first" }];
  const step = { path: "/second" };
  const result = () => getPreviousPath(steps, step, {});

  expect(result).toThrowError(
    /Unable to find {"path":"\/second"} in the list of steps/,
  );
});

test("getPreviousPath() returns root if step is first in sequence and embeddedMainJourney undefined", () => {
  const step = { path: "/first" };
  const steps = [step];
  const result = getPreviousPath(steps, step, {});

  expect(result).toBe("/test-context/");
});

test("getPreviousPath() returns root if step is first in sequence and embeddedMainJourney false", () => {
  const step = { path: "/first", embeddedMainJourney: false };
  const steps = [step];
  const result = getPreviousPath(steps, step, {});

  expect(result).toBe("/test-context/");
});

test("getPreviousPath() returns root if telesales journey does not exist in session", () => {
  stateMachine.getCurrentPath = function () {
    return undefined;
  };

  const step = { path: "/first", embeddedMainJourney: true };
  const steps = [step];
  const result = getPreviousPath(steps, step, {});

  expect(result).toBe("/test-context/");
});

test("getPreviousPath() returns path for telesales journey when exists in session", () => {
  stateMachine.getCurrentPath = function () {
    return "/test-context/telesales/first";
  };

  const step = { path: "/first", embeddedMainJourney: true };
  const steps = [step];
  const result = getPreviousPath(steps, step, {});

  expect(result).toBe("/test-context/telesales/first");
});

test("getPreviousPath() returns path for previous step in sequence", () => {
  const stepOne = { path: "/first" };
  const stepTwo = { path: "/second" };
  const stepThree = { path: "/third" };
  const steps = [stepOne, stepTwo, stepThree];
  const result = getPreviousPath(steps, stepThree, {});

  expect(result).toBe("/second");
});
