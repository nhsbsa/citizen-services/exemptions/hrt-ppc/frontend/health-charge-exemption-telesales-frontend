import { CHECK_ANSWERS_URL, CARD_PAYMENT_URL } from "../../../paths/paths";
import { prefixPath } from "../../../paths/prefix-path";
import { isNextPathNavigable } from "../predicates";
import { getNextForStep } from "./get-next-for-step";

const getStepForPath = (path, steps) =>
  steps.find((step) => {
    if (step.path === path) {
      return true;
    }

    path;
  });

const getNextInReviewPath = (req, prefix) => {
  const checkAnswersUrl = prefixPath(prefix, CHECK_ANSWERS_URL);
  const cardPaymentUrl = prefixPath(prefix, CARD_PAYMENT_URL);
  return req.path === checkAnswersUrl ? cardPaymentUrl : checkAnswersUrl;
};

/**
 * Ask the current step for the next path. Test whether the step matching that path is navigable. If not, ask that step for the next path; repeat.
 * @param path the path of the current step
 * @param req the current request
 * @param steps the steps of the telesales journey
 * @returns the path of the next step
 */
const getNextNavigablePath = (path, req, journey) => {
  const { steps } = journey;
  const thisStep = getStepForPath(path, steps);
  const nextPath = getNextForStep(req, journey, thisStep);
  const nextStep = getStepForPath(nextPath, steps);

  if (isNextPathNavigable(nextStep, req)) {
    return nextPath;
  }
  return getNextNavigablePath(nextPath, req, journey);
};

export { getNextNavigablePath, getNextInReviewPath, getStepForPath };
