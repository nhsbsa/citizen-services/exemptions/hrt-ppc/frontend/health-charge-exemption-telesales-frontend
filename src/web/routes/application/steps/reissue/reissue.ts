import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import { REISSUE_URL } from "../../paths/paths";
import { pageContent } from "./page-content";
import { behaviourForGet } from "./behaviour-for-get";
import { behaviourForPost } from "./post/behaviour-for-post";
import { Event } from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/enums";

const isNavigable = (req) => {
  return (
    notIsUndefinedOrNullOrEmpty(req.params.id) &&
    notIsUndefinedOrNullOrEmpty(req.session.certificate) &&
    notIsUndefinedOrNullOrEmpty(req.session.citizen) &&
    notIsUndefinedOrNullOrEmpty(req.session.certificate.preference.event) &&
    req.session.certificate.preference.event === Event.ANY
  );
};

const reissue = {
  // url: context/HRT1D612A3C/page_url
  path: `/:id(HRT[A-Za-z0-9]+)${REISSUE_URL}`,
  template: "reissue",
  pageContent,
  isNavigable,
  behaviourForGet,
  behaviourForPost,
};

export { reissue, pageContent, isNavigable, behaviourForGet, behaviourForPost };
