const CONTEXT_PATH = process.env.CONTEXT_PATH || "";

const checkBrowserCookie = (req, res, next) => {
  if (typeof req.cookies.isBrowserCookieEnabled === "undefined") {
    res.redirect(`${CONTEXT_PATH}/enable-cookies`);
    res.end();
    return;
  }
  next();
};

export { checkBrowserCookie };
