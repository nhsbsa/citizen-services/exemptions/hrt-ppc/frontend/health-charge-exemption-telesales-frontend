import {
  searchRecords,
  searchResultsFound,
  searchNoResults,
  newApplicantDetailsPage,
  exemptionDetail,
  addNote,
  editPersonalDetails,
  editCertificateDetails,
  reissue,
} from "./steps";
import {
  NEW_APP_JOURNEY_NAME,
  SEARCH_JOURNEY_NAME,
} from "../application/constants";

const CONTEXT_PATH = process.env.CONTEXT_PATH || undefined;

const NEW_APPLICANT = {
  name: NEW_APP_JOURNEY_NAME,
  endpoint: "/v4/claims",
  pathPrefix: CONTEXT_PATH,
  steps: [newApplicantDetailsPage],
};

const SEARCH = {
  name: SEARCH_JOURNEY_NAME,
  endpoint: "/v4/claims",
  pathPrefix: CONTEXT_PATH,
  steps: [
    searchRecords,
    searchResultsFound,
    searchNoResults,
    exemptionDetail,
    editPersonalDetails,
    editCertificateDetails,
    addNote,
    reissue,
  ],
};

export const JOURNEYS = [NEW_APPLICANT, SEARCH];
