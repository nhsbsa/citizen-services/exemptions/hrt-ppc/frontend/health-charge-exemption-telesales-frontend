window.addEventListener('load', function (e) {
  initListener()
}, false)

/*
 * Disable the submit buttons after first click to avoid duplicate submission
 */
function initListener () {
  const form = document.getElementById('form')
  if (form) {
    form.addEventListener('submit', function (event) {
      const buttons = document.getElementsByClassName("nhsuk-button");
      for (let i = 0; i < buttons.length; i++) {
        buttons[i].disabled = true;
      }
    })
  }
  if (document.getElementById('nhsuk-print-certificate')) {
    document.getElementById('nhsuk-print-certificate').onclick = function() {
      javascript:window.print();
    };
  }
}

/*
 * If browser back button was used, flush cache
 * This ensures that user will always see an accurate, up-to-date view based on their state
 * https://stackoverflow.com/questions/8788802/prevent-safari-loading-from-cache-when-back-button-is-clicked
 */
(function () {
  window.onpageshow = function (event) {
    if (event.persisted) {
      window.location.reload()
    }
  }
})()
