import { registerProblemWithServiceError } from "./problem-with-service";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";

const PROBLEM_WITH_SERVICE_URL = "/test-context/problem-with-service";
const TIMED_OUT_URL = "/test-context/timed-out";

test(`registerProblemWithServiceError() should redirect to ${PROBLEM_WITH_SERVICE_URL} or ${TIMED_OUT_URL}`, () => {
  const req = { t: (string) => string };
  const app = {
    ...jest.requireActual("express"),
    get: jest.fn(),
    all: jest.fn(),
  };
  const res = { render: jest.fn() };
  app.all.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res);
  });

  registerProblemWithServiceError(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(
    1,
    [PROBLEM_WITH_SERVICE_URL, TIMED_OUT_URL],
    configureAuthentication,
    expect.any(Function),
  );
});
