import { postJsonData } from "../../request";
import { WIREMOCK_PREFERENCE_MAPPING_URL } from "../paths";
import {
  createGetPreferenceByIdSuccessMapping,
  SUCCESS,
} from "./mappings/mappings";

async function setupSuccessfulGetPreferenceMapping() {
  await postJsonData(
    WIREMOCK_PREFERENCE_MAPPING_URL,
    createGetPreferenceByIdSuccessMapping(SUCCESS),
  );
}

export { setupSuccessfulGetPreferenceMapping };
