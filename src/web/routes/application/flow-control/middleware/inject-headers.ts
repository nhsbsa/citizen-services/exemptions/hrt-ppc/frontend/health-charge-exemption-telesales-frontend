const USE_UNSECURE_COOKIE = process.env.USE_UNSECURE_COOKIE;

const injectAdditionalHeaders = (req, res, next?) => {
  if (USE_UNSECURE_COOKIE === "false") {
    req.connection.encrypted = true;
  }

  res.cookie("isBrowserCookieEnabled", true);
  if (next !== undefined) {
    next();
  }
};

export { injectAdditionalHeaders };
