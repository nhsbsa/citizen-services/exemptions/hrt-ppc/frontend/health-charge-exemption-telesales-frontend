import {
  getCertificate,
  getCitizen,
  getNotes,
  getPreference,
  getAudit,
  makeRequests,
} from "./make-calls";
import { certificateClient } from "../../../../client/certificate-client";
import { citizenClient } from "../../../../client/citizen-client";
import { noteClient } from "../../../../client/note-client";
import { preferenceClient } from "../../../../client/preference-client";
import { compositionClient } from "../../../../client/composition-client";
import * as citizenWithEmailResponse from "../../../../../__mocks__/api-responses/citizen-api/get-citizen-by-id-with-email.json";
import * as certificateResponse from "../../../../../__mocks__/api-responses/certificate-api/get-certificate-by-id.json";
import * as notesResponse from "../../../../../__mocks__/api-responses/note-api/find-notes-by-certificate-id.json";
import * as preferenceResponse from "../../../../../__mocks__/api-responses/preference-api/find-user-preference.json";
import * as compositionResponse from "../../../../../__mocks__/api-responses/composition-api/get-audit-single-row.json";
import { CertificateType } from "@nhsbsa/health-charge-exemption-common-frontend";

const mockMakeRequestCertificateClient = jest.fn();
const mockMakeRequestCitizenClient = jest.fn();
const mockMakeRequestNoteClient = jest.fn();
const mockMakeRequestPreferenceClient = jest.fn();
const mockMakeRequestCompositionClient = jest.fn();

const expectedCallCertificate = {
  method: "GET",
  responseType: "json",
  url: "/v1/certificates/123",
};
const expectedCallCitien = {
  method: "GET",
  responseType: "json",
  url: "/v1/citizens/456",
};
const expectedCallNote = {
  method: "GET",
  responseType: "json",
  url: "/v1/notes/search/findByCertificateId?certificateId=123&size=100",
};
const expectedCallPreference = {
  method: "GET",
  responseType: "json",
  url:
    "/v1/user-preference/search/findByCitizenIdAndCertificateType?citizenId=456&certificateType=" +
    CertificateType.HRT_PPC,
};
const expectedCallComposition = {
  method: "POST",
  responseType: "json",
  url: "/v1/exemptions/audit",
  data: {
    citizenId: "456",
    certificateId: "123",
    citizen: {
      fields: [
        "[]",
        "",
        "firstName",
        "lastName",
        "dateOfBirth",
        "nhsNumber",
        "addressLine1",
        "addressLine2",
        "townOrCity",
        "postcode",
        "emailAddress",
      ],
    },
    certificate: {
      fields: ["startDate", "status"],
    },
    payment: {
      fields: ["status"],
    },
    preference: {
      fields: ["preference"],
    },
  },
};

const expectedCallToSession = { value: "test" };

beforeEach(() => {
  jest.clearAllMocks();
  certificateClient.prototype.makeRequest = mockMakeRequestCertificateClient;
  citizenClient.prototype.makeRequest = mockMakeRequestCitizenClient;
  noteClient.prototype.makeRequest = mockMakeRequestNoteClient;
  preferenceClient.prototype.makeRequest = mockMakeRequestPreferenceClient;
  compositionClient.prototype.makeRequest = mockMakeRequestCompositionClient;

  mockMakeRequestCertificateClient.mockResolvedValue({
    data: certificateResponse,
  });
  mockMakeRequestCitizenClient.mockResolvedValue({
    data: citizenWithEmailResponse,
  });
  mockMakeRequestNoteClient.mockResolvedValue({ data: notesResponse });
  mockMakeRequestPreferenceClient.mockResolvedValue({
    data: preferenceResponse,
  });
  mockMakeRequestCompositionClient.mockResolvedValue({
    data: compositionResponse,
  });
});

describe("makeRequests()", () => {
  it("should make requests and return the correct data", async () => {
    const mockSearchResult = { id: "123", citizen: { id: "456" } };
    const mockReq = {
      session: {
        value: "test",
      },
    };

    const result = await makeRequests(mockReq, mockSearchResult);
    expect(result).toEqual({
      data: {
        certificate: certificateResponse,
        citizen: citizenWithEmailResponse,
        notes: notesResponse,
        preference: preferenceResponse,
        audit: compositionResponse,
      },
    });

    expect(mockMakeRequestCertificateClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestNoteClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestPreferenceClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestCompositionClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestCertificateClient).toHaveBeenCalledWith(
      expectedCallCertificate,
      expectedCallToSession,
    );
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledWith(
      expectedCallCitien,
      expectedCallToSession,
    );
    expect(mockMakeRequestNoteClient).toHaveBeenCalledWith(
      expectedCallNote,
      expectedCallToSession,
    );
    expect(mockMakeRequestPreferenceClient).toHaveBeenCalledWith(
      expectedCallPreference,
      expectedCallToSession,
    );
    expect(mockMakeRequestCompositionClient).toHaveBeenCalledWith(
      expectedCallComposition,
      expectedCallToSession,
    );
  });
});

describe("getCertificate()", () => {
  it("should make request to certificate client", async () => {
    const mockReq = {
      session: {
        value: "test",
      },
    };

    const result = await getCertificate(mockReq.session, "123");
    expect(result).toEqual({ data: certificateResponse });

    expect(mockMakeRequestCertificateClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestNoteClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestPreferenceClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCompositionClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCertificateClient).toHaveBeenCalledWith(
      expectedCallCertificate,
      expectedCallToSession,
    );
  });
});

describe("getCitizen()", () => {
  it("should make request to citizen client", async () => {
    const mockReq = {
      session: {
        value: "test",
      },
    };

    const result = await getCitizen(mockReq.session, "456");
    expect(result).toEqual({ data: citizenWithEmailResponse });

    expect(mockMakeRequestCertificateClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestNoteClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestPreferenceClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCompositionClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledWith(
      expectedCallCitien,
      expectedCallToSession,
    );
  });
});

describe("getNotes()", () => {
  it("should make request to note client", async () => {
    const mockReq = {
      session: {
        value: "test",
      },
    };

    const result = await getNotes(mockReq.session, "123");
    expect(result).toEqual({ data: notesResponse });

    expect(mockMakeRequestCertificateClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestNoteClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestPreferenceClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCompositionClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestNoteClient).toHaveBeenCalledWith(
      expectedCallNote,
      expectedCallToSession,
    );
  });
});

describe("getPreference()", () => {
  it("should make request to preference client", async () => {
    const mockReq = {
      session: {
        value: "test",
      },
    };

    const result = await getPreference(mockReq.session, "456");
    expect(result).toEqual({ data: preferenceResponse });

    expect(mockMakeRequestCertificateClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestNoteClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestPreferenceClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestPreferenceClient).toHaveBeenCalledWith(
      expectedCallPreference,
      expectedCallToSession,
    );
  });
});

describe("getAudit()", () => {
  it("should make request to composition client", async () => {
    const mockReq = {
      session: {
        value: "test",
      },
    };

    const result = await getAudit(mockReq.session, "456", "123");
    expect(result).toEqual({ data: compositionResponse });

    expect(mockMakeRequestCertificateClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestNoteClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestPreferenceClient).toHaveBeenCalledTimes(0);
    expect(mockMakeRequestCompositionClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestCompositionClient).toHaveBeenCalledWith(
      expectedCallComposition,
      expectedCallToSession,
    );
  });
});
