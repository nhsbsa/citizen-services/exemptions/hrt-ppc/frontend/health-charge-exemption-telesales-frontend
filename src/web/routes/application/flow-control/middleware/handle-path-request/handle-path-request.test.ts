import expect from "expect";
import * as sinon from "sinon";
import { DECISION_URL } from "../../../paths/paths";
import { IN_PROGRESS, COMPLETED } from "../../states";
import { buildSessionForJourney } from "../../test-utils/test-utils";

const TELESALES_SEARCH = "search";

const journey = {
  name: TELESALES_SEARCH,
  steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
  pathsInSequence: ["/first", "/second"],
};

const mockSessionDestroyButKeepLoggedIn = jest.fn();

jest.mock("../../../steps/common/session-destroy", () => ({
  sessionDestroyButKeepLoggedIn: mockSessionDestroyButKeepLoggedIn,
}));

import { handleRequestForPath } from "./handle-path-request";

test("handleRequestForPath() should redirect to next allowed step if requested path is not allowed", () => {
  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES_SEARCH,
        state: IN_PROGRESS,
        nextAllowedPath: "/first",
      }),
    },
  };

  const redirect = sinon.spy();
  const res = { redirect };
  const next = sinon.spy();

  handleRequestForPath(journey, undefined)(req, res, next);

  expect(redirect.calledWith("/first")).toBe(true);
  expect(next.called).toBe(false);
});

test("handleRequestForPath() should call next() if requested path is allowed", () => {
  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES_SEARCH,
        state: IN_PROGRESS,
        nextAllowedPath: "/second",
      }),
    },
  };

  const redirect = sinon.spy();
  const res = { redirect };
  const next = sinon.spy();

  handleRequestForPath(journey, undefined)(req, res, next);

  expect(redirect.called).toBe(false);
  expect(next.called).toBe(true);
});

test(`handleRequestForPath() should call mockSessionDestroyButKeepLoggedIn and redirect to first step in journey when navigating away from ${DECISION_URL} to a path in sequence`, () => {
  const clearCookie = sinon.spy();
  const redirect = sinon.spy();
  const next = sinon.spy();

  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES_SEARCH,
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {
    clearCookie,
    redirect,
  };

  handleRequestForPath(journey, undefined)(req, res, next);

  expect(mockSessionDestroyButKeepLoggedIn).toBeCalled();
  expect(mockSessionDestroyButKeepLoggedIn).toBeCalledWith(req);
  expect(clearCookie.calledWith("lang")).toBe(true);
  expect(redirect.calledWith("/first")).toBe(true);
});

test(`handleRequestForPath() should call mockSessionDestroyButKeepLoggedIn and redirects to first step in journey when a ${COMPLETED} journey exists in session`, () => {
  const clearCookie = sinon.spy();
  const redirect = sinon.spy();
  const next = sinon.spy();

  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES_SEARCH,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
      ...buildSessionForJourney({
        journeyName: "another-journey",
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {
    clearCookie,
    redirect,
  };

  handleRequestForPath(journey, undefined)(req, res, next);

  expect(mockSessionDestroyButKeepLoggedIn).toBeCalled();
  expect(mockSessionDestroyButKeepLoggedIn).toBeCalledWith(req);
  expect(clearCookie.calledWith("lang")).toBe(true);
  expect(redirect.calledWith("/first")).toBe(true);
  expect(clearCookie.calledWith("lang")).toBe(true);
  expect(redirect.calledWith("/first")).toBe(true);
});
