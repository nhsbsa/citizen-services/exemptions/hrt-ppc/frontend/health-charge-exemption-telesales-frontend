import * as dotEnv from "dotenv";

dotEnv.config();

const ONE_HOUR = 60 * 60;

const defaultConfig = {
  socket: {
    port: process.env.REDIS_PORT || "6379",
    host: process.env.REDIS_HOST || "127.0.0.1",
  },
  ttl: ONE_HOUR,
  legacyMode: false,
};

export const redis = defaultConfig;
