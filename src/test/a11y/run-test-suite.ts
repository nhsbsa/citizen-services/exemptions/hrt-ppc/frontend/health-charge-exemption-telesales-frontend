/* no-process-exit */
"use strict";

import {
  setupSuccessfulActiveExemptionFoundSearchMatchMapping,
  setupSuccessfulNoSearchMatchMapping,
} from "./wiremock/hrt-search/match";
import { setupSuccessfulGetCitizenMapping } from "./wiremock/citizen-api/success";
import { setupSuccessfulGetProductMapping } from "./wiremock/product-api/success";
import { setupSuccessfulGetCertificateMapping } from "./wiremock/certificate-api/success";
import { setupSuccessfulNewPaymentMapping } from "./wiremock/payment-api/success";
import { setupSuccessfulPinMapping } from "./wiremock/card-payment-api/success";
import { setupSuccessfulGetNotesMapping } from "./wiremock/note-api/success";
import { setupSuccessfulGetPreferenceMapping } from "./wiremock/preference-api/success";
import { setupSuccessfulGetAuditHistoryMapping } from "./wiremock/composition-api/success";
import { deleteAllWiremockMappings } from "./wiremock/wiremock";

import { runAllTests } from "./test-suite";
import { TEST_SUITE } from "./tests";
import {
  setupErrorReissueMapping,
  setupSuccessfulReissueMapping,
} from "./wiremock/reissue-api/reissue";
import { setupSuccessfulIssueCertificateMapping } from "./wiremock/issue-certificate-api/issue-certificate";
/*
  Runs the accessibility test suite, having set up wiremock to mock each API service.
 */
const runTestSuite = async (testSuite) => {
  try {
    const makeArrSearchNoResults: any[] = [];
    makeArrSearchNoResults.push(testSuite[0]);

    const makeArrSearchFoundResults: any[] = [];
    makeArrSearchFoundResults.push(testSuite[1]);

    const makeArrReissueExemption: any[] = [];
    makeArrReissueExemption.push(testSuite[2]);

    const makeArrReissueExemptionError: any[] = [];
    makeArrReissueExemptionError.push(testSuite[3]);

    const makeArrNewApplicant: any[] = [];
    makeArrNewApplicant.push(testSuite[4]);

    const makeArrNoCookies: any[] = [];
    makeArrNoCookies.push(testSuite[5]);

    const makeArrPageNotFound: any[] = [];
    makeArrPageNotFound.push(testSuite[6]);

    const makeArrProblemWithService: any[] = [];
    makeArrProblemWithService.push(testSuite[7]);

    const makeArrAmendCertificateDetails: any[] = [];
    makeArrAmendCertificateDetails.push(testSuite[8]);

    await setupSuccessfulGetCertificateMapping();
    await setupSuccessfulGetCitizenMapping();
    await setupSuccessfulGetProductMapping();
    await setupSuccessfulNewPaymentMapping();
    await setupSuccessfulGetNotesMapping();
    await setupSuccessfulGetPreferenceMapping();
    await setupSuccessfulGetAuditHistoryMapping();
    await setupSuccessfulIssueCertificateMapping();
    await setupSuccessfulPinMapping();
    // stubs wiremocks and run test for where exemptions not found
    await setupSuccessfulNoSearchMatchMapping();
    await runAllTests(makeArrSearchNoResults);
    // stubs wiremocks and run test for where exemptions found
    await setupSuccessfulActiveExemptionFoundSearchMatchMapping();
    await runAllTests(makeArrSearchFoundResults);

    await setupSuccessfulReissueMapping();
    await runAllTests(makeArrReissueExemption);

    await setupErrorReissueMapping();
    await runAllTests(makeArrReissueExemptionError);

    await runAllTests(makeArrNewApplicant);
    await runAllTests(makeArrNoCookies);
    await runAllTests(makeArrPageNotFound);
    await runAllTests(makeArrProblemWithService);
    await runAllTests(makeArrAmendCertificateDetails);
  } catch (error) {
    console.log(error);
    process.exit(1);
  } finally {
    await deleteAllWiremockMappings();
  }
};

runTestSuite(TEST_SUITE);
