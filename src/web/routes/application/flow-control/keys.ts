export const JOURNEYS_KEY = "journeys";
export const NEXT_ALLOWED_PATH_KEY = "nextAllowedStep";
export const STATE_KEY = "state";
export const STEP_DATA_KEY = "stepData";
export const LOCATOR = "locator";
export const CURRENT_STEP_PATH_KEY = "currentStep";
