import {
  Channel,
  PaymentMethod,
  PaymentStatus,
} from "@nhsbsa/health-charge-exemption-common-frontend";

const createNewPaymentSuccessMapping = () => {
  return JSON.stringify({
    request: {
      method: "POST",
      url: `/v1/payments?certificateId=c0a81da7-85b8-1fa3-8185-b936fef50000`,
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
        channel: {
          equalTo: Channel.STAFF,
        },
        "user-id": {
          equalTo: "LOCAL_USER_CYPHER",
        },
        "correlation-id": {
          matches: "([A-Za-z0-9_-])+",
        },
      },
    },
    response: {
      status: 200,
      headers: {
        "Content-Type": "application/json",
      },
      jsonBody: {
        id: "c0a80196-85cf-1c6d-8185-cf8cf96a0006",
        certificateId: "c0a81da7-85b8-1fa3-8185-b936fef50000",
        method: PaymentMethod.CARD,
        amount: 1870,
        date: "2023-01-20",
        status: PaymentStatus.PENDING,
        transactionId: "906944761072A2088F71",
        _meta: {
          channel: Channel.STAFF,
          createdTimestamp: "2023-01-20T14:20:00.7465054",
          createdBy: "POSTMAN",
          updatedTimestamp: "2023-01-20T14:20:00.7465054",
          updatedBy: "POSTMAN",
        },
        _links: {
          self: {
            href: "http://localhost:8110/v1/payments/c0a80196-85cf-1c6d-8185-cf8cf96a0006",
          },
          payment: {
            href: "http://localhost:8110/v1/payments/c0a80196-85cf-1c6d-8185-cf8cf96a0006",
          },
        },
      },
    },
  });
};

export { createNewPaymentSuccessMapping };
