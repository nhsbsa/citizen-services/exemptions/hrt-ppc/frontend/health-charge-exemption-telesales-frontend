declare global {
  declare module "express-session" {
    interface Session {
      userCypher: string;
      locator: string;
      certificate: {
        id: string;
        reference: string;
        type: string;
        status: string;
        startDate: string;
        endDate: string;
        applicationDate: string;
        certificateStartDate: string;
        _meta: {
          channel: string;
        };
        "certificateStartDate-day": string;
        "certificateStartDate-month": string;
        "certificateStartDate-year": string;
      };
    }
  }
}
