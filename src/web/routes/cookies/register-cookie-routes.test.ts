import { registerCookieRoutes } from "./register-cookie-routes";

const COOKIE_URL = "/test-context/enable-cookies";

const req = { t: (string) => string };
const app = {
  ...jest.requireActual("express"),
  get: jest.fn(),
  all: jest.fn(),
};
const res = { render: jest.fn() };

test(`registerCookieRoutes() should redirect to ${COOKIE_URL}`, () => {
  app.all.mockImplementation((path, callback) => {
    callback(req, res);
  });

  registerCookieRoutes(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(1, COOKIE_URL, expect.any(Function));
});
