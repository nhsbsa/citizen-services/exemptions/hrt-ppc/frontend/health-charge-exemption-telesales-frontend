import { contextPath } from "../../paths/context-path";
import { EXEMPTION_VIEW_URL, REISSUE_URL } from "../../paths/paths";

const COMMON_TABLE_PREFIX = "exemptionDetail.tables.";

export const pageContent = ({ translate, req }) => ({
  title: translate("reissue.title"),
  heading: translate("reissue.heading"),
  preferencesText: {
    email: translate("preferencesText.email"),
    post: translate("preferencesText.post"),
    none: translate("preferencesText.none"),
  },
  accessKeyButtonText: translate("buttons:accessKey"),
  accessKeyLink: contextPath(REISSUE_URL),
  tables: {
    certificateDetails: {
      title: translate(COMMON_TABLE_PREFIX + "certificateDetails.title"),
      rows: {
        reference: translate(
          COMMON_TABLE_PREFIX + "certificateDetails.rows.reference",
        ),
        type: translate(COMMON_TABLE_PREFIX + "certificateDetails.rows.type"),
        status: translate(
          COMMON_TABLE_PREFIX + "certificateDetails.rows.status",
        ),
        channel: translate(
          COMMON_TABLE_PREFIX + "certificateDetails.rows.channel",
        ),
        fulfillment: translate(
          COMMON_TABLE_PREFIX + "certificateDetails.rows.fulfillment",
        ),
        startDate: translate(
          COMMON_TABLE_PREFIX + "certificateDetails.rows.startDate",
        ),
        endDate: translate(
          COMMON_TABLE_PREFIX + "certificateDetails.rows.endDate",
        ),
        applicationDate: translate(
          COMMON_TABLE_PREFIX + "certificateDetails.rows.applicationDate",
        ),
      },
    },
    personalDetails: {
      title: translate(COMMON_TABLE_PREFIX + "personalDetails.title"),
      rows: {
        name: translate(COMMON_TABLE_PREFIX + "personalDetails.rows.name"),
        address: translate(
          COMMON_TABLE_PREFIX + "personalDetails.rows.address",
        ),
        telephoneNumber: translate(
          COMMON_TABLE_PREFIX + "personalDetails.rows.telephoneNumber",
        ),
        dateOfBirth: translate(
          COMMON_TABLE_PREFIX + "personalDetails.rows.dateOfBirth",
        ),
        email: translate(COMMON_TABLE_PREFIX + "personalDetails.rows.email"),
        nhsNumber: translate(
          COMMON_TABLE_PREFIX + "personalDetails.rows.nhsNumber",
        ),
      },
    },
  },
  submitButtonText: translate("buttons:submit"),
  previous: `${EXEMPTION_VIEW_URL}/${req.params.id}`,
});
