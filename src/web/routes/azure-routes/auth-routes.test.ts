const token = {
  accessToken: "myAccessToken",
  idToken: "myIdToken",
  account: "myAccount",
};
const mockCreateNewGuid = jest.fn().mockReturnValue("newGuidCsrf");
const mockBase64Encode = jest.fn().mockReturnValue("encodedBase64");
const mockBase64Decode = jest
  .fn()
  .mockReturnValue('{"csrfToken":"newGuidCsrfs","redirectTo":"/test-context"}');
const mockAcquireTokenByCode = jest.fn().mockReturnValue(token);
const mockConfidentialClientApplication = jest.fn().mockImplementation(() => {
  return {
    acquireTokenByCode: mockAcquireTokenByCode,
    base64Encode: mockBase64Encode,
    base64Decode: mockBase64Decode,
  };
});
const mockCryptoProvider = jest.fn().mockImplementation(() => {
  return {
    createNewGuid: mockCreateNewGuid,
    base64Encode: mockBase64Encode,
    base64Decode: mockBase64Decode,
  };
});
const mockRedirectToAuthCodeUrl = jest.fn();

import {
  registerAuthRoutes,
  registerSignIn,
  registerAuthRedirect,
  registerSignOut,
} from "./auth-routes";
import express from "express";
import * as httpStatus from "http-status-codes";
import { environment } from "../../../config/environment";
import { wrapError } from "../application/errors";

environment.MSALCONFIG.auth.authority = "MOCK_AUTHORITY";
environment.POST_LOGOUT_REDIRECT_URI = "MOCK_REDIRECT_URI";

const router = {
  ...jest.requireActual("express"),
  get: jest.fn(),
  post: jest.fn(),
};
jest.spyOn(express, "Router").mockImplementationOnce(() => router);

const res = {
  redirect: jest.fn(),
  cookie: jest.fn(),
  clearCookie: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};
const next = jest.fn();
const app = {
  use: jest.fn(),
};

jest.mock("@azure/msal-node", () => ({
  ConfidentialClientApplication: mockConfidentialClientApplication,
  CryptoProvider: mockCryptoProvider,
}));

jest.mock("./auth", () => ({
  redirectToAuthCodeUrl: mockRedirectToAuthCodeUrl,
}));

beforeEach(() => {
  jest.clearAllMocks();
});

describe("registerAuthRoutes", () => {
  test("regsisters signIn, authRedirect and signOut urls", () => {
    const req = {
      t: (string) => string,
      session: {
        destroy: jest.fn((callback) => callback()),
        isAuthenticated: true,
        account: {
          username: "test@mail.com",
        },
      },
    };

    router.get.mockImplementation((path, callback) => {
      callback(req, res, next);
    });

    registerAuthRoutes(app);

    expect(app.use).toBeCalledTimes(1);
    expect(app.use).toBeCalledWith(router);
    expect(router.get).toBeCalledTimes(2);
    expect(router.get).toHaveBeenNthCalledWith(
      1,
      "/test-context/auth/signin",
      expect.anything(),
    );
    expect(router.get).toHaveBeenNthCalledWith(
      2,
      "/test-context/auth/signout",
      expect.anything(),
    );
    expect(router.post).toBeCalledTimes(1);
    expect(router.post).toHaveBeenNthCalledWith(
      1,
      "/test-context/auth/redirect",
      expect.anything(),
    );
  });
});

describe("registerSignOut`", () => {
  test("sets isAuthenticated to false and redirects to logout url", async () => {
    const logoutUri = `${environment.MSALCONFIG.auth.authority}/oauth2/v2.0/logout?post_logout_redirect_uri=${environment.POST_LOGOUT_REDIRECT_URI}`;
    const req = {
      session: {
        destroy: jest.fn((callback) => callback()),
        isAuthenticated: true,
        account: {
          username: "test@mail.com",
        },
      },
    };

    router.get.mockImplementation((path, callback) => {
      callback(req, res, next);
    });

    registerSignOut(router);

    expect(router.get).toBeCalledTimes(1);
    expect(router.get).toHaveBeenNthCalledWith(
      1,
      "/test-context/auth/signout",
      expect.anything(),
    );
    expect(req.session.isAuthenticated).toBe(false);
    expect(req.session.destroy).toHaveBeenCalled();
    expect(res.redirect).toHaveBeenCalledWith(logoutUri);
    expect(app.use).not.toBeCalled();
    expect(router.post).not.toBeCalled();
    expect(res.clearCookie).not.toBeCalled();
    expect(res.cookie).not.toBeCalled();
    expect(res.status).not.toBeCalled();
    expect(res.render).not.toBeCalled();
    expect(next).not.toBeCalled();
    expect(mockCreateNewGuid).not.toBeCalled();
    expect(mockBase64Encode).not.toBeCalled();
    expect(mockBase64Decode).not.toBeCalled();
    expect(mockConfidentialClientApplication).not.toBeCalled();
    expect(mockCryptoProvider).not.toBeCalled();
    expect(mockRedirectToAuthCodeUrl).not.toBeCalled();
    expect(mockAcquireTokenByCode).not.toBeCalled();
  });
});

describe("registerSignIn`", () => {
  test("sets csrf token and state then calls redirectToAuthCodeUrl", async () => {
    const req = {
      session: {
        csrfToken: undefined,
      },
    };

    router.get.mockImplementation((path, callback) => {
      callback(req, res, next);
    });

    registerSignIn(router);

    expect(router.get).toBeCalledTimes(1);
    expect(router.get).toHaveBeenNthCalledWith(
      1,
      "/test-context/auth/signin",
      expect.anything(),
    );
    expect(mockCryptoProvider).toHaveBeenCalled();
    expect(mockCryptoProvider).toHaveBeenCalledWith();
    expect(mockCreateNewGuid).toHaveBeenCalled();
    expect(mockBase64Encode).toHaveBeenCalledWith(
      JSON.stringify({ csrfToken: "newGuidCsrf", redirectTo: "/test-context" }),
    );
    expect(req.session).toEqual({
      csrfToken: "newGuidCsrf",
    });
    expect(mockRedirectToAuthCodeUrl).toBeCalledTimes(1);
    expect(mockRedirectToAuthCodeUrl).toHaveBeenCalledWith(
      req,
      res,
      next,
      { scopes: ["openid", "profile"], state: "encodedBase64" },
      { scopes: ["openid", "profile"] },
    );
    expect(app.use).not.toBeCalled();
    expect(router.post).not.toBeCalled();
    expect(res.redirect).not.toBeCalled();
    expect(res.clearCookie).not.toBeCalled();
    expect(res.cookie).not.toBeCalled();
    expect(res.status).not.toBeCalled();
    expect(res.render).not.toBeCalled();
    expect(next).not.toBeCalled();
    expect(mockConfidentialClientApplication).not.toBeCalled();
    expect(mockBase64Decode).not.toBeCalled();
    expect(mockAcquireTokenByCode).not.toBeCalled();
  });
});

describe("registerAuthRedirect`", () => {
  test("redirects to auth url", async () => {
    const req = {
      body: {
        state: "newGuidCsrfs",
        code: "myCode",
      },
      session: {
        csrfToken: "newGuidCsrfs",
        pkceCodes: {
          verifier: "myVerifier",
        },
        authCodeRequest: {},
      },
    };

    router.post.mockImplementation((path, callback) => {
      callback(req, res, next);
    });

    await registerAuthRedirect(router);

    expect(router.post).toBeCalledTimes(1);
    expect(router.post).toHaveBeenNthCalledWith(
      1,
      "/test-context/auth/redirect",
      expect.anything(),
    );
    expect(mockCryptoProvider).toHaveBeenCalled();
    expect(mockCryptoProvider).toHaveBeenCalledWith();
    expect(mockBase64Decode).toBeCalledWith(req.body.state);
    expect(mockAcquireTokenByCode).toBeCalledWith(req.session.authCodeRequest);
    expect(req.session).toStrictEqual({
      accessToken: "myAccessToken",
      account: "myAccount",
      authCodeRequest: { code: "myCode", codeVerifier: "myVerifier" },
      csrfToken: "newGuidCsrfs",
      idToken: "myIdToken",
      isAuthenticated: true,
      pkceCodes: { verifier: "myVerifier" },
    });
    expect(app.use).not.toBeCalled();
    expect(router.get).not.toBeCalled();
    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toHaveBeenCalledWith("/test-context");
    expect(res.clearCookie).not.toBeCalled();
    expect(res.cookie).not.toBeCalled();
    expect(res.status).not.toBeCalled();
    expect(res.render).not.toBeCalled();
    expect(next).not.toBeCalled();
    expect(mockRedirectToAuthCodeUrl).not.toBeCalled();
    expect(mockBase64Encode).not.toBeCalled();
    expect(mockCreateNewGuid).not.toBeCalled();
    expect(mockConfidentialClientApplication).not.toBeCalled();
  });

  test("calls next() with error when AcquireTokenByCode throws error", async () => {
    const error = new Error("Error getting token by code");
    mockAcquireTokenByCode.mockRejectedValue(error);
    const req = {
      body: {
        state: "newGuidCsrfs",
        code: "myCode",
      },
      session: {
        csrfToken: "newGuidCsrfs",
        pkceCodes: {
          verifier: "myVerifier",
        },
        authCodeRequest: {},
      },
    };

    router.post.mockImplementation((path, callback) => {
      callback(req, res, next);
    });

    await registerAuthRedirect(router);

    expect(router.post).toBeCalledTimes(1);
    expect(router.post).toHaveBeenNthCalledWith(
      1,
      "/test-context/auth/redirect",
      expect.anything(),
    );
    expect(mockCryptoProvider).toHaveBeenCalled();
    expect(mockCryptoProvider).toHaveBeenCalledWith();
    expect(mockBase64Decode).toBeCalledWith(req.body.state);
    expect(mockAcquireTokenByCode).toBeCalledWith(req.session.authCodeRequest);
    expect(req.session).toStrictEqual({
      authCodeRequest: { code: "myCode", codeVerifier: "myVerifier" },
      csrfToken: "newGuidCsrfs",
      pkceCodes: { verifier: "myVerifier" },
    });
    expect(app.use).not.toBeCalled();
    expect(router.get).not.toBeCalled();
    expect(res.redirect).not.toBeCalled();
    expect(res.clearCookie).not.toBeCalled();
    expect(res.cookie).not.toBeCalled();
    expect(res.status).not.toBeCalled();
    expect(res.render).not.toBeCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(error);
    expect(mockRedirectToAuthCodeUrl).not.toBeCalled();
    expect(mockBase64Encode).not.toBeCalled();
    expect(mockCreateNewGuid).not.toBeCalled();
    expect(mockConfidentialClientApplication).not.toBeCalled();
  });

  test("calls next() with error when csrf token not matching", async () => {
    const req = {
      body: {
        state: "newGuidCsrfs",
        code: "myCode",
      },
      session: {
        csrfToken: "NOTMATCH",
        pkceCodes: {
          verifier: "myVerifier",
        },
        authCodeRequest: {},
      },
    };

    router.post.mockImplementation((path, callback) => {
      callback(req, res, next);
    });

    await registerAuthRedirect(router);

    expect(router.post).toBeCalledTimes(1);
    expect(router.post).toHaveBeenNthCalledWith(
      1,
      "/test-context/auth/redirect",
      expect.anything(),
    );
    expect(mockCryptoProvider).toHaveBeenCalled();
    expect(mockCryptoProvider).toHaveBeenCalledWith();
    expect(mockBase64Decode).toBeCalledWith(req.body.state);

    expect(req.session).toStrictEqual({
      authCodeRequest: {},
      csrfToken: "NOTMATCH",
      pkceCodes: { verifier: "myVerifier" },
    });
    expect(app.use).not.toBeCalled();
    expect(router.get).not.toBeCalled();
    expect(res.redirect).not.toBeCalled();
    expect(res.clearCookie).not.toBeCalled();
    expect(res.cookie).not.toBeCalled();
    expect(res.status).not.toBeCalled();
    expect(res.render).not.toBeCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(
      wrapError({
        cause: new Error("csrf token does not match"),
        message: "csrf token does not match",
        statusCode: httpStatus.StatusCodes.FORBIDDEN,
      }),
    );
    expect(mockAcquireTokenByCode).not.toBeCalled();
    expect(mockRedirectToAuthCodeUrl).not.toBeCalled();
    expect(mockBase64Encode).not.toBeCalled();
    expect(mockCreateNewGuid).not.toBeCalled();
    expect(mockConfidentialClientApplication).not.toBeCalled();
  });

  test("calls next() with error when state is missing from req body", async () => {
    const req = {
      body: {},
    };

    router.post.mockImplementation((path, callback) => {
      callback(req, res, next);
    });

    await registerAuthRedirect(router);

    expect(router.post).toBeCalledTimes(1);
    expect(router.post).toHaveBeenNthCalledWith(
      1,
      "/test-context/auth/redirect",
      expect.anything(),
    );

    expect(req).toStrictEqual({ body: {} });
    expect(app.use).not.toBeCalled();
    expect(router.get).not.toBeCalled();
    expect(res.redirect).not.toBeCalled();
    expect(res.clearCookie).not.toBeCalled();
    expect(res.cookie).not.toBeCalled();
    expect(res.status).not.toBeCalled();
    expect(res.render).not.toBeCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(
      wrapError({
        cause: new Error("state is missing"),
        message: "state is missing",
        statusCode: httpStatus.StatusCodes.FORBIDDEN,
      }),
    );
    expect(mockBase64Decode).not.toBeCalled();
    expect(mockAcquireTokenByCode).not.toBeCalled();
    expect(mockRedirectToAuthCodeUrl).not.toBeCalled();
    expect(mockBase64Encode).not.toBeCalled();
    expect(mockCreateNewGuid).not.toBeCalled();
    expect(mockConfidentialClientApplication).not.toBeCalled();
    expect(mockCryptoProvider).not.toBeCalled();
  });
});
