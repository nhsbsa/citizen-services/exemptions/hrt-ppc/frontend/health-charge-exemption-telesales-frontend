import { stateMachine } from "../../flow-control/state-machine";
import * as states from "../../flow-control/states";
import * as actions from "../../flow-control/state-machine/actions";
import { getPreviousPath } from "../../flow-control/get-previous-path";
import {
  applicantDetailsRowList,
  certificateDetailsRowList,
  notesDetailsRowList,
} from "./get-row-data";

const { INCREMENT_NEXT_ALLOWED_PATH } = actions;
const { IN_REVIEW } = states;

const pageContent = ({ translate }) => ({
  title: translate("checkAnswers.title"),
  heading: translate("checkAnswers.heading"),
  preferencesText: {
    email: translate("preferencesText.email"),
    post: translate("preferencesText.post"),
    none: translate("preferencesText.none"),
  },
  amendDetailsButtonText: translate("buttons:amendDetails"),
  cardPaymentCallButtonText: translate("buttons:cardPaymentCall"),
  cancelButtonText: translate("buttons:cancel"),
  warningCalloutHeading: translate("checkAnswers.warningCalloutHeading"),
  warningCalloutParagraphOne: translate(
    "checkAnswers.warningCalloutParagraphOne",
  ),
  warningCalloutParagraphTwo: translate(
    "checkAnswers.warningCalloutParagraphTwo",
  ),
  summaryListHeadings: {
    applicantDetails: translate("checkAnswers.applicantDetails"),
    certificateDetails: translate("checkAnswers.certificateDetails"),
    notesDetails: translate("checkAnswers.notesDetails"),
  },
  name: translate("checkAnswers.summaryKey.name"),
  dateOfBirth: translate("checkAnswers.summaryKey.dateOfBirth"),
  nhsNumber: translate("checkAnswers.summaryKey.nhsNumber"),
  address: translate("checkAnswers.summaryKey.address"),
  certificateFulfillment: translate(
    "checkAnswers.summaryKey.certificateFulfillment",
  ),
  email: translate("checkAnswers.summaryKey.email"),
  telephone: translate("checkAnswers.summaryKey.telephone"),
  length: translate("checkAnswers.summaryKey.length"),
  startsOn: translate("checkAnswers.summaryKey.startsOn"),
  endsOn: translate("checkAnswers.summaryKey.endsOn"),
  totalCost: translate("checkAnswers.summaryKey.totalCost"),
  paymentMethod: translate("checkAnswers.summaryKey.paymentMethod"),
  notes: translate("checkAnswers.summaryKey.notes"),
});

// a step is navigable if it hasn't defined an isNavigable function.
const stepIsNavigable = (step, req) =>
  !step.hasOwnProperty("isNavigable") || step.isNavigable(req);

const getLastNavigablePath = (steps, req) => {
  const lastStep = steps[steps.length - 1];

  return stepIsNavigable(lastStep, req)
    ? lastStep.path
    : getPreviousPath(steps, lastStep, req.session);
};

const getCheckAnswers = (journey) => (req, res) => {
  const localisation = pageContent({ translate: req.t });
  const { steps } = journey;

  stateMachine.setState(IN_REVIEW, req, journey);
  stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);

  res.render("check-answers", {
    ...localisation,
    csrfToken: req.csrfToken(),
    applicantDetailsLists: applicantDetailsRowList(req, localisation),
    certificateDetailsLists: certificateDetailsRowList(req, localisation),
    notesDetailsLists: notesDetailsRowList(req, localisation),
    previous: getLastNavigablePath(steps, req),
  });
};

export { getCheckAnswers, getLastNavigablePath, pageContent };
