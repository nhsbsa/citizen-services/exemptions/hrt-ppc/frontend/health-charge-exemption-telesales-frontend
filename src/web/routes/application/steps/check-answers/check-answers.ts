import { getCheckAnswers } from "./get";
import { postCheckAnswers } from "./post";
import { CHECK_ANSWERS_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";
import { configureSessionDetails } from "../../flow-control/middleware/session-details";
import { handleRequestForPath } from "../../flow-control/middleware/handle-path-request";

const registerCheckAnswersRoutes = (journey, app, csrfProtection, config) => {
  app
    .route(prefixPath(journey.pathPrefix, CHECK_ANSWERS_URL))
    .get(
      configureSessionDetails(journey),
      handleRequestForPath(journey, undefined),
      csrfProtection,
      getCheckAnswers(journey),
    )
    .post(csrfProtection, postCheckAnswers(config, journey));
};

export { registerCheckAnswersRoutes };
