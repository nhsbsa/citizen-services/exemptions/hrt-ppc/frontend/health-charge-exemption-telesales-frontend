import { contextPath } from "../../paths/context-path";
import { validate } from "./validate";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";
import { ADD_NOTE_URL, EXEMPTION_VIEW_URL } from "../../paths/paths";
import { findRecordInSearchResults } from "../common/search-utils";
import { getNotesRows } from "./get-notes-rows";
import { behaviourForPost } from "./behaviour-for-post";

const isNavigable = (req) => {
  return (
    notIsUndefinedOrNullOrEmpty(findRecordInSearchResults(req)) &&
    req.session.certificate.reference === req.params.id
  );
};

const pageContent = ({ translate, req }) => ({
  title: translate("addNote.title"),
  heading: translate("addNote.heading"),
  tableHeading: translate("addNote.tableHeading"),
  addNoteLabel: translate("addNote.addNoteLabel"),
  submitButtonText: translate("buttons:submit"),
  backButtonText: translate("buttons:back"),
  backButtonUrl: contextPath(`${EXEMPTION_VIEW_URL}/${req.params.id}`),
  tables: {
    notes: {
      rows: {
        date: translate("addNote.tables.notes.rows.date"),
        time: translate("addNote.tables.notes.rows.time"),
        agent: translate("addNote.tables.notes.rows.agent"),
        description: translate("addNote.tables.notes.rows.description"),
      },
    },
  },
  previous: false,
});

const behaviourForGet = () => async (req, res, next) => {
  await getNotesRows(req, res);
  next();
};

const addNote = {
  path: `/:id(HRT[A-Za-z0-9]+)${ADD_NOTE_URL}`,
  template: "add-note",
  isNavigable,
  behaviourForGet,
  pageContent,
  validate,
  behaviourForPost,
};

export {
  addNote,
  isNavigable,
  pageContent,
  validate,
  behaviourForPost,
  behaviourForGet,
};
