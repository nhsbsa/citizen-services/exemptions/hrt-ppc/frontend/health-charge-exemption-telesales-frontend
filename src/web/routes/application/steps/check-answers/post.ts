import {
  createIssueCertificateRequestBody,
  createPaymentRequestBody,
  createCardPaymentRequestBody,
} from "./create-request-body";
import { issueCertificateClient } from "../../../../client/issue-certificate-client";
import { paymentClient } from "../../../../client/payment-client";
import { cardPaymentsClient } from "../../../../client/cps-client";
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { prefixPath } from "../../paths/prefix-path";
import { CARD_PAYMENT_URL } from "../../paths/paths";

const postCheckAnswers = (config, journey) => async (req, res, next) => {
  try {
    const { pathPrefix } = journey;
    const client = new issueCertificateClient(config);
    const issueCertificateBody = createIssueCertificateRequestBody(req.session);

    const response = await client.makeRequest(
      {
        method: "POST",
        url: "/v1/issue-certificate",
        data: issueCertificateBody,
        responseType: "json",
      },
      req,
    );

    req.session.issuedCertificateDetails = {
      certificateId: response.data["certificate"]["id"],
      citizenId: response.data["certificate"]["citizenId"],
      certificateReference: response.data["certificate"]["reference"],
    };

    const payment = new paymentClient(config);
    const paymentBody = createPaymentRequestBody();

    const responseFromPayment = await payment.makeRequest(
      {
        method: "POST",
        url: `/v1/payments?certificateId=${req.session.issuedCertificateDetails.certificateId}`,
        data: paymentBody,
        responseType: "json",
      },
      req.session,
    );

    req.session.paymentDetails = {
      paymentId: responseFromPayment.data["id"],
      transactionId: responseFromPayment.data["transactionId"],
    };

    const cardPayment = new cardPaymentsClient(config);
    const cardPaymentBody = createCardPaymentRequestBody(req.session);

    const responseFromCardPayment = await cardPayment.makeRequestCreatePayment({
      method: "POST",
      url: `/payments`,
      headers: setHeadersIfMockEnabled(config, req),
      data: cardPaymentBody,
      responseType: "json",
    });

    req.session.cardPaymentDetails = {
      pin: responseFromCardPayment.data.pin,
    };
    return res.redirect(prefixPath(pathPrefix, CARD_PAYMENT_URL));
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};

/** The headers are set only when mock is enabled as we need to
 * simulate different status's, we are using firstName for this
 *
 * @param config the app config
 * @param req the request
 * @returns the headers or null
 */
const setHeadersIfMockEnabled = (config, req) => {
  if (config.environment.CARD_PAYMENTS_USE_MOCK === true) {
    return { firstName: req.session.newApplicant.firstName };
  }
  return {};
};

export { postCheckAnswers, setHeadersIfMockEnabled };
