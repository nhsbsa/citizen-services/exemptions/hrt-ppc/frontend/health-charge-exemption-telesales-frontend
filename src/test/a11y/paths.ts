import * as dotEnv from "dotenv";
dotEnv.config();

const BASE_URL =
  (process.env.APP_BASE_URL || "http://localhost:8085") +
    process.env.CONTEXT_PATH || `http://localhost:${process.env.PORT}`;

const URLS = {
  START: `${BASE_URL}/start`,
  SEARCH_HRT_RECORDS: `${BASE_URL}/search-hrt-records`,
  NO_RESULTS: `${BASE_URL}/no-results`,
  SEARCH_RESULTS: `${BASE_URL}/search-results`,
  EXEMPTION_DETAIL: `${BASE_URL}/exemption-information/HRTC50ABAA9`,
  ADD_NOTE: `${BASE_URL}/HRTC50ABAA9/add-note`,
  EDIT_CERTIFICATE_DETAILS: `${BASE_URL}/HRTC50ABAA9/edit-certificate-details`,
  EDIT_PERSONAL_DETAILS: `${BASE_URL}/HRTC50ABAA9/edit-personal-details`,
  REISSUE: `${BASE_URL}/HRTC50ABAA9/reissue`,
  NEW_APPLICANT_DETAILS: `${BASE_URL}/new-applicant-details`,
  CHECK_ANSWERS: `${BASE_URL}/check-their-answers`,
  DECISION: `${BASE_URL}/decision`,
  ENABLE_COOKIES: `${BASE_URL}/enable-cookies`,
  SERVICE_UNAVAILABLE: `${BASE_URL}/service-unavailable`,
  PAGE_NOT_FOUND: `${BASE_URL}/page-not-found`,
  PROBLEM_WITH_SERVICE: `${BASE_URL}/problem-with-service`,
  CARD_PAYMENT: `${BASE_URL}/payment-ref`,
};

export { BASE_URL, URLS };
