const dateLastYear = (dayIncrement = 0) => {
  const date = new Date();
  date.setDate(date.getDate() + dayIncrement);
  date.setFullYear(date.getFullYear() - 1);
  return date;
};

const dateAddMonths = (monthIncrement = 0) => {
  const date = new Date();
  date.setDate(date.getDate());
  date.setMonth(date.getMonth() + monthIncrement);
  return date;
};

const currentDate = () => {
  const today = new Date();
  const day = today.getDate();
  const month = today.getMonth() + 1;
  const year = today.getFullYear();
  const iso = today.toISOString().split("T")[0];
  return {
    day,
    month,
    year,
    iso,
  };
};

export { currentDate, dateLastYear, dateAddMonths };
