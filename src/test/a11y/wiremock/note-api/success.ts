import { postJsonData } from "../../request";
import { WIREMOCK_NOTE_MAPPING_URL } from "../paths";
import { createGetNotesByIdSuccessMapping, SUCCESS } from "./mappings/mappings";

async function setupSuccessfulGetNotesMapping() {
  await postJsonData(
    WIREMOCK_NOTE_MAPPING_URL,
    createGetNotesByIdSuccessMapping(SUCCESS),
  );
}

export { setupSuccessfulGetNotesMapping };
