import { compose, ifElse, isNil, is, toUpper, head, tail } from "ramda";

const capitalizeFirstLetter = compose(
  ifElse(
    (str) => isNil(str) || is(Number, str),
    () => {
      throw new Error("Invalid string");
    },
    (str) => `${toUpper(head(str))}${tail(str)}`,
  ),
);

export { capitalizeFirstLetter };
