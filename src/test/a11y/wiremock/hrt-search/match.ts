import { postJsonData } from "../../request";
import { WIREMOCK_SEARCH_MAPPING_URL } from "../paths";
import {
  createExemptionsSearchMapping,
  NO_MATCH,
  ACTIVE_MATCH,
} from "./mappings/mappings";

async function setupSuccessfulNoSearchMatchMapping() {
  await postJsonData(
    WIREMOCK_SEARCH_MAPPING_URL,
    createExemptionsSearchMapping(NO_MATCH),
  );
}

async function setupSuccessfulActiveExemptionFoundSearchMatchMapping() {
  await postJsonData(
    WIREMOCK_SEARCH_MAPPING_URL,
    createExemptionsSearchMapping(ACTIVE_MATCH),
  );
}

export {
  setupSuccessfulNoSearchMatchMapping,
  setupSuccessfulActiveExemptionFoundSearchMatchMapping,
};
