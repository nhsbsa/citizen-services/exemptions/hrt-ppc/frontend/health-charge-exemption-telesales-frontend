import { pathOr } from "ramda";
import { REQUEST_ID_HEADER } from "../server/headers";
/*eslint-disable */
// printf module if imported does not support logging request meta data
const { format } = require('winston')
const { printf } = format
/*eslint-enable */

const REQUEST_ID_PATH = ["headers", REQUEST_ID_HEADER];
const LOCATOR_PATH = ["session", "locator"];

const pathOrEmpty = pathOr("");
const requestIdPath = pathOrEmpty(REQUEST_ID_PATH);
const locatorPath = pathOrEmpty(LOCATOR_PATH);

const logFormatter = ({ timestamp, level, message, req }) => {
  return `${timestamp} ${level.toUpperCase()} [${locatorPath(
    req,
  )}][${requestIdPath(req)}] ${message}`;
};

const formatLog = printf(logFormatter);

export { logFormatter, formatLog };
