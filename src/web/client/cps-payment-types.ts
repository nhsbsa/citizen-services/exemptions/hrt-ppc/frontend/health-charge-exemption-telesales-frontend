// eslint-disable-next-line @typescript-eslint/no-unused-vars
export type CreatePaymentRequest = {
  reference: string;
  transactionId: string;
  amount: string;
  serviceDescription: string;
};

export type CreatePaymentResponse = {
  pin: string;
};
