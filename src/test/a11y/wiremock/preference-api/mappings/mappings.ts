const SUCCESS = "success";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";
import v1FindByCitizenIdAndCertificateType from "./v1-find-by-citizen-id-and-certificate-type";

const RESPONSE_MAP = {
  [SUCCESS]: v1FindByCitizenIdAndCertificateType,
};

const createGetPreferenceByIdSuccessMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "GET",
      url: `/v1/user-preference/search/findByCitizenIdAndCertificateType?citizenId=c0a8000a-86da-1e45-8186-ee98c395000f&certificateType=HRT_PPC`,
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
        channel: {
          equalTo: Channel.STAFF,
        },
        "user-id": {
          equalTo: "LOCAL_USER_CYPHER",
        },
      },
    },
    response: RESPONSE_MAP[scenario],
  });

export { createGetPreferenceByIdSuccessMapping, SUCCESS };
