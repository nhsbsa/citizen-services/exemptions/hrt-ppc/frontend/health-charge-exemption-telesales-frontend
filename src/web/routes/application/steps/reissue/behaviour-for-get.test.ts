import moment from "moment";

const pageContent = "page content";
const rowsCertificateDetailsTable = "rows certificate";
const rowsPersonalDetailsTable = "rows citizen";
const mockPageContent = jest.fn().mockReturnValue(pageContent);
const mockGetRowsForCertificateDetailsTable = jest
  .fn()
  .mockReturnValue(rowsCertificateDetailsTable);
const mockGetRowsForPersonalDetailsTable = jest
  .fn()
  .mockReturnValue(rowsPersonalDetailsTable);

jest.mock("./page-content", () => ({
  pageContent: mockPageContent,
}));

jest.mock("../exemption-detail/get-detail-rows", () => ({
  getRowsForCertificateDetailsTable: mockGetRowsForCertificateDetailsTable,
  getRowsForPersonalDetailsTable: mockGetRowsForPersonalDetailsTable,
}));

import { behaviourForGet } from "./behaviour-for-get";
import {
  CertificateStatus,
  CertificateType,
} from "@nhsbsa/health-charge-exemption-common-frontend";

const certificate = {
  id: "1234",
  reference: "ref",
  type: CertificateType.HRT_PPC,
  status: CertificateStatus.PENDING,
  startDate: moment().add(1, "d").toDate(),
  endDate: moment().add(2, "d").toDate(),
  citizen: {
    firstName: "fName",
    lastName: "lName",
    dateOfBirth: moment().subtract(2, "d").toDate(),
  },
};
const citizen = {
  citizen: {
    emails: "test@email.com",
  },
};

describe("behaviourForGet()", () => {
  const req = {
    t: "",
    session: {
      certificate: certificate,
      citizen: citizen,
    },
  };
  let res;
  const next = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    res = { locals: {} };
  });

  test("should call pageContent, getRowsForCertificateDetailsTable, getRowsForPersonalDetailsTable with the correct arguments and set correct res.locals", () => {
    behaviourForGet()(req, res, next);

    expect(res).toEqual({
      locals: {
        certificateDetailsData: "rows certificate",
        personalDetailsData: "rows citizen",
      },
    });

    expect(next).toBeCalledTimes(1);
    expect(mockPageContent).toBeCalledTimes(1);
    expect(mockGetRowsForCertificateDetailsTable).toBeCalledTimes(1);
    expect(mockGetRowsForPersonalDetailsTable).toBeCalledTimes(1);
    expect(next).toBeCalledWith();
    expect(mockPageContent).toBeCalledWith({ translate: req.t, req });
    expect(mockGetRowsForCertificateDetailsTable).toBeCalledWith(
      req.session.certificate,
      pageContent,
    );
    expect(mockGetRowsForPersonalDetailsTable).toBeCalledWith(
      req.session.citizen,
      pageContent,
    );
  });
});
