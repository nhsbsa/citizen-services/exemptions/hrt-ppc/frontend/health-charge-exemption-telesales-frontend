export const ERROR = "error";
export const SUCCESS = "success";
import v1SuccessResponse from "./v1-success-response";
import v1ErrorResponse from "./v1-error-response";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";

const RESPONSE_MAP = {
  [ERROR]: v1ErrorResponse,
  [SUCCESS]: v1SuccessResponse,
};

export const createReissueMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "POST",
      url: "/v1/reissue",
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
        channel: {
          equalTo: Channel.STAFF,
        },
        "user-id": {
          equalTo: "LOCAL_USER_CYPHER",
        },
      },
    },
    response: RESPONSE_MAP[scenario],
  });
