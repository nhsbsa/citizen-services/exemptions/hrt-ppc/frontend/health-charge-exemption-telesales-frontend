import { behaviourForGet } from "./behaviour-for-get";

const basicRes = {
  locals: {},
};
const req = { session: {} };

import { copyCertificateSessionDataToLocals } from "./edit-certificate-details";
jest.mock("./edit-certificate-details", () => ({
  copyCertificateSessionDataToLocals: jest.fn((req, res) => {
    const certificateData = {
      startDate: "2023-04-25",
    };
    res.locals.certificateDetailsData = {};
    req.session.certificate = certificateData;
  }),
}));

const next = jest.fn();
describe("behaviourForGet()", () => {
  test("maps session data into locals", () => {
    const res = { ...basicRes };

    behaviourForGet()(req, res, next);

    expect(copyCertificateSessionDataToLocals).toHaveBeenCalledTimes(1);
    expect(copyCertificateSessionDataToLocals).toHaveBeenCalledWith(req, res);
    expect(res).toEqual({
      locals: {
        certificateDetailsData: {
          "certificateStartDate-day": "25",
          "certificateStartDate-month": "04",
          "certificateStartDate-year": 2023,
        },
      },
    });
    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });
});
