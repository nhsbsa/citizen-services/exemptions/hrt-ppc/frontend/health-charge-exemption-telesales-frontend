import { URLS } from "../paths";

const telesalesSearchNoResults = [
  {
    url: URLS.SEARCH_HRT_RECORDS,
    formData: () => {
      return {
        "dateOfBirth-day": "30",
        "dateOfBirth-month": "05",
        "dateOfBirth-year": "1920",
        dateOfBirth: "1920-05-30",
        lastName: "doe",
        postcode: "AA1 1AA",
      };
    },
  },
  {
    url: URLS.NO_RESULTS,
    formData: () => ({}),
  },
];

const telesalesSearchFoundResults = [
  {
    url: URLS.SEARCH_HRT_RECORDS,
    formData: () => {
      return {
        "dateOfBirth-day": "30",
        "dateOfBirth-month": "05",
        "dateOfBirth-year": "1920",
        dateOfBirth: "1920-05-30",
        lastName: "doe",
        postcode: "AA1 1AA",
      };
    },
  },
  {
    url: URLS.SEARCH_RESULTS,
    formData: undefined,
  },
  {
    url: URLS.EXEMPTION_DETAIL,
    formData: undefined,
  },
  {
    url: URLS.EDIT_PERSONAL_DETAILS,
    formData: undefined,
  },
  {
    url: URLS.EDIT_CERTIFICATE_DETAILS,
    formData: undefined,
  },
  {
    url: URLS.ADD_NOTE,
    formData: () => {
      return {
        description: "This describes note",
      };
    },
  },
];

export { telesalesSearchNoResults, telesalesSearchFoundResults };
