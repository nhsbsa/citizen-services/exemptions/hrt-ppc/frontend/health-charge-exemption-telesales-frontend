export function getTypeTag(type) {
  return `<strong class="nhsuk-tag nhsuk-tag--green">${type}</strong>`;
}
