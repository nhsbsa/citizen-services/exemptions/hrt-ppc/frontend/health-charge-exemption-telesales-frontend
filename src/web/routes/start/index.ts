import { getPageMetadata } from "./get-page-meta-data";

export default {
  ...require("./start"),
  ...require("./pages"),
  getPageMetadata,
};
