import { updateCitizen, createRequestBody } from "./update-citizen";
import { citizenClient } from "../../../../client/citizen-client";

const mockMakeRequestCitizenClient = jest.fn();

const response = "dummy response";

beforeEach(() => {
  jest.clearAllMocks();
  citizenClient.prototype.makeRequest = mockMakeRequestCitizenClient;
  mockMakeRequestCitizenClient.mockResolvedValue(response);
});

describe("updateCitizen()", () => {
  it("should call the citizen patch and return the response", async () => {
    const citizenId = "123";
    const mockReq = {
      session: { example: "test" },
      body: {
        firstName: "fName",
        lastName: "lName",
        dateOfBirth: "1990-01-01",
        nhsNumber: "123456",
        addressLine1: "add1",
        addressLine2: "add2",
        townOrCity: "add3",
        sanitizedPostcode: "NE15 8NY",
        emailAddress: "test@live.com",
      },
    };

    const result = await updateCitizen(mockReq, citizenId);
    expect(result).toEqual(response);

    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(1);
    expect(mockMakeRequestCitizenClient).toHaveBeenCalledWith(
      {
        data: {
          addresses: [
            {
              addressLine1: "add1",
              addressLine2: "add2",
              postcode: "NE15 8NY",
              townOrCity: "add3",
            },
          ],
          emails: [
            {
              emailAddress: "test@live.com",
            },
          ],
          dateOfBirth: "1990-01-01",
          firstName: "fName",
          lastName: "lName",
          nhsNumber: "123456",
        },
        method: "PATCH",
        responseType: "json",
        url: "/v1/citizens/123",
      },
      mockReq.session,
    );
  });
});

describe("createRequestBody()", () => {
  it("should create request when all fields provided", async () => {
    const mockReq = {
      body: {
        firstName: "fName",
        lastName: "lName",
        dateOfBirth: "1990-01-01",
        nhsNumber: "123456",
        addressLine1: "add1",
        addressLine2: "add2",
        townOrCity: "add3",
        sanitizedPostcode: "NE15 8NY",
        emailAddress: "test@live.com",
      },
    };

    const result = await createRequestBody(mockReq);
    expect(result).toEqual({
      addresses: [
        {
          addressLine1: "add1",
          addressLine2: "add2",
          postcode: "NE15 8NY",
          townOrCity: "add3",
        },
      ],
      emails: [
        {
          emailAddress: "test@live.com",
        },
      ],
      dateOfBirth: "1990-01-01",
      firstName: "fName",
      lastName: "lName",
      nhsNumber: "123456",
    });

    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
  });

  it.each([[], undefined, ""])(
    "should create request when nhsNumber is %p",
    async (nhsNumber: any) => {
      const mockReq = {
        body: {
          firstName: "fName",
          lastName: "lName",
          dateOfBirth: "1990-01-01",
          nhsNumber: nhsNumber,
          addressLine1: "add1",
          addressLine2: "add2",
          townOrCity: "add3",
          sanitizedPostcode: "NE15 8NY",
        },
      };

      const result = await createRequestBody(mockReq);
      expect(result).toEqual({
        addresses: [
          {
            addressLine1: "add1",
            addressLine2: "add2",
            postcode: "NE15 8NY",
            townOrCity: "add3",
          },
        ],
        dateOfBirth: "1990-01-01",
        firstName: "fName",
        lastName: "lName",
        nhsNumber: null,
      });

      expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
    },
  );

  it.each([[], undefined, ""])(
    "should create request when addressLine2 is %p",
    async (addressLine2: any) => {
      const mockReq = {
        body: {
          firstName: "fName",
          lastName: "lName",
          dateOfBirth: "1990-01-01",
          nhsNumber: "123456",
          addressLine1: "add1",
          addressLine2: addressLine2,
          townOrCity: "add3",
          sanitizedPostcode: "NE15 8NY",
        },
      };

      const result = await createRequestBody(mockReq);
      expect(result).toEqual({
        addresses: [
          {
            addressLine1: "add1",
            addressLine2: null,
            postcode: "NE15 8NY",
            townOrCity: "add3",
          },
        ],
        dateOfBirth: "1990-01-01",
        firstName: "fName",
        lastName: "lName",
        nhsNumber: "123456",
      });

      expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
    },
  );

  it.each([[], undefined, ""])(
    "should create request when emailAddress is %p",
    async (emailAddress: any) => {
      const mockReq = {
        body: {
          firstName: "fName",
          lastName: "lName",
          dateOfBirth: "1990-01-01",
          addressLine1: "add1",
          townOrCity: "add3",
          sanitizedPostcode: "NE15 8NY",
          emailAddress: emailAddress,
        },
      };

      const result = await createRequestBody(mockReq);
      expect(result).toEqual({
        addresses: [
          {
            addressLine1: "add1",
            addressLine2: null,
            postcode: "NE15 8NY",
            townOrCity: "add3",
          },
        ],
        dateOfBirth: "1990-01-01",
        firstName: "fName",
        lastName: "lName",
        nhsNumber: null,
      });

      expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
    },
  );

  it("should create request when optional fields not provided", async () => {
    const mockReq = {
      body: {
        firstName: "fName",
        lastName: "lName",
        dateOfBirth: "1990-01-01",
        addressLine1: "add1",
        townOrCity: "add3",
        sanitizedPostcode: "NE15 8NY",
      },
    };

    const result = await createRequestBody(mockReq);
    expect(result).toEqual({
      addresses: [
        {
          addressLine1: "add1",
          addressLine2: null,
          postcode: "NE15 8NY",
          townOrCity: "add3",
        },
      ],
      dateOfBirth: "1990-01-01",
      firstName: "fName",
      lastName: "lName",
      nhsNumber: null,
    });

    expect(mockMakeRequestCitizenClient).toHaveBeenCalledTimes(0);
  });
});
