import { getErrorPageContent } from "../error-pages/error-page-content";
import { getCookiesEnablerPage } from "./cookies-enabler";

test(`getCookiesEnablerPage() should render the correct page content`, () => {
  const req = { t: (string) => string };
  const res = { render: jest.fn() };

  getCookiesEnablerPage(req, res);

  expect(res.render).toBeCalledWith(
    "cookies-enabler",
    getErrorPageContent(req),
  );
});
