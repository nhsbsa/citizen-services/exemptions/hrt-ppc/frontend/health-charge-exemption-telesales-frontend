const mockMakeRequest = jest.fn();

import expect from "expect";
import { behaviourForPost } from "./behaviour-for-post";
import { productClient } from "../../../../client/product-client";
jest.mock("../../../../client/product-client");
import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { Request, Response } from "express";

const didValidationPass = jest.fn();
jest.mock("express-validator", () => ({
  validationResult: () => ({ isEmpty: didValidationPass }),
}));

const config = {
  environment: {
    OUTBOUND_API_TIMEOUT: 20000,
    PRODUCT_API_URI: "baseURI",
    PRODUCT_API_KEY: "test-key",
  },
};

const req = {
  path: "/test",
  session: {
    newApplicant: {
      certificateStartDate: "2020-05-05",
      firstName: "fname",
      lastName: "lname",
    },
  },
  body: {
    firstName: "changedFname",
    lastName: "changedLname",
  },
} as unknown as Request;
const next = jest.fn();
const res = {} as unknown as Response;

const expectedError = wrapError({
  cause: new Error("Axios error"),
  message: `Error posting ${req.path}`,
  statusCode: httpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
});

const expectedMockRequestCall = {
  method: "GET",
  url: `v1/products/HRT_12m/end-date?startDate=${req.session["newApplicant"].certificateStartDate}`,
};

const expectedMockResponse = {
  endDate: "2024-07-29",
};

beforeEach(() => {
  jest.clearAllMocks();
  productClient.prototype.makeRequest = mockMakeRequest;
  req.session["newApplicant"].certificateEndDate = undefined;
});

describe("behaviourForPost()", () => {
  test("should not set any values in session and call product api when there are validation errors", async () => {
    didValidationPass.mockReturnValue(false);

    await behaviourForPost({})(req, res, next);

    expect(req).toEqual({
      body: {
        firstName: "changedFname",
        lastName: "changedLname",
      },
      session: {
        newApplicant: {
          firstName: "fname",
          lastName: "lname",
          certificateStartDate: "2020-05-05",
        },
      },
      path: "/test",
    });
    expect(productClient).not.toHaveBeenCalled();
    expect(next).toBeCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });

  test("calls products api and sets endDate in session", async () => {
    didValidationPass.mockReturnValue(true);
    mockMakeRequest.mockResolvedValue({ data: expectedMockResponse });

    await behaviourForPost(config)(req, res, next);

    expect(req).toEqual({
      body: {
        firstName: "changedFname",
        lastName: "changedLname",
      },
      session: {
        newApplicant: {
          firstName: "changedFname",
          lastName: "changedLname",
          certificateEndDate: "2024-07-29",
          certificateStartDate: "2020-05-05",
        },
      },
      path: "/test",
    });

    expect(productClient).toBeCalledTimes(1);
    expect(productClient).toBeCalledWith(config);
    expect(productClient.prototype.makeRequest).toBeCalledTimes(1);
    expect(productClient.prototype.makeRequest).toBeCalledWith(
      expectedMockRequestCall,
    );
    expect(req.session["newApplicant"].certificateEndDate).toEqual(
      "2024-07-29",
    );
    expect(next).toBeCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });

  test("calls next with wrapped error when behaviourForPost throws error", async () => {
    didValidationPass.mockReturnValue(true);
    mockMakeRequest.mockRejectedValue(new Error("Axios error"));

    await behaviourForPost(config)(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(req.session["newApplicant"].certificateEndDate).toBeUndefined();
  });
});
