import expect from "expect";
import { isPathAllowed } from "./predicates";

const paths = ["/first", "/second", "/third", "/fourth"];

test("isPathAllowed() should return true if path is before allowed in sequence", () => {
  const result = isPathAllowed(paths, "/third", "/second");
  expect(result).toBe(true);
});

test("isPathAllowed() should return true if path matches allowed", () => {
  const result = isPathAllowed(paths, "/third", "/third");
  expect(result).toBe(true);
});

test("isPathAllowed() should return false if path is after allowed in sequence", () => {
  const result = isPathAllowed(paths, "/third", "/fourth");
  expect(result).toBe(false);
});
