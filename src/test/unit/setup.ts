process.env.CONTEXT_PATH = "/test-context";
process.env.APP_VERSION = "example-version";
process.env.SEARCH_PAST_EXEMPTIONS_PERIOD = "1";
process.env.SEARCH_RESULTS_PAGE_SIZE = "2";
process.env.USE_AUTHENTICATION = "false";
