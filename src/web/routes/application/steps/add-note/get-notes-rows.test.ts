const rowsNotesTable = "rows notes";
const mockGetRowsForNotesTable = jest.fn().mockReturnValue(rowsNotesTable);
const notesHeading = [
  { text: "02/02/1990" },
  { text: "19:38" },
  { text: "AGENT" },
  { text: "note desc" },
];

jest.mock("../exemption-detail/get-detail-rows", () => ({
  getRowsForNotesTable: mockGetRowsForNotesTable,
  getNotesHeadings: jest.fn().mockReturnValue(notesHeading),
}));

const pageContentMocked = "testPageContent";
const mockPageContent = jest.fn().mockReturnValue(pageContentMocked);
jest.mock("./add-note", () => ({
  pageContent: mockPageContent,
}));

import { getNotesRows } from "./get-notes-rows";

const req = {
  t: jest.fn(),
  path: "/page",
  session: {
    certificate: "mock certificate data",
    notes: "mock notes data",
  },
  params: {
    id: "123",
  },
};
let res;

beforeEach(() => {
  jest.clearAllMocks();
  res = { locals: {} };
});

test("should call pageContent and set res.locals.notesData", async () => {
  await getNotesRows(req, res);

  expect(res).toEqual({
    locals: {
      notesData: {
        headings: [
          { text: "02/02/1990" },
          { text: "19:38" },
          { text: "AGENT" },
          { text: "note desc" },
        ],
        rows: "rows notes",
      },
    },
  });

  expect(mockPageContent).toHaveBeenCalled();
  expect(mockGetRowsForNotesTable).toBeCalledTimes(1);
  expect(mockPageContent).toBeCalledWith({ translate: req.t, req });
  expect(mockGetRowsForNotesTable).toBeCalledWith(
    req.session.notes,
    pageContentMocked,
  );
});
