import { APPLICATION_COMPLETE_URL } from "../../../paths/paths";
import { logger } from "../../../../../../web/logger/logger";
import { stateMachine } from "../../state-machine";
import {
  stepNotNavigable,
  completedJourneyExistsInSession,
} from "./predicates";

import {
  IS_PATH_ALLOWED,
  GET_NEXT_ALLOWED_PATH,
} from "../../state-machine/actions";
import { sessionDestroyButKeepLoggedIn } from "../../../steps/common/session-destroy";

const handleRequestForPath = (journey, step?) => (req, res, next) => {
  const { pathsInSequence } = journey;
  const firstPathInSequence = pathsInSequence[0];

  logger.info(`[${req.method}] ${req.path}`, req);

  // Destroy the session if any COMPLETED journeys exists in session, and path is not a DECISION_URL path
  if (
    completedJourneyExistsInSession(req) &&
    !req.path.endsWith(APPLICATION_COMPLETE_URL)
  ) {
    logger.info("Sanatizing session as state is COMPLETED", req);
    sessionDestroyButKeepLoggedIn(req);
    res.clearCookie("lang");

    return res.redirect(firstPathInSequence);
  }

  const nextAllowedPath = stateMachine.dispatch(
    GET_NEXT_ALLOWED_PATH,
    req,
    journey,
  );
  const isPathAllowed = stateMachine.dispatch(IS_PATH_ALLOWED, req, journey);

  // Redirect to nextAllowedPath on invalid path request
  if (!isPathAllowed) {
    logger.info(`Path is not allowed, redirecting to ${nextAllowedPath}`, req);
    return res.redirect(nextAllowedPath);
  }

  // If step is not navigable then return to the next allowed path
  if (stepNotNavigable(step, req)) {
    logger.info(
      `Step is not navigable, redirecting to ${nextAllowedPath}`,
      req,
    );
    return res.redirect(nextAllowedPath);
  }

  next();
};

export { handleRequestForPath };
