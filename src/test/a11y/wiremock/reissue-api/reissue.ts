import { postJsonData } from "../../request";
import { WIREMOCK_REISSUE_MAPPING_URL } from "../paths";
import { createReissueMapping, ERROR, SUCCESS } from "./mappings/mappings";

async function setupSuccessfulReissueMapping() {
  await postJsonData(
    WIREMOCK_REISSUE_MAPPING_URL,
    createReissueMapping(SUCCESS),
  );
}

async function setupErrorReissueMapping() {
  await postJsonData(WIREMOCK_REISSUE_MAPPING_URL, createReissueMapping(ERROR));
}

export { setupSuccessfulReissueMapping, setupErrorReissueMapping };
