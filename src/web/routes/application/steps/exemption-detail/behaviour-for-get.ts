import * as httpStatus from "http-status-codes";
import { wrapError } from "../../errors";
import { findRecordInSearchResults } from "../common/search-utils";
import { makeRequests } from "./make-calls";
import { pageContent } from "./page-content";
import {
  getRowsForCertificateDetailsTable,
  getRowsForPersonalDetailsTable,
  getRowsForNotesTable,
  getNotesHeadings,
  getAuditHistoryHeadings,
  getRowsForAuditHistoryTable,
} from "./get-detail-rows";
import {
  isActiveAndHasNotExpired,
  isEligibleForReissue,
} from "../common/certificate-utils";

export const behaviourForGet = () => async (req, res, next) => {
  const localisation = pageContent({ translate: req.t, req });
  try {
    const searchResult = findRecordInSearchResults(req);

    const results = await makeRequests(req, searchResult);
    // Set in session for later retrieval on 'edit' pages
    req.session.certificate = results.data.certificate;
    req.session.citizen = results.data.citizen;
    req.session.notes = results.data.notes;
    req.session.certificate.preference = {
      id: results.data.preference.id,
      event: results.data.preference.event,
      preference: results.data.preference.preference,
    };

    // data transformation
    const certificateDetailsRowList = getRowsForCertificateDetailsTable(
      results.data.certificate,
      localisation,
    );
    const personalDetailsRowList = getRowsForPersonalDetailsTable(
      results.data.citizen,
      localisation,
    );
    const notesRowList = getRowsForNotesTable(results.data.notes, localisation);
    const notesTableHeadings = getNotesHeadings(localisation);
    const auditHistoryRowList = await getRowsForAuditHistoryTable(
      results.data.audit,
      localisation,
    );
    const auditHistoryTableHeadings = getAuditHistoryHeadings(localisation);

    res.locals.certificateDetailsData = certificateDetailsRowList;
    res.locals.personalDetailsData = personalDetailsRowList;
    res.locals.auditHistoryData = {
      headings: auditHistoryTableHeadings,
      rows: auditHistoryRowList,
    };
    res.locals.notesData = {
      headings: notesTableHeadings,
      rows: notesRowList,
    };
    const certificate = results.data.certificate;
    res.locals.showReissue = isEligibleForReissue(certificate);
    res.locals.showEditPersonalDetails = isActiveAndHasNotExpired(certificate);
    res.locals.reference = results.data.certificate.reference;
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error on load ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
    return;
  }
  next();
};
