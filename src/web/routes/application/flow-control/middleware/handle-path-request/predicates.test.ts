import expect from "expect";
import * as sinon from "sinon";
import {
  stepNotNavigable,
  completedJourneyExistsInSession,
} from "./predicates";
import * as testUtils from "../../test-utils/test-utils";
import { IN_PROGRESS, IN_REVIEW, COMPLETED } from "../../states";

const { buildSessionForJourney } = testUtils;

test("stepNotNavigable() returns false if isNavigable prop does not exist", () => {
  const step = {
    path: "/step",
  };

  const result = stepNotNavigable(step, undefined);
  expect(result).toBe(false);
});

test("stepNotNavigable() returns false if isNavigable prop is not a function", () => {
  const isNavigablePropValues = ["string", 22, true, false];

  isNavigablePropValues.forEach((value) => {
    const step = {
      path: "/step",
      isNavigable: value,
    };

    const result = stepNotNavigable(step, undefined);
    expect(result).toBe(false);
  });
});

test("stepNotNavigable() returns true if isNavigable prop returns false", () => {
  const session = {};
  const req = { session };
  const isNavigable = sinon.stub().returns(false);

  const step = {
    path: "/step",
    isNavigable,
  };

  const result = stepNotNavigable(step, req);
  expect(isNavigable.calledWith(req)).toBe(true);
  expect(result).toBe(true);
});

test("stepNotNavigable() returns false if isNavigable prop returns true", () => {
  const session = {};
  const req = { session };
  const isNavigable = sinon.stub().returns(true);

  const step = {
    path: "/step",
    isNavigable,
  };

  const result = stepNotNavigable(step, req);
  expect(isNavigable.calledWith(req)).toBe(true);
  expect(result).toBe(false);
});

test("completedJourneyExistsInSession() returns true if completed journey exists in session", () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: "telesales",
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
      ...buildSessionForJourney({
        journeyName: "report-a-change",
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const result = completedJourneyExistsInSession(req);
  expect(result).toBe(true);
});

test("completedJourneyExistsInSession() returns false if completed journey does not exist in session", () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: "telesales",
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
      ...buildSessionForJourney({
        journeyName: "report-a-change",
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
  };

  const result = completedJourneyExistsInSession(req);
  expect(result).toBe(false);
});

test("completedJourneyExistsInSession() returns false if no journeys exist in session", () => {
  const req = {
    session: {},
  };

  const result = completedJourneyExistsInSession(req);
  expect(result).toBe(false);
});
