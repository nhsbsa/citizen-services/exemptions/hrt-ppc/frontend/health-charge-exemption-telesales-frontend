const SUCCESS = "success";
import { Channel } from "@nhsbsa/health-charge-exemption-common-frontend";
import v1FindByCertificateId from "./v1-find-certificate-by-id";

const RESPONSE_MAP = {
  [SUCCESS]: v1FindByCertificateId,
};

const createGetCertificateByIdSuccessMapping = (scenario = SUCCESS) =>
  JSON.stringify({
    request: {
      method: "GET",
      url: `/v1/certificates/c0a8000a-86da-1e6a-8186-ee9a40dd000f`,
      headers: {
        "x-api-key": {
          matches: "([A-Za-z0-9_-])+",
        },
        channel: {
          equalTo: Channel.STAFF,
        },
        "user-id": {
          equalTo: "LOCAL_USER_CYPHER",
        },
      },
    },
    response: RESPONSE_MAP[scenario],
  });

export { createGetCertificateByIdSuccessMapping, SUCCESS };
