import expect from "expect";
import { partial } from "ramda";
import {
  IN_PROGRESS,
  IN_REVIEW,
  COMPLETED,
  COMPLETED_SEARCH,
  IN_PROGRESS_COOKIES,
  COMPLETED_COOKIES,
} from "../states";
import {
  GET_NEXT_PATH,
  INVALIDATE_REVIEW,
  SET_NEXT_ALLOWED_PATH,
  INCREMENT_NEXT_ALLOWED_PATH,
} from "./actions";
import {
  CHECK_ANSWERS_URL,
  DECISION_URL,
  APPLICATION_COMPLETE_URL,
} from "../../paths/paths";
import {
  setCurrentPathInSession,
  getCurrentPathFromSession,
} from "../session-accessors";
import {
  buildSessionForJourney,
  getStateForJourney,
  getNextAllowedPathForJourney,
} from "../test-utils/test-utils";

const info = jest.fn();
const logger = { info };

jest.mock("../../../../logger/logger", () => ({
  logger,
}));

import { stateMachine } from "./state-machine";

jest.mock("../session-accessors", () => ({
  ...jest.requireActual("../session-accessors"),
  getCurrentPathFromSession: jest.fn(() => {
    /*do nothing*/
  }),
  setCurrentPathInSession: jest.fn(),
}));

const TELESALES = "telesales";

const TELESALES_JOURNEY = {
  name: TELESALES,
  steps: [
    { path: "/first", next: () => "/second" },
    { path: "/second", next: () => "/third" },
    { path: "/third" },
  ],
  pathsInSequence: ["/first", "/second", "/third"],
};

const getStateForTelesalesJourney = partial(getStateForJourney, [TELESALES]);

const getNextAllowedPathForTelesalesJourney = partial(
  getNextAllowedPathForJourney,
  [TELESALES],
);

test(`Dispatching ${GET_NEXT_PATH} should return next property of associated step when state of ${IN_PROGRESS} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, TELESALES_JOURNEY)).toBe(
    "/second",
  );
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test(`Dispatching ${GET_NEXT_PATH} should return next property of associated step when state of ${IN_PROGRESS_COOKIES} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS_COOKIES,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, TELESALES_JOURNEY)).toBe(
    "/second",
  );
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test(`Dispatching ${GET_NEXT_PATH} should return next navigable path when state of ${IN_PROGRESS} defined in session and next step is not navigable`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/second",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, TELESALES_JOURNEY)).toBe(
    "/third",
  );
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test(`Dispatching ${GET_NEXT_PATH} should return next navigable path when state of ${IN_PROGRESS_COOKIES} defined in session and next step is not navigable`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS_COOKIES,
        nextAllowedPath: undefined,
      }),
    },
    path: "/second",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, TELESALES_JOURNEY)).toBe(
    "/third",
  );
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test(`Dispatching ${GET_NEXT_PATH} should return ${CHECK_ANSWERS_URL} path when state of ${IN_REVIEW} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, TELESALES_JOURNEY)).toBe(
    CHECK_ANSWERS_URL,
  );
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test(`Dispatching ${GET_NEXT_PATH} should return ${DECISION_URL} path when state of ${COMPLETED} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, TELESALES_JOURNEY)).toBe(
    DECISION_URL,
  );
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test(`Dispatching ${GET_NEXT_PATH} should return next navigable path path when state of ${COMPLETED_SEARCH} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: COMPLETED_SEARCH,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, TELESALES_JOURNEY)).toBe(
    "/second",
  );
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test(`Dispatching ${GET_NEXT_PATH} should return ${APPLICATION_COMPLETE_URL} path when state of ${COMPLETED_COOKIES} defined in session`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: COMPLETED_COOKIES,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch(GET_NEXT_PATH, req, TELESALES_JOURNEY)).toBe(
    APPLICATION_COMPLETE_URL,
  );
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test(`Dispatching an invalid action should return null when state is ${IN_PROGRESS}`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch("INVALID_ACTION", req, TELESALES_JOURNEY)).toBe(
    null,
  );
  expect(setCurrentPathInSession).not.toBeCalled();
});

test(`Dispatching an invalid action should return null when state is ${IN_PROGRESS_COOKIES}`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS_COOKIES,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  expect(stateMachine.dispatch("INVALID_ACTION", req, TELESALES_JOURNEY)).toBe(
    null,
  );
  expect(setCurrentPathInSession).not.toBeCalled();
});

test(`Dispatching ${INVALIDATE_REVIEW} should set state to ${IN_PROGRESS} when state is ${IN_REVIEW}`, () => {
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  stateMachine.dispatch(INVALIDATE_REVIEW, req, TELESALES_JOURNEY);
  expect(getStateForTelesalesJourney(req)).toBe(IN_PROGRESS);
  expect(setCurrentPathInSession).toBeCalledWith(
    req,
    TELESALES_JOURNEY,
    req.path,
  );
});

test("setState does not log a change in state if the new state is the same as the current state", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  stateMachine.setState(IN_PROGRESS, req, TELESALES_JOURNEY);

  expect(info).not.toBeCalled();
});

test("setState sets the state if the new state is different from the current state", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    headers: {
      REQUEST_ID_HEADER: "123456",
    },
  };

  stateMachine.setState(COMPLETED, req, TELESALES_JOURNEY);
  expect(getStateForTelesalesJourney(req)).toBe(COMPLETED);
  expect(info).toBeCalledTimes(1);
});

test(`Dispatching ${SET_NEXT_ALLOWED_PATH} sets next allowed path in session`, () => {
  const appStates = [
    { state: IN_PROGRESS, path: "/name" },
    { state: IN_REVIEW, path: "/check" },
    { state: COMPLETED, path: "/success" },
    { state: COMPLETED_SEARCH, path: "/new-applicant-details" },
  ];

  appStates.forEach((appState) => {
    const { state, path } = appState;
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: TELESALES,
          state,
          nextAllowedPath: undefined,
        }),
      },
    };

    stateMachine.dispatch(SET_NEXT_ALLOWED_PATH, req, TELESALES_JOURNEY, path);
    expect(getNextAllowedPathForTelesalesJourney(req)).toBe(path);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      TELESALES_JOURNEY,
      undefined,
    );
  });
});

test(`Dispatching ${INCREMENT_NEXT_ALLOWED_PATH} updates next allowed path in session with the next allowed path in sequence`, () => {
  const appStates = [
    { state: IN_PROGRESS, path: "/first", expectedNextPath: "/second" },
    {
      state: IN_REVIEW,
      path: "/second",
      expectedNextPath: CHECK_ANSWERS_URL,
    },
    { state: COMPLETED, path: DECISION_URL, expectedNextPath: DECISION_URL },
    {
      state: COMPLETED_SEARCH,
      path: "/second",
      expectedNextPath: "/third",
    },
  ];

  appStates.forEach((appState) => {
    const { state, path, expectedNextPath } = appState;
    const req = {
      path,
      session: {
        ...buildSessionForJourney({
          journeyName: "telesales",
          state,
          nextAllowedPath: undefined,
        }),
      },
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, TELESALES_JOURNEY);
    expect(getNextAllowedPathForTelesalesJourney(req)).toBe(expectedNextPath);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      TELESALES_JOURNEY,
      req.path,
    );
  });
});

test("getCurrentPath calls the function getCurrentPathFromSession", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  stateMachine.getCurrentPath(req, TELESALES_JOURNEY);

  expect(getCurrentPathFromSession).toBeCalledTimes(1);
});
