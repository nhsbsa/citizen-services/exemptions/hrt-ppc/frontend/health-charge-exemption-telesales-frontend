import * as httpStatus from "http-status-codes";
import { wrapError } from "../../../errors";
import { makeReissuePostRequest } from "./reissue-lambda-provider";

export const reissue = async (req, res, next) => {
  try {
    const response = await makeReissuePostRequest(req.session);
    return response;
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      }),
    );
  }
};
