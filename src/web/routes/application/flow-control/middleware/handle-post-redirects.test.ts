import expect from "expect";
import * as sinon from "sinon";
import { handlePostRedirects } from "./handle-post-redirects";
import * as states from "../states";
import { buildSessionForJourney } from "../test-utils/test-utils";

const { IN_PROGRESS } = states;

const TELESALES = "telesales";

const journey = {
  name: TELESALES,
  steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
};

test("handlePostRedirects() should redirect when no response errors", async () => {
  const redirect = sinon.spy();
  const next = sinon.spy();
  const req = {
    method: "POST",
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  const res = {
    redirect,
    locals: {},
  };

  handlePostRedirects(journey)(req, res, next);

  expect(redirect.called).toBe(true);
  expect(next.called).toBe(false);
});

test("handlePostRedirects() should call next() when response errors are present", async () => {
  const redirect = sinon.spy();
  const next = sinon.spy();

  const req = {
    method: "POST",
    csrfToken: () => "myCsrfToken",
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    path: "/first",
  };

  const res = {
    redirect,
    locals: {
      errors: true,
    },
  };

  handlePostRedirects(journey)(req, res, next);

  expect(redirect.called).toBe(false);
  expect(next.called).toBe(true);
});
