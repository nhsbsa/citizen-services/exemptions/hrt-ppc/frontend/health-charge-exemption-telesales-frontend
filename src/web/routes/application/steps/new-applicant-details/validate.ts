import { check } from "express-validator";
import { parsePhoneNumberFromString } from "libphonenumber-js";
import {
  FIRSTNAME_MAX_LENGTH,
  LASTNAME_MAX_LENGTH,
  NAME_PATTERN,
  NOTE_DESCRIPTION_MAX_LENGTH,
} from "../../constants";
import { translateValidationMessage } from "../common/translate-validation-message";
import validateNhsNumber from "nhs-numbers/lib/validateNhsNumber";
import {
  CERT_START_DATE_DAY,
  CERT_START_DATE_MONTH,
  CERT_START_DATE_YEAR,
  DATE_OF_BIRTH_DAY_KEY,
  DATE_OF_BIRTH_MONTH_KEY,
  DATE_OF_BIRTH_YEAR_KEY,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/constant-field-keys";
import {
  addDateToBody as addDateOfBirthDateToBody,
  entirePermittedStartDateRange,
  validateAddress,
  validateCertificateStartDate as validateCertificateStartDateCommon,
  validateCertificateStartDateDay,
  validateCertificateStartDateMonth,
  validateCertificateStartDateYear,
  validateDateOfBirth,
  validateDateOfBirthAgeCap,
  validateDateOfBirthIsEmpty,
  validateDateOfBirthDay,
  validateDateOfBirthMonth,
  validateDateOfBirthYear,
  toDateString,
} from "@nhsbsa/health-charge-exemption-common-frontend";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";

const validatePhoneNumber = (phoneNumber, { req }) => {
  const parsedPhoneNumber = parsePhoneNumberFromString(phoneNumber, "GB");
  if (parsedPhoneNumber && parsedPhoneNumber.isValid()) {
    return true;
  }
  throw new Error(req.t("validation:invalidPhoneNumber"));
};

const addStartDateToBody = (req, res, next) => {
  req.body.certificateStartDate = toDateString(
    req.body[CERT_START_DATE_DAY],
    req.body[CERT_START_DATE_MONTH],
    req.body[CERT_START_DATE_YEAR],
  );
  next();
};

const validateCertificateStartDate = (certificateStartDate, { req }) => {
  const permittedStartDateRange = entirePermittedStartDateRange(new Date());
  return validateCertificateStartDateCommon(
    certificateStartDate,
    permittedStartDateRange,
    { req },
  );
};

const validate = (config) => [
  validateAddress(),

  check("firstName")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingFirstName"))
    .isLength({ max: FIRSTNAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:firstNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternFirstName")),

  check("lastName")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingLastName"))
    .isLength({ max: LASTNAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:lastNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternLastName")),

  check("telephoneNumber")
    .optional({ checkFalsy: true })
    .custom(validatePhoneNumber),

  addStartDateToBody,
  check("certificateStartDate").custom(validateCertificateStartDate),
  check(CERT_START_DATE_DAY).custom(validateCertificateStartDateDay),
  check(CERT_START_DATE_MONTH).custom(validateCertificateStartDateMonth),
  check(CERT_START_DATE_YEAR).custom(validateCertificateStartDateYear),

  check("description")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingDescription"))
    .isLength({ max: NOTE_DESCRIPTION_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:descriptionTooLong")),

  addDateOfBirthDateToBody,
  check("dateOfBirth")
    .custom(validateDateOfBirth)
    .bail()
    .custom(validateDateOfBirthIsEmpty)
    .bail()
    .custom(validateDateOfBirthAgeCap),
  check(DATE_OF_BIRTH_DAY_KEY).custom(validateDateOfBirthDay),
  check(DATE_OF_BIRTH_MONTH_KEY).custom(validateDateOfBirthMonth),
  check(DATE_OF_BIRTH_YEAR_KEY).custom(validateDateOfBirthYear),

  check("nhsNumber")
    .optional({ checkFalsy: true })
    .custom((value, { req }) => {
      if (!validateNhsNumber(value)) {
        throw new Error(req.t("validation:invalidNhsNumber"));
      }
      if (value.length > 10) {
        throw new Error(req.t("validation:invalidNhsNumber"));
      }
      return true;
    }),

  check("certificateFulfillment")
    .if(() => config.features.FULFILLMENT_POST_ENABLED === false)
    .isIn([Preference.EMAIL])
    .withMessage(
      translateValidationMessage("validation:missingCertificateFulfillment"),
    ),

  check("certificateFulfillment")
    .if(() => config.features.FULFILLMENT_POST_ENABLED === true)
    .isIn([Preference.EMAIL, Preference.POSTAL])
    .withMessage(
      translateValidationMessage("validation:missingCertificateFulfillment"),
    ),

  check("emailAddress")
    .if(
      (value, { req }) =>
        req.body["certificateFulfillment"] === Preference.EMAIL,
    )
    .notEmpty()
    .withMessage(translateValidationMessage("validation:missingEmailAddress"))
    .bail()
    .isEmail()
    .withMessage(translateValidationMessage("validation:invalidEmailAddress")),

  check("emailAddress")
    .if(
      (value, { req }) =>
        req.body["certificateFulfillment"] === Preference.POSTAL,
    )
    .optional({ checkFalsy: true })
    .isEmail()
    .withMessage(translateValidationMessage("validation:invalidEmailAddress")),
];

export {
  validate,
  validatePhoneNumber,
  addStartDateToBody,
  validateCertificateStartDate,
};
