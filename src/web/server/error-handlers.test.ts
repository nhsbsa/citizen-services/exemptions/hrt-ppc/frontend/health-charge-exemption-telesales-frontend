import expect from "expect";
import * as sinon from "sinon";
import * as handler from "./error-handlers";

test("Error handler should set the status to 500", () => {
  const status = sinon.spy();
  const redirect = sinon.spy();
  const req = {
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
  };

  const next = jest.fn();

  handler.errorHandler({}, req, res, next);
  expect(status.calledWith(500)).toBe(true);
  expect(redirect.calledWith("/test-context/problem-with-service")).toBe(true);
  expect(next).toBeCalledTimes(1);
});

test("Error handler should set the status to 408", () => {
  const status = sinon.spy();
  const redirect = sinon.spy();

  const req = {
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
  };

  const err = {
    statusCode: 408,
  };

  const next = jest.fn();

  handler.errorHandler(err, req, res, next);
  expect(status.calledWith(408)).toBe(true);
  expect(redirect.calledWith("/test-context/timed-out")).toBe(true);
  expect(next).toBeCalledTimes(1);
});

test("Error handler should set the status to 403", () => {
  const status = sinon.spy();
  const redirect = sinon.spy();

  const req = {
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
  };

  const err = {
    statusCode: 403,
  };

  const next = jest.fn();

  handler.errorHandler(err, req, res, next);
  expect(status.calledWith(403)).toBe(true);
  expect(redirect.calledWith("/test-context/no-access")).toBe(true);
  expect(next).toBeCalledTimes(1);
});

test("registerErrorHandlers() should configure app to use error handler and logging", () => {
  const app = {
    use: jest.fn(),
    listen: jest.fn(),
  };

  handler.registerErrorHandlers(app);
  expect(app.use).toBeCalledTimes(2);
});
