const mockLambdaProviderPostRequest = jest.fn();

import * as httpStatus from "http-status-codes";
import { wrapError } from "../../../errors";
import { reissue } from "./post";

jest.mock("./reissue-lambda-provider", () => ({
  makeReissuePostRequest: mockLambdaProviderPostRequest,
}));

beforeEach(() => {
  jest.clearAllMocks();
});

describe("reissue()", () => {
  test("should call reissue with correct req and return an array of data without throwing an error", async () => {
    mockLambdaProviderPostRequest.mockResolvedValue({ something: "data" });

    const req = {
      session: {
        citizen: { id: "123" },
        certificate: {
          id: "",
          reference: "",
          type: "",
          startDate: "",
          endDate: "",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    const result = await reissue(req, res, next);

    expect(next).not.toBeCalled();
    expect(result).toEqual({ something: "data" });
    expect(mockLambdaProviderPostRequest).toBeCalledTimes(1);
    expect(mockLambdaProviderPostRequest).toBeCalledWith(req.session);
  });

  test("should call next with wrapped error when makeReissuePostRequest throws error", async () => {
    mockLambdaProviderPostRequest.mockReturnValue(
      Promise.reject(new Error("Axios error")),
    );
    const req = {
      path: "/test",
      session: {},
    };
    const res = {};
    const next = jest.fn();
    const expectedError = wrapError({
      cause: new Error("Axios error"),
      message: `Error posting ${req.path}`,
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    });

    await reissue(req, res, next);

    expect(next).toBeCalledTimes(1);
    expect(next).toBeCalledWith(expectedError);
    expect(mockLambdaProviderPostRequest).toBeCalledTimes(1);
    expect(mockLambdaProviderPostRequest).toBeCalledWith(req.session);
  });
});
