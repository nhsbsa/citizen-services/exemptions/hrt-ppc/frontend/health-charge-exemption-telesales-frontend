import expect from "expect";
const mockSanitizeAddress = jest.fn();
const mockSanitizeDateOfBirth = jest.fn();
const mockSanitizeEmailAddress = jest.fn();
const mockSanitizeStartDate = jest.fn();
jest.mock("@nhsbsa/health-charge-exemption-common-frontend", () => ({
  sanitizeAddress: mockSanitizeAddress,
  sanitizeDateOfBirth: mockSanitizeDateOfBirth,
  sanitizeEmailAddress: mockSanitizeEmailAddress,
  sanitizeStartDate: mockSanitizeStartDate,
}));
import { getPhoneNumber, sanitize } from "./sanitize";

const next = jest.fn();

const request = {
  body: {
    telephoneNumber: "07453243456",
  },
};

describe("getPhoneNumber", () => {
  it.each([null, undefined, ""])(
    "should return undefined when phoneNumber is %p",
    async (phoneNumber: null | undefined | string) => {
      const result = getPhoneNumber(phoneNumber);
      expect(result).toEqual(undefined);
    },
  );
  test("converts leading zero", async () => {
    const phoneNumber = "07700900645";
    const expected = "+447700900645";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });

  test("converts leading zero for geographic numbers", async () => {
    const phoneNumber = "(0141) 111 1111";
    const expected = "+441411111111";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });

  test("converts no leading zero or +44", async () => {
    const phoneNumber = "7700900645";
    const expected = "+447700900645";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });

  test("converts no leading zero or +44 for geographic numbers", async () => {
    const phoneNumber = "141 111 1111";
    const expected = "+441411111111";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });

  test("doesn’t convert leading +44", async () => {
    const phoneNumber = "+447700900645";
    const expected = "+447700900645";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });

  test("converts and removes spaces", async () => {
    const phoneNumber = "07700 900645 ";
    const expected = "+447700900645";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });

  test("converts and removes hyphens", async () => {
    const phoneNumber = "07700-900-645";
    const expected = "+447700900645";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });

  test("converts and removes brackets", async () => {
    const phoneNumber = "(07700)900645";
    const expected = "+447700900645";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });

  test("converts and removes leading 0044", async () => {
    const phoneNumber = "00447700900645";
    const expected = "+447700900645";

    const result = getPhoneNumber(phoneNumber);
    expect(result).toEqual(expected);
  });
});

test("sanitize() sets phone number & calls sanitizeAddress, sanitizeDateOfBirth, sanitizeEmailAddress, sanitizeStartDate", () => {
  sanitize()(request, {}, next);

  expect(request.body["formattedPhoneNumber"]).toEqual("+447453243456");
  expect(mockSanitizeAddress).toHaveBeenCalledTimes(1);
  expect(mockSanitizeAddress).toHaveBeenCalledWith(request);
  expect(mockSanitizeDateOfBirth).toHaveBeenCalledTimes(1);
  expect(mockSanitizeDateOfBirth).toHaveBeenCalledWith(request);
  expect(mockSanitizeEmailAddress).toHaveBeenCalledTimes(1);
  expect(mockSanitizeEmailAddress).toHaveBeenCalledWith(request);
  expect(mockSanitizeStartDate).toHaveBeenCalledTimes(1);
  expect(mockSanitizeStartDate).toHaveBeenCalledWith(request);
  expect(next).toHaveBeenCalledTimes(1);
  expect(next).toHaveBeenCalledWith();
});
