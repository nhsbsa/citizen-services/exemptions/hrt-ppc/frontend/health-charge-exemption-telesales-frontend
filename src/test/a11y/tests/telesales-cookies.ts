import { URLS } from "../paths";

const telesalesCookiesNotEnabled = [
  {
    url: URLS.ENABLE_COOKIES,
    cookieDisabled: true,
    formData: () => ({}),
  },
];

export { telesalesCookiesNotEnabled };
