import { config } from "../../../../../config";
import { certificateClient } from "../../../../client/certificate-client";
import { citizenClient } from "../../../../client/citizen-client";
import { noteClient } from "../../../../client/note-client";
import { preferenceClient } from "../../../../client/preference-client";
import {
  AuditResponseEntity,
  compositionClient,
} from "../../../../client/composition-client";
import { CertificateType } from "@nhsbsa/health-charge-exemption-common-frontend";
import { AxiosResponse } from "axios";

const CITIZEN_AUDIT_FIELDS = [
  "[]",
  "",
  "firstName",
  "lastName",
  "dateOfBirth",
  "nhsNumber",
  "addressLine1",
  "addressLine2",
  "townOrCity",
  "postcode",
  "emailAddress",
];
const CERTIFICATE_AUDIT_FIELDS = ["startDate", "status"];
const PAYMENT_AUDIT_FIELDS = ["status"];
const PREFERENCE_AUDIT_FIELDS = ["preference"];

const getCertificate = async (session, certificateId) => {
  const client = new certificateClient(config);
  const response = await client.makeRequest(
    {
      method: "GET",
      url: "/v1/certificates/" + certificateId,
      responseType: "json",
    },
    session,
  );
  return response;
};

const getCitizen = async (session, citizenId) => {
  const client = new citizenClient(config);
  const response = await client.makeRequest(
    {
      method: "GET",
      url: "/v1/citizens/" + citizenId,
      responseType: "json",
    },
    session,
  );
  return response;
};

const getNotes = async (session, certificateId) => {
  const client = new noteClient(config);
  const response = await client.makeRequest(
    {
      method: "GET",
      url:
        "/v1/notes/search/findByCertificateId?certificateId=" +
        certificateId +
        "&size=100",
      responseType: "json",
    },
    session,
  );
  return response;
};

const getPreference = async (session, citizenId) => {
  const client = new preferenceClient(config);
  const response = await client.makeRequest(
    {
      method: "GET",
      url:
        "/v1/user-preference/search/findByCitizenIdAndCertificateType?citizenId=" +
        citizenId +
        "&certificateType=" +
        CertificateType.HRT_PPC,
      responseType: "json",
    },
    session,
  );
  return response;
};

const getAudit = async (session, citizenId, certificateId) => {
  const client = new compositionClient(config);
  const response: AxiosResponse<AuditResponseEntity, any> =
    await client.makeRequest(
      {
        method: "POST",
        url: "/v1/exemptions/audit",
        data: createAuditRequestBody(citizenId, certificateId),
        responseType: "json",
      },
      session,
    );
  return response;
};

const createAuditRequestBody = (citizenId: string, certificateId: string) => {
  return {
    citizenId,
    certificateId,
    citizen: {
      fields: CITIZEN_AUDIT_FIELDS,
    },
    certificate: {
      fields: CERTIFICATE_AUDIT_FIELDS,
    },
    payment: {
      fields: PAYMENT_AUDIT_FIELDS,
    },
    preference: {
      fields: PREFERENCE_AUDIT_FIELDS,
    },
  };
};

/**
 * Functions to add each set of data to an array so that the array can be ran in parallel
 * and then make requests
 * @param req the request
 * @param searchResult the search result record
 * @returns result data from api calls
 */
const makeRequests = async (req, searchResult) => {
  const resultData: any = [];
  // Array of requests
  const promiseArray: Promise<any>[] = [];

  const certificateId = searchResult.id;
  const citizenId = searchResult.citizen.id;
  const session = req.session;

  // Add each request to the array
  promiseArray.push(getCertificate(session, certificateId));
  promiseArray.push(getCitizen(session, citizenId));
  promiseArray.push(getNotes(session, certificateId));
  promiseArray.push(getPreference(session, citizenId));
  promiseArray.push(getAudit(session, citizenId, certificateId));

  // promise.all allows you to make multiple axios requests at the same time.
  // It returns an array of the results of all your axios requests
  const resolvedPromises = await Promise.all(promiseArray);

  for (let i = 0; i < resolvedPromises.length; i++) {
    const promise = resolvedPromises[i];
    const res = promise.data;
    resultData.push(res);
  }
  return {
    data: {
      certificate: resultData[0],
      citizen: resultData[1],
      notes: resultData[2],
      preference: resultData[3],
      audit: resultData[4],
    },
  };
};

export {
  makeRequests,
  getCitizen,
  getCertificate,
  getNotes,
  getPreference,
  getAudit,
};
