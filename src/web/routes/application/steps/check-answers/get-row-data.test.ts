import {
  applicantDetailsRowList,
  certificateDetailsRowList,
  notesDetailsRowList,
} from "./get-row-data";
import { Preference } from "@nhsbsa/health-charge-exemption-common-frontend";
jest.mock("../../../../../config", () => ({
  config: {
    environment: {
      HRT_PPC_VALUE: 1930,
    },
  },
}));

describe("Applicant Details Row List", () => {
  const translate = {
    name: "Name",
    dateOfBirth: "Date of Birth",
    nhsNumber: "NHS Number",
    address: "Address",
    certificateFulfillment: "Certificate Fulfillment",
    email: "Email",
    telephone: "Telephone",
    preferencesText: {
      email: "Email",
      post: "Post",
      none: "None",
    },
  };

  test("should return an array with applicant details", () => {
    const req = {
      t: (string) => string,
      session: {
        newApplicant: {
          firstName: "John",
          lastName: "Doe",
          "dateOfBirth-day": 15,
          "dateOfBirth-month": 9,
          "dateOfBirth-year": 1990,
          dateOfBirth: "1990-09-15",
          nhsNumber: "1234567890",
          addressLine1: "123 Main Street",
          addressLine2: "Harley compound",
          townOrCity: "London",
          postcode: "SW1A 1AA",
          certificateFulfillment: Preference.EMAIL,
          emailAddress: "john.doe@example.com",
          telephoneNumber: "1234567890",
        },
      },
    };

    const result = applicantDetailsRowList(req, translate);

    expect(result).toEqual([
      {
        key: {
          text: "Name",
        },
        value: {
          text: "John Doe",
        },
      },
      {
        key: {
          text: "Date of Birth",
        },
        value: {
          text: "15/09/1990",
        },
      },
      {
        key: {
          text: "NHS Number",
        },
        value: {
          text: "1234567890",
        },
      },
      {
        key: {
          text: "Address",
        },
        value: {
          html: "123 Main Street<br>Harley compound<br>London<br>SW1A 1AA",
        },
      },
      {
        key: {
          text: "Certificate Fulfillment",
        },
        value: {
          text: "Email",
        },
      },
      {
        key: {
          text: "Email",
        },
        value: {
          text: "john.doe@example.com",
        },
      },
      {
        key: {
          text: "Telephone",
        },
        value: {
          text: "1234567890",
        },
      },
    ]);
  });

  test("should return an array with applicant details when addressLine2, nhsNumber, emailAddress and telephoneNumber is missing", () => {
    const req = {
      session: {
        newApplicant: {
          firstName: "John",
          lastName: "Doe",
          "dateOfBirth-day": 15,
          "dateOfBirth-month": 9,
          "dateOfBirth-year": 1990,
          dateOfBirth: "1990-09-15",
          addressLine1: "123 Main Street",
          townOrCity: "London",
          postcode: "SW1A 1AA",
          certificateFulfillment: Preference.POSTAL,
        },
      },
    };

    const result = applicantDetailsRowList(req, translate);

    expect(result).toEqual([
      {
        key: {
          text: "Name",
        },
        value: {
          text: "John Doe",
        },
      },
      {
        key: {
          text: "Date of Birth",
        },
        value: {
          text: "15/09/1990",
        },
      },
      {
        key: {
          text: "Address",
        },
        value: {
          html: "123 Main Street<br>London<br>SW1A 1AA",
        },
      },
      {
        key: {
          text: "Certificate Fulfillment",
        },
        value: {
          text: "Post",
        },
      },
    ]);
  });
});

describe("Certificate Details Row List", () => {
  test("should return an array with certificate details", () => {
    const req = {
      session: {
        newApplicant: {
          "certificateStartDate-day": 1,
          "certificateStartDate-month": 1,
          "certificateStartDate-year": 2023,
          certificateStartDate: "2023-01-01",
          certificateEndDate: "2024-01-01",
        },
      },
    };

    const translate = {
      length: "Length",
      startsOn: "Starts On",
      endsOn: "Ends On",
      totalCost: "Total Cost",
      paymentMethod: "Payment Method",
    };

    const result = certificateDetailsRowList(req, translate);

    expect(result).toEqual([
      {
        key: {
          text: "Length",
        },
        value: {
          text: "12 months",
        },
      },
      {
        key: {
          text: "Starts On",
        },
        value: {
          text: "01/01/2023",
        },
      },
      {
        key: {
          text: "Ends On",
        },
        value: {
          text: "01/01/2024",
        },
      },
      {
        key: {
          text: "Total Cost",
        },
        value: {
          text: "£19.30",
        },
      },
      {
        key: {
          text: "Payment Method",
        },
        value: {
          text: "Card payment",
        },
      },
    ]);
  });
});

describe("Notes Details Row List", () => {
  test("should return an array with notes details", () => {
    const req = {
      session: {
        newApplicant: {
          description: "This is a note.",
        },
      },
    };

    const translate = {
      notes: "Notes",
    };

    const result = notesDetailsRowList(req, translate);

    expect(result).toEqual([
      {
        key: {
          text: "Notes",
        },
        value: {
          text: "This is a note.",
        },
      },
    ]);
  });
});
