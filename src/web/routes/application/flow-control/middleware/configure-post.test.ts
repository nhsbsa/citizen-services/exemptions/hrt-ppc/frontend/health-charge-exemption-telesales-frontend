import expect from "expect";
import { configurePost } from "./configure-post";
import * as previousPath from "../get-previous-path";
import * as prefixPath from "../../paths/prefix-path";

import * as sinon from "sinon";

import { IN_PROGRESS, IN_REVIEW } from "../states";
import { CHECK_ANSWERS_URL } from "../../paths/paths";
import { buildSessionForJourney } from "../test-utils/test-utils";

const TELESALES = "telesales";

const TELESALES_JOURNEY = {
  name: TELESALES,
  pathPrefix: "/telesales-path",
};

const stepOne = { path: "/first" };
const stepTwo = { path: "/second" };
const stepThree = { path: "/third" };
const steps = [stepOne, stepTwo, stepThree];

const res = {
  locals: {
    previous: null,
  },
};

jest.spyOn(previousPath, "getPreviousPath");
jest.spyOn(prefixPath, "prefixPath");

test("configurePost() should return previous path in journey when state is IN_PROGRESS", () => {
  const req = {
    t: (string) => string,
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };
  const next = sinon.spy();

  configurePost(steps, stepTwo, TELESALES_JOURNEY)(req, res, next);
  expect(previousPath.getPreviousPath).toBeCalledTimes(1);
  expect(previousPath.getPreviousPath).toBeCalledWith(steps, stepTwo, req);
  expect(res.locals.previous).toBe(stepOne.path);
});

test("configurePost() should return check-answers path in journey when state is IN_REVIEW", () => {
  const req = {
    t: (string) => string,
    session: {
      ...buildSessionForJourney({
        journeyName: TELESALES,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
  };
  const next = sinon.spy();

  configurePost(steps, stepTwo, TELESALES_JOURNEY)(req, res, next);
  expect(previousPath.getPreviousPath).not.toBeCalled();
  expect(prefixPath.prefixPath).toBeCalledTimes(1);
  expect(prefixPath.prefixPath).toBeCalledWith(
    TELESALES_JOURNEY.pathPrefix,
    CHECK_ANSWERS_URL,
  );
  expect(res.locals.previous).toBe(
    TELESALES_JOURNEY.pathPrefix + CHECK_ANSWERS_URL,
  );
});
