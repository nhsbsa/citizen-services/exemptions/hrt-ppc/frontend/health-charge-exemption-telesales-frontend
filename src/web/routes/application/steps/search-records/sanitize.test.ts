import expect from "expect";
const mockSanitizeDateOfBirth = jest.fn();
jest.mock(
  "@nhsbsa/health-charge-exemption-common-frontend/dist/src/date-of-birth",
  () => ({
    sanitizeDateOfBirth: mockSanitizeDateOfBirth,
  }),
);
import { sanitize } from "./sanitize";

const next = jest.fn();

const request = {
  body: {
    "dateOfBirth-day": "05",
    "dateOfBirth-month": "06",
    "dateOfBirth-year": "1999",
    dateOfBirth: "1997-03-24",
    postcode: "NE15 8NY",
    lastName: "lname",
  },
};

describe("sanitize()", () => {
  test("sets sanitizedPostcode in req.body and calls sanitizeDateOfBirth()", () => {
    sanitize()(request, {}, next);

    expect(request.body["sanitizedPostcode"]).toEqual("NE15 8NY");
    expect(mockSanitizeDateOfBirth).toHaveBeenCalledTimes(1);
    expect(mockSanitizeDateOfBirth).toHaveBeenCalledWith(request);
    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });

  test("replaces multiple whitespace with one, converts to uppercase and saves to a new variable", () => {
    const req = {
      body: {
        ...request.body,
        postcode: "bs1     4tb",
      },
    };
    const expectedSanitizedPostcode = "BS1 4TB";
    const expectedPostcode = "bs1     4tb";

    sanitize()(req, {}, next);

    expect(req.body["sanitizedPostcode"]).toEqual(expectedSanitizedPostcode);
    expect(req.body.postcode).toEqual(expectedPostcode);
    expect(mockSanitizeDateOfBirth).toHaveBeenCalledTimes(1);
    expect(mockSanitizeDateOfBirth).toHaveBeenCalledWith(req);
    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });

  it.each([
    ["cb3  0qb", "CB3 0QB"],
    ["n1  2nl", "N1 2NL"],
    ["cb24  9lq", "CB24 9LQ"],
    ["ox14  5fb", "OX14 5FB"],
    ["ox145fc", "OX14 5FC"],
  ])(
    "adds a single space before the last 3 ending characters, and saves to a new variable for postcode %p",
    async (value: string, expectedSanitizedPostcode: string) => {
      const req = {
        body: {
          ...request.body,
          postcode: value,
        },
      };

      sanitize()(req, {}, next);

      expect(req.body["sanitizedPostcode"]).toEqual(expectedSanitizedPostcode);
      expect(mockSanitizeDateOfBirth).toHaveBeenCalledTimes(1);
      expect(mockSanitizeDateOfBirth).toHaveBeenCalledWith(req);
      expect(next).toHaveBeenCalledTimes(1);
      expect(next).toHaveBeenCalledWith();
    },
  );
});
