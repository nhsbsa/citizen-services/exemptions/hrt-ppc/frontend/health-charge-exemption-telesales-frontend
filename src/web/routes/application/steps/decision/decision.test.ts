import expect from "expect";
import * as sinon from "sinon";
import { identity } from "ramda";
import { DecisionStatus } from "./decision-statuses";

const getDecisionStatus = sinon.stub();

const LANGUAGE_ENGLISH = "en";
const LANGUAGE_WELSH = "cy";

jest.mock("./get-decision-status", () => ({
  getDecisionStatus,
}));

import { getDecisionPage } from "./decision";

const resetStubs = () => {
  getDecisionStatus.reset();
};

const requestWithLanguage = (lang) => {
  return {
    t: identity,
    language: lang,
    session: {
      verificationResult: {},
      reference: "ASDF123",
    },
  };
};

const expectedSuccessTemplateVariables = {
  title: "decision.success.title",
  heading: "decision.success.heading",
  referenceNumber: requestWithLanguage(LANGUAGE_ENGLISH).session.reference,
  panelContent: {
    html: "decision.success.panel.html",
    concludingHtml: "decision.success.panel.concludingHtml",
  },
  body: {
    header: {
      lineOne: "decision.success.body.header.lineOne",
      lineTwo: "decision.success.body.header.lineTwo",
      lineThree: "decision.success.body.header.lineThree",
    },
    paragraph: {
      lineOne: "decision.success.body.paragraph.lineOne",
      lineTwo: "decision.success.body.paragraph.lineTwo",
      lineThree: "decision.success.body.paragraph.lineThree",
      lineFour: "decision.success.body.paragraph.lineFour",
      lineFive: "decision.success.body.paragraph.lineFive",
      lineSix: "decision.success.body.paragraph.lineSix",
    },
  },
  template: "success",
};

const expectedFailureTemplateVariables = {
  title: "decision.failureRejected.title",
  heading: "decision.failureRejected.heading",
  referenceNumber: requestWithLanguage(LANGUAGE_ENGLISH).session.reference,
  panelContent: {
    html: "decision.failureRejected.panel.html",
    concludingHtml: "decision.failureRejected.panel.concludingHtml",
  },
  body: {
    header: {
      lineOne: "decision.failureRejected.body.header.lineOne",
      lineTwo: "decision.failureRejected.body.header.lineTwo",
    },
    paragraph: {
      lineOne: "decision.failureRejected.body.paragraph.lineOne",
      lineTwo: "decision.failureRejected.body.paragraph.lineTwo",
      lineThree: "decision.failureRejected.body.paragraph.lineThree",
      lineFour: "decision.failureRejected.body.paragraph.lineFour",
      lineFive: "decision.failureRejected.body.paragraph.lineFive",
      lineSix: "decision.failureRejected.body.paragraph.lineSix",
    },
  },
  template: "failure",
};

const next = sinon.spy();

test("getDecisionPage() throws error if decision status is undefined", () => {
  getDecisionStatus.returns(undefined);

  const render = sinon.spy();

  const ineligibleReq = {
    t: identity,
    session: {
      verificationResult: {},
      eligibilityStatus: "INELIGIBLE",
    },
  };

  const res = {
    render,
  };

  expect(() => getDecisionPage(ineligibleReq, res, next)).toThrowError(
    /No decision status generated from EligibilityStatus: INELIGIBLE and VerificationResult: {}/,
  );
  expect(render.called).toBe(false);
  resetStubs();
});

test(`getDecisionPage() renders success view if decision status is ${DecisionStatus.SUCCESS} for english language`, () => {
  getDecisionStatus.returns(DecisionStatus.SUCCESS);

  const render = sinon.spy();

  const res = {
    render,
  };

  getDecisionPage(requestWithLanguage(LANGUAGE_ENGLISH), res, next);

  expect(render.calledWith("decision", expectedSuccessTemplateVariables)).toBe(
    true,
  );
  resetStubs();
});

test(`getDecisionPage() renders success view if decision status is ${DecisionStatus.SUCCESS} for welsh language`, () => {
  getDecisionStatus.returns(DecisionStatus.SUCCESS);

  const render = sinon.spy();

  const res = {
    render,
  };

  getDecisionPage(requestWithLanguage(LANGUAGE_WELSH), res, next);

  expect(render.calledWith("decision", expectedSuccessTemplateVariables)).toBe(
    true,
  );
  resetStubs();
});

test(`getDecisionPage() renders failure view if decision status is ${DecisionStatus.FAIL} for english language`, () => {
  getDecisionStatus.returns(DecisionStatus.FAIL);

  const render = sinon.spy();

  const res = {
    render,
  };

  getDecisionPage(requestWithLanguage(LANGUAGE_ENGLISH), res, next);

  expect(render.calledWith("decision", expectedFailureTemplateVariables)).toBe(
    true,
  );
  resetStubs();
});

test(`getDecisionPage() renders failure view if decision status is ${DecisionStatus.FAIL} for welsh language`, () => {
  getDecisionStatus.returns(DecisionStatus.FAIL);

  const render = sinon.spy();

  const res = {
    render,
  };

  getDecisionPage(requestWithLanguage(LANGUAGE_WELSH), res, next);

  expect(render.calledWith("decision", expectedFailureTemplateVariables)).toBe(
    true,
  );
  resetStubs();
});
