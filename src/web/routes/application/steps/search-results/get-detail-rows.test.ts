import expect from "expect";
import moment from "moment";
import {
  buildRow,
  getCertificateAndCitizenDetailsRows,
} from "./get-detail-rows";
import {
  CertificateStatus,
  AddressType,
  CertificateType,
  PaymentStatus,
} from "@nhsbsa/health-charge-exemption-common-frontend";

const translate = {
  tableHeadingOne: "headerOne",
  tableHeadingTwo: "headerTwo",
  tableHeadingThree: "headerThree",
  tableHeadingFour: "headerFour",
  tableHeadingFive: "headerFive",
  tableHeadingSix: "headerSix",
  tableHeadingSeven: "headerSeven",
  tableHeadingEight: "headerEight",
  tableHeadingNine: "headerNine",
};

describe("buildRow", () => {
  test("should return an row with table headers and columns when all address fields exist", () => {
    const date = new Date("2021-01-01");
    const certificate = {
      id: "myCertId",
      reference: "myRef",
      type: CertificateType.HRT_PPC,
      status: CertificateStatus.PENDING,
      startDate: moment(date).subtract(5, "d").toDate(),
      endDate: moment(date).subtract(1, "d").toDate(),
      citizen: {
        id: "myCitizenId",
        firstName: "myFname",
        lastName: "myLname",
        dateOfBirth: moment(date).toDate(),
        addresses: [
          {
            id: "myAddId",
            addressLine1: "add1",
            addressLine2: "add2",
            townOrCity: "addTown",
            country: "addCountry",
            postcode: "NE15 8NY",
            type: AddressType.POSTAL,
          },
        ],
      },
    };

    const result = buildRow(certificate, translate);

    expect(result).toEqual([
      {
        header: translate.tableHeadingOne,
        html: `<a role=\"button\" href=/test-context/exemption-information/${certificate.reference} id=\"cert-link-${certificate.reference}\">${certificate.reference}</a>`,
      },
      {
        header: translate.tableHeadingTwo,
        text: `${certificate.citizen.firstName} ${certificate.citizen.lastName}`,
      },
      {
        header: translate.tableHeadingThree,
        text: `${certificate.citizen.addresses[0].addressLine1} ${certificate.citizen.addresses[0].addressLine2} ${certificate.citizen.addresses[0].townOrCity}`,
      },
      {
        header: translate.tableHeadingFour,
        text: `${certificate.citizen.addresses[0].postcode}`,
      },
      { header: translate.tableHeadingFive, text: `01/01/2021` },
      { header: translate.tableHeadingSix, text: `HRT PPC` },
      { header: translate.tableHeadingSeven, text: `${certificate.status}` },
      { header: translate.tableHeadingEight, text: `27/12/2020` },
      { header: translate.tableHeadingNine, text: `31/12/2020` },
    ]);
  });

  test("should return an row with table headers and columns when address exists with no address line 2", () => {
    const date = new Date("2021-01-01");
    const certificate = {
      id: "myCertId",
      reference: "myRef",
      type: CertificateType.HRT_PPC,
      status: CertificateStatus.PENDING,
      startDate: moment(date).subtract(5, "d").toDate(),
      endDate: moment(date).subtract(1, "d").toDate(),
      citizen: {
        id: "myCitizenId",
        firstName: "myFname",
        lastName: "myLname",
        dateOfBirth: moment(date).toDate(),
        addresses: [
          {
            id: "myAddId",
            addressLine1: "add1",
            townOrCity: "addTown",
            country: "addCountry",
            postcode: "NE15 8NY",
            type: AddressType.POSTAL,
          },
        ],
      },
    };

    const result = buildRow(certificate, translate);

    expect(result).toEqual([
      {
        header: translate.tableHeadingOne,
        html: `<a role=\"button\" href=/test-context/exemption-information/${certificate.reference} id=\"cert-link-${certificate.reference}\">${certificate.reference}</a>`,
      },
      {
        header: translate.tableHeadingTwo,
        text: `${certificate.citizen.firstName} ${certificate.citizen.lastName}`,
      },
      {
        header: translate.tableHeadingThree,
        text: `${certificate.citizen.addresses[0].addressLine1}  ${certificate.citizen.addresses[0].townOrCity}`,
      },
      {
        header: translate.tableHeadingFour,
        text: `${certificate.citizen.addresses[0].postcode}`,
      },
      { header: translate.tableHeadingFive, text: `01/01/2021` },
      { header: translate.tableHeadingSix, text: `HRT PPC` },
      { header: translate.tableHeadingSeven, text: `${certificate.status}` },
      { header: translate.tableHeadingEight, text: `27/12/2020` },
      { header: translate.tableHeadingNine, text: `31/12/2020` },
    ]);
  });

  test("should return an row with table headers and columns when address does not exist", () => {
    const date = new Date("2021-01-01");
    const certificate = {
      id: "myCertId",
      reference: "myRef",
      type: CertificateType.HRT_PPC,
      status: CertificateStatus.PENDING,
      startDate: moment(date).subtract(5, "d").toDate(),
      endDate: moment(date).subtract(1, "d").toDate(),
      citizen: {
        id: "myCitizenId",
        firstName: "myFname",
        lastName: "myLname",
        dateOfBirth: moment(date).toDate(),
      },
    };

    const result = buildRow(certificate, translate);

    expect(result).toEqual([
      {
        header: translate.tableHeadingOne,
        html: `<a role=\"button\" href=/test-context/exemption-information/${certificate.reference} id=\"cert-link-${certificate.reference}\">${certificate.reference}</a>`,
      },
      {
        header: translate.tableHeadingTwo,
        text: `${certificate.citizen.firstName} ${certificate.citizen.lastName}`,
      },
      { header: translate.tableHeadingThree, text: `` },
      { header: translate.tableHeadingFour, text: `` },
      { header: translate.tableHeadingFive, text: `01/01/2021` },
      { header: translate.tableHeadingSix, text: `HRT PPC` },
      { header: translate.tableHeadingSeven, text: `${certificate.status}` },
      { header: translate.tableHeadingEight, text: `27/12/2020` },
      { header: translate.tableHeadingNine, text: `31/12/2020` },
    ]);
  });

  describe("getCertificateAndCitizenDetailsRows", () => {
    test("should return rows", () => {
      const date = new Date("2021-01-01");
      const results = [
        {
          id: "myCertId1",
          reference: "myRef1",
          type: CertificateType.HRT_PPC,
          status: PaymentStatus.SUCCESS,
          startDate: moment(date).subtract(4, "d").toDate(),
          endDate: moment(date).subtract(2, "d").toDate(),
          citizen: {
            id: "myCitizenId1",
            firstName: "myFname1",
            lastName: "myLname1",
            dateOfBirth: moment(date).toDate(),
          },
        },
        {
          id: "myCertId2",
          reference: "myRef2",
          type: CertificateType.HRT_PPC,
          status: CertificateStatus.PENDING,
          startDate: moment(date).subtract(5, "d").toDate(),
          endDate: moment(date).subtract(1, "d").toDate(),
          citizen: {
            id: "myCitizenId2",
            firstName: "myFname2",
            lastName: "myLname2",
            dateOfBirth: moment(date).toDate(),
          },
        },
      ];

      const result = getCertificateAndCitizenDetailsRows(results, translate);

      expect(result).toEqual([
        [
          {
            header: translate.tableHeadingOne,
            html: `<a role=\"button\" href=/test-context/exemption-information/${results[0].reference} id=\"cert-link-${results[0].reference}\">${results[0].reference}</a>`,
          },
          {
            header: translate.tableHeadingTwo,
            text: `${results[0].citizen.firstName} ${results[0].citizen.lastName}`,
          },
          { header: translate.tableHeadingThree, text: `` },
          { header: translate.tableHeadingFour, text: `` },
          { header: translate.tableHeadingFive, text: `01/01/2021` },
          { header: translate.tableHeadingSix, text: `HRT PPC` },
          { header: translate.tableHeadingSeven, text: `${results[0].status}` },
          { header: translate.tableHeadingEight, text: `28/12/2020` },
          { header: translate.tableHeadingNine, text: `30/12/2020` },
        ],
        [
          {
            header: translate.tableHeadingOne,
            html: `<a role=\"button\" href=/test-context/exemption-information/${results[1].reference} id=\"cert-link-${results[1].reference}\">${results[1].reference}</a>`,
          },
          {
            header: translate.tableHeadingTwo,
            text: `${results[1].citizen.firstName} ${results[1].citizen.lastName}`,
          },
          { header: translate.tableHeadingThree, text: `` },
          { header: translate.tableHeadingFour, text: `` },
          { header: translate.tableHeadingFive, text: `01/01/2021` },
          { header: translate.tableHeadingSix, text: `HRT PPC` },
          { header: translate.tableHeadingSeven, text: `${results[1].status}` },
          { header: translate.tableHeadingEight, text: `27/12/2020` },
          { header: translate.tableHeadingNine, text: `31/12/2020` },
        ],
      ]);
    });
  });
});
