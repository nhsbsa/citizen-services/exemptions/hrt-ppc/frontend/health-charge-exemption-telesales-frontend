import {
  equals,
  compose,
  path,
  prop,
  cond,
  always,
  allPass,
  anyPass,
} from "ramda";
import { DecisionStatus } from "./decision-statuses";
import { EligibilityStatus } from "./eligibility-statuses";

const verificationResultProp = (prop) =>
  path(["verificationResult", "applicant", prop]);

const outcomeEqualsNotMatched = equals("not_matched");
const outcomeEqualsMatched = equals("matched");
const outcomeNotConfirmed = equals("not_confirmed");
const outcomeConfirmed = equals("confirmed");

const identityStatusNotMatched = compose(
  outcomeEqualsNotMatched,
  verificationResultProp("identityStatus"),
);
const identityStatusMatched = compose(
  outcomeEqualsMatched,
  verificationResultProp("identityStatus"),
);
const eligibilityStatusNotConfirmed = compose(
  outcomeNotConfirmed,
  verificationResultProp("eligibilityStatus"),
);
const eligibilityStatusConfirmed = compose(
  outcomeConfirmed,
  verificationResultProp("eligibilityStatus"),
);
const addressLine1MatchNotMatched = compose(
  outcomeEqualsNotMatched,
  verificationResultProp("addressLine1Match"),
);
const postcodeMatchNotMatched = compose(
  outcomeEqualsNotMatched,
  verificationResultProp("postcodeMatch"),
);

const isDuplicateClaim = compose(
  equals(EligibilityStatus.DUPLICATE),
  prop("eligibilityStatus"),
);
const isEligibleClaim = compose(
  equals(EligibilityStatus.ELIGIBLE),
  prop("eligibilityStatus"),
);
const isIneligibleClaim = compose(
  equals(EligibilityStatus.INELIGIBLE),
  prop("eligibilityStatus"),
);

const addressMismatches = anyPass([
  addressLine1MatchNotMatched,
  postcodeMatchNotMatched,
]);

const isFullMatch = allPass([
  eligibilityStatusConfirmed,
  identityStatusMatched,
  isEligibleClaim,
]);

const getDecisionStatus = cond([
  [isDuplicateClaim, always(DecisionStatus.FAIL)],
  [isIneligibleClaim, always(DecisionStatus.FAIL)],
  [identityStatusNotMatched, always(DecisionStatus.FAIL)],
  [eligibilityStatusNotConfirmed, always(DecisionStatus.FAIL)],
  [addressMismatches, always(DecisionStatus.FAIL)],
  [isFullMatch, always(DecisionStatus.SUCCESS)],
]);

export { getDecisionStatus };
