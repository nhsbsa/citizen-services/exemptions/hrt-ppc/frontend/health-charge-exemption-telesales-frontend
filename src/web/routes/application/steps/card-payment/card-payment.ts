import { CARD_PAYMENT_URL, CHECK_ANSWERS_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";
import { configureSessionDetails } from "../../flow-control/middleware/session-details";
import { handleRequestForPath } from "../../flow-control/middleware/handle-path-request";

const pageContent = ({ translate, req, journey }) => ({
  title: translate("cardPayment.title"),
  heading: translate("cardPayment.heading"),
  searchBoxHeading: translate("cardPayment.searchBoxHeading"),
  paragraphOne: translate("cardPayment.paragraphOne"),
  pin: req.session.cardPaymentDetails.pin,
  backText: translate("buttons:back"),
  submitText: translate("buttons:submit"),
  previous: prefixPath(journey.pathPrefix, CHECK_ANSWERS_URL),
});

const getCardPayments = (journey) => (req, res) => {
  const localisation = pageContent({ translate: req.t, req, journey });
  res.render("card-payment", {
    ...localisation,
  });
};

const registerCardPaymentRoutes = (journey, app, csrfProtection) => {
  app
    .route(prefixPath(journey.pathPrefix, CARD_PAYMENT_URL))
    .get(
      configureSessionDetails(journey),
      handleRequestForPath(journey, undefined),
      csrfProtection,
      getCardPayments(journey),
    );
};

export { registerCardPaymentRoutes, getCardPayments, pageContent };
