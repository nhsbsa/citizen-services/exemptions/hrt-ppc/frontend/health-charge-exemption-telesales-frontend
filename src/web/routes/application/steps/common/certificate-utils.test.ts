import moment, { unitOfTime } from "moment";
import {
  Event,
  Preference,
  CertificateStatus,
} from "@nhsbsa/health-charge-exemption-common-frontend/dist/src/enums";
import {
  isActiveAndHasNotExpired,
  isEligibleForEditing,
  isEligibleForReissue,
} from "./certificate-utils";

interface Certificate {
  id: string;
  reference: string;
  type: string;
  status: string;
  startDate: Date;
  endDate: Date;
  applicationDate: Date;
  preference: {
    preference: Preference;
    event: Event;
  };
}
const validCertificate: Certificate = {
  id: "HRT",
  reference: "ref",
  type: "HRT",
  status: CertificateStatus.ACTIVE,
  startDate: moment().subtract(10, "days").toDate(),
  endDate: moment().add(10, "days").toDate(),
  applicationDate: moment().subtract(15, "days").toDate(),
  preference: {
    event: Event.ANY,
    preference: Preference.EMAIL,
  },
};
const invalidCertificate: Certificate = {
  id: "HRT",
  reference: "ref",
  type: "HRT",
  status: CertificateStatus.CANCELLED,
  startDate: new Date(),
  endDate: new Date(),
  applicationDate: new Date(),
  preference: {
    event: Event.ANY,
    preference: Preference.EMAIL,
  },
};

const makeCertificate = (
  base: Certificate,
  overrides: Partial<Certificate>,
): Certificate => ({
  ...base,
  ...overrides,
});

const unitsOfTime: unitOfTime.Base[] = [
  "day",
  "month",
  "hour",
  "second",
  "millisecond",
  "year",
];
describe("isActiveAndHasNotExpired()", () => {
  test("should return false when certificate is undefined", () => {
    const result = isActiveAndHasNotExpired(undefined);
    expect(result).toBe(false);
  });

  test.each([CertificateStatus.PENDING, CertificateStatus.CANCELLED, ""])(
    "should return false when status is %s",
    (status) => {
      const certificate = makeCertificate(validCertificate, { status });
      const result = isActiveAndHasNotExpired(certificate);
      expect(result).toBe(false);
    },
  );

  test("should return true when status is active and endDate is in future", () => {
    const result = isActiveAndHasNotExpired(validCertificate);
    expect(result).toBe(true);
  });

  test.each(unitsOfTime)(
    "should return false when endDate boundary is 1 %s in the past",
    (boundary: unitOfTime.Base) => {
      const certificate = makeCertificate(validCertificate, {
        endDate: moment().subtract(1, boundary).toDate(),
      });

      const result = isActiveAndHasNotExpired(certificate);
      expect(result).toBe(false);
    },
  );

  test.each(unitsOfTime)(
    "should return true when status is active, applicationDate is more than a month ago, and startDate boundary is 1 %s in the future",
    (boundary) => {
      const startDate = moment().add(1, boundary).toDate();
      const certificate = makeCertificate(invalidCertificate, {
        status: CertificateStatus.ACTIVE,
        applicationDate: moment().subtract(2, "months").toDate(),
        startDate,
        endDate: moment(startDate).add(1, "day").toDate(),
      });

      const result = isActiveAndHasNotExpired(certificate);
      expect(result).toBe(true);
    },
  );
});

describe("isEligibleForEditing()", () => {
  test("should return false when certificate is undefined", () => {
    const result = isEligibleForEditing(undefined);
    expect(result).toBe(false);
  });

  test.each([CertificateStatus.PENDING, CertificateStatus.CANCELLED, ""])(
    "should return false when status is %s",
    (status) => {
      const certificate = makeCertificate(validCertificate, { status });
      const result = isEligibleForEditing(certificate);
      expect(result).toBe(false);
    },
  );

  test("should return false when application was made more than a month ago", () => {
    const certificate = makeCertificate(validCertificate, {
      applicationDate: moment()
        .subtract(1, "month")
        .subtract(1, "day")
        .toDate(),
    });
    const result = isEligibleForEditing(certificate);
    expect(result).toBe(false);
  });

  test("should return true when status is active, endDate is in future, and application was made less than a month ago", () => {
    const result = isEligibleForEditing(validCertificate);
    expect(result).toBe(true);
  });

  test.each(unitsOfTime)(
    "should return false when endDate boundary is 1 %s in the past",
    (boundary: unitOfTime.Base) => {
      const certificate = makeCertificate(validCertificate, {
        endDate: moment().subtract(1, boundary).toDate(),
      });

      const result = isEligibleForEditing(certificate);
      expect(result).toBe(false);
    },
  );

  test.each(unitsOfTime)(
    "should return true when startDate boundary is 1 %s in the future",
    (boundary) => {
      const certificate = {
        id: "HRT",
        reference: "ref",
        type: "HRT",
        status: CertificateStatus.ACTIVE,
        startDate: moment().add(1, boundary).toDate(),
        endDate: moment().add(1, "day").toDate(),
      };

      const result = isActiveAndHasNotExpired(certificate);
      expect(result).toBe(true);
    },
  );
});

describe("isEligibleForReissue()", () => {
  test("should return false when certificate is undefined", () => {
    const result = isEligibleForReissue(undefined);
    expect(result).toBe(false);
  });

  test.each([CertificateStatus.PENDING, CertificateStatus.CANCELLED, ""])(
    "should return false when status is %s",
    (status) => {
      const certificate = makeCertificate(validCertificate, { status });
      const result = isEligibleForReissue(certificate);
      expect(result).toBe(false);
    },
  );

  test.each([Preference.EMAIL, Preference.POSTAL])(
    "should return false when status is active, endDate is in future, and event is not set to ANY and preference is %s",
    (preference) => {
      const certificate = makeCertificate(validCertificate, {
        preference: {
          event: Event.REMINDER,
          preference: preference,
        },
      });
      const result = isEligibleForReissue(certificate);
      expect(result).toBe(false);
    },
  );

  test.each([Preference.EMAIL, Preference.POSTAL])(
    "should return true when status is active, endDate is in future, and event is set to ANY and preference is %s",
    (preference) => {
      const certificate = makeCertificate(validCertificate, {
        preference: {
          event: Event.ANY,
          preference: preference,
        },
      });
      const result = isEligibleForReissue(certificate);
      expect(result).toBe(true);
    },
  );
});
